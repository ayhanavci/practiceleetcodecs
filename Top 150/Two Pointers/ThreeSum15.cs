namespace Top150;

internal class ThreeSum15
{
    public IList<IList<int>> ThreeSum(int[] nums) 
    {
        IList<IList<int>> retVal = new List<IList<int>> ();
        Array.Sort(nums);

        for (int i = 0; i < nums.Length; ++i)
        {
            if (i > 0 && nums[i] == nums[i-1])                            
                continue;

            int left = i + 1;
            int right = nums.Length - 1;            
            
            while (right > left)
            {
                int sum = nums[i] + nums[left] + nums[right];
                if (sum == 0)
                {
                    List<int> item = new List<int>
                    {
                        nums[i],
                        nums[left],
                        nums[right]
                    };
                    retVal.Add(item);                    
                    
                    right--;
                    left++;

                    while (right > left && nums[right] == nums[right+1])                    
                        right--;

                    while (right > left && nums[left] == nums[left-1])                    
                        left++;
                }
                else if (sum > 0)
                {
                    right--;
                }
                else
                {
                    left++;
                }
            }

        }
        return retVal;
    }

    public void Test()
    {
        int[] nums1 = {-1,0,1,2,-1,-4};
        var result1 = ThreeSum(nums1);

        int[] nums2 = {-2,0,0,2,2};
        var result2 = ThreeSum(nums2);
    }
}

/*
15. 3Sum
Medium
29.5K
2.7K
Companies

Given an integer array nums, return all the triplets [nums[i], nums[j], nums[k]] such that i != j, i != k, and j != k, and nums[i] + nums[j] + nums[k] == 0.

Notice that the solution set must not contain duplicate triplets.

 

Example 1:

Input: nums = [-1,0,1,2,-1,-4]
Output: [[-1,-1,2],[-1,0,1]]
Explanation: 
nums[0] + nums[1] + nums[2] = (-1) + 0 + 1 = 0.
nums[1] + nums[2] + nums[4] = 0 + 1 + (-1) = 0.
nums[0] + nums[3] + nums[4] = (-1) + 2 + (-1) = 0.
The distinct triplets are [-1,0,1] and [-1,-1,2].
Notice that the order of the output and the order of the triplets does not matter.

Example 2:

Input: nums = [0,1,1]
Output: []
Explanation: The only possible triplet does not sum up to 0.

Example 3:

Input: nums = [0,0,0]
Output: [[0,0,0]]
Explanation: The only possible triplet sums up to 0.

 

Constraints:

    3 <= nums.length <= 3000
    -105 <= nums[i] <= 105


*/