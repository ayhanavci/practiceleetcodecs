namespace Top150;

internal class ValidPalindrome125
{
    public bool IsPalindrome(string s) 
    {
        int left = 0;
        int right = s.Length-1;

        while (right > left)
        {                        
            if (!Char.IsLetterOrDigit(s[left]))
            {
                left++;
                continue;
            }
            char leftChar = Char.ToLower(s[left]);            
            if (!Char.IsLetterOrDigit(s[right]))
            {
                right--;
                continue;
            }            
            if (Char.ToLower(s[left]) != Char.ToLower(s[right])) return false;

            right--;
            left++;
        }
        return true;
    }
    public void Test()
    {
        var result1 = IsPalindrome("A man, a plan, a canal: Panama");
        var result2 = IsPalindrome("race a car");
        var result3 = IsPalindrome(" ");
        var result4 = IsPalindrome("0P");
        var result5 = IsPalindrome("0 P");
        var result6 = IsPalindrome("0 PZP");
    }
}

/*
125. Valid Palindrome
Easy
8.7K
8.2K
Companies

A phrase is a palindrome if, after converting all uppercase letters into lowercase letters and removing all non-alphanumeric characters, it reads the same forward and backward. Alphanumeric characters include letters and numbers.

Given a string s, return true if it is a palindrome, or false otherwise.

 

Example 1:

Input: s = "A man, a plan, a canal: Panama"
Output: true
Explanation: "amanaplanacanalpanama" is a palindrome.

Example 2:

Input: s = "race a car"
Output: false
Explanation: "raceacar" is not a palindrome.

Example 3:

Input: s = " "
Output: true
Explanation: s is an empty string "" after removing non-alphanumeric characters.
Since an empty string reads the same forward and backward, it is a palindrome.

 

Constraints:

    1 <= s.length <= 2 * 105
    s consists only of printable ASCII characters.


*/