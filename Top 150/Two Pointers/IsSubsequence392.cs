namespace Top150;

internal class IsSubsequence392
{
    public bool IsSubsequence(string s, string t) 
    {
        if (s.Length == 0) return true;
        int pointer = 0;
        for (int i = 0; i < t.Length; ++i)
        {            
            if (t[i] == s[pointer])
                ++pointer;
            if (pointer == s.Length)
                return true;
        }
        
        return false;
    }
    public void Test()
    {
        var result1 = IsSubsequence("abc", "ahbgdc");
        var result2 = IsSubsequence("axc", "ahbgdc");
        var result3 = IsSubsequence("", "ahbgdc");
        var result4 = IsSubsequence("b", "c");
        var result5 = IsSubsequence("c", "c");
    }
}

/*

392. Is Subsequence
Easy
9.2K
491
Companies

Given two strings s and t, return true if s is a subsequence of t, or false otherwise.

A subsequence of a string is a new string that is formed from the original string by deleting some (can be none) of the characters without disturbing the relative positions of the remaining characters. (i.e., "ace" is a subsequence of "abcde" while "aec" is not).

 

Example 1:

Input: s = "abc", t = "ahbgdc"
Output: true

Example 2:

Input: s = "axc", t = "ahbgdc"
Output: false

 

Constraints:

    0 <= s.length <= 100
    0 <= t.length <= 104
    s and t consist only of lowercase English letters.

 
Follow up: Suppose there are lots of incoming s, say s1, s2, ..., sk where k >= 109, and you want to check one by one to see if t has its subsequence. In this scenario, how would you change your code?
*/