namespace Top150;

internal class RemoveDuplicatesfromSortedListII82
{
    //Definition for singly-linked list.
    public class ListNode
    {
        public int val;
        public ListNode next;
        public ListNode(int val = 0, ListNode next = null)
        {
            this.val = val;
            this.next = next;
        }
    }
    //TODO: Simplify code
    public ListNode DeleteDuplicates(ListNode head) 
    {
        if (head == null) return null;
        if (head.next == null) return head;

        ListNode previous = head;
        ListNode firstUnique = null;
        ListNode previousUnique = null;
        ListNode current = head.next;        
        int repetation = 0;        

        while (current != null)
        {
            if (previous.val == current.val)
            {
                repetation++;
            }
            else
            {
                if (repetation == 0)
                {
                    if (previousUnique != null)                    
                        previousUnique.next = previous;
                    else 
                        firstUnique = previous;

                    previousUnique = previous;  
                    previousUnique.next = null;
                }
                repetation = 0;                
                if (current.next == null) 
                {
                    if (previousUnique == null)   
                    {
                        firstUnique = current;
                        firstUnique.next = null;
                    }                                                                                      
                    else
                        previousUnique.next = current;
                }        
                    
                
            }            
            previous = current;
            current = current.next;
        }

        return firstUnique;
    }
    public void Test()
    {
        Test1();
        Test2();
        Test3();
        Test4();
        Test5();
    }
    public void Test1()
    {
        ListNode head1 = new ListNode(1);
        ListNode node1 = new ListNode(2);
        ListNode node2 = new ListNode(3);
        ListNode node3 = new ListNode(3);
        ListNode node4 = new ListNode(4);
        ListNode node5 = new ListNode(4);
        ListNode node6 = new ListNode(5);

        head1.next = node1;
        node1.next = node2;
        node2.next = node3;
        node3.next = node4;
        node4.next = node5;
        node5.next = node6;

        var result1 = DeleteDuplicates(head1);
    }

    public void Test2()
    {
        ListNode head1 = new ListNode(1);
        ListNode node1 = new ListNode(1);
        ListNode node2 = new ListNode(1);
        ListNode node3 = new ListNode(2);
        ListNode node4 = new ListNode(3);
        
        head1.next = node1;
        node1.next = node2;
        node2.next = node3;
        node3.next = node4;        

        var result1 = DeleteDuplicates(head1);
    }

    public void Test3()
    {
        ListNode head1 = new ListNode(1);
                
        var result1 = DeleteDuplicates(head1);
    }

    public void Test4()
    {
        ListNode head1 = new ListNode(1);
        ListNode node1 = new ListNode(2);
        ListNode node2 = new ListNode(2);
        
        
        head1.next = node1;
        node1.next = node2;
        
        var result1 = DeleteDuplicates(head1);
    }

    public void Test5()
    {
        ListNode head1 = new ListNode(1);
        ListNode node1 = new ListNode(1);
        ListNode node2 = new ListNode(2);
        
        
        head1.next = node1;
        node1.next = node2;
        
        var result1 = DeleteDuplicates(head1);
    }
}

/*
82. Remove Duplicates from Sorted List II
Medium
Topics
Companies

Given the head of a sorted linked list, delete all nodes that have duplicate numbers, leaving only distinct numbers from the original list. Return the linked list sorted as well.

 

Example 1:

Input: head = [1,2,3,3,4,4,5]
Output: [1,2,5]

Example 2:

Input: head = [1,1,1,2,3]
Output: [2,3]

 

Constraints:

    The number of nodes in the list is in the range [0, 300].
    -100 <= Node.val <= 100
    The list is guaranteed to be sorted in ascending order.


*/