namespace Top150;

internal class AddTwoNumbers2
{
     //Definition for singly-linked list.
    public class ListNode {
        public int val;
        public ListNode next;
        public ListNode(int x) {
            val = x;
            next = null;
        }
    }
    public ListNode AddTwoNumbers(ListNode l1, ListNode l2) 
    {
        ListNode head = null;
        ListNode currentNode = null;
        
        int overFlow = 0;
        
        int sum = l1.val + l2.val;
        if (sum > 9)
        {
            sum -= 10;
            overFlow = 1;
        }
        l1 = l1.next;
        l2 = l2.next;
        currentNode = head = new ListNode(sum);


        while (l1 != null || l2 != null)
        {
            if (l1 != null && l2 != null)
            {
                sum = l1.val + l2.val + overFlow;
                l1 = l1.next;
                l2 = l2.next;
            }
            else if (l1 != null)
            {
                sum = l1.val + overFlow;
                l1 = l1.next;
                
            }
            else if (l2 != null)
            {
                sum = l2.val + overFlow;
                l2 = l2.next;
            }
            
            if (sum > 9)
            {
                sum -= 10;
                overFlow = 1;
            }    
            else
                overFlow = 0;
            
            ListNode nextItem = new ListNode(sum);
            currentNode.next = nextItem;                     
            currentNode = nextItem;

        }       
        if (overFlow > 0)
        {
            ListNode nextItem = new ListNode(overFlow);
            currentNode.next = nextItem;          
        }

        return head;
    }
    public void Test()
    {
        Test1();
    }
     public void Test2()
    {
        ListNode head = new ListNode(9);
        ListNode node1 = new ListNode(9);
        ListNode node2 = new ListNode(9);        
        ListNode node3 = new ListNode(9);
        ListNode node4 = new ListNode(9);        
        ListNode node5 = new ListNode(9);
        ListNode node6 = new ListNode(9);        

        head.next = node1;
        node1.next = node2;      
        node2.next = node3;
        node3.next = node4;
        node4.next = node5;
        node5.next = node6;


        ListNode head2 = new ListNode(9);
        ListNode node21 = new ListNode(9);
        ListNode node22 = new ListNode(9);        
        ListNode node23 = new ListNode(9);        

        head2.next = node21;
        node21.next = node22;     
        node22.next = node23;     
                
        var result1 = AddTwoNumbers(head, head2);
    }

    public void Test1()
    {
        ListNode head = new ListNode(2);
        ListNode node1 = new ListNode(4);
        ListNode node2 = new ListNode(3);        

        head.next = node1;
        node1.next = node2;      


        ListNode head2 = new ListNode(5);
        ListNode node21 = new ListNode(6);
        ListNode node22 = new ListNode(4);        

        head2.next = node21;
        node21.next = node22;     
                
        var result1 = AddTwoNumbers(head, head2);
    }
     public void Test3()
    {
        ListNode head = new ListNode(2);
        ListNode node1 = new ListNode(4);
        ListNode node2 = new ListNode(3);        

        head.next = node1;
        node1.next = node2;      


        ListNode head2 = new ListNode(5);
        ListNode node21 = new ListNode(6);
        ListNode node22 = new ListNode(4);        

        head2.next = node21;
        node21.next = node22;     
                
        var result1 = AddTwoNumbers(head, head2);
    }
}
/*
2. Add Two Numbers
Solved
Medium
Topics
Companies

You are given two non-empty linked lists representing two non-negative integers. The digits are stored in reverse order, and each of their nodes contains a single digit. Add the two numbers and return the sum as a linked list.

You may assume the two numbers do not contain any leading zero, except the number 0 itself.

 

Example 1:

Input: l1 = [2,4,3], l2 = [5,6,4]
Output: [7,0,8]
Explanation: 342 + 465 = 807.

Example 2:

Input: l1 = [0], l2 = [0]
Output: [0]

Example 3:

Input: l1 = [9,9,9,9,9,9,9], l2 = [9,9,9,9]
Output: [8,9,9,9,0,0,0,1]

 

Constraints:

    The number of nodes in each linked list is in the range [1, 100].
    0 <= Node.val <= 9
    It is guaranteed that the list represents a number that does not have leading zeros.


*/