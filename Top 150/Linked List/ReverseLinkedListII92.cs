namespace Top150;

internal class ReverseLinkedListII92
{
    //Definition for singly-linked list.
    public class ListNode
    {
        public int val;
        public ListNode next;
        public ListNode(int val = 0, ListNode next = null)
        {
            this.val = val;
            this.next = next;
        }
    }
    public ListNode ReverseBetween(ListNode head, int left, int right) 
    {
        if (left == right) return head;                
        ListNode previous = null;
        ListNode current = head;
        ListNode end = null;
        ListNode start = null;        

        for (int i = 1; current != null; ++i)
        {          
            if (i < left)
            {
                previous = current;
                current = current.next;
            }            
            
            if (i == left)
            {   
                start = current;
                end = previous;                                       
                ListNode temp = current;
                current = current.next;
                temp.next = previous;
                previous = temp;
            }
            
            if (i == right)
            {
                if (end == null)
                    head = current;
                else                     
                    end.next = current;                    
                    
                start.next = current.next;
                current.next = previous;
                
                break;
            }  
            
            else if (i > left && i < right)
            {                        
                    
                ListNode temp = current;
                current = current.next;
                temp.next = previous;
                previous = temp;                             
                
            }              
                                                           
        }
        

        return head;
    }
    public void Test()
    {
        Test1();
    }
    public void Test1()
    {
        ListNode head = new ListNode(1);
        ListNode node1 = new ListNode(2);
        ListNode node2 = new ListNode(3);
        ListNode node3 = new ListNode(4);
        ListNode node4 = new ListNode(5);

        head.next = node1;
        node1.next = node2;
        node2.next = node3;
        node3.next = node4;

        //var result1 = HasCycle(head);
        var result1 = ReverseBetween(head, 1, 2);
    }
    public void Test2()
    {
        ListNode head = new ListNode(5);
        
        
        var result1 = ReverseBetween(head, 1, 1);
    }
}

/*
92. Reverse Linked List II
Medium
Topics
Companies

Given the head of a singly linked list and two integers left and right where left <= right, reverse the nodes of the list from position left to position right, and return the reversed list.

 

Example 1:

Input: head = [1,2,3,4,5], left = 2, right = 4
Output: [1,4,3,2,5]

Example 2:

Input: head = [5], left = 1, right = 1
Output: [5]

 

Constraints:

    The number of nodes in the list is n.
    1 <= n <= 500
    -500 <= Node.val <= 500
    1 <= left <= right <= n

 
Follow up: Could you do it in one pass?
*/