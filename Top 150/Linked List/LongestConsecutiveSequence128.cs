namespace Top150;

internal class LongestConsecutiveSequence128
{

    //TODO: SLOW
    public int LongestConsecutive(int[] nums) 
    {
        HashSet<int> numsMap = new HashSet<int>(); 
        for (int i = 0; i < nums.Length; ++i)
            numsMap.Add(nums[i]);
        
        int longest = 0;

        for (int i = 0; i < nums.Length; ++i)
        {
            if (numsMap.Contains(nums[i] - 1))
                continue;
            int length = 1;
            int next = nums[i] + 1;
            while (numsMap.Contains(next))
            {
                length++;
                next++;
            }
            longest = Math.Max(longest, length);
        }
        return longest;
    }
    
    public int LongestConsecutive_TOOSLOW2(int[] nums) 
    {
        HashSet<int> numsMap = new HashSet<int>(); 
        int longest = 0;

        for (int i = 0; i < nums.Length; ++i)
        {
            numsMap.Add(nums[i]);
            int length = 1;
            int next = nums[i] + 1;
            while (numsMap.Contains(next))
            {
                length++;
                next++;
            }
            int prev = nums[i] - 1;
            while (numsMap.Contains(prev))
            {
                length++;
                prev--;
            }
            longest = Math.Max(longest, length);
        }
            
        return longest;
        
    }
    public int LongestConsecutive_TOOSLOW(int[] nums) 
    {       
        HashSet<int> numsMap = new HashSet<int>(); 
        for (int i = 0; i < nums.Length; ++i)
            numsMap.Add(nums[i]);
        
        int longest = 0;

        for (int i = 0; i < nums.Length; ++i)
        {
            int length = 1;
            int next = nums[i] + 1;
            while (numsMap.Contains(next))
            {
                length++;
                next++;
            }
            longest = Math.Max(longest, length);
        }
        return longest;
    }
    public void Test()
    {
        int[] nums1 = {100,4,200,1,3,2};
        var result1 = LongestConsecutive(nums1);

        int[] nums2 = {0,3,7,2,5,8,4,6,0,1};
        var result2 = LongestConsecutive(nums2);
    }
}
/*
128. Longest Consecutive Sequence
Medium
19.1K
897
Companies

Given an unsorted array of integers nums, return the length of the longest consecutive elements sequence.

You must write an algorithm that runs in O(n) time.

 

Example 1:

Input: nums = [100,4,200,1,3,2]
Output: 4
Explanation: The longest consecutive elements sequence is [1, 2, 3, 4]. Therefore its length is 4.

Example 2:

Input: nums = [0,3,7,2,5,8,4,6,0,1]
Output: 9

 

Constraints:

    0 <= nums.length <= 105
    -109 <= nums[i] <= 109


*/