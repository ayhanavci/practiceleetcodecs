namespace Top150;

internal class ReverseNodesinkGroup25
{
    //Definition for singly-linked list.
    public class ListNode
    {
        public int val;
        public ListNode next;
        public ListNode(int val = 0, ListNode next = null)
        {
            this.val = val;
            this.next = next;
        }
    }

    public ListNode ReverseKGroup(ListNode head, int k)
    {
        ListNode endOfPreviousChunk = null;
        ListNode nextNode = head;          

        while (nextNode != null)
        {
            ListNode chunkNode = nextNode;            
            Stack<ListNode> chunkToReverse = new Stack<ListNode>();

            for (int i = 0; i < k && chunkNode != null; ++i)
            {                                                                
                chunkToReverse.Push(chunkNode);
                chunkNode = chunkNode.next;
            }
            if (chunkToReverse.Count < k)
                break;                        
            
            ListNode nextReverseItem = chunkToReverse.Pop();
            ListNode startOfNextChunk = nextReverseItem.next;
            if (endOfPreviousChunk != null)
                endOfPreviousChunk.next = nextReverseItem;
            else
                head = nextReverseItem;
            while (chunkToReverse.Count > 0)
            {
                ListNode item = chunkToReverse.Pop();
                nextReverseItem.next = item;
                nextReverseItem = item;
            }
            endOfPreviousChunk = nextReverseItem;
            nextReverseItem.next = startOfNextChunk;

            nextNode = startOfNextChunk;
        }

        return head;
    }
    public void Test()
    {
        ListNode head1 = new ListNode(1);
        ListNode node1 = new ListNode(2);
        ListNode node2 = new ListNode(3);
        ListNode node3 = new ListNode(4);
        ListNode node4 = new ListNode(5);

        head1.next = node1;
        node1.next = node2;
        node2.next = node3;
        node3.next = node4;

        var result1 = ReverseKGroup(head1, 2);

        ListNode head2 = new ListNode(1);
        ListNode node21 = new ListNode(2);
        ListNode node22 = new ListNode(3);
        ListNode node23 = new ListNode(4);
        ListNode node24 = new ListNode(5);

        head2.next = node21;
        node21.next = node22;
        node22.next = node23;
        node23.next = node24;

        var result2 = ReverseKGroup(head2, 3);

        ListNode head3 = new ListNode(1);
        ListNode node31 = new ListNode(2);        
    
        head3.next = node31;                

        var result3 = ReverseKGroup(head3, 2);
    }
}
/*
25. Reverse Nodes in k-Group
Hard
13.1K
645
Companies

Given the head of a linked list, reverse the nodes of the list k at a time, and return the modified list.

k is a positive integer and is less than or equal to the length of the linked list. If the number of nodes is not a multiple of k then left-out nodes, in the end, should remain as it is.

You may not alter the values in the list's nodes, only nodes themselves may be changed.

 

Example 1:

Input: head = [1,2,3,4,5], k = 2
Output: [2,1,4,3,5]

Example 2:

Input: head = [1,2,3,4,5], k = 3
Output: [3,2,1,4,5]

 

Constraints:

    The number of nodes in the list is n.
    1 <= k <= n <= 5000
    0 <= Node.val <= 1000

 

Follow-up: Can you solve the problem in O(1) extra memory space?

*/