namespace Top150;

internal class RotateList61
{
    //Definition for singly-linked list.
    public class ListNode
    {
        public int val;
        public ListNode next;
        public ListNode(int val = 0, ListNode next = null)
        {
            this.val = val;
            this.next = next;
        }
    }
    public ListNode RotateRight(ListNode head, int k) 
    {
        if (k == 0) return head;
        ListNode tail = null;
        ListNode currentNode = head;
        
        int count = 0;
        while (currentNode != null)
        {
            ++count;                   
            if (currentNode.next == null)            
                tail = currentNode;               
            currentNode = currentNode.next;     
        }   
        if (count == 0) return head;
        k %= count;
        if (k == 0) return head;

        ListNode newTail = head;
        currentNode = head;

        for (int i = 0; currentNode != null; ++i)
        {            
            if (i > k)            
                newTail = newTail.next;                        
            
            currentNode = currentNode.next;                 
        }
        tail.next = head;
        ListNode newHead = newTail.next;
        newTail.next = null;
                
        return newHead;
    }

    public void Test()
    {
        Test2();
    }
    public void Test1()
    {
        ListNode head = new ListNode(1);
        ListNode node1 = new ListNode(2);
        ListNode node2 = new ListNode(3);
        ListNode node3 = new ListNode(4);
        ListNode node4 = new ListNode(5);

        head.next = node1;
        node1.next = node2;
        node2.next = node3;
        node3.next = node4;
        
        var result1 = RotateRight(head, 2);
    }
    public void Test2()
    {
        ListNode head = new ListNode(0);
        ListNode node1 = new ListNode(1);
        ListNode node2 = new ListNode(2);
        

        head.next = node1;
        node1.next = node2;
                
        var result1 = RotateRight(head, 4);
    }
}

/*
61. Rotate List
Medium
Topics
Companies

Given the head of a linked list, rotate the list to the right by k places.

 

Example 1:

Input: head = [1,2,3,4,5], k = 2
Output: [4,5,1,2,3]

Example 2:

Input: head = [0,1,2], k = 4
Output: [2,0,1]

 

Constraints:

    The number of nodes in the list is in the range [0, 500].
    -100 <= Node.val <= 100
    0 <= k <= 2 * 109


*/