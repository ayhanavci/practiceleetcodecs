namespace Top150;

internal class PartitionList86
{
    //Definition for singly-linked list.
    public class ListNode
    {
        public int val;
        public ListNode next;
        public ListNode(int val = 0, ListNode next = null)
        {
            this.val = val;
            this.next = next;
        }
    }
    public ListNode Partition(ListNode head, int x) 
    {
        ListNode leftHead = null;
        ListNode rightHead = null;
        
        ListNode currentNode = head;
        ListNode currentLeft = null;
        ListNode currentRight = null;
        
        while (currentNode != null)
        {                          
            if (currentNode.val < x)
            {                                
                if (leftHead == null)             
                    leftHead = currentNode;                     
                else                
                    currentLeft.next = currentNode;            
                
                currentLeft = currentNode;                
            }
            else
            {
                if (rightHead == null)                
                    rightHead = currentNode;                                    
                else
                    currentRight.next = currentNode;
                
                currentRight = currentNode;
            }
            
            currentNode = currentNode.next;
        }
        if (currentRight != null)
            currentRight.next = null;
        if (currentLeft != null)
            currentLeft.next = rightHead;
        else
            leftHead = rightHead;
        return leftHead;
    }

    public void Test()
    {
        Test1();
        //Test2();
        //Test3();
    }
    public void Test1()
    {
        ListNode head = new ListNode(1);
        ListNode node1 = new ListNode(4);
        ListNode node2 = new ListNode(3);
        ListNode node3 = new ListNode(2);
        ListNode node4 = new ListNode(5);
        ListNode node5 = new ListNode(2);

        head.next = node1;
        node1.next = node2;
        node2.next = node3;
        node3.next = node4;
        node4.next = node5;
        
        var result1 = Partition(head, 0);
    }
    public void Test2()
    {
        ListNode head = new ListNode(2);
        ListNode node1 = new ListNode(1);
        
        head.next = node1;
        
        
        var result1 = Partition(head, 2);
    }

    public void Test3()
    {
        ListNode head = new ListNode(1);    
        
        var result1 = Partition(head, 0);
    }

}