namespace Top150;

internal class MergeTwoSortedLists21
{
    //Definition for singly-linked list.
    public class ListNode
    {
        public int val;
        public ListNode next;
        public ListNode(int x)
        {
            val = x;
            next = null;
        }
    }
    public ListNode MergeTwoLists(ListNode list1, ListNode list2)
    {
        ListNode head = null;
        ListNode previous = null;
        ListNode current = null;

        while (list1 != null || list2 != null)
        {
            if (list2 != null && list1 != null)
            {
                if (list1.val <= list2.val)
                {
                    current = list1;
                    list1 = list1.next;
                }
                else
                {
                    current = list2;
                    list2 = list2.next;
                }
            }
            else if (list1 != null)
            {
                current = list1;
                list1 = list1.next;
            }
            else if (list2 != null)
            {
                current = list2;
                list2 = list2.next;
            }
            if (head == null)               
                head = current;                            
            else            
                previous.next = current;                            
            previous = current;
                
        }

        return head;
    }
    public void Test()
    {
        ListNode head1 = new ListNode(1);
        ListNode node11 = new ListNode(2);
        ListNode node12 = new ListNode(4);

        head1.next = node11;
        node11.next = node12;


        ListNode head2 = new ListNode(1);
        ListNode node121 = new ListNode(3);
        ListNode node122 = new ListNode(4);

        head2.next = node121;
        node121.next = node122;

        var result1 = MergeTwoLists(head1, head2);
    }
}
/*
21. Merge Two Sorted Lists
Easy
20.9K
2K
Companies

You are given the heads of two sorted linked lists list1 and list2.

Merge the two lists into one sorted list. The list should be made by splicing together the nodes of the first two lists.

Return the head of the merged linked list.

 

Example 1:

Input: list1 = [1,2,4], list2 = [1,3,4]
Output: [1,1,2,3,4,4]

Example 2:

Input: list1 = [], list2 = []
Output: []

Example 3:

Input: list1 = [], list2 = [0]
Output: [0]

 

Constraints:

    The number of nodes in both lists is in the range [0, 50].
    -100 <= Node.val <= 100
    Both list1 and list2 are sorted in non-decreasing order.


*/