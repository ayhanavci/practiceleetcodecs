namespace Top150;

internal class RemoveNthNodeFromEndofList19
{
    public class ListNode
    {
        public int val;
        public ListNode next;
        public ListNode(int val = 0, ListNode next = null)
        {
            this.val = val;
            this.next = next;
        }
    }

    public ListNode RemoveNthFromEnd(ListNode head, int n) 
    {
        ListNode target = head;
        ListNode current = head;
        ListNode prev = null;

        for (int i = 1; current != null; ++i)
        {
            if (i > n)
            {      
                prev = target;          
                target = target.next;
            }
            current = current.next;
        }
        if (prev == null)                    
            return target.next;                    
        else
            prev.next = target.next;    
                
        return head;
    }
    public void Test()
    {
        Test1();
        Test2();
        Test3();
        Test4();
        
    }
    public void Test1()
    {
        ListNode head1 = new ListNode(1);
        ListNode node1 = new ListNode(2);
        ListNode node2 = new ListNode(3);
        ListNode node3 = new ListNode(4);
        ListNode node4 = new ListNode(5);

        head1.next = node1;
        node1.next = node2;
        node2.next = node3;
        node3.next = node4;

        var result1 = RemoveNthFromEnd(head1, 2);
    }

    public void Test2()
    {
        ListNode head1 = new ListNode(1);
        
        
        var result1 = RemoveNthFromEnd(head1, 1);
    }
     public void Test3()
    {
        ListNode head1 = new ListNode(1);
        ListNode node1 = new ListNode(2);
        
        head1.next = node1;
        
        var result1 = RemoveNthFromEnd(head1, 1);
    }
     public void Test4()
    {
        ListNode head1 = new ListNode(1);
        ListNode node1 = new ListNode(2);
        
        head1.next = node1;
        
        var result1 = RemoveNthFromEnd(head1, 2);
    }
}

/*
19. Remove Nth Node From End of List
Medium
Topics
Companies
Hint

Given the head of a linked list, remove the nth node from the end of the list and return its head.

 

Example 1:

Input: head = [1,2,3,4,5], n = 2
Output: [1,2,3,5]

Example 2:

Input: head = [1], n = 1
Output: []

Example 3:

Input: head = [1,2], n = 1
Output: [1]

 

Constraints:

    The number of nodes in the list is sz.
    1 <= sz <= 30
    0 <= Node.val <= 100
    1 <= n <= sz

 

Follow up: Could you do this in one pass?

*/