namespace Top150;

internal class LRUCache146
{
    public class LRUCache
    {
        public class ListNode
        {
            public int key;
            public int value;
            public ListNode prev;
            public ListNode next;
            public ListNode(int key, int value)
            {
                this.value = value;
                this.key = key;
            }
        }
        int count = 0;
        int capacity = 0;
        ListNode head = null;
        ListNode tail = null;
        public LRUCache(int capacity)
        {
            this.capacity = capacity;
        }
        Dictionary<int, ListNode> mapNodes = new Dictionary<int, ListNode>();
        private void PutInFront(ListNode node)
        {
            if (node == head)
                return; //Already in front        
            
            //Refresh tail if needed
            if (node == tail)
                tail = node.prev;

            //Re-bind prev and next nodes of node
            if (node.next != null)
                node.next.prev = node.prev;
            if (node.prev != null)
                node.prev.next = node.next;
            
            node.prev = null; //new head has no prev
            head.prev = node; //old head is now 2nd.            
            node.next = head; //old head is now second
            head = node;
           
        }
        public int Get(int key)
        {
            ListNode nextNode = head;
            if (mapNodes.ContainsKey(key))
            {
                ListNode node = mapNodes[key];                
                PutInFront(node);
                return node.value;
            }
           
            return -1;
        }

        public void Put(int key, int value)
        {
            //Find if the key exists.            
            if (mapNodes.ContainsKey(key))
            {
                ListNode node = mapNodes[key];
                node.value = value;
                PutInFront(node);
                return;
            }
            ListNode newNode = new ListNode(key, value);
            mapNodes.Add(key, newNode);
            //Key doesn't exist. Need to add a new entry to the head (highest prio).            
            if (count == 0) //First entry.
            {
                head = tail = newNode;
                count++;
            }
            else if (count < capacity) //Has capacity exceeded?
            {
                //No. There is free space. Add it to the head.                
                PutInFront(newNode);
                count++;
            }
            else //Capacity full. Remove tail. Add new entry to the head.
            {
                mapNodes.Remove(tail.key);
                if (tail == head) //Single entry. Capacity=1.
                {
                    tail = head = newNode;                    
                }
                else
                {
                    PutInFront(newNode);
                    tail.prev.next = null;
                    tail = tail.prev;
                }

            }
        }
    }
 
    public void Test()
    {
        Test3();
    }
    public void Test1()
    {
        LRUCache lRUCache = new LRUCache(2);
        lRUCache.Put(1, 1); // cache is {1=1}
        lRUCache.Put(2, 2); // cache is {1=1, 2=2}
        var result = lRUCache.Get(1);    // return 1
        lRUCache.Put(3, 3); // LRU key was 2, evicts key 2, cache is {1=1, 3=3}
        result = lRUCache.Get(2);    // returns -1 (not found)
        lRUCache.Put(4, 4); // LRU key was 1, evicts key 1, cache is {4=4, 3=3}
        result = lRUCache.Get(1);    // return -1 (not found)
        result = lRUCache.Get(3);    // return 3
        result = lRUCache.Get(4);    // return 4
    }
    public void Test2()
    {
        LRUCache lRUCache = new LRUCache(2);
        lRUCache.Put(2, 1); // cache is {1=1}
        lRUCache.Put(2, 2); // cache is {1=1, 2=2}
        var result = lRUCache.Get(2);    // return 2
        lRUCache.Put(1, 1); // cache is {1=1}
        lRUCache.Put(4, 1); // cache is {1=1, 2=2}
        result = lRUCache.Get(2);    // return -1
    }
    public void Test3()
    {
        LRUCache lRUCache = new LRUCache(3);
        lRUCache.Put(1, 1); // cache is {1=1}
        lRUCache.Put(2, 2); // cache is {1=1, 2=2}
        lRUCache.Put(3, 3); // cache is {3=3, 2=2, 1=1}
        lRUCache.Put(4, 4); // cache is {4=4, 3=3, 2=2}
        var result = lRUCache.Get(4);    // return 4
        result = lRUCache.Get(3);    // return 3 {3=3, 4=4, 2=2}
        result = lRUCache.Get(2);    // return 2 {2=2, 3=3, 4=4}
        result = lRUCache.Get(1);    // return -1 {2=2, 3=3, 4=4}
        lRUCache.Put(5, 5); // cache is { 5=5, 2=2, 3=3 }
        result = lRUCache.Get(1);    // return -1 { 5=5, 2=2, 3=3 }
        result = lRUCache.Get(2);    // return 2  { 2=2, 5=5, 3=3 }
        result = lRUCache.Get(3);    // return 3  { 3=3, 2=2, 5=5 } (O:-1) 
        result = lRUCache.Get(4);    // return -1 { 3=3, 2=2, 5=5 }
        result = lRUCache.Get(5);    // return 5  {  5=5, 3=3, 2=2 }
    }
}
  /*
   while (nextNode != null)
            {
                if (nextNode.key == key)
                {
                    PutInFront(nextNode);
                    return nextNode.value;
                }
                nextNode = nextNode.next;
            }
  while (nextNode != null)
            {
                //It does. Update its value and put it in front
                if (nextNode.key == key)
                {
                    nextNode.value = value;
                    PutInFront(nextNode);
                    return;
                }
                nextNode = nextNode.next;

            }*/
/*
146. LRU Cache
Medium
Topics
Companies

Design a data structure that follows the constraints of a Least Recently Used (LRU) cache.

Implement the LRUCache class:

    LRUCache(int capacity) Initialize the LRU cache with positive size capacity.
    int get(int key) Return the value of the key if the key exists, otherwise return -1.
    void put(int key, int value) Update the value of the key if the key exists. Otherwise, add the key-value pair to the cache. If the number of keys exceeds the capacity from this operation, evict the least recently used key.

The functions get and put must each run in O(1) average time complexity.

 

Example 1:

Input
["LRUCache", "put", "put", "get", "put", "get", "put", "get", "get", "get"]
[[2], [1, 1], [2, 2], [1], [3, 3], [2], [4, 4], [1], [3], [4]]
Output
[null, null, null, 1, null, -1, null, -1, 3, 4]

Explanation
LRUCache lRUCache = new LRUCache(2);
lRUCache.put(1, 1); // cache is {1=1}
lRUCache.put(2, 2); // cache is {1=1, 2=2}
lRUCache.get(1);    // return 1
lRUCache.put(3, 3); // LRU key was 2, evicts key 2, cache is {1=1, 3=3}
lRUCache.get(2);    // returns -1 (not found)
lRUCache.put(4, 4); // LRU key was 1, evicts key 1, cache is {4=4, 3=3}
lRUCache.get(1);    // return -1 (not found)
lRUCache.get(3);    // return 3
lRUCache.get(4);    // return 4

 

Constraints:

    1 <= capacity <= 3000
    0 <= key <= 104
    0 <= value <= 105
    At most 2 * 105 calls will be made to get and put.


*/