namespace Top150;

internal class CopyListwithRandomPointer138
{
    public class Node
    {
        public int val;
        public Node next;
        public Node random;

        public Node(int _val)
        {
            val = _val;
            next = null;
            random = null;
        }
    }
    public Node CopyRandomList(Node head)
    {
        if (head == null) return null;        
        Dictionary<Node, Node> pairs = new Dictionary<Node, Node>();

        Node newHead = new Node(head.val)
        {
            random = head.random
        };
        Node newPrev = newHead;
        Node oldCurrent = head;
        Node newCurrent;
        
        pairs.Add(head, newHead); //same position

        while (oldCurrent.next != null)
        {            
            oldCurrent = oldCurrent.next;
            
            newPrev.next = new Node(oldCurrent.val)
            {
                random = oldCurrent.random
            };;
            
            newPrev = newPrev.next;                             
            
            pairs.Add(oldCurrent, newPrev); //same position
        }

        newCurrent = newHead;
        while (newCurrent != null)
        {
            if (newCurrent.random != null)            
                newCurrent.random = pairs[newCurrent.random];
            
            newCurrent = newCurrent.next;
        }

        return newHead;
    }
    public void Test()
    {
        Test1();
    }
    public void Test1()
    {
        Node head1 = new Node(7);
        Node node1 = new Node(13);
        Node node2 = new Node(11);
        Node node3 = new Node(10);
        Node node4 = new Node(1);

        head1.next = node1;
        head1.random = null;

        node1.next = node2;
        node1.random = head1;

        node2.next = node3;
        node2.random = node4;

        node3.next = node4;
        node3.random = node2;

        node4.next = null;
        node4.random = head1;

        var result = CopyRandomList(head1);
    }
}