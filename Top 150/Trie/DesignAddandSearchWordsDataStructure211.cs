namespace Top150;

internal class DesignAddandSearchWordsDataStructure211
{
    //TODO: Slow: 18%
    public class WordDictionary
    {
        public record TrieNode
        {
            public List<TrieNode> children = new List<TrieNode>();
            public char ch;
            public bool endOfWord = false;
            public TrieNode(char ch)
            {
                this.ch = ch;
            }
        }
        TrieNode root;
        public WordDictionary()
        {
            root = new TrieNode('.');
        }

        public void AddWord(string word)
        {
            TrieNode current = root;

            foreach (char ch in word)
            {
                TrieNode? child = null;
                foreach (var _child in current.children)
                {
                    if (_child.ch == ch)
                    {
                        child = _child;
                        break;
                    }
                }
                if (child == null)
                {
                    child = new TrieNode(ch);
                    current.children.Add(child);
                }
                current = child;
            }       
            current.endOfWord = true;     
        }

        public bool Search(string word)
        {        
            TrieNode current = root;

            Queue<Tuple<int, TrieNode>> items = new Queue<Tuple<int, TrieNode>>();
            items.Enqueue(new Tuple<int, TrieNode>(0, current));
            
            while (items.Count > 0)
            {
                var item = items.Dequeue();
                int index = item.Item1;
                current = item.Item2;                

                for (int i = index; i < word.Length; ++i)
                {
                    if (word[i] == '.')
                    {
                        foreach (var _child in current.children)
                            items.Enqueue(new Tuple<int, TrieNode>(i + 1, _child));
                        current = null;
                        break;
                    }
                    //<--
                    TrieNode? next = null;
                    foreach (var _child in current.children)
                    {
                        if (_child.ch == word[i])
                        {
                            next = _child;
                            break;
                        }
                    }
                    current = next;
                    if (current == null)                                            
                        break;                                                                                                
                    //<--
                }
                if (current != null && current.endOfWord)
                    return true;                
            }                   
            return false;   
        }
        
    }
    public void Test()
    {
        Test1();
    }
    public void Test1()
    {
        WordDictionary wordDictionary = new WordDictionary();
        wordDictionary.AddWord("bad");
        wordDictionary.AddWord("dad");
        wordDictionary.AddWord("mad");
        var result1 = wordDictionary.Search("pad"); // return False
        var result2 = wordDictionary.Search("bad"); // return True
        var result3 = wordDictionary.Search(".ad"); // return True
        var result4 = wordDictionary.Search("b.."); // return True
    }
}

/*
211. Design Add and Search Words Data Structure
Medium
Topics
Companies
Hint

Design a data structure that supports adding new words and finding if a string matches any previously added string.

Implement the WordDictionary class:

    WordDictionary() Initializes the object.
    void addWord(word) Adds word to the data structure, it can be matched later.
    bool search(word) Returns true if there is any string in the data structure that matches word or false otherwise. word may contain dots '.' where dots can be matched with any letter.

 

Example:

Input
["WordDictionary","addWord","addWord","addWord","search","search","search","search"]
[[],["bad"],["dad"],["mad"],["pad"],["bad"],[".ad"],["b.."]]
Output
[null,null,null,null,false,true,true,true]

Explanation
WordDictionary wordDictionary = new WordDictionary();
wordDictionary.addWord("bad");
wordDictionary.addWord("dad");
wordDictionary.addWord("mad");
wordDictionary.search("pad"); // return False
wordDictionary.search("bad"); // return True
wordDictionary.search(".ad"); // return True
wordDictionary.search("b.."); // return True

 

Constraints:

    1 <= word.length <= 25
    word in addWord consists of lowercase English letters.
    word in search consist of '.' or lowercase English letters.
    There will be at most 2 dots in word for search queries.
    At most 104 calls will be made to addWord and search.


*/