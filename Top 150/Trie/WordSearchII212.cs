using System.Text;

namespace Top150;

internal class WordSearchII212
{
    public class TrieNode
    {
        public Dictionary<char, TrieNode> children = new Dictionary<char, TrieNode>();
        public bool endOfWord;
        public List<string> words;
    }
    TrieNode root;
    char[][] board;
    int rowCount;
    int colCount;
    HashSet<string> resultSet;
    public IList<string> FindWords(char[][] board, string[] words) 
    {
        //var watch1 = System.Diagnostics.Stopwatch.StartNew();                      
        this.board = board;
        IList<string> result = new List<string>();
        resultSet = new HashSet<string>();
        root = new TrieNode();
        rowCount = board.Length;
        colCount = board[0].Length;

        foreach (string word in words)
        {
            TrieNode current = root;
            foreach (char ch in word)
            {
                TrieNode child;
                if (!current.children.TryGetValue(ch, out child))
                {
                    child = new TrieNode();
                    current.children.Add(ch, child);
                }
                current = child;
            }
            current.endOfWord = true;
            if (current.words == null)
                current.words = new List<string>();
            current.words.Add(word);
        }
        /*watch1.Stop();
        
        var watch2 = System.Diagnostics.Stopwatch.StartNew();    */
        for (int i = 0; i < board.Length; ++i)
        {
            for (int j = 0; j < board[0].Length; ++j)
            {
                //HashSet<Tuple<int, int>> visited = new HashSet<Tuple<int, int>>();
                //dfs(i, j, root, visited);
                //dfs(i, j, new StringBuilder(), root, visited);
                dfs(i, j, root);
            }
        }
        /*watch2.Stop();
        var watch3 = System.Diagnostics.Stopwatch.StartNew();   */
        foreach (var word in resultSet)
            result.Add(word);
        //watch3.Stop();
        
        /*var elapsedMs1 = watch1.ElapsedMilliseconds;
        var elapsedMs2 = watch2.ElapsedMilliseconds;
        var elapsedMs3 = watch3.ElapsedMilliseconds;*/
        return result;
    }
    public void dfs(int row, int col, TrieNode root)
    {        
        if (row < 0 || col < 0 || row >= rowCount || col >= colCount || board[row][col] == '#') return;
        
        char ch = board[row][col];
        TrieNode child;
        if (!root.children.TryGetValue(ch, out child))
            return;
        
        board[row][col] = '#';
        if (child.endOfWord)
        {
            foreach (var word in child.words)
                if (!resultSet.Contains(word))
                    resultSet.Add(word);                
        }                            

        dfs(row - 1, col, child);        
        dfs(row + 1, col, child);
        dfs(row, col - 1, child);        
        dfs(row, col + 1, child);

        board[row][col] = ch;
    }
    /*public void dfs(int row, int col, TrieNode root, HashSet<Tuple<int, int>> visited)
    {
        Tuple<int, int> pos = new Tuple<int, int>(row, col);
        if (row < 0 || col < 0 || row >= rowCount || col >= colCount || visited.Contains(pos)) return;
        
        char ch = board[row][col];
        TrieNode child;
        if (!root.children.TryGetValue(ch, out child))
            return;

        visited.Add(pos);
        
        if (child.endOfWord)
        {
            foreach (var word in child.words)
                if (!resultSet.Contains(word))
                    resultSet.Add(word);                
        }                            

        dfs(row - 1, col, child, visited);        
        dfs(row + 1, col, child, visited);
        dfs(row, col - 1, child, visited);        
        dfs(row, col + 1, child, visited);

        visited.Remove(pos);
    }*/
    /*public void dfs(int row, int col, StringBuilder word, TrieNode root, HashSet<Tuple<int, int>> visited)
    {
        Tuple<int, int> pos = new Tuple<int, int>(row, col);
        if (row < 0 || col < 0 || row >= rowCount || col >= colCount || visited.Contains(pos)) return;
        
        char ch = board[row][col];
        TrieNode child;
        if (!root.children.TryGetValue(ch, out child))
            return;

        visited.Add(pos);

        word.Append(ch);
        if (child.endOfWord)
            result.Add(word.ToString());
        
        StringBuilder down = new StringBuilder();
        down.Append(word);
        dfs(row - 1, col, down, child, visited);

        StringBuilder up = new StringBuilder();
        up.Append(word);
        dfs(row + 1, col, up, child, visited);

        StringBuilder left = new StringBuilder();
        left.Append(word);
        dfs(row, col - 1, left, child, visited);

        StringBuilder right = new StringBuilder();
        right.Append(word);
        dfs(row, col + 1, right, child, visited);

        visited.Remove(pos);
    }*/

    /*public class TrieNode 
    {
        public Dictionary<char, TrieNode> children = new Dictionary<char, TrieNode>();        
    }
    TrieNode root;
    char[][] board;
    int rowCount;
    int colCount;
    public IList<string> FindWords_SLOW(char[][] board, string[] words) 
    {
        IList<string> result = new List<string>();
        root = new TrieNode();
        this.board = board;        
        rowCount = board.Length;
        colCount = board[0].Length;

        //Create Trie
        for (int row = 0; row < rowCount; ++row)        
            for (int col = 0; col < colCount; ++col) 
            {       
                HashSet<Tuple<int, int>> visited = new HashSet<Tuple<int, int>>();                         
                CreateBranches(root, row, col, visited);
            }                                  

        //Find words
        for (int i = 0; i < words.Length; ++i)
        {
            string word = words[i];
            if (SearchWord(word))
                result.Add(word);
        }

        return result;
    }
    private bool SearchWord(string word)
    {   
        TrieNode current = root;
        foreach (char ch in word)
        {
            TrieNode next;
            if (!current.children.TryGetValue(ch, out next))
                return false;
            current = next;
        }
        return true;
    }
    private void CreateBranches(TrieNode parent, int row, int col, HashSet<Tuple<int, int>> visited)
    {     
        if (row < 0 || row >= rowCount || col < 0 || col >= colCount) return;       
        if (visited.Contains(new Tuple<int, int>(row, col))) return;

        char ch = board[row][col];
        TrieNode node;        
        if (!parent.children.TryGetValue(ch, out node))
        {
            node = new TrieNode();
            parent.children.Add(ch, node);    
        }
                
        visited.Add(new Tuple<int, int>(row, col));
        
        CreateBranches(node, row + 1, col, new HashSet<Tuple<int, int>>(visited));
        CreateBranches(node, row - 1, col, new HashSet<Tuple<int, int>>(visited));
        CreateBranches(node, row, col + 1, new HashSet<Tuple<int, int>>(visited));
        CreateBranches(node, row, col - 1, new HashSet<Tuple<int, int>>(visited));   
    }*/
    public void Test()
    {  
        var watch = System.Diagnostics.Stopwatch.StartNew();      
        Test2();
        watch.Stop();
        var elapsedMs = watch.ElapsedMilliseconds;
    }
    public void Test1()
    {
        char[][] board = new char[4][];
        board[0] = new char[]{'o', 'a', 'a', 'n'};
        board[1] = new char[]{'e', 't', 'a', 'e'};
        board[2] = new char[]{'i', 'h', 'k', 'r'};
        board[3] = new char[]{'i', 'f', 'l', 'v'};

        string[] words = {"oath","pea","eat","rain"};

        var result = FindWords(board, words);//["eat","oath"]
    }
    public void Test2()
    {
        char[][] board = new char[12][];
        board[0] = new char[]{'a','a','a','a','a','a','a','a','a','a','a','a'};
        board[1] = new char[]{'a','a','a','a','a','a','a','a','a','a','a','a'};
        board[2] = new char[]{'a','a','a','a','a','a','a','a','a','a','a','a'};
        board[3] = new char[]{'a','a','a','a','a','a','a','a','a','a','a','a'};
        board[4] = new char[]{'a','a','a','a','a','a','a','a','a','a','a','a'};
        board[5] = new char[]{'a','a','a','a','a','a','a','a','a','a','a','a'};
        board[6] = new char[]{'a','a','a','a','a','a','a','a','a','a','a','a'};
        board[7] = new char[]{'a','a','a','a','a','a','a','a','a','a','a','a'};
        board[8] = new char[]{'a','a','a','a','a','a','a','a','a','a','a','a'};
        board[9] = new char[]{'a','a','a','a','a','a','a','a','a','a','a','a'};
        board[10] = new char[]{'a','a','a','a','a','a','a','a','a','a','a','a'};
        board[11] = new char[]{'a','a','a','a','a','a','a','a','a','a','a','a'};

        string[] words = {"a","aa","aaa","aaaa","aaaaa","aaaaaa","aaaaaaa","aaaaaaaa","aaaaaaaaa","aaaaaaaaaa"};

        var result = FindWords(board, words);//["eat","oath"]
    }
    /*
    [
    ['a','a','a','a','a','a','a','a','a','a','a','a'],
    ['a','a','a','a','a','a','a','a','a','a','a','a'],
    ['a','a','a','a','a','a','a','a','a','a','a','a'],
    ['a','a','a','a','a','a','a','a','a','a','a','a'],
    ['a','a','a','a','a','a','a','a','a','a','a','a'],
    ['a','a','a','a','a','a','a','a','a','a','a','a'],
    ['a','a','a','a','a','a','a','a','a','a','a','a'],
    ['a','a','a','a','a','a','a','a','a','a','a','a'],
    ['a','a','a','a','a','a','a','a','a','a','a','a'],
    ['a','a','a','a','a','a','a','a','a','a','a','a'],
    ['a','a','a','a','a','a','a','a','a','a','a','a'],
    ['a','a','a','a','a','a','a','a','a','a','a','a']]    
    ["a","aa","aaa","aaaa","aaaaa","aaaaaa","aaaaaaa","aaaaaaaa","aaaaaaaaa","aaaaaaaaaa"]
    */
}
/*
212. Word Search II
Hard
Topics
Companies
Hint

Given an m x n board of characters and a list of strings words, return all words on the board.

Each word must be constructed from letters of sequentially adjacent cells, where adjacent cells are horizontally or vertically neighboring. The same letter cell may not be used more than once in a word.

 

Example 1:

Input: board = [["o","a","a","n"],["e","t","a","e"],["i","h","k","r"],["i","f","l","v"]], words = ["oath","pea","eat","rain"]
Output: ["eat","oath"]

Example 2:

Input: board = [["a","b"],["c","d"]], words = ["abcb"]
Output: []

 

Constraints:

    m == board.length
    n == board[i].length
    1 <= m, n <= 12
    board[i][j] is a lowercase English letter.
    1 <= words.length <= 3 * 104
    1 <= words[i].length <= 10
    words[i] consists of lowercase English letters.
    All the strings of words are unique.


*/