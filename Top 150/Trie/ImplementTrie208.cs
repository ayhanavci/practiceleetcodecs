namespace Top150;

internal class ImplementTrie208
{
    public class Trie
    {
        public record TrieNode
        {
            public Dictionary<char, TrieNode> children = new Dictionary<char, TrieNode>();
            public bool endOfWord = false;
        }
        TrieNode root;
        public Trie()
        {
            root = new TrieNode();
        }

        public void Insert(string word)
        {
            TrieNode cur = root;

            foreach (char ch in word)
            {
                if (!cur.children.ContainsKey(ch))
                    cur.children.Add(ch, new TrieNode());
                cur = cur.children[ch];
            }
            cur.endOfWord = true;
        }

        public bool Search(string word)
        {
            TrieNode cur = root;

            foreach (char ch in word)
            {
                if (!cur.children.ContainsKey(ch))
                    return false;
                cur = cur.children[ch];
            }

            return cur.endOfWord;
        }

        public bool StartsWith(string prefix)
        {
            TrieNode cur = root;

            foreach (char ch in prefix)
            {
                if (!cur.children.ContainsKey(ch))
                    return false;
                cur = cur.children[ch];
            }

            return true;
        }
    }
    public void Test()
    {
        Test1();
    }
    public void Test1()
    {
        Trie trie = new Trie();
        trie.Insert("apple");
        var result1 = trie.Search("apple");   // return True
        var result2 = trie.Search("app");     // return False
        var result3 = trie.StartsWith("app"); // return True
        trie.Insert("app");
        var result4 = trie.Search("app");     // return True
    }
}

/*
208. Implement Trie (Prefix Tree)
Medium
Topics
Companies

A trie (pronounced as "try") or prefix tree is a tree data structure used to efficiently store and retrieve keys in a dataset of strings. There are various applications of this data structure, such as autocomplete and spellchecker.

Implement the Trie class:

    Trie() Initializes the trie object.
    void insert(String word) Inserts the string word into the trie.
    boolean search(String word) Returns true if the string word is in the trie (i.e., was inserted before), and false otherwise.
    boolean startsWith(String prefix) Returns true if there is a previously inserted string word that has the prefix prefix, and false otherwise.

 

Example 1:

Input
["Trie", "insert", "search", "search", "startsWith", "insert", "search"]
[[], ["apple"], ["apple"], ["app"], ["app"], ["app"], ["app"]]
Output
[null, null, true, false, true, null, true]

Explanation
Trie trie = new Trie();
trie.insert("apple");
trie.search("apple");   // return True
trie.search("app");     // return False
trie.startsWith("app"); // return True
trie.insert("app");
trie.search("app");     // return True

 

Constraints:

    1 <= word.length, prefix.length <= 2000
    word and prefix consist only of lowercase English letters.
    At most 3 * 104 calls in total will be made to insert, search, and startsWith.


*/