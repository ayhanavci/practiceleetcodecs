namespace Top150;

internal class CourseSchedule207
{
    Dictionary<int, List<int>> courses = new Dictionary<int, List<int>>();
    
    public bool CanFinish(int numCourses, int[][] prerequisites) 
    {        
        if (prerequisites == null) return true;        

        int [] dependencyCount = new int[numCourses];
        for (int i = 0; i < prerequisites.Length; ++i)
        {            
            List <int> clients;
            if (!courses.TryGetValue(prerequisites[i][1], out clients))
            {
                clients = new List<int>();       
                courses.Add(prerequisites[i][1], clients);                
            }            
            clients.Add(prerequisites[i][0]);
            dependencyCount[prerequisites[i][0]]++;
        }
        
        Queue<int> freeCourses = new Queue<int>();
        for (int i = 0; i < numCourses; ++i)
        {
            if (dependencyCount[i] == 0)
                freeCourses.Enqueue(i);
        }
        
        while (freeCourses.Count > 0)
        {
            int host = freeCourses.Dequeue();
            List<int> clients;

            if (!courses.TryGetValue(host, out clients))            
                continue;            
            

            foreach (var client in clients)
            {                
                if (--dependencyCount[client] == 0)
                {
                    freeCourses.Enqueue(client);
                }
            }
            courses.Remove(host);
        }

        return courses.Count == 0;
    }

    public bool CanFinish_OLD(int numCourses, int[][] prerequisites) 
    {        
        if (prerequisites == null) return true;        

        for (int i = 0; i < prerequisites.Length; ++i)
        {            
            List <int> depends;
            if (!courses.TryGetValue(prerequisites[i][0], out depends))
            {
                depends = new List<int>();       
                courses.Add(prerequisites[i][0], depends);                
            }            
            depends.Add(prerequisites[i][1]);

            if (!courses.ContainsKey(prerequisites[i][1]))
            {
                depends = new List<int>();       
                courses.Add(prerequisites[i][1], depends);                
            }
        }

        Queue<int> queueRemove = new Queue<int>();
        foreach (var pair in courses)
        {
            if (pair.Value.Count == 0)
                queueRemove.Enqueue(pair.Key);
        }

        while (queueRemove.Count > 0)
        {
            int freeCourse = queueRemove.Dequeue();
            
        }

        return false;
    }
    public bool IsCyclic(int course, HashSet<int> visited)
    {
        if (visited.Contains(course)) return true;

        List<int> dependencies;
        if (courses.TryGetValue(course, out dependencies))
        {
            if (dependencies.Count > 0)            
                visited.Add(course);
            
            foreach (int dependency in dependencies)                            
            {
                if (IsCyclic(dependency, visited))
                    return true;
            }
        }    

        return false;
    }
    public bool IsCyclic_WRONG(int course)
    {
        HashSet<int> visited = new HashSet<int>();
        
        Queue<int> queue = new Queue<int>();
        queue.Enqueue(course);

        while (queue.Count > 0)
        {
            int current = queue.Dequeue();
            if (visited.Contains(current))
                return true;
            visited.Add(current);
            List<int> dependencies;
            if (courses.TryGetValue(current, out dependencies))
            {
                foreach (int dependency in dependencies)            
                    queue.Enqueue(dependency);            
            }            
        }

        return false;
    }
    public void Test()
    {
        //Test1();
        Test2();
        Test3();
        Test4();
        Test5();
    }
    public void Test1()
    {
        int[][] prerequisites = new int[1][];
        prerequisites[0] = new int[] {1, 0};

        var result = CanFinish(2, prerequisites);
    }
    public void Test2()
    {
        int[][] prerequisites = new int[2][];
        prerequisites[0] = new int[] {1, 0};
        prerequisites[1] = new int[] {0, 1};

        var result = CanFinish(2, prerequisites);
    }
    public void Test3()
    {
        var result = CanFinish(1, null);
    }
    public void Test4()
    {
        int[][] prerequisites = new int[4][];
        prerequisites[0] = new int[] {1, 4};
        prerequisites[1] = new int[] {2, 4};
        prerequisites[2] = new int[] {3, 1};
        prerequisites[3] = new int[] {3, 2};

        var result = CanFinish(5, prerequisites);
    }

    public void Test5()
    {
        int[][] prerequisites = new int[6][];
        prerequisites[0] = new int[] {1, 0};
        prerequisites[1] = new int[] {2, 6};
        prerequisites[2] = new int[] {1, 7};
        prerequisites[3] = new int[] {6, 4};
        prerequisites[4] = new int[] {7, 0};
        prerequisites[5] = new int[] {0, 5};
        

        var result = CanFinish(8, prerequisites);
    }
}
/*
207. Course Schedule
Medium
Topics
Companies
Hint

There are a total of numCourses courses you have to take, labeled from 0 to numCourses - 1. You are given an array prerequisites where prerequisites[i] = [ai, bi] indicates that you must take course bi first if you want to take course ai.

    For example, the pair [0, 1], indicates that to take course 0 you have to first take course 1.

Return true if you can finish all courses. Otherwise, return false.

 

Example 1:

Input: numCourses = 2, prerequisites = [[1,0]]
Output: true
Explanation: There are a total of 2 courses to take. 
To take course 1 you should have finished course 0. So it is possible.

Example 2:

Input: numCourses = 2, prerequisites = [[1,0],[0,1]]
Output: false
Explanation: There are a total of 2 courses to take. 
To take course 1 you should have finished course 0, and to take course 0 you should also have finished course 1. So it is impossible.

 

Constraints:

    1 <= numCourses <= 2000
    0 <= prerequisites.length <= 5000
    prerequisites[i].length == 2
    0 <= ai, bi < numCourses
    All the pairs prerequisites[i] are unique.


*/