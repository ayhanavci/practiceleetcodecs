namespace Top150;

internal class SurroundedRegions130
 {
    int rowCount;
    int colCount;
    char[][] _board;
    HashSet<Tuple<int, int>> immuneCoords = new HashSet<Tuple<int, int>>();
    public void Solve(char[][] board) 
    {
        _board = board;
        rowCount = board.Length;
        colCount = board[0].Length;

        //Top Row
        int topRow = 0;
        for (int i = 0; i < colCount; ++i)
        {
            if (_board[topRow][i] == 'O')
                MarkImmunes(topRow, i);

        }
        //Bottom Row
        int bottomRow = rowCount - 1;
        for (int i = 0; i < colCount; ++i)
        {
            if (_board[bottomRow][i] == 'O')
                MarkImmunes(bottomRow, i);
        }
        //Left Column
        int leftCol = 0;
        for (int i = 1; i < rowCount - 1; ++i)
        {
            if (_board[i][leftCol] == 'O')
                MarkImmunes(i, leftCol);
        }

        //Right Column
        int rightCol = colCount - 1;
        for (int i = 1; i < rowCount - 1; ++i)
        {
            if (_board[i][rightCol] == 'O')
                MarkImmunes(i, rightCol);
        }

        for (int i = 1; i < rowCount - 1; ++i)
            for (int j = 1; j < colCount - 1; ++j)
            {
                if (!immuneCoords.Contains(new Tuple<int, int>(i, j)))
                    _board[i][j] = 'X';
            }
    }

    public void MarkImmunes(int x, int y)
    {
        if (x < 0 || y < 0 || x >= rowCount || y >= colCount 
            || _board[x][y] == 'X' || immuneCoords.Contains(new Tuple<int, int>(x, y))) 
            return;

        immuneCoords.Add(new Tuple<int, int>(x, y));
        MarkImmunes(x - 1, y);
        MarkImmunes(x + 1, y);
        MarkImmunes(x, y - 1);
        MarkImmunes(x, y + 1);
    }
    public void Test()
    {
        Test1();
        Test2();
    }
    public void Test1()
    {
        char [][] board = new char[4][];
        board[0] = new char[] {'X', 'X', 'X', 'X'};
        board[1] = new char[] {'X', 'O', 'O', 'X'};
        board[2] = new char[] {'X', 'X', 'O', 'X'};
        board[3] = new char[] {'X', 'O', 'X', 'X'};
        Solve(board);
    }

    public void Test2()
    {
        char [][] board = new char[1][];
        board[0] = new char[] {'X'};        
        Solve(board);
    }
 }

 /*
 130. Surrounded Regions
Medium
Topics
Companies

Given an m x n matrix board containing 'X' and 'O', capture all regions that are 4-directionally surrounded by 'X'.

A region is captured by flipping all 'O's into 'X's in that surrounded region.

 

Example 1:

Input: board = [["X","X","X","X"],["X","O","O","X"],["X","X","O","X"],["X","O","X","X"]]
Output: [["X","X","X","X"],["X","X","X","X"],["X","X","X","X"],["X","O","X","X"]]
Explanation: Notice that an 'O' should not be flipped if:
- It is on the border, or
- It is adjacent to an 'O' that should not be flipped.
The bottom 'O' is on the border, so it is not flipped.
The other three 'O' form a surrounded region, so they are flipped.

Example 2:

Input: board = [["X"]]
Output: [["X"]]

 

Constraints:

    m == board.length
    n == board[i].length
    1 <= m, n <= 200
    board[i][j] is 'X' or 'O'.


 */