namespace Top150;

internal class NumberofIslands200
{
    char[][] _grid;
    int rowCount = 0;
    int colCount = 0;
    public int NumIslands(char[][] grid) 
    {
        _grid = grid;
        rowCount = grid.Length;
        colCount = grid[0].Length;
        
        int count = 0;
        //visitedCoords.Clear();

        for (int x = 0; x < rowCount; ++x)
            for (int y = 0; y < colCount; ++y)
                if (_grid[x][y] == '1')
                {
                    count++;
                    RemoveIsland(x, y);
                }

        return count;
    }
    public void RemoveIsland(int x, int y)
    {   
        if (x < 0 || y < 0 || x >= rowCount || y >= colCount || _grid[x][y] == '0') return;
        
        _grid[x][y] = '0';
        RemoveIsland(x-1, y);
        RemoveIsland(x+1, y);
        RemoveIsland(x, y-1);
        RemoveIsland(x, y+1);        

    }
    HashSet<Tuple<int, int>> visitedCoords = new HashSet<Tuple<int, int>>();
    
    
    public bool IsNewIsland(int x, int y)
    {        
        if (_grid[x][y] == '0') return false;
        if (visitedCoords.Contains(new Tuple<int, int>(x, y))) return false;        

        visitedCoords.Add(new Tuple<int, int>(x, y));
        if (x > 0) IsNewIsland(x - 1, y);
        if (x + 1 < rowCount) IsNewIsland(x + 1, y);
        if (y > 0) IsNewIsland(x, y - 1);
        if (y + 1 < colCount) IsNewIsland(x, y + 1);

        return true;
    }
    public int NumIslands_OLD(char[][] grid) 
    {
        _grid = grid;
        rowCount = grid.Length;
        colCount = grid[0].Length;
        
        int count = 0;
        visitedCoords.Clear();

        for (int x = 0; x < rowCount; ++x)
            for (int y = 0; y < colCount; ++y)
                if (IsNewIsland(x, y)) count++;

        return count;
    }
    public void Test()
    {
        Test1();
        Test2();
        /*visitedCoords.Add(new Tuple<int, int>(5, 7));
        visitedCoords.Add(new Tuple<int, int>(5, 8));
        bool retVal = visitedCoords.Contains(new Tuple<int, int>(5, 4));
        retVal = visitedCoords.Contains(new Tuple<int, int>(5, 7));
        retVal = visitedCoords.Contains(new Tuple<int, int>(5, 9));
        retVal = visitedCoords.Contains(new Tuple<int, int>(4, 7));
        retVal = visitedCoords.Contains(new Tuple<int, int>(5, 8));
        retVal = visitedCoords.Contains(new Tuple<int, int>(5, 6));*/        
    }

    public void Test1()
    {
        char[][] grid = new char[4][];
        grid[0] = new char[] {'1', '1', '1', '1', '0'};
        grid[1] = new char[] {'1', '1', '0', '1', '0'};
        grid[2] = new char[] {'1', '1', '0', '0', '0'};
        grid[3] = new char[] {'0', '0', '0', '0', '0'};

        var result = NumIslands(grid);
    }
     public void Test2()
    {
        char[][] grid = new char[4][];
        grid[0] = new char[] {'1', '1', '0', '0', '0'};
        grid[1] = new char[] {'1', '1', '0', '0', '0'};
        grid[2] = new char[] {'0', '0', '1', '0', '0'};
        grid[3] = new char[] {'0', '0', '0', '1', '1'};

        var result = NumIslands(grid);
    }
}
/*
200. Number of Islands
Medium
Topics
Companies

Given an m x n 2D binary grid grid which represents a map of '1's (land) and '0's (water), return the number of islands.

An island is surrounded by water and is formed by connecting adjacent lands horizontally or vertically. You may assume all four edges of the grid are all surrounded by water.

 

Example 1:

Input: grid = [
  ["1","1","1","1","0"],
  ["1","1","0","1","0"],
  ["1","1","0","0","0"],
  ["0","0","0","0","0"]
]
Output: 1

Example 2:

Input: grid = [
  ["1","1","0","0","0"],
  ["1","1","0","0","0"],
  ["0","0","1","0","0"],
  ["0","0","0","1","1"]
]
Output: 3

 

Constraints:

    m == grid.length
    n == grid[i].length
    1 <= m, n <= 300
    grid[i][j] is '0' or '1'.


*/