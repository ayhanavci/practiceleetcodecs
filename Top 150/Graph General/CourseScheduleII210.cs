namespace Top150;

internal class CourseScheduleII210
{
    public int[] FindOrder(int numCourses, int[][] prerequisites) 
    {
        List<int> ordered = new List<int>();
        Dictionary<int, List<int>> hostClientMap = new Dictionary<int, List<int>>();
        
        int [] courseDependencyCount = new int[numCourses];
        for (int i = 0; i < prerequisites.Length; ++i)
        {
            List<int> clients;
            int host = prerequisites[i][1];
            int client = prerequisites[i][0];
            if (!hostClientMap.TryGetValue(host, out clients))
            {
                clients = new List<int>();
                hostClientMap.Add(host, clients);
            }
            clients.Add(client);
            courseDependencyCount[client]++;
        }

        Queue<int> queue = new Queue<int>();

        for (int i = 0; i < numCourses; ++i)
        {
            int count = courseDependencyCount[i];
            if (count == 0)            
                queue.Enqueue(i);                
            
        }

        while (queue.Count > 0)
        {
            int freeCourse = queue.Dequeue();
            ordered.Add(freeCourse);
            List<int> clients;
            if (hostClientMap.TryGetValue(freeCourse, out clients))
            {
                foreach (int client in clients)
                {
                    if (--courseDependencyCount[client] == 0)                    
                        queue.Enqueue(client);                    
                }
            }
        }

        return ordered.Count == numCourses ? ordered.ToArray() : new int[0];

    }
    public void Test()
    {
        Test2();
    }
    public void Test1()
    {
        int [][] prerequisites = new int[4][];
        prerequisites[0] = new int[] {1,0};
        prerequisites[1] = new int[] {2,0};
        prerequisites[2] = new int[] {3,1};
        prerequisites[3] = new int[] {3,2};

        var result = FindOrder(4, prerequisites);
    }
    public void Test2()
    {
        int [][] prerequisites = new int[3][];
        prerequisites[0] = new int[] {1,0};
        prerequisites[1] = new int[] {1,2};
        prerequisites[2] = new int[] {0,1};        

        var result = FindOrder(3, prerequisites);
    }
}
/*
210. Course Schedule II
Medium
Topics
Companies
Hint

There are a total of numCourses courses you have to take, labeled from 0 to numCourses - 1. You are given an array prerequisites where prerequisites[i] = [ai, bi] indicates that you must take course bi first if you want to take course ai.

    For example, the pair [0, 1], indicates that to take course 0 you have to first take course 1.

Return the ordering of courses you should take to finish all courses. If there are many valid answers, return any of them. If it is impossible to finish all courses, return an empty array.

 

Example 1:

Input: numCourses = 2, prerequisites = [[1,0]]
Output: [0,1]
Explanation: There are a total of 2 courses to take. To take course 1 you should have finished course 0. So the correct course order is [0,1].

Example 2:

Input: numCourses = 4, prerequisites = [[1,0],[2,0],[3,1],[3,2]]
Output: [0,2,1,3]
Explanation: There are a total of 4 courses to take. To take course 3 you should have finished both courses 1 and 2. Both courses 1 and 2 should be taken after you finished course 0.
So one correct course order is [0,1,2,3]. Another correct ordering is [0,2,1,3].

Example 3:

Input: numCourses = 1, prerequisites = []
Output: [0]

 

Constraints:

    1 <= numCourses <= 2000
    0 <= prerequisites.length <= numCourses * (numCourses - 1)
    prerequisites[i].length == 2
    0 <= ai, bi < numCourses
    ai != bi
    All the pairs [ai, bi] are distinct.


*/