namespace Top150;

internal class EvaluateDivision399
{
     // Definition for a Node.
    /*public class Node
    {        
        public IList<Tuple<double, Node>> neighbors;

        public Node()
        {     
            neighbors = new List<Tuple<double, Node>>();
        }
        
        public Node(List<Tuple<double, Node>> _neighbors)
        {
            neighbors = _neighbors;
        }
    }*/
    Dictionary<string, List<Tuple<string, double>> > nodeMap = new Dictionary<string, List<Tuple<string, double>>>();
    
    public double[] CalcEquation(IList<IList<string>> equations, double[] values, IList<IList<string>> queries) 
    {
        List<double> result = new List<double>();
        for (int i = 0; i < equations.Count; ++i)
        {
            var equation = equations[i];
            if (!nodeMap.TryGetValue(equation[0], out List<Tuple<string, double>> neighbours))
            {
                neighbours = new List<Tuple<string, double>>();                
                nodeMap.Add(equation[0], neighbours);
            }
            neighbours.Add(new Tuple<string, double>(equation[1], values[i]));

            if (!nodeMap.TryGetValue(equation[1], out List<Tuple<string, double>> reverseNeighbours))
            {
                reverseNeighbours = new List<Tuple<string, double>>();                
                nodeMap.Add(equation[1], reverseNeighbours);
            }
            reverseNeighbours.Add(new Tuple<string, double>(equation[0], 1 / values[i]));          
            
        }
        foreach (var query in queries)
        {                                    
            result.Add(BFS(query[0], query[1]));
        }
        return result.ToArray();
    }
    
    public double BFS(string source, string target)
    {
        HashSet<string> visited = new HashSet<string>();        
        Queue<Tuple<string, double>> queue = new Queue<Tuple<string, double>>();

        queue.Enqueue(new Tuple<string, double>(source, 1));
        visited.Add(source);

        while (queue.Count > 0)
        {            
            var next = queue.Dequeue();            
            if (!nodeMap.ContainsKey(next.Item1))
                return -1;

            List<Tuple<string, double>> neighbours = nodeMap[next.Item1];        

            foreach (var neighbour in neighbours) {                
                if (neighbour.Item1 == target)
                {
                    //Bingo
                    return neighbour.Item2 * next.Item2;
                }    
                else
                {
                    if (visited.Contains(neighbour.Item1))
                        continue;
                    visited.Add(neighbour.Item1);
                    queue.Enqueue(new Tuple<string, double>(neighbour.Item1, neighbour.Item2 * next.Item2));
                }
            }
        }
                
        return -1;
    }
    public void Test()
    {
        Test1();
    }
    public void Test1()
    {
        IList<IList<string>> equations = new List<IList<string>>
        {
            new List<string>() { "a", "b" },
            new List<string>() { "b", "c" },
        };    
        double[] values = new double[] {2.0, 3.0};
        
        IList<IList<string>> queries = new List<IList<string>>
        {
            new List<string>() { "a", "c" },
            new List<string>() { "b", "a" },
            new List<string>() { "a", "e" },
            new List<string>() { "a", "a" },
            new List<string>() { "x", "x" },
        };  
        
        var result = CalcEquation(equations, values, queries); //6.0, 0.5, -1.0, 1.0, -1.0 
    }

}

/*
399. Evaluate Division
Medium
Topics
Companies
Hint

You are given an array of variable pairs equations and an array of real numbers values, where equations[i] = [Ai, Bi] and values[i] represent the equation Ai / Bi = values[i]. Each Ai or Bi is a string that represents a single variable.

You are also given some queries, where queries[j] = [Cj, Dj] represents the jth query where you must find the answer for Cj / Dj = ?.

Return the answers to all queries. If a single answer cannot be determined, return -1.0.

Note: The input is always valid. You may assume that evaluating the queries will not result in division by zero and that there is no contradiction.

Note: The variables that do not occur in the list of equations are undefined, so the answer cannot be determined for them.

 

Example 1:

Input: equations = [["a","b"],["b","c"]], values = [2.0,3.0], queries = [["a","c"],["b","a"],["a","e"],["a","a"],["x","x"]]
Output: [6.00000,0.50000,-1.00000,1.00000,-1.00000]
Explanation: 
Given: a / b = 2.0, b / c = 3.0
queries are: a / c = ?, b / a = ?, a / e = ?, a / a = ?, x / x = ? 
return: [6.0, 0.5, -1.0, 1.0, -1.0 ]
note: x is undefined => -1.0

Example 2:

Input: equations = [["a","b"],["b","c"],["bc","cd"]], values = [1.5,2.5,5.0], queries = [["a","c"],["c","b"],["bc","cd"],["cd","bc"]]
Output: [3.75000,0.40000,5.00000,0.20000]

Example 3:

Input: equations = [["a","b"]], values = [0.5], queries = [["a","b"],["b","a"],["a","c"],["x","y"]]
Output: [0.50000,2.00000,-1.00000,-1.00000]

 

Constraints:

    1 <= equations.length <= 20
    equations[i].length == 2
    1 <= Ai.length, Bi.length <= 5
    values.length == equations.length
    0.0 < values[i] <= 20.0
    1 <= queries.length <= 20
    queries[i].length == 2
    1 <= Cj.length, Dj.length <= 5
    Ai, Bi, Cj, Dj consist of lower case English letters and digits.


*/