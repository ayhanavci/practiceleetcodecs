namespace Top150;

internal class PlusOne66
{
    public int[] PlusOne(int[] digits) 
    {
        List<int> result = new List<int>();                
        
        bool overflow = false;

        int last = digits[digits.Length - 1] + 1;
        if (last >= 10)
        {
            overflow = true;
            result.Add(last % 10);
        }
        else
        {
            result.Add(last);
        }

        for (int i = digits.Length - 2; i >= 0; --i)
        {
            int digit = digits[i];
            if (overflow)
            {
                digit += 1;
                if (digit >= 10)                
                    digit %= 10;                
                else                
                    overflow = false;                

            }
            result.Insert(0, digit);
        }
        if (overflow)
            result.Insert(0, 1);

        return result.ToArray();

    }
    public void Test()
    {
        //Test1();
        //Test2();
        //Test3();
        Test4();
    }
    public void Test1()
    {
        int[] digits = new int[] {1,2,3};
        var result = PlusOne(digits);
    }
    public void Test3()
    {
        int[] digits = new int[] {9};
        var result = PlusOne(digits);
    }
    public void Test2()
    {
        int[] digits = new int[] {4,3,2,1};
        var result = PlusOne(digits);
    }
    public void Test4()
    {
        int[] digits = new int[] {9,9,9,9};
        var result = PlusOne(digits);
    }
}

/*
66. Plus One
Solved
Easy
Topics
Companies

You are given a large integer represented as an integer array digits, where each digits[i] is the ith digit of the integer. The digits are ordered from most significant to least significant in left-to-right order. The large integer does not contain any leading 0's.

Increment the large integer by one and return the resulting array of digits.

 

Example 1:

Input: digits = [1,2,3]
Output: [1,2,4]
Explanation: The array represents the integer 123.
Incrementing by one gives 123 + 1 = 124.
Thus, the result should be [1,2,4].

Example 2:

Input: digits = [4,3,2,1]
Output: [4,3,2,2]
Explanation: The array represents the integer 4321.
Incrementing by one gives 4321 + 1 = 4322.
Thus, the result should be [4,3,2,2].

Example 3:

Input: digits = [9]
Output: [1,0]
Explanation: The array represents the integer 9.
Incrementing by one gives 9 + 1 = 10.
Thus, the result should be [1,0].

 

Constraints:

    1 <= digits.length <= 100
    0 <= digits[i] <= 9
    digits does not contain any leading 0's.


*/