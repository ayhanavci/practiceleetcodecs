namespace Top150;

internal class FactorialTrailingZeroes172
{
    public int TrailingZeroes(int n) 
    {
        int count = 0;

        for (int i = 5; n / i >= 1; i *= 5)
            count += n / i;

        return count;
    }
    public void Test()
    {

    }
}
/*
Count trailing zeroes by counting occurrence of 5 in the factorial:

    A simple method is to first calculate factorial of n, then count trailing 0s in the result but this can cause overflow for bigger numbers. The idea is to consider prime factors of a factorial n. A trailing zero is always produced by prime factors 2 and 5. If we can count the number of 5s and 2s, our task is done.

Illustration:

    Consider the following examples:

    Input: n = 5: There is one 5 and three 2s in prime factors of 5! (2 * 2 * 2 * 3 * 5). So a count of trailing 0s is 1.
    Input: n = 11: There are two 5s and eight 2s in prime factors of 11! (2 8 * 34 * 52 * 7). So the count of trailing 0s is 2.

    We can observe that the number of 2s in prime factors is always more than or equal to the number of 5s. So, if we count 5s in prime factors, we are done.

How to count the total number of 5s in prime factors of n! ?

A simple way is to calculate floor(n/5). For example, 7! has one 5, 10! has two 5s. But, numbers like 25, 125, etc have more than 5 instead of floor (n / 5). For example, if we consider 28! we get one extra 5 and the number of 0s becomes 6. Handling this is simple, first, divide n by 5 and remove all single 5s, then divide by 25 to remove extra 5s, and so on.

Following is the summarized formula for counting trailing 0s.

    Trailing 0s in n! = Count of 5s in prime factors of n! = floor(n/5) + floor(n/25) + floor(n/125) + ….

*/
/*
172. Factorial Trailing Zeroes
Medium
Topics
Companies

Given an integer n, return the number of trailing zeroes in n!.

Note that n! = n * (n - 1) * (n - 2) * ... * 3 * 2 * 1.

 

Example 1:

Input: n = 3
Output: 0
Explanation: 3! = 6, no trailing zero.

Example 2:

Input: n = 5
Output: 1
Explanation: 5! = 120, one trailing zero.

Example 3:

Input: n = 0
Output: 0

 

Constraints:

    0 <= n <= 104

 

Follow up: Could you write a solution that works in logarithmic time complexity?

*/