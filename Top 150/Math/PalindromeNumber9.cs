namespace Top150;

internal class PalindromeNumber9
{
    public bool IsPalindrome(int x) 
    {
        if (x < 0) return false;
        if (x == 0) return true;

        int [] digits = new int[10];
        int count = 0;

        while (x > 0)
        {
            digits[count++] = x % 10;
            x /= 10;            
        }
        for (int i = 0; i < count / 2; ++i)
        {
            if (digits[i] != digits[count-i-1])
                return false;
        }
        return true;
    }
    public void Test()
    {
        var result1 = IsPalindrome(121);
        var result2 = IsPalindrome(10);
        var result3 = IsPalindrome(101);
        var result4 = IsPalindrome(21012);
        var result5 = IsPalindrome(3210123);
        var result6 = IsPalindrome(432101235);
        //2147483647
    }
}

/*
9. Palindrome Number
Solved
Easy
Topics
Companies
Hint

Given an integer x, return true if x is a
palindrome
, and false otherwise.

 

Example 1:

Input: x = 121
Output: true
Explanation: 121 reads as 121 from left to right and from right to left.

Example 2:

Input: x = -121
Output: false
Explanation: From left to right, it reads -121. From right to left, it becomes 121-. Therefore it is not a palindrome.

Example 3:

Input: x = 10
Output: false
Explanation: Reads 01 from right to left. Therefore it is not a palindrome.

 

Constraints:

    -231 <= x <= 231 - 1

 
Follow up: Could you solve it without converting the integer to a string?
*/