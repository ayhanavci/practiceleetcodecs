namespace Top150;

internal class Pow_x_n50
{
    //TODO: Anlamadim!?
    public double MyPow(double x, int n) 
    {
        if(n < 0)                     
            x = 1 / x;        
        
        long num = Math.Abs((long)n);
                                    
        double pow = 1;
        
        while(num > 0){
            if((num & 1) != 0) //multiply only when the number is odd: n % 2 == 1
                pow *= x;            
            
            x *= x;
            num >>= 1; //equivalent to n = n / 2; i.e. keep dividing the number by 2
        }
        
        return pow;
    }
    public void Test()
    {
        //var result = MyPow(2.0, 10);//1024.00000
        //var result1 = MyPow(2.1, 3);//9.26100
        //var result2 = MyPow(2.0, -2);//0.25000
        var result3 = MyPow(2.0, -2147483648);//0.0
    }
}
/*


    FIRST APPROACH

    We can solve this problem by multiplying x by n times
    eg:
    x = 7 and n = 11
    7 * 7 * 7 * 7 * 7 * 7 * 7 * 7 * 7 * 7 * 7 = 1977326743
    Here we have multiplied 7 for 11 times, which will result in O(n)
    But, Suppose x = 1 and n = 2147483647
    If we follow this approach then, 1 will be multiplied 2147483647 times which is not efficient at all.

    COMPLEXITY
        Time: O(n), where n is the given power
        Space: O(1), in-place

    SECOND APPROACH

    In order to improve efficiency we will opt for Binary Exponentiation using which we can calculate xn using O log2(N) multiplications.

    Basic Idea is to divide the work using binary representation of exponents
    i.e. is to keep multiplying pow with x, if the bit is odd, and multiplying x with itself until we get 0
    We will use very 1st example of 1st Approach i.e.
    x = 7, n = 11 and pow = 1
    Here, we have to calculate 711
    Binary of n i.e. (11)10 is (1011)2
    1   0   1   1
    23  22  21  20   <-- Corresponding place values of each bit

    OR we can also write this as
    1 0 1 1
    8 4 2 1 <-- Corresponding place values of each bit

    Now, 78 × 72 × 71 == 711 as 7(8 + 2 + 1) == 711
    NOTE: We have not considered 74 in this case as the 4th place bit is OFF

    So, 78 × 72 × 71 == 5764801 × 49 × 7 == 1977326743 <-- Desired Output
    Now, applying logic keeping this concept in mind

    double pow = 1;
    while(n != 0){
    	if((n & 1) != 0) // equivalent to if((n % 2) != 0) i.e. multiply only when the number is odd  
    	pow *= x;

    	x *= x;
    	n >>>= 1; // equivalent to n = n / 2; i.e. keep dividing the number by 2

    }

    PROCESS

        Iteration 1
        pow = 1 × 7 = 7
        x = 7 × 7 = 49
        n = 11 >>> 1 = 5

        Iteration 2
        pow = 7 × 49 = 343
        x = 49 × 49 = 2401
        n = 5 >>> 1 = 2

        Iteration 3
        x = 2401 × 2401 = 5764801
        n = 2 >>> 1 = 1

        Iteration 4
        pow = 343 × 5764801 = 1977326743
        x = 5764801 × 5764801 = 3.323293057 × 10¹³
        n = 1 >>> 1 = 0

We exit the loop as the number has become 0 and we got pow as 1977326743 which is the desired output
In this binary exponentiation approach, the loop iterated for only 4 times which is nothing but (O log2(N) + 1) ~ (O log2(N))

And for 2nd example of 1st Approach where
x = 1 and n = 2147483647
This loop executed for only 31 times (O log2(N)) which is far far less than 2147483647 times(in case of O(N) approach)
*/
/*
50. Pow(x, n)
Attempted
Medium
Topics
Companies

Implement pow(x, n), which calculates x raised to the power n (i.e., xn).

 

Example 1:

Input: x = 2.00000, n = 10
Output: 1024.00000

Example 2:

Input: x = 2.10000, n = 3
Output: 9.26100

Example 3:

Input: x = 2.00000, n = -2
Output: 0.25000
Explanation: 2-2 = 1/22 = 1/4 = 0.25

 

Constraints:

    -100.0 < x < 100.0
    -231 <= n <= 231-1
    n is an integer.
    Either x is not zero or n > 0.
    -104 <= xn <= 104


*/