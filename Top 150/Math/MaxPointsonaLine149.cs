namespace Top150;

internal class MaxPointsonaLine149
{
    public int MaxPoints2(int[][] points) 
    {
        Dictionary<double, int> slopeCount = new Dictionary<double, int>();
        Dictionary<int, int> perpendicularCount = new Dictionary<int, int>();

        int max = 1;
        for (int i = 0; i < points.Length; ++i)
        {
            int[] p1 = points[i];
            for (int j = i + 1; j < points.Length; ++j)
            {
                int[] p2 = points[j];

                if (p1[0] == p2[0]) //Perpendicular to X
                {
                    if (perpendicularCount.ContainsKey(p1[0]))
                        perpendicularCount[p1[0]] += 1; //Add point 2 (1 already added)
                    else
                        perpendicularCount.Add(p1[0], 2);//Add point 1 and 2
                    
                    max = Math.Max(perpendicularCount[p1[0]], max);
                }       
                else
                {
                    double slope = ((double)p2[1] - p1[1]) / ((double)p2[0] - p1[0]);
                    if (slopeCount.ContainsKey(slope))
                        slopeCount[slope] += 1; //Add point 2
                    else
                        slopeCount.Add(slope, 2); //Add point 1 and 2

                    max = Math.Max(slopeCount[slope], max);
                }

            }            
        }
        return max;
    
    }

    public int MaxPoints(int[][] points) 
    {
        Dictionary<
            Tuple<double, double>, //Slope & y intercept
            HashSet<Tuple<int, int>>> //Point
            
            slopeCount = new Dictionary<Tuple<double, double>, HashSet<Tuple<int, int>>>();
        
        
        Dictionary<
            int, //X point is enough
            HashSet<Tuple<int, int>>> //Point
            
            perpendicularCount = new Dictionary<int, HashSet<Tuple<int, int>>>();


        int max = 1;
        for (int i = 0; i < points.Length; ++i)
        {
            int[] p1 = points[i];
            for (int j = i + 1; j < points.Length; ++j)
            {
                int[] p2 = points[j];
                
                if (p1[0] == p2[0]) //Perpendicular to X
                {
                    if (perpendicularCount.ContainsKey(p1[0]))
                    {
                        perpendicularCount[p1[0]].Add(new Tuple<int, int>(p2[0], p2[1]));//Add point 2
                    }                                                
                    else
                    {
                        HashSet<Tuple<int, int>> pointSet = new HashSet<Tuple<int, int>>
                        {
                            new Tuple<int, int>(p1[0], p1[1]),//Add point 1
                            new Tuple<int, int>(p2[0], p2[1])//Add point 2                        
                        };
                        perpendicularCount.Add(p1[0], pointSet);
                    }                        
                    
                    max = Math.Max(perpendicularCount[p1[0]].Count, max);
                }       
                else
                {                    
                    //Slope (y2 - y1) / (x2 - x1)
                    //Slope intercept: y = mx + b => b = y - mx
                    double slope = ((double)p2[1] - p1[1]) / ((double)p2[0] - p1[0]);                                        
                                        
                    double intercept;
                    
                    if (p1[0] == 0 && p1[1] == 0)
                    {
                        intercept = p2[1] - (p2[0] * slope);
                    }
                    else
                    {
                        intercept = p1[1] - (p1[0] * slope);
                    }                    
                    /*
                    slope = (y2 - y1) / (x2 - x1)
                    b = y1 - x1 * slope
                    */
                    
                    var key = new Tuple<double, double>(Math.Round(slope, 10), Math.Round(intercept, 7));
                    if (slopeCount.ContainsKey(key))
                    {
                        slopeCount[key].Add(new Tuple<int, int>(p2[0], p2[1]));//Add point 2
                    }                                        
                    else 
                    {
                        HashSet<Tuple<int, int>> pointSet = new HashSet<Tuple<int, int>>
                        {
                            new Tuple<int, int>(p1[0], p1[1]),//Add point 1
                            new Tuple<int, int>(p2[0], p2[1])//Add point 2                        
                        };
                        slopeCount.Add(key, pointSet);
                    }                        

                    max = Math.Max(slopeCount[key].Count, max);
                }

            }            
        }
        return max;
        //[[0,0],[4,5],[7,8],[8,9],[5,6],[3,4],[1,1]]
    }
    public void Test()
    {
        //Test1();
        //Test2();
        //Test3();
        //Test4();
        Test5();
    }
    public void Test1()
    {
        int[][] points = new int[3][];
        points[0] = new int[] {1,1};
        points[1] = new int[] {2,2};
        points[2] = new int[] {3,3};

        var result = MaxPoints(points);
    }
    public void Test2()
    {
        int[][] points = new int[6][];
        points[0] = new int[] {1,1};
        points[1] = new int[] {3,2};
        points[2] = new int[] {5,3};
        points[3] = new int[] {4,1};
        points[4] = new int[] {2,3};
        points[5] = new int[] {1,4};

        var result = MaxPoints(points);
    }
    public void Test3()
    {
        //[[0,0],[4,5],[7,8],[8,9],[5,6],[3,4],[1,1]]
        int[][] points = new int[7][];
        points[0] = new int[] {0,0};
        points[1] = new int[] {4,5};
        points[2] = new int[] {7,8};
        points[3] = new int[] {8,9};
        points[4] = new int[] {5,6};
        points[5] = new int[] {3,4};
        points[6] = new int[] {1,1};

        var result = MaxPoints(points);//o:6 e:5
    }
    public void Test4()
    {
        int[][] points = new int[3][];
        points[0] = new int[] {-6,-1};
        points[1] = new int[] {3,1};
        points[2] = new int[] {12,3};

        var result = MaxPoints(points);
    }
    public void Test5()
    {
        int[][] points = new int[3][];
        points[0] = new int[] {5151,5150};
        points[1] = new int[] {0,0};
        points[2] = new int[] {5152,5151};

        var result = MaxPoints(points);
    }
}
/*
149. Max Points on a Line
Hard
Topics
Companies

Given an array of points where points[i] = [xi, yi] represents a point on the X-Y plane, return the maximum number of points that lie on the same straight line.

 

Example 1:

Input: points = [[1,1],[2,2],[3,3]]
Output: 3

Example 2:

Input: points = [[1,1],[3,2],[5,3],[4,1],[2,3],[1,4]]
Output: 4

 

Constraints:

    1 <= points.length <= 300
    points[i].length == 2
    -104 <= xi, yi <= 104
    All the points are unique.


*/