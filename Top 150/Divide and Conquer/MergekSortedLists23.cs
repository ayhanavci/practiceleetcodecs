namespace Top150;

internal class MergekSortedLists23
{
    //TODO: Slow: 26%
    // Definition for singly-linked list.
    public class ListNode
    {
        public int val;
        public ListNode next;
        public ListNode(int val = 0, ListNode next = null)
        {
            this.val = val;
            this.next = next;
        }
    }

    public ListNode MergeKLists(ListNode[] lists)
    {
        if (lists.Length == 0) return null;
        
        ListNode result = lists[0];
        
        for (int i = 1; i < lists.Length; ++i)
        {
            result = Merge2Lists(result, lists[i]);
        }

        return result;
    }
    public ListNode Merge2Lists(ListNode first, ListNode second)
    {
        if (first == null) return second;
        if (second == null) return first;
        ListNode head = null;
        
        if (first != null && second != null)
        {
            if (first.val <= second.val)
            {
                head = first;
                first = first.next;
            }   
            else
            {
                head = second;
                second = second.next;
            }            
        }
        ListNode current = head;
        while (first != null && second != null)
        {
            if (first.val <= second.val)
            {
                current.next = first;
                first = first.next;                
            }
            else
            {
                current.next = second;
                second = second.next;
            }
            current = current.next;
        }
        if (first != null)        
            current.next = first;
        
        if (second != null)        
            current.next = second;

        return head;
    }
    public void Test()
    {
        Test2();
    }
    public void Test1()
    {
        ListNode[] lists = new ListNode[3];
        lists[0] = new ListNode(1, new ListNode(4,  new ListNode(5)));
        lists[1] = new ListNode(1, new ListNode(3,  new ListNode(4)));
        lists[2] = new ListNode(2, new ListNode(6));

        var result = MergeKLists(lists);

    }
    public void Test2()
    {
        ListNode[] lists = new ListNode[2];
        lists[0] = null;
        lists[1] = new ListNode(1);      

        var result = MergeKLists(lists);

    }
}

/*
23. Merge k Sorted Lists
Hard
Topics
Companies

You are given an array of k linked-lists lists, each linked-list is sorted in ascending order.

Merge all the linked-lists into one sorted linked-list and return it.

 

Example 1:

Input: lists = [[1,4,5],[1,3,4],[2,6]]
Output: [1,1,2,3,4,4,5,6]
Explanation: The linked-lists are:
[
  1->4->5,
  1->3->4,
  2->6
]
merging them into one sorted list:
1->1->2->3->4->4->5->6

Example 2:

Input: lists = []
Output: []

Example 3:

Input: lists = [[]]
Output: []

 

Constraints:

    k == lists.length
    0 <= k <= 104
    0 <= lists[i].length <= 500
    -104 <= lists[i][j] <= 104
    lists[i] is sorted in ascending order.
    The sum of lists[i].length will not exceed 104.


*/