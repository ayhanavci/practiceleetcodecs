namespace Top150;

internal class SortList148
{
    //TODO: Slow: 23%
    //Definition for singly-linked list.
    public class ListNode
    {
        public int val;
        public ListNode next;
        public ListNode(int val = 0, ListNode next = null)
        {
            this.val = val;
            this.next = next;
        }
    }
    public ListNode SortList(ListNode head)
    {
        if (head == null || head.next == null) return head;
        List<Tuple<int, ListNode>> flattened = new List<Tuple<int, ListNode>>();
        
        
        while (head != null)
        {
            flattened.Add(new Tuple<int, ListNode>(head.val, head));
            head = head.next;
        }

        List<Tuple<int, ListNode>> ordered = flattened.OrderBy(x => x.Item1).ToList();
        head = ordered[0].Item2;
        
        ListNode current = head;
        
        for (int i = 1; i < ordered.Count; ++i)
        {
            ListNode next = ordered[i].Item2;
            current.next = next;
            current = next;    
        }        
        current.next = null;
        return head;
    }
    public ListNode SortList_TO(ListNode head)
    {
        if (head == null || head.next == null) return head;
        
        ListNode? start = null;
        ListNode? latestSorted = null;
                
        ListNode prev;
        ListNode next;
        ListNode? prevOfMin = null;

        ListNode minimum;
        while (head.next != null)
        {
            minimum = head;            

            prev = head;
            next = head.next;

            while (next != null)
            {
                if (next.val < minimum.val) 
                {               
                    prevOfMin = prev;     
                    minimum = next;
                }
                    
                prev = next;
                next = next.next;                                
            }
            if (start == null) 
            {
                start = minimum;
                latestSorted = minimum;
            } 
            else
            {
                latestSorted.next = minimum;
                latestSorted = minimum;
            }
            if (head == minimum)
            {
                head = head.next;
            }
            else if (prevOfMin != null)
            {
                prevOfMin.next = minimum.next;                 
            }
            
            latestSorted.next = null;          
        }

        latestSorted.next = head;

        return start;
    }
    public void Test()
    {
        Test1();
        Test2();
        Test3();

    }
    public void Test1()
    {
        ListNode head = new ListNode(4);
        ListNode node1 = new ListNode(2);
        ListNode node2 = new ListNode(1);
        ListNode node3 = new ListNode(3);

        head.next = node1;
        node1.next = node2;
        node2.next = node3;

        var result = SortList(head);
    }
    public void Test2()
    {
        ListNode head = new ListNode(-1);
        ListNode node1 = new ListNode(5);
        ListNode node2 = new ListNode(3);
        ListNode node3 = new ListNode(4);
        ListNode node4 = new ListNode(0);

        head.next = node1;
        node1.next = node2;
        node2.next = node3;
        node3.next = node4;

        var result = SortList(head);
    }
     public void Test3()
    {
        ListNode head = new ListNode(3);
        ListNode node1 = new ListNode(2);
        ListNode node2 = new ListNode(4);        
       

        head.next = node1;
        node1.next = node2;        
       

        var result = SortList(head);
    }
}

/*
148. Sort List
Medium
Topics
Companies

Given the head of a linked list, return the list after sorting it in ascending order.

 

Example 1:

Input: head = [4,2,1,3]
Output: [1,2,3,4]

Example 2:

Input: head = [-1,5,3,4,0]
Output: [-1,0,3,4,5]

Example 3:

Input: head = []
Output: []

 

Constraints:

    The number of nodes in the list is in the range [0, 5 * 104].
    -105 <= Node.val <= 105

 

Follow up: Can you sort the linked list in O(n logn) time and O(1) memory (i.e. constant space)?

*/