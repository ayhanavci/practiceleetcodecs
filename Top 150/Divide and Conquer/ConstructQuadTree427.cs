namespace Top150;

internal class ConstructQuadTree427
{
    // Definition for a QuadTree node.
    public class Node
    {
        public bool val;
        public bool isLeaf;
        public Node topLeft;
        public Node topRight;
        public Node bottomLeft;
        public Node bottomRight;

        public Node()
        {
            val = false;
            isLeaf = false;
            topLeft = null;
            topRight = null;
            bottomLeft = null;
            bottomRight = null;
        }

        public Node(bool _val, bool _isLeaf)
        {
            val = _val;
            isLeaf = _isLeaf;
            topLeft = null;
            topRight = null;
            bottomLeft = null;
            bottomRight = null;
        }

        public Node(bool _val, bool _isLeaf, Node _topLeft, Node _topRight, Node _bottomLeft, Node _bottomRight)
        {
            val = _val;
            isLeaf = _isLeaf;
            topLeft = _topLeft;
            topRight = _topRight;
            bottomLeft = _bottomLeft;
            bottomRight = _bottomRight;
        }
    }
    int[][] grid;
    public Node Construct(int[][] grid)
    {
        this.grid = grid;

        return CreateLeaf(0, 0, grid.Length);
    }   
    public Node CreateLeaf(int startRow, int startCol, int size)
    {
        if (size == 0) return null;

        Node newLeaf = new Node();

        newLeaf.topLeft = CreateLeaf(startRow, startCol, size / 2);
        newLeaf.topRight = CreateLeaf(startRow, startCol + size / 2, size / 2);
        newLeaf.bottomLeft = CreateLeaf(startRow + size / 2, startCol, size / 2);
        newLeaf.bottomRight = CreateLeaf(startRow + size / 2, startCol + size / 2, size / 2);        
        if (size == 1)
        {
            newLeaf.val = grid[startRow][startCol] == 1;
            newLeaf.isLeaf = true;
        }       
        else if (newLeaf.topLeft.isLeaf && newLeaf.topRight.isLeaf && newLeaf.bottomLeft.isLeaf && newLeaf.bottomRight.isLeaf)
        {   
            newLeaf.val = newLeaf.topLeft.val;
            newLeaf.isLeaf = false;    
             if ((newLeaf.topLeft.val && newLeaf.topRight.val && newLeaf.bottomLeft.val && newLeaf.bottomRight.val) ||
               (!newLeaf.topLeft.val && !newLeaf.topRight.val && !newLeaf.bottomLeft.val && !newLeaf.bottomRight.val))
               {
                    newLeaf.isLeaf = true;    
                    newLeaf.topLeft = null;
                    newLeaf.topRight = null;
                    newLeaf.bottomLeft = null;
                    newLeaf.bottomRight = null;   
               }                 
        }

        return newLeaf;
    }
    public void Test()
    {   
        Test2();
        Test1();
        Test3();
    }
    public void Test1()
    {
        int[][] grid = new int[8][];
        grid[0] = new int[] {1,1,1,1,0,0,0,0};
        grid[1] = new int[] {1,1,1,1,0,0,0,0};
        grid[2] = new int[] {1,1,1,1,1,1,1,1};
        grid[3] = new int[] {1,1,1,1,1,1,1,1};
        grid[4] = new int[] {1,1,1,1,0,0,0,0};
        grid[5] = new int[] {1,1,1,1,0,0,0,0};
        grid[6] = new int[] {1,1,1,1,0,0,0,0};
        grid[7] = new int[] {1,1,1,1,0,0,0,0};

        var result = Construct(grid);        
    }
    public void Test2()
    {
        int[][] grid = new int[2][];
        grid[0] = new int[] {0,1};
        grid[1] = new int[] {1,0};        

        var result = Construct(grid);        
    }
    public void Test3()
    {
        int[][] grid = new int[4][];
        grid[0] = new int[] {1,1,0,0};
        grid[1] = new int[] {0,0,1,1};
        grid[2] = new int[] {1,1,0,0};
        grid[3] = new int[] {0,0,1,1};        

        var result = Construct(grid);        

        //[[0,1],[0,1],[0,1],[0,1],[0,1],[1,1],[1,1],[1,0],[1,0],[1,0],[1,0],[1,1],[1,1],[1,1],[1,1],[1,0],[1,0],[1,0],[1,0],[1,1],[1,1]]
    }
}
/*
427. Construct Quad Tree
Medium
Topics
Companies

Given a n * n matrix grid of 0's and 1's only. We want to represent grid with a Quad-Tree.

Return the root of the Quad-Tree representing grid.

A Quad-Tree is a tree data structure in which each internal node has exactly four children. Besides, each node has two attributes:

    val: True if the node represents a grid of 1's or False if the node represents a grid of 0's. Notice that you can assign the val to True or False when isLeaf is False, and both are accepted in the answer.
    isLeaf: True if the node is a leaf node on the tree or False if the node has four children.

class Node {
    public boolean val;
    public boolean isLeaf;
    public Node topLeft;
    public Node topRight;
    public Node bottomLeft;
    public Node bottomRight;
}

We can construct a Quad-Tree from a two-dimensional area using the following steps:

    If the current grid has the same value (i.e all 1's or all 0's) set isLeaf True and set val to the value of the grid and set the four children to Null and stop.
    If the current grid has different values, set isLeaf to False and set val to any value and divide the current grid into four sub-grids as shown in the photo.
    Recurse for each of the children with the proper sub-grid.

If you want to know more about the Quad-Tree, you can refer to the wiki.

Quad-Tree format:

You don't need to read this section for solving the problem. This is only if you want to understand the output format here. The output represents the serialized format of a Quad-Tree using level order traversal, where null signifies a path terminator where no node exists below.

It is very similar to the serialization of the binary tree. The only difference is that the node is represented as a list [isLeaf, val].

If the value of isLeaf or val is True we represent it as 1 in the list [isLeaf, val] and if the value of isLeaf or val is False we represent it as 0.

 

Example 1:

Input: grid = [[0,1],[1,0]]
Output: [[0,1],[1,0],[1,1],[1,1],[1,0]]
Explanation: The explanation of this example is shown below:
Notice that 0 represents False and 1 represents True in the photo representing the Quad-Tree.

Example 2:

Input: grid = [[1,1,1,1,0,0,0,0],[1,1,1,1,0,0,0,0],[1,1,1,1,1,1,1,1],[1,1,1,1,1,1,1,1],[1,1,1,1,0,0,0,0],[1,1,1,1,0,0,0,0],[1,1,1,1,0,0,0,0],[1,1,1,1,0,0,0,0]]
Output: [[0,1],[1,1],[0,1],[1,1],[1,0],null,null,null,null,[1,0],[1,0],[1,1],[1,1]]
Explanation: All values in the grid are not the same. We divide the grid into four sub-grids.
The topLeft, bottomLeft and bottomRight each has the same value.
The topRight have different values so we divide it into 4 sub-grids where each has the same value.
Explanation is shown in the photo below:

 

Constraints:

    n == grid.length == grid[i].length
    n == 2x where 0 <= x <= 6


*/