namespace Top150;

internal class ConvertSortedArraytoBinarySearchTree108
{
    //Definition for a binary tree node.
    public class TreeNode
    {
        public int val;
        public TreeNode left;
        public TreeNode right;
        public TreeNode(int val = 0, TreeNode left = null, TreeNode right = null)
        {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }

    public TreeNode SortedArrayToBST(int[] nums)
    {
        return SortedArrayToBstPart(nums, 0, nums.Length - 1);
    }
    public TreeNode SortedArrayToBstPart(int [] nums, int start, int end)
    {        
        if (start > end) return null;
        int mid = start + (end - start) / 2;                
        TreeNode node = new TreeNode(nums[mid]);
        if (start == end) return node;
        
        node.left = SortedArrayToBstPart(nums, start, mid - 1);
        node.right = SortedArrayToBstPart(nums, mid + 1, end);
        
        return node;
    }

    public void Test()
    {
        //Test1();
        //Test2();
        Test3();
    }
    public void Test1()
    {
        int[] nums = new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
        var result = SortedArrayToBST(nums);
        //1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15
        //1, 2, 3, 4, 5, 6, 7, [8], 9, 10, 11, 12, 13, 14, 15
        //1, 2, 3, [4], 5, 6, 7, [8], 9, 10, 11, [12], 13, 14, 15
    }
    public void Test2()
    {
        int[] nums = new int[] {-10,-3,0,5,9};
        var result = SortedArrayToBST(nums);     
    }
    public void Test3()
    {
        int[] nums = new int[] {1,3};
        var result = SortedArrayToBST(nums);     
    }
}
/*
108. Convert Sorted Array to Binary Search Tree
Solved
Easy
Topics
Companies

Given an integer array nums where the elements are sorted in ascending order, convert it to a
height-balanced
binary search tree.

 

Example 1:

Input: nums = [-10,-3,0,5,9]
Output: [0,-3,9,-10,null,5]
Explanation: [0,-10,5,null,-3,null,9] is also accepted:

Example 2:

Input: nums = [1,3]
Output: [3,1]
Explanation: [1,null,3] and [3,1] are both height-balanced BSTs.

 

Constraints:

    1 <= nums.length <= 104
    -104 <= nums[i] <= 104
    nums is sorted in a strictly increasing order.


*/