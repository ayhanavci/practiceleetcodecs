namespace Top150;

internal class SpiralMatrix54
{
    public IList<int> SpiralOrder(int[][] matrix) 
    {
        IList<int> retval = new List<int>();

        int rowStart = 0;
        int colStart = 0;
        int rowEnd = matrix.Length - 1;
        int colEnd = matrix[0].Length - 1;

        while (rowEnd >= rowStart && colEnd >= colStart)
        {
            //Go Right
            for (int i = colStart; i <= colEnd; ++i)
            {
                retval.Add(matrix[rowStart][i]);
            }            
            rowStart++;
            
            if (rowStart > rowEnd) break;
            //Go Down
            for (int i = rowStart; i <= rowEnd; ++i)
            {
                retval.Add(matrix[i][colEnd]);
            }            
            colEnd--;                   
            
            if (colStart > colEnd) break;

            //Go Left
            for (int i = colEnd; i >= colStart; --i)
            {
                retval.Add(matrix[rowEnd][i]);
            }
            rowEnd--;
          
            if (rowStart > rowEnd) break;
            //Go Up
            for (int i = rowEnd; i >= rowStart; --i)
            {
                retval.Add(matrix[i][colStart]);
            }
            colStart++;                        

        }

        return retval;
    }
    public void Test()
    {
        int [][]matrix1 = new int[3][];
        matrix1[0] = new int[]{1,2,3};
        matrix1[1] = new int[]{4,5,6};
        matrix1[2] = new int[]{7,8,9};

        var result1 = SpiralOrder(matrix1); //Output: [1,2,3,6,9,8,7,4,5]

        int [][]matrix2 = new int[3][];
        matrix2[0] = new int[]{1,2,3,4};
        matrix2[1] = new int[]{5,6,7,8};
        matrix2[2] = new int[]{9,10,11,12};

        var result2 = SpiralOrder(matrix2); //Output: [1,2,3,4,8,12,11,10,9,5,6,7]

        int [][]matrix3 = new int[10][]; //[[1,11],[2,12],[3,13],[4,14],[5,15],[6,16],[7,17],[8,18],[9,19],[10,20]]
        matrix3[0] = new int[]{1,11};
        matrix3[1] = new int[]{2,12};
        matrix3[2] = new int[]{3,13};
        matrix3[3] = new int[]{4,14};
        matrix3[4] = new int[]{5,15};
        matrix3[5] = new int[]{6,16};
        matrix3[6] = new int[]{7,17};
        matrix3[7] = new int[]{8,18};
        matrix3[8] = new int[]{9,19};
        matrix3[9] = new int[]{10,20};        

        var result3 = SpiralOrder(matrix3); //[1,11,12,13,14,15,16,17,18,19,20,10,9,8,7,6,5,4,3,2,3,4,5,6,7,8,9] [1,11,12,13,14,15,16,17,18,19,20,10,9,8,7,6,5,4,3,2]
    }
}
/*
54. Spiral Matrix
Medium
14K
1.2K
Companies

Given an m x n matrix, return all elements of the matrix in spiral order.

 

Example 1:

Input: matrix = [[1,2,3],[4,5,6],[7,8,9]]
Output: [1,2,3,6,9,8,7,4,5]

Example 2:

Input: matrix = [[1,2,3,4],[5,6,7,8],[9,10,11,12]]
Output: [1,2,3,4,8,12,11,10,9,5,6,7]

 

Constraints:

    m == matrix.length
    n == matrix[i].length
    1 <= m, n <= 10
    -100 <= matrix[i][j] <= 100


*/