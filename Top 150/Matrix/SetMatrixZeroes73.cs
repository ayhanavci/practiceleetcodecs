namespace Top150;

internal class SetMatrixZeroes73
{
    public void SetZeroes(int[][] matrix) 
    {
        HashSet<int> zeroCols = new HashSet<int>();

        int rowCount = matrix.Length;
        int colCount = matrix[0].Length;

        for (int i = 0; i < rowCount; ++i)
        {
            bool rowIsZero = false;
            for (int j = 0; j < colCount; ++j)
            {                
                if (matrix[i][j] == 0)
                {
                    zeroCols.Add(j);       
                    
                    //Just mark the previous ones with 0. If we mark the whole col zero, we can't know if it was a natural 0 or we set it.             
                    for (int prev = j - 1; prev >= 0; --prev)
                        matrix[i][prev] = 0;                    

                    for (int prev = i - 1; prev >= 0; --prev)
                        matrix[prev][j] = 0;                    

                    rowIsZero = true;
                }
                if (rowIsZero || zeroCols.Contains(j))
                    matrix[i][j] = 0;

            }            
        }   
    }
    public void Test()
    {
        //[
        /* 
          [0,0,0,5],
          [4,3,1,4],
          [0,1,1,4],
          [1,2,1,3],
          [0,0,1,1]


          [0,0,0,0],
          [0,0,0,4],
          [0,0,0,0],
          [0,0,0,3],
          [0,0,0,0]

        */

        //O:[[0,0,0,0],[0,0,0,4],[0,0,0,4],[0,0,0,3],[0,0,0,1]]
        //E:[[0,0,0,0],[0,0,0,4],[0,0,0,0],[0,0,0,3],[0,0,0,0]]

        int[][] matrix3 = new int[2][];
        matrix3[0] = new int[]{1};
        matrix3[1] = new int[]{0};
        SetZeroes(matrix3);

        int[][] matrix1 = new int[5][];
        matrix1[0] = new int[]{0,0,0,5};
        matrix1[1] = new int[]{4,3,1,4};
        matrix1[2] = new int[]{0,1,1,4};
        matrix1[3] = new int[]{1,2,1,3};
        matrix1[4] = new int[]{0,0,1,1};
        
        SetZeroes(matrix1);

        int[][] matrix2 = new int[3][];
        matrix2[0] = new int[]{1,1,1};
        matrix2[1] = new int[]{1,0,1};
        matrix2[2] = new int[]{1,1,1};
        
        SetZeroes(matrix2);

    }
}
/*
73. Set Matrix Zeroes
Medium
13.7K
688
Companies

Given an m x n integer matrix matrix, if an element is 0, set its entire row and column to 0's.

You must do it in place.

 

Example 1:

Input: matrix = [[1,1,1],[1,0,1],[1,1,1]]
Output: [[1,0,1],[0,0,0],[1,0,1]]

Example 2:

Input: matrix = [[0,1,2,0],[3,4,5,2],[1,3,1,5]]
Output: [[0,0,0,0],[0,4,5,0],[0,3,1,0]]

 

Constraints:

    m == matrix.length
    n == matrix[0].length
    1 <= m, n <= 200
    -231 <= matrix[i][j] <= 231 - 1

 

Follow up:

    A straightforward solution using O(mn) space is probably a bad idea.
    A simple improvement uses O(m + n) space, but still not the best solution.
    Could you devise a constant space solution?


*/