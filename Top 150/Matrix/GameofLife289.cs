namespace Top150;

internal class GameofLife289
{
    public void GameOfLife(int[][] board) 
    {        
        //0 -> dead to dead
        //-2 -> dead to alive
        //2 -> alive to dead
        //1 -> alive to alive

        int rowCount = board.Length;
        int colCount = board[0].Length;

        for (int row = 0; row < rowCount; ++row)
        {
            for (int col = 0; col < colCount; ++col)
            {            
                int aliveNeighbours = 0;
                int deadNeighbours = 0;
                
                int leftCol = col - 1;
                int rightCol = col + 1;
                int topRow = row - 1;
                int bottomRow = row + 1;

                //Top neighbour                            
                if (topRow >= 0)
                {
                    if (board[topRow][col] <= 0) deadNeighbours++;
                    else aliveNeighbours++;
                }
                //Bottom neighbour                                
                if (bottomRow < rowCount)
                {
                    if (board[bottomRow][col] <= 0) deadNeighbours++;
                    else aliveNeighbours++;
                }        

                //Left neighbours                
                if (leftCol >= 0)
                {
                    //Left neighbour
                    if (board[row][leftCol] <= 0) deadNeighbours++;
                    else aliveNeighbours++;

                    //Topleft neighbour
                    if (topRow >= 0)
                    {
                        if (board[topRow][leftCol] <= 0) deadNeighbours++;
                        else aliveNeighbours++;
                    }
                    //Bottomleft neighbour
                    if (bottomRow < rowCount)
                    {
                        if (board[bottomRow][leftCol] <= 0) deadNeighbours++;
                        else aliveNeighbours++;
                    }
                }

                //Right neighbours                
                if (rightCol < colCount)
                {
                    //Right neighbour
                    if (board[row][rightCol] <= 0) deadNeighbours++;
                    else aliveNeighbours++;

                    //Topright neighbour
                    if (topRow >= 0)
                    {
                        if (board[topRow][rightCol] <= 0) deadNeighbours++;
                        else aliveNeighbours++;
                    }
                    
                    //Bottomright neighbour
                    if (bottomRow < rowCount)
                    {
                        if (board[bottomRow][rightCol] <= 0) deadNeighbours++;
                        else aliveNeighbours++;
                    }
                }    
                if (board[row][col] == 1) //For live cells
                {
                    if (aliveNeighbours < 2 || aliveNeighbours > 3) //dies to under population OR over population
                        board[row][col] = 2; //2 means marking a living cell as dead                    
                    
                }        
                else //For dead cells
                {
                    if (aliveNeighbours == 3) //comes alive due to reproduction
                        board[row][col] = -2; //-2 means marking a dead cell as alive
                }    
            }
        }
        for (int row = 0; row < rowCount; ++row)
        {
            for (int col = 0; col < colCount; ++col)
            {
                if (board[row][col] == -2)
                    board[row][col] = 1;
                else if (board[row][col] == 2)
                    board[row][col] = 0;
            }
        }
    }
    public void Test()
    {
        int[][] board1 = new int[4][];
        board1[0] = new int[] {0,1,0};
        board1[1] = new int[] {0,0,1};
        board1[2] = new int[] {1,1,1};
        board1[3] = new int[] {0,0,0};

        GameOfLife(board1);


        int[][] board2 = new int[2][];
        board2[0] = new int[] {1,1};
        board2[1] = new int[] {1,0};
    

        GameOfLife(board2);
    }
}
/*
289. Game of Life
Medium
6.2K
523
Companies

According to Wikipedia's article: "The Game of Life, also known simply as Life, is a cellular automaton devised by the British mathematician John Horton Conway in 1970."

The board is made up of an m x n grid of cells, where each cell has an initial state: live (represented by a 1) or dead (represented by a 0). Each cell interacts with its eight neighbors (horizontal, vertical, diagonal) using the following four rules (taken from the above Wikipedia article):

    Any live cell with fewer than two live neighbors dies as if caused by under-population.
    Any live cell with two or three live neighbors lives on to the next generation.
    Any live cell with more than three live neighbors dies, as if by over-population.
    Any dead cell with exactly three live neighbors becomes a live cell, as if by reproduction.

The next state is created by applying the above rules simultaneously to every cell in the current state, where births and deaths occur simultaneously. Given the current state of the m x n grid board, return the next state.

 

Example 1:

Input: board = [[0,1,0],[0,0,1],[1,1,1],[0,0,0]]
Output: [[0,0,0],[1,0,1],[0,1,1],[0,1,0]]

Example 2:

Input: board = [[1,1],[1,0]]
Output: [[1,1],[1,1]]

 

Constraints:

    m == board.length
    n == board[i].length
    1 <= m, n <= 25
    board[i][j] is 0 or 1.

 

Follow up:

    Could you solve it in-place? Remember that the board needs to be updated simultaneously: You cannot update some cells first and then use their updated values to update other cells.
    In this question, we represent the board using a 2D array. In principle, the board is infinite, which would cause problems when the active area encroaches upon the border of the array (i.e., live cells reach the border). How would you address these problems?


*/