namespace Top150;

internal class ValidSudoku36
{
    public bool IsValidSudoku(char[][] board) 
    {
        List<HashSet<int>> rows = new List<HashSet<int>>();
        List<HashSet<int>> cols = new List<HashSet<int>>();
        List<HashSet<int>> miniSquares = new List<HashSet<int>>();
        
        
        for (int i = 0; i < 9; ++i)
        {
            rows.Add(new HashSet<int>());
            cols.Add(new HashSet<int>());
            miniSquares.Add(new HashSet<int>());
        }

        for (int i = 0; i < 9; ++i)
        {
            for (int j = 0; j < 9; ++j)
            {
                if (board[i][j] == '.') continue;
                
                if (rows[i].Contains(board[i][j]))                
                    return false;
                
                rows[i].Add(board[i][j]);

                if (cols[j].Contains(board[i][j]))                
                    return false;
                
                cols[j].Add(board[i][j]);

                int squareNumber = 0;

                if (i < 3 && j < 3)
                    squareNumber = 0;
                else if (i < 3 && j > 2 && j < 6)
                    squareNumber = 1;
                else if (i < 3 && j > 5)
                    squareNumber = 2;

                else if (i > 2 && i < 6 && j < 3)
                    squareNumber = 3;
                else if (i > 2 && i < 6 && j > 2 && j < 6)
                    squareNumber = 4;
                else if (i > 2 && i < 6 && j > 5)
                    squareNumber = 5;

                else if (i > 5 && j < 3)
                    squareNumber = 6;
                else if (i > 5 && j > 2 && j < 6)
                    squareNumber = 7;
                else if (i > 5 && j > 5)
                    squareNumber = 8;
                
                if (miniSquares[squareNumber].Contains(board[i][j]))                
                    return false;
                
                miniSquares[squareNumber].Add(board[i][j]);
            }
        }

        return true;
    }
    public void Test()
    {
        char[][] board = new char[9][];
        board[0] = new char[]{'5','3','.','.','7','.','.','.','.'};
        board[1] = new char[]{'6','.','.','1','9','5','.','.','.'};
        board[2] = new char[]{'.','9','8','.','.','.','.','6','.'};
        board[3] = new char[]{'8','.','.','.','6','.','.','.','3'};
        board[4] = new char[]{'4','.','.','8','.','3','.','.','1'};
        board[5] = new char[]{'7','.','.','.','2','.','.','.','6'};
        board[6] = new char[]{'.','6','.','.','.','.','2','8','.'};
        board[7] = new char[]{'.','.','.','4','1','9','.','.','5'};
        board[8] = new char[]{'.','.','.','.','8','.','.','7','9'};

        //var result1 = IsValidSudoku(board);


        char[][] board2 = new char[9][];
        board2[0] = new char[]{'.','.','4','.','.','.','6','3','.'};
        board2[1] = new char[]{'.','.','.','.','.','.','.','.','.'};
        board2[2] = new char[]{'5','.','.','.','.','.','.','9','.'};
        board2[3] = new char[]{'.','.','.','5','6','.','.','.','.'};
        board2[4] = new char[]{'4','.','3','.','.','.','.','.','1'};
        board2[5] = new char[]{'.','.','.','7','.','.','.','.','.'};
        board2[6] = new char[]{'.','.','.','5','.','.','.','.','.'};
        board2[7] = new char[]{'.','.','.','.','.','.','.','.','.'};
        board2[8] = new char[]{'.','.','.','.','.','.','.','.','.'};

        var result2 = IsValidSudoku(board2);

        /*
        
        ['.','.','4','.','.','.','6','3','.'],
        ['.','.','.','.','.','.','.','.','.'],
        ['5','.','.','.','.','.','.','9','.'],
        ['.','.','.','5','6','.','.','.','.'],
        ['4','.','3','.','.','.','.','.','1'],
        ['.','.','.','7','.','.','.','.','.'],
        ['.','.','.','5','.','.','.','.','.'],
        ['.','.','.','.','.','.','.','.','.'],
        ['.','.','.','.','.','.','.','.','.']]
        */
    }
}
/*
36. Valid Sudoku
Medium
10.2K
1.1K
Companies

Determine if a 9 x 9 Sudoku board is valid. Only the filled cells need to be validated according to the following rules:

    Each row must contain the digits 1-9 without repetition.
    Each column must contain the digits 1-9 without repetition.
    Each of the nine 3 x 3 sub-boxes of the grid must contain the digits 1-9 without repetition.

Note:

    A Sudoku board (partially filled) could be valid but is not necessarily solvable.
    Only the filled cells need to be validated according to the mentioned rules.

 

Example 1:

Input: board = 
[['5','3','.','.','7','.','.','.','.']
,['6','.','.','1','9','5','.','.','.']
,['.','9','8','.','.','.','.','6','.']
,['8','.','.','.','6','.','.','.','3']
,['4','.','.','8','.','3','.','.','1']
,['7','.','.','.','2','.','.','.','6']
,['.','6','.','.','.','.','2','8','.']
,['.','.','.','4','1','9','.','.','5']
,['.','.','.','.','8','.','.','7','9']]
Output: true

Example 2:

Input: board = 
[['8','3','.','.','7','.','.','.','.']
,['6','.','.','1','9','5','.','.','.']
,['.','9','8','.','.','.','.','6','.']
,['8','.','.','.','6','.','.','.','3']
,['4','.','.','8','.','3','.','.','1']
,['7','.','.','.','2','.','.','.','6']
,['.','6','.','.','.','.','2','8','.']
,['.','.','.','4','1','9','.','.','5']
,['.','.','.','.','8','.','.','7','9']]
Output: false
Explanation: Same as Example 1, except with the 5 in the top left corner being modified to 8. Since there are two 8's in the top left 3x3 sub-box, it is invalid.

 

Constraints:

    board.length == 9
    board[i].length == 9
    board[i][j] is a digit 1-9 or '.'.


*/