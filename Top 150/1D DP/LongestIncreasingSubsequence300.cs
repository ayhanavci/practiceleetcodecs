namespace Top150;

internal class LongestIncreasingSubsequence300
{
     public int LengthOfLIS(int[] nums) //68%
    {               
        int maxLength = 0;
        int[] maxLengthArray = new int[nums.Length];
        for (int i = nums.Length - 1; i >= 0; --i)
        {           
            maxLengthArray[i] = 1;                          
            for (int j = i + 1; j < nums.Length; ++j)
            {
                if (nums[i] < nums[j])                
                    maxLengthArray[i] = Math.Max(maxLengthArray[j] + 1, maxLengthArray[i]);                
            }
             
            maxLength = Math.Max(maxLengthArray[i], maxLength); 
        }
        return maxLength;
    }
    int[] _nums;
    public int LengthOfLIS2(int[] nums) //5%
    {        
        _nums = nums;
        memo = new Dictionary<int, int>();
        int maxLength = 0;
        for (int i = 0; i < nums.Length; ++i)
        {                        
             maxLength = Math.Max(maxLength, Traverse(i));
        }
        return maxLength;
    }
    Dictionary<int, int> memo;
    public int Traverse(int curIndex)
    {       
        if (memo.ContainsKey(curIndex)) return memo[curIndex];
        int maxLength = 1;
        
        for (int i = curIndex + 1; i < _nums.Length; ++i)
        {
            if (_nums[i] > _nums[curIndex])
            {
                int length = Traverse(i);
                if (length > 0)                
                    maxLength = Math.Max(maxLength, length + 1);                
            }
            
        }
        memo[curIndex] = maxLength;
        return maxLength;
    }
   
    public void Test()
    {
        Test1();
        Test2();
        Test3();
    }
    public void Test1()
    {
        int [] nums = new int[] {10,9,2,5,3,7,101,18};
        var result = LengthOfLIS(nums);//[2,3,7,101]
        //10,9,2,5,3,7,101,18
        //10,9,2,4,5,3,7,101,18
    }
    public void Test2()
    {
        int [] nums = new int[] {0,1,0,3,2,3};
        var result = LengthOfLIS(nums);       
    }
    public void Test3()
    {
        int [] nums = new int[] {7,7,7,7,7,7,7};
        var result = LengthOfLIS(nums);       
    }
}
/*
lass Solution {
    public int lengthOfLIS(int[] nums) {
        // Initialize an array dp with the same length as nums and fill it with 1s
        int[] dp = new int[nums.length];
        Arrays.fill(dp, 1);

        // Iterate through each element in nums
        for (int i = 1; i < nums.length; i++) {
            // Iterate through all previous elements
            for (int j = 0; j < i; j++) {
                // If nums[j] < nums[i], update dp[i]
                if (nums[j] < nums[i]) {
                    dp[i] = Math.max(dp[j] + 1, dp[i]);
                }
            }
        }

        // Find the maximum value in the dp array
        int maxLength = 0;
        for (int len : dp) {
            maxLength = Math.max(maxLength, len);
        }

        // Return the maximum value
        return maxLength;
    }
}

Example

Given the input nums = [10,9,2,5,3,7,101,18]:

    Initialize dp = [1, 1, 1, 1, 1, 1, 1, 1].
    Update dp based on the LIS ending at each index.
    After the iteration, dp = [1, 1, 1, 2, 2, 3, 4, 4].
    The maximum value in dp is 4, representing the length of the LIS.
    Return 4 as the output.

*/
/*
300. Longest Increasing Subsequence
Medium
Topics
Companies

Given an integer array nums, return the length of the longest strictly increasing
subsequence
.

 

Example 1:

Input: nums = [10,9,2,5,3,7,101,18]
Output: 4
Explanation: The longest increasing subsequence is [2,3,7,101], therefore the length is 4.

Example 2:

Input: nums = [0,1,0,3,2,3]
Output: 4

Example 3:

Input: nums = [7,7,7,7,7,7,7]
Output: 1

 

Constraints:

    1 <= nums.length <= 2500
    -104 <= nums[i] <= 104

 

Follow up: Can you come up with an algorithm that runs in O(n log(n)) time complexity?

*/