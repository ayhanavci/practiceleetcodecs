namespace Top150;

internal class CoinChange322
{    
    Dictionary<int, int> memo = new Dictionary<int, int>();
    public int CoinChange(int[] coins, int amount) 
    {
       if (amount == 0) return 0;
       if (memo.ContainsKey(amount)) 
            return memo[amount];
        if (amount < 0) 
            return -1;
        if (amount == 0) 
            return 0;

        int shortest = -1;

        foreach (var num in coins)
        {   
            int remainder = amount - num;
            var result = CoinChange(coins, remainder);
            if (result != -1)
            {
                result++;
                if (shortest == -1)
                {
                    shortest = result;
                }
                else 
                {                      
                    if (result < shortest)                                     
                        shortest = result;
                }
            }
        }           

        memo.Add(amount, shortest);
        return shortest;
    }
    
   
    public void Test()
    {        
        Test1();
        Test2();
        Test3();
        Test4();
    }
    public void Test1()
    {
        memo = new Dictionary<int, int>();
        int[] coins = new int[] {1,2,5};
        var result = CoinChange(coins, 11); //3
    }
    public void Test2()
    {
        memo = new Dictionary<int, int>();
        int[] coins = new int[] {2};
        var result = CoinChange(coins, 3);//-1
    }
    public void Test3()
    {
        memo = new Dictionary<int, int>();
        int[] coins = new int[] {1};
        var result = CoinChange(coins, 0);//0
    }
    public void Test4()
    {
        memo = new Dictionary<int, int>();
        int[] coins = new int[] {1, 3, 7, 4, 8};
        var result = CoinChange(coins, 12);
    }
}

/*
322. Coin Change
Medium
Topics
Companies

You are given an integer array coins representing coins of different denominations and an integer amount representing a total amount of money.

Return the fewest number of coins that you need to make up that amount. If that amount of money cannot be made up by any combination of the coins, return -1.

You may assume that you have an infinite number of each kind of coin.

 

Example 1:

Input: coins = [1,2,5], amount = 11
Output: 3
Explanation: 11 = 5 + 5 + 1

Example 2:

Input: coins = [2], amount = 3
Output: -1

Example 3:

Input: coins = [1], amount = 0
Output: 0

 

Constraints:

    1 <= coins.length <= 12
    1 <= coins[i] <= 231 - 1
    0 <= amount <= 104


*/