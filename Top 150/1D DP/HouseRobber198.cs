namespace Top150;

internal class HouseRobber198
{
    public int Rob(int[] nums) 
    {
        int[] maxValues = new int[nums.Length];

        maxValues[0] = nums[0];
        if (nums.Length > 1)
            maxValues[1] = Math.Max(nums[0], nums[1]);        

        for (int i = 2; i < nums.Length; ++i)        
            maxValues[i] = Math.Max(maxValues[i-1], maxValues[i-2] + nums[i]);        

        return maxValues[nums.Length-1];
    }

    public int Rob_TO(int[] nums) 
    {
        int maxValue = int.MinValue;

        int value = GetCombinations(0, nums[0], nums);
        maxValue = Math.Max(value, maxValue);

        if (nums.Length > 1)
        {
            value = GetCombinations(1, nums[1], nums);
            maxValue = Math.Max(value, maxValue);
        }        

        return maxValue;
    }    
    public int GetCombinations(int lastIndex, int sum, int[] nums)
    {
        int maxValue = sum;           
        
        if (lastIndex + 2 < nums.Length)
        {                        
            int value = GetCombinations(lastIndex + 2, sum + nums[lastIndex + 2], nums);
            maxValue = Math.Max(value, maxValue);
        }
        
        if (lastIndex + 3 < nums.Length)
        {            
            int value = GetCombinations(lastIndex + 3, sum + nums[lastIndex + 3], nums);
            maxValue = Math.Max(value, maxValue);
        }
        
        return maxValue;
    }
    public void Test()
    {
        var result1 = Rob(new int[] {1,2,3,1});
        var result2 = Rob(new int[] {2,7,9,3,1});
        var result3 = Rob(new int[] {2,1,1,2});
        var result4 = Rob(new int[] {183,219,57,193,94,233,202,154,65,240,97,234,100,249,186,66,90,238,168,128,177,235,50,81,185,165,217,207,88,80,112,78,135,62,228,247,211});
        var result5 = Rob(new int[] {226,174,214,16,218,48,153,131,128,17,157,142,88,43,37,157,43,221,191,68,206,23,225,82,54,118,111,46,80,49,245,63,25,194,72,80,143,55,209,18,55,122,65,66,177,101,63,201,172,130,103,225,142,46,86,185,62,138,212,192,125,77,223,188,99,228,90,25,193,211,84,239,119,234,85,83,123,120,131,203,219,10,82,35,120,180,249,106,37,169,225,54,103,55,166,124});
        
    }
}
/*
198. House Robber
Medium
Topics
Companies

You are a professional robber planning to rob houses along a street. Each house has a certain amount of money stashed, the only constraint stopping you from robbing each of them is that adjacent houses have security systems connected and it will automatically contact the police if two adjacent houses were broken into on the same night.

Given an integer array nums representing the amount of money of each house, return the maximum amount of money you can rob tonight without alerting the police.

 

Example 1:

Input: nums = [1,2,3,1]
Output: 4
Explanation: Rob house 1 (money = 1) and then rob house 3 (money = 3).
Total amount you can rob = 1 + 3 = 4.

Example 2:

Input: nums = [2,7,9,3,1]
Output: 12
Explanation: Rob house 1 (money = 2), rob house 3 (money = 9) and rob house 5 (money = 1).
Total amount you can rob = 2 + 9 + 1 = 12.

 

Constraints:

    1 <= nums.length <= 100
    0 <= nums[i] <= 400


*/