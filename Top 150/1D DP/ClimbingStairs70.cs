namespace Top150;

internal class ClimbingStairs70
{
    Dictionary<int, int> memo = new Dictionary<int, int>();
    public int ClimbStairs(int n) 
    {
        if (n == 0) return 1;
        if (n == -1) return 0;
        if (memo.ContainsKey(n)) return memo[n];

        int ways = ClimbStairs(n - 1);
        ways += ClimbStairs(n - 2);
        memo.Add(n, ways);
        return ways;
    }
    public void Test()
    {
        var result1 = ClimbStairs(3);
        var result2 = ClimbStairs(2);
        var result3 = ClimbStairs(5);
    }
    
}
/*
70. Climbing Stairs
Solved
Easy
Topics
Companies
Hint

You are climbing a staircase. It takes n steps to reach the top.

Each time you can either climb 1 or 2 steps. In how many distinct ways can you climb to the top?

 

Example 1:

Input: n = 2
Output: 2
Explanation: There are two ways to climb to the top.
1. 1 step + 1 step
2. 2 steps

Example 2:

Input: n = 3
Output: 3
Explanation: There are three ways to climb to the top.
1. 1 step + 1 step + 1 step
2. 1 step + 2 steps
3. 2 steps + 1 step

 

Constraints:

    1 <= n <= 45


*/