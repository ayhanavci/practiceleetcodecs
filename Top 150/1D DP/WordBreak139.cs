namespace Top150;

internal class WordBreak139
{
    Dictionary<string, bool> memo = new Dictionary<string, bool>();
    public bool WordBreak(string s, IList<string> wordDict) 
    {
        if (s == string.Empty) return true;
        if (memo.ContainsKey(s)) return memo[s];

        foreach (var word in wordDict)
        {
            int startIndex = s.IndexOf(word);
            if (startIndex == 0)
            {
                bool result = WordBreak(s.Substring(word.Length), wordDict);
                
                if (result)
                {
                    memo.Add(s, true);
                    return true;
                } 
            }
        }
        memo.Add(s, false);
        return false;
    }
    public void Test()
    {
        //Test1();
        //Test2();
        Test3();
    }
    public void Test1()
    {
        List<string> wordDict = new List<string>()
        {
            "leet",
            "code"
        };
        
        var result = WordBreak("leetcode", wordDict);
    }
    public void Test2()
    {
        List<string> wordDict = new List<string>()
        {
            "apple",
            "pen"
        };
        
        var result = WordBreak("applepenapple", wordDict);
    }
    public void Test3()
    {
        List<string> wordDict = new List<string>()
        {
            "cats",
            "dog",
            "sand",
            "and",
            "cat"
        };
        
        var result = WordBreak("catsandog", wordDict);
    }
}

/*
139. Word Break
Medium
Topics
Companies

Given a string s and a dictionary of strings wordDict, return true if s can be segmented into a space-separated sequence of one or more dictionary words.

Note that the same word in the dictionary may be reused multiple times in the segmentation.

 

Example 1:

Input: s = "leetcode", wordDict = ["leet","code"]
Output: true
Explanation: Return true because "leetcode" can be segmented as "leet code".

Example 2:

Input: s = "applepenapple", wordDict = ["apple","pen"]
Output: true
Explanation: Return true because "applepenapple" can be segmented as "apple pen apple".
Note that you are allowed to reuse a dictionary word.

Example 3:

Input: s = "catsandog", wordDict = ["cats","dog","sand","and","cat"]
Output: false

 

Constraints:

    1 <= s.length <= 300
    1 <= wordDict.length <= 1000
    1 <= wordDict[i].length <= 20
    s and wordDict[i] consist of only lowercase English letters.
    All the strings of wordDict are unique.


*/