namespace Top150;

internal class Searcha2DMatrix74
{
    public bool SearchMatrix(int[][] matrix, int target) 
    {
        int rowCount = matrix.Length;
        int colCount = matrix[0].Length;

        if (target > matrix[rowCount - 1][colCount - 1]) return false;
        if (target < matrix[0][0]) return false;

        int row;
        
        int start = 0;
        int end = rowCount - 1;
        
        int mid;
        while (start < end)
        {
            mid = start + (end - start) / 2;
            if (matrix[mid][colCount-1] == target)
                return true;
            if (matrix[mid][colCount-1] > target) 
                end = mid;
            else 
                start = mid + 1;
        }
        row = start;
        
        start = 0;
        end = colCount - 1;
                
        while (start < end)
        {
            mid = start + (end - start) / 2;
            if (matrix[row][mid] == target)
                return true;
            if (matrix[row][mid] > target) 
                end = mid;
            else 
                start = mid + 1;
        }
        int col = start;
        return matrix[row][col] == target;
    }
    public void Test()
    {
        Test1();
        Test2();
    }
    public void Test1()
    {
        int[][] matrix = new int[3][];
        matrix[0] = new int[] {1,3,5,7};
        matrix[1] = new int[] {10,11,16,20};
        matrix[2] = new int[] {23,30,34,60};

        var result = SearchMatrix(matrix, 13);
        var result1 = SearchMatrix(matrix, 16);
        var result2 = SearchMatrix(matrix, 34);
        var result3 = SearchMatrix(matrix, 60);
    }
     public void Test2()
    {
        int[][] matrix = new int[3][];
        matrix[0] = new int[] {1,3,5,7};
        matrix[1] = new int[] {10,11,16,20};
        matrix[2] = new int[] {23,30,34,60};

        var result = SearchMatrix(matrix, 13);
    }
     public void Test3()
    {
        int[][] matrix = new int[3][];
        matrix[0] = new int[] {};
        matrix[1] = new int[] {};
        matrix[2] = new int[] {};

        var result = SearchMatrix(matrix, 3);
    }
}

/*
74. Search a 2D Matrix
Medium
Topics
Companies

You are given an m x n integer matrix matrix with the following two properties:

    Each row is sorted in non-decreasing order.
    The first integer of each row is greater than the last integer of the previous row.

Given an integer target, return true if target is in matrix or false otherwise.

You must write a solution in O(log(m * n)) time complexity.

 

Example 1:

Input: matrix = [[1,3,5,7],[10,11,16,20],[23,30,34,60]], target = 3
Output: true

Example 2:

Input: matrix = [[1,3,5,7],[10,11,16,20],[23,30,34,60]], target = 13
Output: false

 

Constraints:

    m == matrix.length
    n == matrix[i].length
    1 <= m, n <= 100
    -104 <= matrix[i][j], target <= 104


*/