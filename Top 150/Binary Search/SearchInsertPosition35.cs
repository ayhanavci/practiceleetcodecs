namespace Top150;

internal class SearchInsertPosition35
{
    public int SearchInsert(int[] nums, int target) 
    {
        int start = 0;
        int end = nums.Length - 1;

        if (target < nums[0]) return 0;
        if (target > nums[end]) return end + 1;

        while (end >= start)
        {
            int mid = start + (end - start) / 2;
            if (nums[mid] == target) return mid;
            if (nums[mid] > target) end = mid - 1;
            else if (nums[mid] < target) start = mid + 1;
        }
        return start;
    }
    public void Test()
    {
        Test1();
        //Test2();
        //Test3();
        //Test4();
    }
    public void Test1()
    {
        int [] nums = new int[] {1,3,5,6};
        var result = SearchInsert(nums, 0);
        var result1 = SearchInsert(nums, 1);
        var result2 = SearchInsert(nums, 2);
        var result3 = SearchInsert(nums, 3);
        var result4 = SearchInsert(nums, 4);
        var result5 = SearchInsert(nums, 5);
        var result6 = SearchInsert(nums, 6);
        var result7 = SearchInsert(nums, 7);
    }
    public void Test2()
    {
        int [] nums = new int[] {1,3,5,6};
        var result = SearchInsert(nums, 2);//1
    }
    public void Test3()
    {
        int [] nums = new int[] {1,3,5,6};
        var result = SearchInsert(nums, 7);//4
    }
    public void Test4()
    {
         int [] nums = new int[] {4,5,6,7,0,1,2};
        var result = SearchInsert(nums, 5);//1
    }
}
/*
35. Search Insert Position
Solved
Easy
Topics
Companies

Given a sorted array of distinct integers and a target value, return the index if the target is found. If not, return the index where it would be if it were inserted in order.

You must write an algorithm with O(log n) runtime complexity.

 

Example 1:

Input: nums = [1,3,5,6], target = 5
Output: 2

Example 2:

Input: nums = [1,3,5,6], target = 2
Output: 1

Example 3:

Input: nums = [1,3,5,6], target = 7
Output: 4

 

Constraints:

    1 <= nums.length <= 104
    -104 <= nums[i] <= 104
    nums contains distinct values sorted in ascending order.
    -104 <= target <= 104


*/