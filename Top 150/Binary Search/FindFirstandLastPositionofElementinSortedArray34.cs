namespace Top150;

internal class FindFirstandLastPositionofElementinSortedArray34
{
    int[] nums;
    int target;
    public int[] SearchRange(int[] nums, int target) 
    {
        if (nums.Length == 0) return new int[2]{-1, -1};
        this.nums = nums;
        this.target = target;
        return HalfSearch(0, nums.Length - 1);
    }
    public int[] HalfSearch(int start, int end)
    {
        int[] range = new int[2]{-1, -1};
        if (nums[start] > target || nums[end] < target) return range;        
        
        int mid = (start + end) / 2;
            
        if (nums[start] == target)        
            range[0] = start;
        
        if (nums[end] == target)        
            range[1] = end;
        
        if (range[0] != -1 && range[1] != -1)
            return range;

        var leftRange = HalfSearch(start, mid);
        var rightRange = HalfSearch(mid + 1, end);

        if (range[0] == -1)
        {
            if (leftRange[0] != -1)
                range[0] = leftRange[0];
            else if (leftRange[1] != -1)
                range[0] = leftRange[1];
            else if (rightRange[0] != -1)
                range[0] = rightRange[0];
            else
                range[0] = rightRange[1];
        }      
        if (range[1] == -1)
        {
            if (rightRange[1] != -1)
                range[1] = rightRange[1];
            else if (rightRange[0] != -1)
                range[1] = rightRange[0];
            else if (leftRange[1] != -1)
                range[1] = leftRange[1];
            else
                range[1] = leftRange[0];
        }     
        
        return range;
    }
    public void Test()
    {
        Test1();
        Test2();
        Test3();
        Test4();
    }
    public void Test1()
    {
        int[] nums = new int[] {5,7,7,8,8,10};
        var result = SearchRange(nums, 8);//[3,4]
    }
    public void Test2()
    {
        int[] nums = new int[] {5,7,7,8,8,10};
        var result = SearchRange(nums, 6);//[-1,-1]
    }
    public void Test3()
    {
        int[] nums = new int[] {};
        var result = SearchRange(nums, 0);
    }
    public void Test4()
    {
        int[] nums = new int[] {5};
        var result = SearchRange(nums, 5);
    }
}

/*
34. Find First and Last Position of Element in Sorted Array
Medium
Topics
Companies

Given an array of integers nums sorted in non-decreasing order, find the starting and ending position of a given target value.

If target is not found in the array, return [-1, -1].

You must write an algorithm with O(log n) runtime complexity.

 

Example 1:

Input: nums = [5,7,7,8,8,10], target = 8
Output: [3,4]

Example 2:

Input: nums = [5,7,7,8,8,10], target = 6
Output: [-1,-1]

Example 3:

Input: nums = [], target = 0
Output: [-1,-1]

 

Constraints:

    0 <= nums.length <= 105
    -109 <= nums[i] <= 109
    nums is a non-decreasing array.
    -109 <= target <= 109


*/