namespace Top150;

internal class SearchinRotatedSortedArray33
{
    int[] nums;
    int target;
    public int Search(int[] nums, int target) 
    {
        this.target = target;
        this.nums = nums;
        
        return SearchHalf(0, nums.Length - 1);
    }
    public int SearchHalf(int start, int end)
    {       
        if (start == end) return nums[start] == target ? start : -1; //Array of 1 number

        if (nums[end] > nums[start]) //ordered
        {
            if (target > nums[end] || target < nums[start])
                return -1;
        }
        
        int mid = (start + end) / 2;

        int left = SearchHalf(start, mid);
        if (left != -1) return left;

        int right = SearchHalf(mid + 1, end);
        if (right != -1) return right;

        return -1;

    }
    //if (target > nums[end] && target < nums[start]) return -1;
    public void Test()
    {
        Test1();
        Test2();
        Test3();
        Test4();
    }
    public void Test1()
    {
        int [] nums = new int[] {4,5,6,7,0,1,2};
        var result = Search(nums, 0);//4
    }
    public void Test2()
    {
        int [] nums = new int[] {4,5,6,7,0,1,2};
        var result = Search(nums, 3);//-1
    }
    public void Test3()
    {
        int [] nums = new int[] {1};
        var result = Search(nums, 0);//-1
    }
    public void Test4()
    {
         int [] nums = new int[] {4,5,6,7,0,1,2};
        var result = Search(nums, 5);//1
    }
}

/*
33. Search in Rotated Sorted Array
Medium
Topics
Companies

There is an integer array nums sorted in ascending order (with distinct values).

Prior to being passed to your function, nums is possibly rotated at an unknown pivot index k (1 <= k < nums.length) such that the resulting array is [nums[k], nums[k+1], ..., nums[n-1], nums[0], nums[1], ..., nums[k-1]] (0-indexed). For example, [0,1,2,4,5,6,7] might be rotated at pivot index 3 and become [4,5,6,7,0,1,2].

Given the array nums after the possible rotation and an integer target, return the index of target if it is in nums, or -1 if it is not in nums.

You must write an algorithm with O(log n) runtime complexity.

 

Example 1:

Input: nums = [4,5,6,7,0,1,2], target = 0
Output: 4

Example 2:

Input: nums = [4,5,6,7,0,1,2], target = 3
Output: -1

Example 3:

Input: nums = [1], target = 0
Output: -1

 

Constraints:

    1 <= nums.length <= 5000
    -104 <= nums[i] <= 104
    All values of nums are unique.
    nums is an ascending array that is possibly rotated.
    -104 <= target <= 104


*/