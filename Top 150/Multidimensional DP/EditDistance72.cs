namespace Top150;

internal class EditDistance72
{
    public int MinDistance(string word1, string word2) 
    {
        if (word1.Equals(word2)) return 0;
        if (word1.Length == 0) return word2.Length;
        if (word2.Length == 0) return word1.Length;

        int [][] dp = new int[word1.Length + 1][];
        for (int i = 0; i < word1.Length + 1; ++i)
        {
            dp[i] = new int[word2.Length + 1];
            Array.Fill(dp[i], int.MaxValue);
        }
        for (int i = 0; i < word2.Length + 1; ++i)        
            dp[word1.Length][i] = word2.Length - i;
        
        for (int i = 0; i < word1.Length + 1; ++i)        
            dp[i][word2.Length] = word1.Length - i;

        for (int i = word1.Length - 1; i >= 0; --i)
        {
            for (int j = word2.Length - 1; j >= 0; --j)
            {
                if (word1[i] == word2[j])
                    dp[i][j] = dp[i + 1][j + 1];
                else
                {
                    dp[i][j] = Math.Min(dp[i + 1][j], dp[i][j + 1]);//Min of: Insert or Delete
                    dp[i][j] = 1 + Math.Min(dp[i][j], dp[i + 1][j + 1]);//Min of Insert or Delete or Replace
                }
            }
        }
        return dp[0][0];
    }
    int minOperation;
    HashSet<Tuple<int, int>> memo;
    string word1, word2;
    public int MinDistance_BRUTE(string word1, string word2) 
    {  
        if (word1.Equals(word2)) return 0;
        if (word1.Length == 0) return word2.Length;
        if (word2.Length == 0) return word1.Length;

        this.word1 = word1;
        this.word2   = word2;
        minOperation = int.MaxValue;     
        memo = new HashSet<Tuple<int, int>>();   
        
        
        Dig_BRUTE(0, 0, 0);

        return minOperation;
    }
    
    public void Dig_BRUTE(int i, int j, int count)
    {        
        if (i == word1.Length && j == word2.Length) 
        {            
            minOperation = Math.Min(minOperation, count);
            return;
        }
        if (i == word1.Length)
        {          
            count += word2.Length - j;  
            minOperation = Math.Min(minOperation, count);
            return;
        }
        if (j == word2.Length)
        {
            count += word1.Length - i;  
            minOperation = Math.Min(minOperation, count);
            return;
        }
            
        if (word1[i] == word2[j]) //Do nothing
            Dig_BRUTE(i + 1, j + 1, count);
        else 
        {
            Dig_BRUTE(i, j + 1, count + 1); //Insert
            Dig_BRUTE(i + 1, j, count + 1); //Delete            
            Dig_BRUTE(i + 1, j + 1, count + 1); //Replace
        } 
    }
    public void Test()
    {
        var result1 = MinDistance("horse", "ros");
        var result2 = MinDistance("intention", "execution");
    }
}
/*
72. Edit Distance
Medium
Topics
Companies

Given two strings word1 and word2, return the minimum number of operations required to convert word1 to word2.

You have the following three operations permitted on a word:

    Insert a character
    Delete a character
    Replace a character

 

Example 1:

Input: word1 = "horse", word2 = "ros"
Output: 3
Explanation: 
horse -> rorse (replace 'h' with 'r')
rorse -> rose (remove 'r')
rose -> ros (remove 'e')

Example 2:

Input: word1 = "intention", word2 = "execution"
Output: 5
Explanation: 
intention -> inention (remove 't')
inention -> enention (replace 'i' with 'e')
enention -> exention (replace 'n' with 'x')
exention -> exection (replace 'n' with 'c')
exection -> execution (insert 'u')

 

Constraints:

    0 <= word1.length, word2.length <= 500
    word1 and word2 consist of lowercase English letters.


*/