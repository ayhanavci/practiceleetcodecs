namespace Top150;

internal class MaximalSquare221
{
    Dictionary<Tuple<int, int>, int> memo;
    char[][] matrix;
    int maxLength = 0;
    public int MaximalSquare(char[][] matrix) 
    {
        this.matrix = matrix;
        memo = new Dictionary<Tuple<int, int>, int>();
        Traverse(0, 0);
        return maxLength * maxLength;
    }  
    public int Traverse(int i, int j)
    {
        if (i >= matrix.Length || j >= matrix[0].Length)
            return 0;
        
        Tuple<int, int> key = new Tuple<int, int>(i, j);
        int value = 0;
        if (memo.TryGetValue(key, out value))
            return value;
        
        int down = Traverse(i + 1, j);
        int right = Traverse(i, j + 1);
        int diag = Traverse(i + 1, j + 1);

        if (matrix[i][j] == '1')
        {
            value = Math.Min(down, right);
            value = Math.Min(value, diag) + 1;        
            maxLength = Math.Max(value, maxLength);    
        }
        memo.Add(key, value);
        return value;
    }

    public void Test()
    {
        Test1();
    }
    public void Test1()
    {        
        char[][] matrix = new char[4][];
        matrix[0] = new char[]{'1','0','1','0','0'};
        matrix[1] = new char[]{'1','0','1','1','1'};
        matrix[2] = new char[]{'1','1','1','1','1'};
        matrix[3] = new char[]{'1','0','0','1','0'};

        var result = MaximalSquare(matrix);
    }
}
/*
221. Maximal Square
Medium
Topics
Companies

Given an m x n binary matrix filled with 0's and 1's, find the largest square containing only 1's and return its area.

 

Example 1:

Input: matrix = [["1","0","1","0","0"],["1","0","1","1","1"],["1","1","1","1","1"],["1","0","0","1","0"]]
Output: 4

Example 2:

Input: matrix = [["0","1"],["1","0"]]
Output: 1

Example 3:

Input: matrix = [["0"]]
Output: 0

 

Constraints:

    m == matrix.length
    n == matrix[i].length
    1 <= m, n <= 300
    matrix[i][j] is '0' or '1'.


*/