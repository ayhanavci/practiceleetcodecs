namespace Top150;

internal class BestTimetoBuyandSellStockIV188
{    
   
    
    public int MaxProfit(int k, int[] prices) //State Machine approach
    {
        int length = prices.Length;

        //case 1 - can't sell
        if (k == 0 || length <= 1)
            return 0;
        
        //case 2 - unlimited purchase and buy
        if (2 * k > length)
        {
            int profit = 0;
            for (int i = 1; i < length; ++i)            
                profit += Math.Max(0, prices[i] - prices[i - 1]);                            
            return profit;
        }

        int []profits = new int[2 * k + 1];
        
        profits[0] = -prices[0];
        for (int i = 1; i < 2 * k; ++i)                    
            profits[i] = i % 2 == 0 ? int.MinValue : 0; //Even: buy position. Odd: sell position
        
        for (int i = 0; i < length; ++i)
        {
            profits[0] = Math.Max(profits[0], -prices[i]);
            for (int j = 1; j < 2 * k; ++j)
            {                
                if (j % 2 == 0) //BUY
                    profits[j] = Math.Max(profits[j], profits[j - 1] - prices[i]);
                else //SELL
                    profits[j] = Math.Max(profits[j], profits[j - 1] + prices[i]);
            }       
        }
        return profits[2 * k - 1];
    }
    
    public void Test()
    {
        Test1();
    }
    public void Test1()
    {
        int[] prices = {3,2,6,5,0,3};
        var result = MaxProfit(2, prices);
    }
}
/*
//Memoization
class Solution {
     
    //    pos->current position
    //    t->transactions done
    //    bought->If current stock is bought
    
    vector<vector<vector<int>>> mem;
    int recursion(vector<int>& prices,int pos,int t,bool bought)
    {
        if(pos>=prices.size() || t==0)      //Out of bounds case
            return 0;
        if(mem[bought][t][pos]!=-1)     //Return if already calculated
            return mem[bought][t][pos];
        
        //3 choices for a position-->Buy/Sell/Skip
        int result = recursion(prices,pos+1,t,bought);    //SKIP
        if(bought)
            result = max(result,recursion(prices,pos+1,t-1,false)+prices[pos]);   //SELL
        else
            result = max(result,recursion(prices,pos+1,t,true)-prices[pos]);    //BUY
        
        mem[bought][t][pos] = result;
        return result;
    }
public:
    int maxProfit(vector<int>& prices) {
        mem.resize(2,vector<vector<int>>(3,vector<int>(prices.size(),-1)));
        int res = recursion(prices,0,2,false);
        return res;
    }
};
*/
/*
188. Best Time to Buy and Sell Stock IV
Hard
Topics
Companies

You are given an integer array prices where prices[i] is the price of a given stock on the ith day, and an integer k.

Find the maximum profit you can achieve. You may complete at most k transactions: i.e. you may buy at most k times and sell at most k times.

Note: You may not engage in multiple transactions simultaneously (i.e., you must sell the stock before you buy again).

 

Example 1:

Input: k = 2, prices = [2,4,1]
Output: 2
Explanation: Buy on day 1 (price = 2) and sell on day 2 (price = 4), profit = 4-2 = 2.

Example 2:

Input: k = 2, prices = [3,2,6,5,0,3]
Output: 7
Explanation: Buy on day 2 (price = 2) and sell on day 3 (price = 6), profit = 6-2 = 4. Then buy on day 5 (price = 0) and sell on day 6 (price = 3), profit = 3-0 = 3.

 

Constraints:

    1 <= k <= 100
    1 <= prices.length <= 1000
    0 <= prices[i] <= 1000


*/