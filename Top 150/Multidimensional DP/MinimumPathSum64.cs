namespace Top150;

internal class MinimumPathSum64
{
    public int MinPathSum(int[][] grid) 
    {
        int rows = grid.Length;
        int cols = grid[0].Length;

        for (int i = rows - 1; i >= 0; --i)
        {
            for (int j = cols - 1; j >= 0; --j)
            {                
                int bottom = i + 1 < rows ? grid[i+1][j] : int.MaxValue;
                int left = j + 1 < cols ? grid[i][j+1] : int.MaxValue;                

                if (bottom == left && bottom == int.MaxValue)
                    bottom = 0;
                grid[i][j] = Math.Min(bottom, left) + grid[i][j];
            }
        }
        return grid[0][0];
    }

    public void Test()
    {
        Test1();
    }
    public void Test1()
    {
        int[][] grid = new int[3][];
        grid[0] = new int[]{1,3,1};
        grid[1] = new int[]{1,5,1};
        grid[2] = new int[]{4,2,1};

        var result = MinPathSum(grid);
    }
}

/*
64. Minimum Path Sum
Medium
Topics
Companies

Given a m x n grid filled with non-negative numbers, find a path from top left to bottom right, which minimizes the sum of all numbers along its path.

Note: You can only move either down or right at any point in time.

 

Example 1:

Input: grid = [[1,3,1],[1,5,1],[4,2,1]]
Output: 7
Explanation: Because the path 1 → 3 → 1 → 1 → 1 minimizes the sum.

Example 2:

Input: grid = [[1,2,3],[4,5,6]]
Output: 12

 

Constraints:

    m == grid.length
    n == grid[i].length
    1 <= m, n <= 200
    0 <= grid[i][j] <= 200


*/