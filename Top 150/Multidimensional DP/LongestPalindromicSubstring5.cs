namespace Top150;

internal class LongestPalindromicSubstring5
{
    
    //TODO: Slow 29%
    Dictionary<string, bool> memo = new Dictionary<string, bool>();
    
    public string LongestPalindrome(string s) 
    {    
        string longest = "";
        int longestLen = 1;
        longest += s[0];        

        for (int i = 0; i < s.Length - 1; ++i)
        {   
            int nextMinPos = i + longestLen;
            if (nextMinPos >= s.Length)
                break;
            for (int j = nextMinPos; j < s.Length; ++j)
            {
                if (s[i] == s[j])
                {
                    string substring = s.Substring(i, j - i + 1);//abcdefgh
                    if (IsPalindrome(substring))
                    {
                        longestLen = substring.Length;
                        longest = substring;
                    }

                }
            }
        }

        return longest;
    }
    public bool IsPalindrome(string substring)
    {   
        bool exists;
        if (memo.TryGetValue(substring, out exists))
            return exists;

        int length = substring.Length;
        for (int i = 0; i < length / 2; ++i)
        {                        
            if (substring[i] != substring[length - i - 1]) 
            {
                memo.Add(substring, false);                
                return false;
            }
                
        }
        memo.Add(substring, true);                
        return true;
    }   
    public void Test()
    {
        //var result1 = LongestPalindrome("babad");
        var result2 = LongestPalindrome("cbbd");
    }
   
}

/*
5. Longest Palindromic Substring
Solved
Medium
Topics
Companies
Hint

Given a string s, return the longest
palindromic
substring
in s.

 

Example 1:

Input: s = "babad"
Output: "bab"
Explanation: "aba" is also a valid answer.

Example 2:

Input: s = "cbbd"
Output: "bb"

 

Constraints:

    1 <= s.length <= 1000
    s consist of only digits and English letters.


*/