namespace Top150;

internal class Triangle120
{
    public int MinimumTotal(IList<IList<int>> triangle) 
    {        
        for (int i = triangle.Count - 2; i >= 0; --i)
        {       
            List<int> current = (List<int>)triangle[i];    
            List<int> next = (List<int>)triangle[i+1];
            for (int j = 0; j < current.Count; ++j)
            {
                int left = next[j];
                int right = next[j+1];
                current[j] += left < right ? left : right;
            }
        }
        return triangle[0][0];
    }
    public void Test()
    {
        Test1();
    }
    public void Test1()
    {
        /*int[][] triangle = new int[4][];
        triangle[0] = new int[]{2};
        triangle[1] = new int[]{3,4};
        triangle[2] = new int[]{6,5,7};
        triangle[3] = new int[]{4,1,8,3};*/

        IList<IList<int>> triangle = new List<IList<int>>();
        triangle.Add(new List<int>(){2});
        triangle.Add(new List<int>(){3,4});
        triangle.Add(new List<int>(){6,5,7});
        triangle.Add(new List<int>(){4,1,8,3});

        var result = MinimumTotal(triangle);
    }
}

/*
120. Triangle
Medium
Topics
Companies

Given a triangle array, return the minimum path sum from top to bottom.

For each step, you may move to an adjacent number of the row below. More formally, if you are on index i on the current row, you may move to either index i or index i + 1 on the next row.

 

Example 1:

Input: triangle = [[2],[3,4],[6,5,7],[4,1,8,3]]
Output: 11
Explanation: The triangle looks like:
   2
  3 4
 6 5 7
4 1 8 3
The minimum path sum from top to bottom is 2 + 3 + 5 + 1 = 11 (underlined above).

Example 2:

Input: triangle = [[-10]]
Output: -10

 

Constraints:

    1 <= triangle.length <= 200
    triangle[0].length == 1
    triangle[i].length == triangle[i - 1].length + 1
    -104 <= triangle[i][j] <= 104

 
Follow up: Could you do this using only O(n) extra space, where n is the total number of rows in the triangle?
*/