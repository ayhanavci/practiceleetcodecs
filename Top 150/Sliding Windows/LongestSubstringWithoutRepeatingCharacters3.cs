using PracticeLeetCodeCS.Medium;

namespace Top150;

internal class LongestSubstringWithoutRepeatingCharacters3
{
    public int LengthOfLongestSubstring(string s) 
    {                
        int length = s.Length;
        if (length == 0) return 0;

        int longest = 0;
        
        int left = 0;
        int right = 0;

        HashSet<char> subs = new HashSet<char>();        
        while (right < length)
        {            
            while (subs.Contains(s[right]))
            {
                subs.Remove(s[left]);
                left++;
            }
            subs.Add(s[right]);
            longest = Math.Max(longest, right - left + 1);
            right++;
        }

        return longest;
    }
    public void Test()
    {
        var result1 = LengthOfLongestSubstring("abcabcbb");
        var result2 = LengthOfLongestSubstring("bbbbb");
        var result3 = LengthOfLongestSubstring("pwwkew");
        var result4 = LengthOfLongestSubstring("pwaadkew");
        var result5 = LengthOfLongestSubstring("a");
    }
}
/*
3. Longest Substring Without Repeating Characters
Medium
38.3K
1.8K
Companies

Given a string s, find the length of the longest
substring
without repeating characters.

 

Example 1:

Input: s = "abcabcbb"
Output: 3
Explanation: The answer is "abc", with the length of 3.

Example 2:

Input: s = "bbbbb"
Output: 1
Explanation: The answer is "b", with the length of 1.

Example 3:

Input: s = "pwwkew"
Output: 3
Explanation: The answer is "wke", with the length of 3.
Notice that the answer must be a substring, "pwke" is a subsequence and not a substring.

 

Constraints:

    0 <= s.length <= 5 * 104
    s consists of English letters, digits, symbols and spaces.


*/