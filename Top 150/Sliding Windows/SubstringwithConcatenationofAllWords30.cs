using System.Text;

namespace Top150;

//TODO: Tekrar bak, zor
internal class SubstringwithConcatenationofAllWords30
{
    public IList<int> FindSubstring(string s, string[] words) 
    {        
        IList<int> result = new List<int>();   
        Dictionary<string, int> frequency = new Dictionary<string, int>();

        foreach (string word in words)            
            if (frequency.ContainsKey(word))
                frequency[word]++;
            else    
                frequency.Add(word, 1);

        
        int eachWordLen = words[0].Length;   
        int totalWords = words.Length;
        int stringLen = s.Length;

        StringBuilder nextWord = new StringBuilder();
        for (int i = 0; i <= stringLen - (totalWords * eachWordLen); ++i)
        {    
            Dictionary<string, int> seenWords = new Dictionary<string, int>();               
            int j = 0;
            while (j < totalWords)
            {
                int wordIndex = i + j * eachWordLen;
                string word = s.Substring(wordIndex, eachWordLen);

                if (!frequency.ContainsKey(word)) break;

                if (seenWords.ContainsKey(word)) seenWords[word]++;
                else seenWords.Add(word, 1);

                if (seenWords[word] > frequency[word]) break;

                ++j;
            }
            if (j == totalWords) result.Add(i);

            seenWords.Clear();
        }

        return result;
    }    
    public IList<int> FindSubstring_TIMEOUT(string s, string[] words) 
    {        
        IList<int> result = new List<int>();   
        Dictionary<string, int> frequency = new Dictionary<string, int>();

        foreach (string word in words)            
            if (frequency.ContainsKey(word))
                frequency[word]++;
            else    
                frequency.Add(word, 1);

        
        int eachWordLen = words[0].Length;   
        int totalWords = words.Length;
        int stringLen = s.Length;

        StringBuilder nextWord = new StringBuilder();
        for (int i = 0; i <= stringLen - (totalWords * eachWordLen); ++i)
        {    
            Dictionary<string, int> seen = new Dictionary<string, int>();   
            int seenCount = 0;
            int pointer = i;
            while (true) //Searching with i'th index
            {                
                int wordEnd = pointer + eachWordLen;
                nextWord.Clear();
                while (pointer < stringLen && pointer < wordEnd)
                {                              
                    nextWord.Append(s[pointer++]);
                }
                if (frequency.ContainsKey(nextWord.ToString()))
                {
                    if (seen.ContainsKey(nextWord.ToString()))
                    {
                        if (frequency[nextWord.ToString()] == seen[nextWord.ToString()]) //Already used all matching words.
                            break;                        
                        seen[nextWord.ToString()]++;
                    }
                    else
                    {
                        seen.Add(nextWord.ToString(), 1);
                    }
                    if (++seenCount == totalWords) //Found them all
                    {
                        result.Add(i);
                        break;
                    }
                }
                else 
                {       
                    //Not found             
                    break;
                }
                
            }
            seen.Clear();
        }

        return result;
    }    
    public void Test()
    {
        var result1 = FindSubstring("barfoothefoobarman", new string[]{"foo","bar"});
        var result2 = FindSubstring("wordgoodgoodgoodbestword", new string[]{"word","good","best","word"});
        var result3 = FindSubstring("barfoofoobarthefoobarman", new string[]{"bar","foo","the"});
        var result4 = FindSubstring("wordgoodgoodgoodbestword", new string[]{"word","good","best","good"});//o:[] e:[8]
    }
}

/*
30. Substring with Concatenation of All Words
Hard
1.6K
170
Companies

You are given a string s and an array of strings words. All the strings of words are of the same length.

A concatenated substring in s is a substring that contains all the strings of any permutation of words concatenated.

    For example, if words = ["ab","cd","ef"], then "abcdef", "abefcd", "cdabef", "cdefab", "efabcd", and "efcdab" are all concatenated strings. "acdbef" is not a concatenated substring because it is not the concatenation of any permutation of words.

Return the starting indices of all the concatenated substrings in s. You can return the answer in any order.

 

Example 1:

Input: s = "barfoothefoobarman", words = ["foo","bar"]
Output: [0,9]
Explanation: Since words.length == 2 and words[i].length == 3, the concatenated substring has to be of length 6.
The substring starting at 0 is "barfoo". It is the concatenation of ["bar","foo"] which is a permutation of words.
The substring starting at 9 is "foobar". It is the concatenation of ["foo","bar"] which is a permutation of words.
The output order does not matter. Returning [9,0] is fine too.

Example 2:

Input: s = "wordgoodgoodgoodbestword", words = ["word","good","best","word"]
Output: []
Explanation: Since words.length == 4 and words[i].length == 4, the concatenated substring has to be of length 16.
There is no substring of length 16 in s that is equal to the concatenation of any permutation of words.
We return an empty array.

Example 3:

Input: s = "barfoofoobarthefoobarman", words = ["bar","foo","the"]
Output: [6,9,12]
Explanation: Since words.length == 3 and words[i].length == 3, the concatenated substring has to be of length 9.
The substring starting at 6 is "foobarthe". It is the concatenation of ["foo","bar","the"] which is a permutation of words.
The substring starting at 9 is "barthefoo". It is the concatenation of ["bar","the","foo"] which is a permutation of words.
The substring starting at 12 is "thefoobar". It is the concatenation of ["the","foo","bar"] which is a permutation of words.

 

Constraints:

    1 <= s.length <= 104
    1 <= words.length <= 5000
    1 <= words[i].length <= 30
    s and words[i] consist of lowercase English letters.


*/