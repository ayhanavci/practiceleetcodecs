using System.Text;

namespace Top150;

internal class MinimumWindowSubstring76
{
    public string MinWindow(string s, string t) 
    {         
        if(s.Length < t.Length)
            return "";
                
        Dictionary<char, int> tMap = new Dictionary<char, int>();        

        foreach(char ch in t)
            if (tMap.ContainsKey(ch)) tMap[ch]++;
            else tMap.Add(ch, 1);
        
        
        int left = 0, minLeft = 0, count = 0;
        int minLen = int.MaxValue;

        for (int right = 0; right < s.Length; ++right)
        {
            if (tMap.ContainsKey(s[right])) //the goal of this part is to get the first window that contains whole t
            {
                tMap[s[right]]--;
                if (tMap[s[right]] >= 0) 
                    count++; //identify if the first window is found by counting frequency of the characters of t showing up in S
                while (count == t.Length) //if the count is equal to the length of t, then we find such window
                {
                    if (right - left + 1 < minLen)//update the minleft and minlen value
                    {
                        minLeft = left;
                        minLen = right - left + 1;
                    }
                    if (tMap.ContainsKey(s[left])) //starting from the leftmost index of the window, we want to check if s[left] is in t. If so, we will remove it from the window, and increase 1 time on its counter in hashmap which means we will expect the same character later by shifting right index. At the same time, we need to reduce the size of the window due to the removal.
                    {
                        tMap[s[left]]++;
                        if (tMap[s[left]] > 0) 
                            count--;
                    }
                    left++;//if it doesn't exist in t, it is not supposed to be in the window, left++. If it does exist in t, the reason is stated as above. left++.
                }
            }
            
        }

        return minLen == int.MaxValue ? "" : s.Substring(minLeft, minLen);
                    
    }
    public void Test()
    {
        var result1 = MinWindow("ADOBECODEBANC", "ABC");

    }
}
/*
Generally, there are following steps:

    create a hashmap for each character in t and count their frequency in t as the value of hashmap.
    Find the first window in S that contains T. But how? there the author uses the count.
    Checking from the leftmost index of the window and to see if it belongs to t. The reason we do so is that we want to shrink the size of the window.
    3-1) If the character at leftmost index does not belong to t, we can directly remove this leftmost value and update our window(its minLeft and minLen value)
    3-2) If the character indeed exists in t, we still remove it, but in the next step, we will increase the right pointer and expect the removed character. If find so, repeat step 3.

public String minWindow(String s, String t) {
        HashMap<Character, Integer> map = new HashMap();
        for(char c : t.toCharArray()){
            if(map.containsKey(c)){
                map.put(c, map.get(c)+1);
            }
            else{
                map.put(c, 1);
            }
        }
        int left = 0, minLeft=0, minLen =s.length()+1, count = 0;
        for(int right = 0; right<s.length(); right++){
            char r = s.charAt(right);
            if(map.containsKey(r)){//the goal of this part is to get the first window that contains whole t
                map.put(r, map.get(r)-1);
                if(map.get(r)>=0) count++;//identify if the first window is found by counting frequency of the characters of t showing up in S
            }
            while(count == t.length()){//if the count is equal to the length of t, then we find such window
                if(right-left+1 < minLen){//jsut update the minleft and minlen value
                    minLeft = left;
                    minLen = right-left+1;
                }
                char l = s.charAt(left);
                if(map.containsKey(l)){//starting from the leftmost index of the window, we want to check if s[left] is in t. If so, we will remove it from the window, and increase 1 time on its counter in hashmap which means we will expect the same character later by shifting right index. At the same time, we need to reduce the size of the window due to the removal.
                    map.put(l, map.get(l)+1);
                    if(map.get(l)>0) count--;
                }
                left++;//if it doesn't exist in t, it is not supposed to be in the window, left++. If it does exist in t, the reason is stated as above. left++.
            }
        }
        return minLen==s.length()+1?"":s.substring(minLeft, minLeft+minLen);
    }

76. Minimum Window Substring
Hard
16.7K
683
Companies

Given two strings s and t of lengths m and n respectively, return the minimum window
substring
of s such that every character in t (including duplicates) is included in the window. If there is no such substring, return the empty string "".

The testcases will be generated such that the answer is unique.

 

Example 1:

Input: s = "ADOBECODEBANC", t = "ABC"
Output: "BANC"
Explanation: The minimum window substring "BANC" includes 'A', 'B', and 'C' from string t.

Example 2:

Input: s = "a", t = "a"
Output: "a"
Explanation: The entire string s is the minimum window.

Example 3:

Input: s = "a", t = "aa"
Output: ""
Explanation: Both 'a's from t must be included in the window.
Since the largest window of s only has one 'a', return empty string.

 

Constraints:

    m == s.length
    n == t.length
    1 <= m, n <= 105
    s and t consist of uppercase and lowercase English letters.

 

Follow up: Could you find an algorithm that runs in O(m + n) time?
    if(s == null || s.length() < t.length() || s.length() == 0){
        return "";
    }
    HashMap<Character,Integer> map = new HashMap<Character,Integer>();
    for(char c : t.toCharArray()){
        if(map.containsKey(c)){
            map.put(c,map.get(c)+1);
        }else{
            map.put(c,1);
        }
    }
    int left = 0;
    int minLeft = 0;
    int minLen = s.length()+1;
    int count = 0;
    for(int right = 0; right < s.length(); right++){
        if(map.containsKey(s.charAt(right))){
            map.put(s.charAt(right),map.get(s.charAt(right))-1);
            if(map.get(s.charAt(right)) >= 0){
                count ++;
            }
            while(count == t.length()){
                if(right-left+1 < minLen){
                    minLeft = left;
                    minLen = right-left+1;
                }
                if(map.containsKey(s.charAt(left))){
                    map.put(s.charAt(left),map.get(s.charAt(left))+1);
                    if(map.get(s.charAt(left)) > 0){
                        count --;
                    }
                }
                left ++ ;
            }
        }
    }
    if(minLen>s.length())  
    {  
        return "";  
    }  
    
    return s.substring(minLeft,minLeft+minLen);

string minWindow(string s, string t) {
	unordered_map<char, int> m;
	// Statistic for count of char in t
	for (auto c : t) m[c]++;
	// counter represents the number of chars of t to be found in s.
	size_t start = 0, end = 0, counter = t.size(), minStart = 0, minLen = INT_MAX;
	size_t size = s.size();
	
	// Move end to find a valid window.
	while (end < size) {
		// If char in s exists in t, decrease counter
		if (m[s[end]] > 0)
			counter--;
		// Decrease m[s[end]]. If char does not exist in t, m[s[end]] will be negative.
		m[s[end]]--;
		end++;
		// When we found a valid window, move start to find smaller window.
		while (counter == 0) {
			if (end - start < minLen) {
				minStart = start;
				minLen = end - start;
			}
			m[s[start]]++;
			// When char exists in t, increase counter.
			if (m[s[start]] > 0)
				counter++;
			start++;
		}
	}
	if (minLen != INT_MAX)
		return s.substr(minStart, minLen);
	return "";
}
class Solution(object):
    def minWindow(self, s, t):
        """
        :type s: str
        :type t: str
        :rtype: str
        """
        
        n, counter = len(s), len(t)
        begin, end, head = 0, 0, 0 
        
        # set minimum length to len(string)+1. It cannot be greater than that
        min_len = n+1        
        
        # hashmap to hold character count in T
        dic = dict()
        for ch in t:
            dic[ch] = dic.get(ch, 0)+1        
            
        # Iterate throught the loop till we reach end of string S    
        while end < n:
            
            # 1. if character in S is present in T then 
            # 2. if count of that character in hashmap is greater than zero then decrement the counter
            # 3. Decrement the count for that character in hashmap
            if s[end] in dic:
                if dic[s[end]] > 0:
                    counter -= 1
                dic[s[end]] -= 1
                
            end += 1            

            # While counter is zero (it means we have all characters between begin and end)
            # Calculate the length and min_length
            while counter == 0:
                #print begin, end, dic
                if end - begin < min_len:
                    min_len = end-begin
                    head = begin
                
                # If character at begin index is present in T, then increment its count in hashmap
                # If its count in hashmap is <= 0, then continue the inner while loop until the count is +ve
                # If its count in hashmap is > 0 then increment the counter. It means we have found first character
                # which is in S and T. So we can continue searching for shortest len from begin+1 to ...
                # Check the output below:
                    # 1. First the begin and end window are at idx - 0 and 6
                    #     min_len - 6
                    # 2. Since begin (idx = 0) is `A`, so we set next window from begin+1 to end 
                    #    and continue with the process.
                    # 3. When end idx reaches 11, we have all characters of T in S. We calculate len and compare min_len 
                    # 4. Now, since begin(idx=1) is 'D' which is not in T, we continue in inner while loop until
                    # we set counter to non-zero. Here, we can see than count of B in hashmap is -1 since we 
                    # have 2occurances of B between begin to end. So we have to skip the first occurance.
                #
                #   Input - "ADOBECODEBANC"
                #           "ABC"
                #
                # The output of above commented print statement
                # 0 6 {u'A': 0, u'C': 0, u'B': 0}
                # 1 11 {u'A': 0, u'C': 0, u'B': -1}
                # 2 11 {u'A': 0, u'C': 0, u'B': -1}
                # 3 11 {u'A': 0, u'C': 0, u'B': -1}
                # 4 11 {u'A': 0, u'C': 0, u'B': 0}
                # 5 11 {u'A': 0, u'C': 0, u'B': 0}
                # 6 13 {u'A': 0, u'C': 0, u'B': 0}
                # 7 13 {u'A': 0, u'C': 0, u'B': 0}
                # 8 13 {u'A': 0, u'C': 0, u'B': 0}
                # 9 13 {u'A': 0, u'C': 0, u'B': 0}
                
                if s[begin] in dic:
                    dic[s[begin]] += 1
                    if dic[s[begin]] > 0: 
                        counter += 1                
                begin += 1
        
        # Calculate the min string
        if min_len == n+1:
            return ""
        return s[head: head+min_len]
        

*/