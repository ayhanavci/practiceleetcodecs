namespace Top150;

internal class MinimumSizeSubarraySum209
{
    public int MinSubArrayLen(int target, int[] nums) 
    {
        int left = 0;
        int right = 0;
        int length = nums.Length;
        int answer = int.MaxValue;
        int currentSum = 0;

        while (right < length)
        {
            currentSum += nums[right];
            while (right >= left && currentSum >= target)
            { 
                answer = Math.Min(answer, right - left + 1);
                currentSum -= nums[left];
                left++;
            }
            right++;
        }
        return answer == int.MaxValue ? 0 : answer;
    }
    public void Test()
    {
        
    }
}

/*
209. Minimum Size Subarray Sum
Medium
12.1K
382
Companies

Given an array of positive integers nums and a positive integer target, return the minimal length of a
subarray
whose sum is greater than or equal to target. If there is no such subarray, return 0 instead.

 

Example 1:

Input: target = 7, nums = [2,3,1,2,4,3]
Output: 2
Explanation: The subarray [4,3] has the minimal length under the problem constraint.

Example 2:

Input: target = 4, nums = [1,4,4]
Output: 1

Example 3:

Input: target = 11, nums = [1,1,1,1,1,1,1,1]
Output: 0

 

Constraints:

    1 <= target <= 109
    1 <= nums.length <= 105
    1 <= nums[i] <= 104

 
Follow up: If you have figured out the O(n) solution, try coding another solution of which the time complexity is O(n log(n)).
*/