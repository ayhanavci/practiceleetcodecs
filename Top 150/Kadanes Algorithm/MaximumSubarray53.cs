namespace Top150;

internal class MaximumSubarray53
{
    //TODO: Slow 20%
    public int MaxSubArray(int[] nums) 
    {
        int max = nums[0];
        int sum = 0;

        for (int i = 0; i < nums.Length; ++i)
        {
            if (sum < 0)
                sum = 0;
            
            sum += nums[i];
            max = Math.Max(sum, max);
        }

        return max;
    }
    public void Test()
    {   
        Test1();
        Test2();
        Test3();
    }
    public void Test1()
    {
        int [] nums = {-2,1,-3,4,-1,2,1,-5,4};
        var result = MaxSubArray(nums);//6
    }
    public void Test2()
    {
        int [] nums = {1};
        var result = MaxSubArray(nums);//1
    }
    public void Test3()
    {
        int [] nums = {5,4,-1,7,8};
        var result = MaxSubArray(nums);//23
    }
}

/*
53. Maximum Subarray
Medium
Topics
Companies

Given an integer array nums, find the
subarray
with the largest sum, and return its sum.

 

Example 1:

Input: nums = [-2,1,-3,4,-1,2,1,-5,4]
Output: 6
Explanation: The subarray [4,-1,2,1] has the largest sum 6.

Example 2:

Input: nums = [1]
Output: 1
Explanation: The subarray [1] has the largest sum 1.

Example 3:

Input: nums = [5,4,-1,7,8]
Output: 23
Explanation: The subarray [5,4,-1,7,8] has the largest sum 23.

 

Constraints:

    1 <= nums.length <= 105
    -104 <= nums[i] <= 104

 

Follow up: If you have figured out the O(n) solution, try coding another solution using the divide and conquer approach, which is more subtle.

*/