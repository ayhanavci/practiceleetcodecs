using System.Text;

namespace Top150;

internal class SummaryRanges228
{
    public IList<string> SummaryRanges(int[] nums) 
    {
        IList<string> ranges = new List<string> ();
        if (nums.Length == 0) return ranges;    
        
        int rangeStart = 0;        
        int rangeEnd = 0;
        string nextItem = nums[0].ToString();
        
        for (int i = 1; i < nums.Length; ++i)
        {
            if (nums[i] != nums[i-1] + 1)
            {
                if (rangeEnd > rangeStart)                
                    nextItem += String.Format("->{0}", nums[rangeEnd]);                    
                
                ranges.Add(nextItem);
                nextItem = nums[i].ToString();
                rangeStart = i;
            }
            
            rangeEnd = i;
        }

        if (rangeEnd > rangeStart)        
            nextItem += String.Format("->{0}", nums[rangeEnd]);    
        ranges.Add(nextItem);
        return ranges;
    }
    public void Test()
    {
        int[] nums1 = {0,1,2,4,5,7};
        var result1 = SummaryRanges(nums1);//["0->2","4->5","7"]

        int[] nums2 = {0,2,3,4,6,8,9};
        var result2 = SummaryRanges(nums2);//["0","2->4","6","8->9"]

        int[] nums3 = {};
        var result3 = SummaryRanges(nums3);

        int[] nums4 = {1,2};
        var result4 = SummaryRanges(nums4);

        int[] nums5 = {1,3};
        var result5 = SummaryRanges(nums5);
    }
}

/*
228. Summary Ranges
Easy
3.8K
2.1K
Companies

You are given a sorted unique integer array nums.

A range [a,b] is the set of all integers from a to b (inclusive).

Return the smallest sorted list of ranges that cover all the numbers in the array exactly. That is, each element of nums is covered by exactly one of the ranges, and there is no integer x such that x is in one of the ranges but not in nums.

Each range [a,b] in the list should be output as:

    "a->b" if a != b
    "a" if a == b

 

Example 1:

Input: nums = [0,1,2,4,5,7]
Output: ["0->2","4->5","7"]
Explanation: The ranges are:
[0,2] --> "0->2"
[4,5] --> "4->5"
[7,7] --> "7"

Example 2:

Input: nums = [0,2,3,4,6,8,9]
Output: ["0","2->4","6","8->9"]
Explanation: The ranges are:
[0,0] --> "0"
[2,4] --> "2->4"
[6,6] --> "6"
[8,9] --> "8->9"

 

Constraints:

    0 <= nums.length <= 20
    -231 <= nums[i] <= 231 - 1
    All the values of nums are unique.
    nums is sorted in ascending order.


*/