namespace Top150;

internal class InsertInterval57
{
    public int[][] Insert(int[][] intervals, int[] newInterval) 
    {
        if (intervals.Length == 0) 
        {
            List<int[]> retVal = new List<int[]>
            {
                newInterval
            };
            return retVal.ToArray();
        }
            
        int startIndex = 0;
        int endIndex = 0;

        List<int[]> intervalList = new List<int[]>(intervals);

        for (int i = 0; i < intervals.Length; ++i)
        {
            if (newInterval[0] >= intervals[i][0])            
                endIndex = startIndex = i;                            
            if (newInterval[1] >= intervals[i][0])            
                endIndex = i;            
            else
                break;
        }

        //inclusive at start?
        if (newInterval[0] > intervalList[startIndex][1]) 
        {
            startIndex++; //no
            if (startIndex > endIndex) endIndex = startIndex;
        }               
        if (startIndex == intervalList.Count)
        {
            intervalList.Add(newInterval);
        }        
        else if (startIndex == endIndex)
        {        
            if (newInterval[1] < intervalList[startIndex][0])
            {
                intervalList.Insert(startIndex, newInterval);
            }                
            else if (newInterval[0] > intervals[startIndex][1])
            {
                if (startIndex + 1 == intervalList.Count)
                    intervalList.Add(newInterval);
                else
                    intervalList.Insert(startIndex+1, newInterval);
            }
            else
            {
                intervalList[startIndex][0] = Math.Min(intervals[startIndex][0], newInterval[0]);
                intervalList[startIndex][1] = Math.Max(intervals[startIndex][1], newInterval[1]);            
            }
        }
        else
        {
            int[] insertItem = new int[]
            {
                Math.Min(intervals[startIndex][0], newInterval[0]),
                Math.Max(intervals[endIndex][1], newInterval[1])
            };

            int removeCount = endIndex - startIndex + 1;
            while (removeCount-- > 0)        
                intervalList.RemoveAt(startIndex);
            intervalList.Insert(startIndex, insertItem);                
        }

        return intervalList.ToArray();
    }
     /*
        intervals1[0] = new int[]{10,15};
        intervals1[1] = new int[]{20,30};
        intervals1[2] = new int[]{40,50};
        intervals1[3] = new int[]{60,70};
        intervals1[4] = new int[]{80,90};
        
        */
    public int[][] Insert_OLD(int[][] intervals, int[] newInterval) 
    {
        if (intervals.Length == 0) 
        {
            List<int[]> retVal = new List<int[]>
            {
                newInterval
            };
            return retVal.ToArray();
        }
            
        int startIndex = 0;
        int endIndex = 0;

        List<int[]> intervalList = new List<int[]>(intervals);

        for (int i = 0; i < intervals.Length; ++i)
        {   
            if (newInterval[0] >= intervals[i][0])
            {
                if (newInterval[0] > intervals[i][1])                
                    startIndex = i + 1;                
                else                
                    startIndex = i;            
            }
            if (newInterval[1] >= intervals[i][0])
            {
                if (newInterval[1] > intervals[i][1])                
                    endIndex = i + 1;                
                else                
                    endIndex = i;
            }
            else
                break;
            
        }
        if (startIndex >= intervals.Length)
        {
            intervalList.Add(newInterval);
        }
        else
        {
            int[] insertItem = new int[]
            {
                Math.Min(intervals[startIndex][0], newInterval[0]),
                Math.Max(intervals[endIndex][1], newInterval[1])
            };

            int removeCount = endIndex - startIndex + 1;
            while (removeCount-- > 0)        
                intervalList.RemoveAt(startIndex);
            intervalList.Insert(startIndex, insertItem);                
        }
        

        return intervalList.ToArray();
    }

    public void Test()
    {

        int[][] intervals7 = new int[2][];
        intervals7[0] = new int[]{0,5};
        intervals7[1] = new int[]{9,12};
        int[] newInterval7 = {7,16};
        var result7 = Insert(intervals7, newInterval7);//O:[[0,16]] E:[[0,5],[7,16]]

        int[][] intervals6 = new int[1][];
        intervals6[0] = new int[]{9,12};
        int[] newInterval6 = {6,8};
        var result6 = Insert(intervals6, newInterval6);

        int[][] intervals5 = new int[1][];
        intervals5[0] = new int[]{1,5};
        int[] newInterval5 = {6,8};
        var result5 = Insert(intervals5, newInterval5);//O: [[1,8]] E:[[1,5],[6,8]]

        
        int[][] intervals3 = new int[0][];
        int[] newInterval3 = {2,5};
        var result3 = Insert(intervals3, newInterval3);

        int[][] intervals4 = new int[1][];
        intervals4[0] = new int[]{1,4};
        int[] newInterval4 = {2,5};
        var result4 = Insert(intervals4, newInterval4);

        int[][] intervals1 = new int[5][];
        intervals1[0] = new int[]{1,2};
        intervals1[1] = new int[]{3,5};
        intervals1[2] = new int[]{6,7};
        intervals1[3] = new int[]{8,10};
        intervals1[4] = new int[]{12,16};
        
        int[] newInterval1 = { 4, 8};

        var result1 = Insert(intervals1, newInterval1);//[[1,2],[3,10],[12,16]]


        int[][] intervals2 = new int[2][];
        intervals2[0] = new int[]{1,3};
        intervals2[1] = new int[]{6,9};
        
        int[] newInterval2 = {2,5};

        var result2 = Insert(intervals2, newInterval2);//[[1,5],[6,9]]
    }
}

/*
57. Insert Interval
Medium
9.4K
690
Companies

You are given an array of non-overlapping intervals intervals where intervals[i] = [starti, endi] represent the start and the end of the ith interval and intervals is sorted in ascending order by starti. You are also given an interval newInterval = [start, end] that represents the start and end of another interval.

Insert newInterval into intervals such that intervals is still sorted in ascending order by starti and intervals still does not have any overlapping intervals (merge overlapping intervals if necessary).

Return intervals after the insertion.

 

Example 1:

Input: intervals = [[1,3],[6,9]], newInterval = [2,5]
Output: [[1,5],[6,9]]

Example 2:

Input: intervals = [[1,2],[3,5],[6,7],[8,10],[12,16]], newInterval = [4,8]
Output: [[1,2],[3,10],[12,16]]
Explanation: Because the new interval [4,8] overlaps with [3,5],[6,7],[8,10].

 

Constraints:

    0 <= intervals.length <= 104
    intervals[i].length == 2
    0 <= starti <= endi <= 105
    intervals is sorted by starti in ascending order.
    newInterval.length == 2
    0 <= start <= end <= 105


*/