namespace Top150;


internal class MergeIntervals56
{
    //TODO: Slow 22%
    public int[][] Merge(int[][] intervals) 
    {
        List<int[]> intervalList = new List<int[]>(intervals);
        
        var ordered = intervalList.OrderBy(interval => interval[0]).ToArray();
        
        List<int[]> merged = new List<int[]>();        
        
        for (int i = 0; i < ordered.Length; ++i)
        {
            int[] nextRange = new int[2];
            nextRange[0] = ordered[i][0];            
            nextRange[1] = ordered[i][1];
            while(i < ordered.Length - 1 && ordered[i+1][0] <= nextRange[1])
            {
                nextRange[1] = Math.Max(nextRange[1], ordered[i+1][1]);                
                i++;
            }
            nextRange[1] = nextRange[1];
            merged.Add(nextRange);
        }
        return merged.ToArray();
    }
    public void Test()
    {
        int[][] intervals6 = new int[2][];
        intervals6[0] = new int[]{1,4};
        intervals6[1] = new int[]{0,0};
        
        var result6 = Merge(intervals6);//O:[[0,4]] E:[[0,0],[1,4]]

        int[][] intervals5 = new int[2][];
        intervals5[0] = new int[]{1,4};
        intervals5[1] = new int[]{0,4};
        
        var result5 = Merge(intervals5);//[[0,4]]

        int[][] intervals1 = new int[4][];
        intervals1[0] = new int[]{1,3};
        intervals1[1] = new int[]{2,6};
        intervals1[2] = new int[]{8,10};
        intervals1[3] = new int[]{15,18};

        var result1 = Merge(intervals1);//[[1,6],[8,10],[15,18]]

        int[][] intervals2 = new int[2][];
        intervals2[0] = new int[]{1,4};
        intervals2[1] = new int[]{4,5};
        
        var result2 = Merge(intervals2);//[[1,6],[8,10],[15,18]]

        int[][] intervals3 = new int[1][];
        intervals3[0] = new int[]{1,8};        
        
        var result3 = Merge(intervals3);

        int[][] intervals4= new int[4][];
        intervals4[0] = new int[]{1,15};
        intervals4[1] = new int[]{2,6};
        intervals4[2] = new int[]{8,10};
        intervals4[3] = new int[]{15,18};

        var result4 = Merge(intervals4);
    }
}

/*
56. Merge Intervals
Medium
21.4K
739
Companies

Given an array of intervals where intervals[i] = [starti, endi], merge all overlapping intervals, and return an array of the non-overlapping intervals that cover all the intervals in the input.

 

Example 1:

Input: intervals = [[1,3],[2,6],[8,10],[15,18]]
Output: [[1,6],[8,10],[15,18]]
Explanation: Since intervals [1,3] and [2,6] overlap, merge them into [1,6].

Example 2:

Input: intervals = [[1,4],[4,5]]
Output: [[1,5]]
Explanation: Intervals [1,4] and [4,5] are considered overlapping.

 

Constraints:

    1 <= intervals.length <= 104
    intervals[i].length == 2
    0 <= starti <= endi <= 104


*/