namespace Top150;

internal class MinimumAbsoluteDifferenceinBST530
{
    //Definition for a binary tree node.
    public class TreeNode
    {
        public int val;
        public TreeNode left;
        public TreeNode right;
        public TreeNode(int val = 0, TreeNode left = null, TreeNode right = null)
        {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }
    int minimum = int.MaxValue;
    List<int> items;
    public int GetMinimumDifference(TreeNode root) 
    {
        items = new List<int>();
        Traverse(root);        
        return minimum;
    }
    public void Traverse(TreeNode root)
    {        
        if (root == null) return;

        Traverse(root.left);
        items.Add(root.val);        
        if (items.Count > 1)
            minimum = Math.Min(minimum, Math.Abs(root.val - items[items.Count-2]));                
        Traverse(root.right);                                        
    }
    public void Test()
    {
        Test1();
    }
    public void Test1()
    {
        TreeNode r = new TreeNode(701)
        {         
            right = new TreeNode(911),           
        };

        TreeNode l = new TreeNode(104)
        {            
            right = new TreeNode(227),            
        };

        TreeNode root = new TreeNode(236)
        {
            left = l,
            right = r
        };

        var result = GetMinimumDifference(root);
    }
}

/*
530. Minimum Absolute Difference in BST
Solved
Easy
Topics
Companies

Given the root of a Binary Search Tree (BST), return the minimum absolute difference between the values of any two different nodes in the tree.

 

Example 1:

Input: root = [4,2,6,1,3]
Output: 1

Example 2:

Input: root = [1,0,48,null,null,12,49]
Output: 1

 

Constraints:

    The number of nodes in the tree is in the range [2, 104].
    0 <= Node.val <= 105

 

Note: This question is the same as 783: https://leetcode.com/problems/minimum-distance-between-bst-nodes/

*/