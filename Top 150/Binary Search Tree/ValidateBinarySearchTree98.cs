namespace Top150;

internal class ValidateBinarySearchTree98
{
     //Definition for a binary tree node.
    public class TreeNode
    {
        public int val;
        public TreeNode left;
        public TreeNode right;
        public TreeNode(int val = 0, TreeNode left = null, TreeNode right = null)
        {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }
    public bool IsValidBST(TreeNode root) 
    {
        if (!Traverse(root.left, root.val, null))
            return false;
        
        if (!Traverse(root.right, null, root.val))
            return false;
        return true;
    }
    public bool Traverse(TreeNode root, int? max, int? min)
    {
        if (root == null)
            return true;
        
        if (max != null && root.val >= max)
            return false;

        if (min != null && root.val <= min)
            return false;        

        if (!Traverse(root.left, root.val, min))
            return false;
       
        if (!Traverse(root.right, max, root.val))
            return false;

        return true;
    }

    public void Test()
    {
        Test2();
    }
    public void Test1()
    {
        TreeNode l = new TreeNode(1)
        {         
            
        };

        TreeNode r = new TreeNode(4)
        {            
            right = new TreeNode(6),
            left = new TreeNode(3)
        };

        TreeNode root = new TreeNode(5)
        {
            left = l,
            right = r
        };

        var result = IsValidBST(root);
    }
    public void Test2()
    {        
        TreeNode r = new TreeNode(2147483647);
        TreeNode root = new TreeNode(-2147483648)
        {            
            right = r
        };

        var result = IsValidBST(root);
    }
}

/*

98. Validate Binary Search Tree
Medium
Topics
Companies

Given the root of a binary tree, determine if it is a valid binary search tree (BST).

A valid BST is defined as follows:

    The left
    subtree
    of a node contains only nodes with keys less than the node's key.
    The right subtree of a node contains only nodes with keys greater than the node's key.
    Both the left and right subtrees must also be binary search trees.

 

Example 1:

Input: root = [2,1,3]
Output: true

Example 2:

Input: root = [5,1,4,null,null,3,6]
Output: false
Explanation: The root node's value is 5 but its right child's value is 4.

 

Constraints:

    The number of nodes in the tree is in the range [1, 104].
    -231 <= Node.val <= 231 - 1


*/