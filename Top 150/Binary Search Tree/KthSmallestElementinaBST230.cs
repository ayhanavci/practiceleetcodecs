namespace Top150;
internal class KthSmallestElementinaBST230
{
    
    //Definition for a binary tree node.
    public class TreeNode
    {
        public int val;
        public TreeNode left;
        public TreeNode right;
        public TreeNode(int val = 0, TreeNode left = null, TreeNode right = null)
        {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }
    int count = 0;
    int answer = 0;
    public int KthSmallest(TreeNode root, int k) 
    {                
        if (root == null) return answer; 

        KthSmallest(root.left, k);  
        count++;               
                    
        if (count == k) 
        {
            answer = root.val;
            return answer;
        }                                     
        
        KthSmallest(root.right, k);     
        return answer;        
    }


    List<int> smallItems;
    public int KthSmallest_V2(TreeNode root, int k) 
    {
        smallItems = new List<int>();
        Traverse_V2(root, k);
        return smallItems[k-1];
    }
    public void Traverse_V2(TreeNode root, int k)
    {
        if (root == null) return; 

        if (smallItems.Count < k) 
            Traverse_V2(root.left, k);  
                    
        if (smallItems.Count < k)         
            smallItems.Add(root.val);        
        
        if (smallItems.Count < k)         
            Traverse_V2(root.right, k);                      
    }

    
    public int KthSmallest_OLD(TreeNode root, int k) 
    {
        smallItems = new List<int>();    
        Traverse_OLD(root, k);
        smallItems.Sort();
        return smallItems[smallItems.Count - 1];
    }

    public void Traverse_OLD(TreeNode root, int k)
    {
        if (root == null) return;
        
        if (smallItems.Count < k)
        {
            smallItems.Add(root.val);
        }
        else if (smallItems.Count == k)
        {
            smallItems.Sort();
            if (root.val < smallItems[smallItems.Count - 1])                    
                smallItems[smallItems.Count - 1] = root.val;
            else
                return;
        }
        Traverse_OLD(root.left, k);        
        Traverse_OLD(root.right, k);        

    }
    public void Test()
    {
        Test1();
        Test2();
        Test3();
    }
    public void Test1()
    {
        TreeNode r = new TreeNode(6)
        {         
            
        };

        TreeNode l = new TreeNode(3)
        {            
            right = new TreeNode(4),
            left = new TreeNode(2,
                new TreeNode(1),
                null)
        };

        TreeNode root = new TreeNode(5)
        {
            left = l,
            right = r
        };

        var result = KthSmallest(root, 3);
    }
    public void Test2()
    {
       

        TreeNode root = new TreeNode(1)
        {
           
        };

        var result = KthSmallest(root, 1);
    }
    public void Test3()
    {
        TreeNode r = new TreeNode(6)
        {         
            
        };

        TreeNode l = new TreeNode(1)
        {            
          
        };

        TreeNode root = new TreeNode(2)
        {
            left = l,
          
        };

        var result = KthSmallest(root, 2);
    }
}

/*
class Solution {
public:
    void inorder(TreeNode* root, int &count, int &ans, int k){
        if(root == NULL)    return;
        //left, root, right 
        inorder(root->left, count, ans, k);
        count++;
        if(count == k){
            ans = root->val;
            return;
        }
        inorder(root->right, count, ans, k);
    }
    int kthSmallest(TreeNode* root, int k) {
        int count = 0;        
        int ans;
        inorder(root, count, ans, k);
        return ans;
    }
};
*/
/*
230. Kth Smallest Element in a BST
Medium
Topics
Companies
Hint

Given the root of a binary search tree, and an integer k, return the kth smallest value (1-indexed) of all the values of the nodes in the tree.

 

Example 1:

Input: root = [3,1,4,null,2], k = 1
Output: 1

Example 2:

Input: root = [5,3,6,2,4,null,null,1], k = 3
Output: 3

 

Constraints:

    The number of nodes in the tree is n.
    1 <= k <= n <= 104
    0 <= Node.val <= 104

 

Follow up: If the BST is modified often (i.e., we can do insert and delete operations) and you need to find the kth smallest frequently, how would you optimize?

*/