namespace Top150;

internal class ConstructBinaryTreefromInorderandPostorderTraversal106
{
    //Definition for a binary tree node.
    public class TreeNode
    {
        public int val;
        public TreeNode left;
        public TreeNode right;
        public TreeNode(int val = 0, TreeNode left = null, TreeNode right = null)
        {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }
    Dictionary<int, int> mapInOrder = new Dictionary<int, int>();
    int[] _postOrder;
    public TreeNode BuildTree(int[] inorder, int[] postorder) 
    {
        _postOrder = postorder;

        for (int i = 0; i < inorder.Length; ++i)
            mapInOrder.Add(inorder[i], i);
        
        return Build(0, postorder.Length - 1, 0, inorder.Length - 1);
    }
    public TreeNode Build(int postStart, int postEnd, int inStart, int inEnd)
    {
        if (postStart > postEnd || inStart > inEnd)
            return null;
        
        TreeNode root = new TreeNode(_postOrder[postEnd]);

        int inIndex = mapInOrder[root.val];
        int numsRight = inEnd - inIndex;
        
        root.left = Build(postStart, postEnd - numsRight - 1, inStart, inIndex - 1);
        root.right = Build(postStart, postEnd - 1, inIndex + 1, inEnd);

        return root;
    }

    public void Test()
    {
        Test1();
    }
    public void Test1()
    {
        int[] inorder = {9,3,15,20,7};
        int[] postorder = {9,15,7,20,3};
        var root = BuildTree(inorder, postorder);
    }
}
/*
106. Construct Binary Tree from Inorder and Postorder Traversal
Medium
Topics
Companies

Given two integer arrays inorder and postorder where inorder is the inorder traversal of a binary tree and postorder is the postorder traversal of the same tree, construct and return the binary tree.

 

Example 1:

Input: inorder = [9,3,15,20,7], postorder = [9,15,7,20,3]
Output: [3,9,20,null,null,15,7]

Example 2:

Input: inorder = [-1], postorder = [-1]
Output: [-1]

 

Constraints:

    1 <= inorder.length <= 3000
    postorder.length == inorder.length
    -3000 <= inorder[i], postorder[i] <= 3000
    inorder and postorder consist of unique values.
    Each value of postorder also appears in inorder.
    inorder is guaranteed to be the inorder traversal of the tree.
    postorder is guaranteed to be the postorder traversal of the tree.


*/