namespace Top150;

internal class PathSum112
{
     //Definition for a binary tree node.
    public class TreeNode
    {
        public int val;
        public TreeNode left;
        public TreeNode right;
        public TreeNode(int val = 0, TreeNode left = null, TreeNode right = null)
        {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }
    public bool HasPathSum(TreeNode root, int targetSum) 
    {
        if (root == null)
            return false;
        
        if (HasPathSum(root.left, targetSum - root.val)) 
            return true;
        if (HasPathSum(root.right, targetSum - root.val)) 
            return true;
        
        if (root.left == null && root.right == null && targetSum - root.val == 0)
            return true;
        
        return false;
    }
    public void Test()
    {
        Test1();
    }
    public void Test1()
    {
        TreeNode leftR = new TreeNode(13);
        TreeNode rightR = new TreeNode(4, null, new TreeNode(1));
        TreeNode right = new TreeNode(8, leftR, rightR);

        TreeNode left = new TreeNode(4,
            new TreeNode(11, new TreeNode(7), new TreeNode(2)),
            null);        

        TreeNode root = new TreeNode(5, left, right);        
        
        var result1 = HasPathSum(root, 22);
    }
}
/*
112. Path Sum
Solved
Easy
Topics
Companies

Given the root of a binary tree and an integer targetSum, return true if the tree has a root-to-leaf path such that adding up all the values along the path equals targetSum.

A leaf is a node with no children.

 

Example 1:

Input: root = [5,4,8,11,null,13,4,7,2,null,null,null,1], targetSum = 22
Output: true
Explanation: The root-to-leaf path with the target sum is shown.

Example 2:

Input: root = [1,2,3], targetSum = 5
Output: false
Explanation: There two root-to-leaf paths in the tree:
(1 --> 2): The sum is 3.
(1 --> 3): The sum is 4.
There is no root-to-leaf path with sum = 5.

Example 3:

Input: root = [], targetSum = 0
Output: false
Explanation: Since the tree is empty, there are no root-to-leaf paths.

 

Constraints:

    The number of nodes in the tree is in the range [0, 5000].
    -1000 <= Node.val <= 1000
    -1000 <= targetSum <= 1000


*/