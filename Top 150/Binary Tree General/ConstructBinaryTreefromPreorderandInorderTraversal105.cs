namespace Top150;

internal class ConstructBinaryTreefromPreorderandInorderTraversal105
{
    //Definition for a binary tree node.
    public class TreeNode
    {
        public int val;
        public TreeNode left;
        public TreeNode right;
        public TreeNode(int val = 0, TreeNode left = null, TreeNode right = null)
        {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }
        
    int[] _preorder;
    
    Dictionary<int, int> mapInOrderIndex = new Dictionary<int, int>();

    public TreeNode Build1(int preStart, int preEnd, int inStart, int inEnd)    {
        if (preStart > preEnd || inStart > inEnd)
            return null;
        
        TreeNode root = new TreeNode(_preorder[preStart]);
        
        //Find the index of the root
        int inRoot = mapInOrderIndex[root.val];

        //Find the number of elements ( say nElem) in the left subtree = elem – inStart
        int numLeft = inRoot - inStart;

        root.left = Build1(preStart + 1, preStart + numLeft, inStart, inRoot - 1);
        root.right = Build1(preStart + numLeft + 1, preEnd, inRoot + 1, inEnd);
        
        return root;
    }
   
    public TreeNode BuildTree(int[] preorder, int[] inorder)
    {
        _preorder = preorder;        
        
        for (int i = 0; i < inorder.Length; ++i)        
            mapInOrderIndex.Add(inorder[i], i);
        
        //return Build1(0, preorder.Length - 1, 0, inorder.Length - 1);        
        return Build(0, preorder.Length - 1, 0, inorder.Length - 1);
    }

    
    public TreeNode Build(int preStart, int preEnd, int inStart, int inEnd)
    {
        if (preStart > preEnd || inStart > inEnd)
            return null;
                
        TreeNode root = new TreeNode(_preorder[preStart]);

        int inRoot = mapInOrderIndex[root.val];
        int numsLeft = inRoot - inStart;

        int leftPreStart = preStart + 1;
        int leftPreEnd = preStart + numsLeft;
        int leftInStart = inStart;
        int leftInEnd = inRoot - 1;

        int rightPreStart = preStart + numsLeft + 1;
        int rightPreEnd = preEnd;
        int rightInStart = inRoot + 1;
        int rightInEnd = inEnd;

        root.left = Build(leftPreStart, leftPreEnd, leftInStart, leftInEnd);
        root.right = Build(rightPreStart, rightPreEnd, rightInStart, rightInEnd);
        
        return root;        
    }
    public void Test()
    {

    }
}
/*
def buildTree(self, preorder, inorder):
    if inorder:
        ind = inorder.index(preorder.pop(0))
        root = TreeNode(inorder[ind])
        root.left = self.buildTree(preorder, inorder[0:ind])
        root.right = self.buildTree(preorder, inorder[ind+1:])
        return root
*/
/*
Say we have 2 arrays, PRE and IN.
Preorder traversing implies that PRE[0] is the root node.
Then we can find this PRE[0] in IN, say it's IN[5].
Now we know that IN[5] is root, so we know that IN[0] - IN[4] is on the left side, IN[6] to the end is on the right side.
Recursively doing this on subarrays, we can build a tree out of it :)

 public TreeNode _helper(int preStart, int inStart, int inEnd) {
        if (preStart > _preorder.Length - 1 || inStart > inEnd)         
            return null;
        
        TreeNode root = new TreeNode(_preorder[preStart]);
        int inIndex = 0; // Index of current root in inorder
        for (int i = inStart; i <= inEnd; i++) {
            if (_inorder[i] == root.val) 
                inIndex = i;            
        }
        root.left = helper(preStart + 1, inStart, inIndex - 1);
        root.right = helper(preStart + inIndex - inStart + 1, inIndex + 1, inEnd);
        return root;
    }
    
*/