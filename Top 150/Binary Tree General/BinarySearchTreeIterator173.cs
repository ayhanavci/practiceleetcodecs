namespace Top150;

internal class BinarySearchTreeIterator173
{
    public class TreeNode
    {
        public int val;
        public TreeNode left;
        public TreeNode right;
        public TreeNode(int val = 0, TreeNode left = null, TreeNode right = null)
        {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }
    public class BSTIterator
    {        
        Queue<int> queue = new Queue<int>();
        public void PopulateQueue(TreeNode root)
        {
            if (root == null)
                return;
            
            PopulateQueue(root.left);
            queue.Enqueue(root.val);
            PopulateQueue(root.right);
        }
        public BSTIterator(TreeNode root)
        {
            PopulateQueue(root);
        }

        public int Next()
        {
            return queue.Dequeue();
        }

        public bool HasNext()
        {
            return queue.Count > 0;
        }
    }
    public void Test()
    {
        Test1();
    }

    public void Test1()
    {
        TreeNode root = new TreeNode(7,
            new TreeNode(3, null, null),
            new TreeNode(15, 
                new TreeNode(9, null, null),
                new TreeNode(20, null, null)));

        BSTIterator bSTIterator = new BSTIterator(root);
        int val = bSTIterator.Next();    // return 3
        val = bSTIterator.Next();    // return 7
        bool has = bSTIterator.HasNext(); // return True
        val = bSTIterator.Next();    // return 9
        has = bSTIterator.HasNext(); // return True
        val = bSTIterator.Next();    // return 15
        has = bSTIterator.HasNext(); // return True
        val = bSTIterator.Next();    // return 20
        has = bSTIterator.HasNext(); // return False
    }
}