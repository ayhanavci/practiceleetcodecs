namespace Top150;

internal class SymmetricTree101
{
    //Definition for a binary tree node.
    public class TreeNode
    {
        public int val;
        public TreeNode left;
        public TreeNode right;
        public TreeNode(int val = 0, TreeNode left = null, TreeNode right = null)
        {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }
    Dictionary<int, List<int>> depthItems;
    public bool IsSymmetric(TreeNode root) 
    {
        depthItems = new Dictionary<int, List<int>>();
        Traverse(root.left, 0);
        Traverse(root.right, 0);

        foreach (var valList in depthItems.Values)
        {
            int count = valList.Count;
            for (int i = 0; i <  count / 2; ++i)
            {
                if (valList[i] != valList[count - i - 1])
                    return false;
            }
        }
        return true;
    }
    public void Traverse(TreeNode root, int level)
    {
        List<int> levelItems;
        if (!depthItems.TryGetValue(level, out levelItems))
        {
            levelItems = new List<int>();
            depthItems.Add(level, levelItems);
        }
        if (root == null)
        {
            levelItems.Add(-101);
            return;
        }
        levelItems.Add(root.val);
        Traverse(root.left, level + 1);
        Traverse(root.right, level + 1);
    }
    
    public void Test()
    {
        Test1();
        Test2();
    }
    public void Test1()
    {        
                    
        TreeNode l = new TreeNode(2)
        {
            left = new TreeNode(3),
            right = new TreeNode(4)
        };

        TreeNode r = new TreeNode(2)
        {
            left = new TreeNode(4),
            right = new TreeNode(3)
        };

        TreeNode root = new TreeNode(1)
        {
            left = l,
            right = r
        };

        var result = IsSymmetric(root);
    }

    public void Test2()
    {        
                    
        TreeNode l = new TreeNode(2)
        {         
            right = new TreeNode(3)
        };

        TreeNode r = new TreeNode(2)
        {            
            right = new TreeNode(3)
        };

        TreeNode root = new TreeNode(1)
        {
            left = l,
            right = r
        };

        var result = IsSymmetric(root);
    }
    
}
/*
        def dfs(left,right):
            if not left and not right:
                return True
            if not left or not right: 
                return False
            
            return left.val==right.val and dfs(left.left,right.right) and dfs(left.right,right.left)
        return dfs(root.left,root.right)
*/
/*
101. Symmetric Tree
Solved
Easy
Topics
Companies

Given the root of a binary tree, check whether it is a mirror of itself (i.e., symmetric around its center).

 

Example 1:

Input: root = [1,2,2,3,4,4,3]
Output: true

Example 2:

Input: root = [1,2,2,null,3,null,3]
Output: false

 

Constraints:

    The number of nodes in the tree is in the range [1, 1000].
    -100 <= Node.val <= 100

 
Follow up: Could you solve it both recursively and iteratively?
*/