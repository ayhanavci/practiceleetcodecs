namespace Top150;

internal class BinaryTreeMaximumPathSum124
{
    //Definition for a binary tree node.
    public class TreeNode
    {
        public int val;
        public TreeNode left;
        public TreeNode right;
        public TreeNode(int val = 0, TreeNode left = null, TreeNode right = null)
        {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }
    int maxSum = int.MinValue;
    
    public int HelperMAxPathSum(TreeNode root)
    {
        if (root == null)
            return 0;

        int maxLeftSum = HelperMAxPathSum(root.left);
        int maxRightSum = HelperMAxPathSum(root.right);
        
        maxSum = Math.Max(maxSum, root.val);
        maxSum = Math.Max(maxSum, maxLeftSum + root.val);
        maxSum = Math.Max(maxSum, maxRightSum + root.val);
        maxSum = Math.Max(maxSum, maxLeftSum + maxRightSum + root.val);
                
        int maxThis = Math.Max(maxLeftSum, maxRightSum) + root.val;
        maxThis = Math.Max(maxThis, root.val);
        
        return maxThis;
    }
    public int MaxPathSum(TreeNode root) 
    {
        HelperMAxPathSum(root);
        return maxSum;
    }
    public void Test()
    {
        
        Test4();
    }

    public void Test2()
    {        
        TreeNode leftR = new TreeNode(15);
        
        TreeNode rightR = new TreeNode(7);

        TreeNode left = new TreeNode(9);

        TreeNode right = new TreeNode(20, leftR, rightR);

        TreeNode root = new TreeNode(-10, left, right);

        int maxSum = MaxPathSum(root);
    }

    public void Test3()
    {        
        TreeNode leftR = new TreeNode(15,
            new TreeNode(8),
            new TreeNode(-2));
        
        TreeNode rightR = new TreeNode(7,
            new TreeNode(4),
            new TreeNode(5));

        TreeNode left = new TreeNode(9,
            new TreeNode(-3),
            new TreeNode(6));

        TreeNode right = new TreeNode(20, leftR, rightR);

        TreeNode root = new TreeNode(-10, left, right);

        int maxSum = MaxPathSum(root);
    }
     public void Test4()
    {                

        TreeNode left = new TreeNode(-1);        

        TreeNode root = new TreeNode(2, left);

        int maxSum = MaxPathSum(root);
    }
}