namespace Top150;

internal class PopulatingNextRightPointersinEachNodeII117
{
    // Definition for a Node.
    public class Node
    {
        public int val;
        public Node left;
        public Node right;
        public Node next;

        public Node() { }

        public Node(int _val)
        {
            val = _val;
        }

        public Node(int _val, Node _left, Node _right, Node _next)
        {
            val = _val;
            left = _left;
            right = _right;
            next = _next;
        }
    }
    Dictionary<int, Node> depthMap = new Dictionary<int, Node>();
    public Node Connect(Node root)
    {
        if (root == null) return null;
        Build(root, 1);
        return root;
    }
    public void Build(Node root, int depth)
    {
        if (root == null) return;

        Build(root.right, depth+1);
        Build(root.left, depth+1); 

        if (depthMap.ContainsKey(depth))
        {            
            root.next = depthMap[depth];
            depthMap[depth] = root;
        }        
        else
        {
            depthMap.Add(depth, root);
        }

    }
    /*public void BuildOld(Node root, Node pair)
    {
        if (root == null)
            return;
        
        Node leftPair = root.right;
        if (root.val == 2)
        {
            int hede = 0;
        }

        if (root.right == null && pair != null)
        {
            if (pair.left != null)
                leftPair = pair.left;
            else
                leftPair = pair.right;
        }

        Build(root.left, leftPair);        

        Node rightPair = null;
        if (pair != null)
        {
            if (pair.left != null)
                rightPair = pair.left;
            else
                rightPair = pair.right;
        }

        Build(root.right, rightPair);

        root.next = pair;
    }*/

    public void Test()
    {
        Test1();
        Test2();
        Test3();
    }
    public void Test1()
    {                            
        Node left = new Node(9, null, null, null);

        Node right = new Node(20, 
            new Node(15, null, null, null), 
            new Node(7, null, null, null), 
            null);

        Node root = new Node(3, left, right, null);

        Connect(root);
    }
    public void Test2()
    {                            
        Node left = new Node(9, null, null, null);

        Node right = new Node(20, 
            new Node(15, null, null, null), 
            new Node(7, null, null, null), 
            null);

        Node root = new Node(1, left, right, null);

        Connect(root);
    }
    public void Test3()
    {        
                    
        Node left = new Node(2,
            new Node(4, null, null, null),
            new Node(5, null, null, null),
            null);

        Node right = new Node(3, 
            null, 
            new Node(7, null, null, null), 
            null);

        Node root = new Node(1, left, right, null);

        Connect(root);
    }
}

/*
117. Populating Next Right Pointers in Each Node II
Medium
Topics
Companies

Given a binary tree

struct Node {
  int val;
  Node *left;
  Node *right;
  Node *next;
}

Populate each next pointer to point to its next right node. If there is no next right node, the next pointer should be set to NULL.

Initially, all next pointers are set to NULL.

 

Example 1:

Input: root = [1,2,3,4,5,null,7]
Output: [1,#,2,3,#,4,5,7,#]
Explanation: Given the above binary tree (Figure A), your function should populate each next pointer to point to its next right node, just like in Figure B. The serialized output is in level order as connected by the next pointers, with '#' signifying the end of each level.

Example 2:

Input: root = []
Output: []

 

Constraints:

    The number of nodes in the tree is in the range [0, 6000].
    -100 <= Node.val <= 100

 

Follow-up:

    You may only use constant extra space.
    The recursive approach is fine. You may assume implicit stack space does not count as extra space for this problem.


*/