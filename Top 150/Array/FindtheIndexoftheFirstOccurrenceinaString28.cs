namespace Top150;

internal class FindtheIndexoftheFirstOccurrenceinaString28
{
    public int StrStr(string haystack, string needle) 
    {
        for (int i = 0; i < haystack.Length - needle.Length + 1; ++i)
        {
            if (haystack[i] == needle[0])
            {                                
                bool fFound = true;
                for (int j = 1; j < needle.Length; ++j)
                {
                    if (haystack[i+j] != needle[j])
                    {
                        fFound = false;
                        break;
                    }
                }
                if (fFound) return i;                
            }
        }
        return -1;
    }
    public void Test()
    {
       

        var result1 = StrStr("sadbutsad", "sad");
        var result2 = StrStr("leetcode", "leeto");
        var result3 = StrStr("leetcode", "e");
        var result4 = StrStr("leetcode", "de");
        var result5 = StrStr("leetcode", "d");
    }
}
/*
28. Find the Index of the First Occurrence in a String
Easy
5.3K
326
Companies

Given two strings needle and haystack, return the index of the first occurrence of needle in haystack, or -1 if needle is not part of haystack.

 

Example 1:

Input: haystack = "sadbutsad", needle = "sad"
Output: 0
Explanation: "sad" occurs at index 0 and 6.
The first occurrence is at index 0, so we return 0.

Example 2:

Input: haystack = "leetcode", needle = "leeto"
Output: -1
Explanation: "leeto" did not occur in "leetcode", so we return -1.

 

Constraints:

    1 <= haystack.length, needle.length <= 104
    haystack and needle consist of only lowercase English characters.


*/