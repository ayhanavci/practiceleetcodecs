namespace Top150;

public class BestTimetoBuyandSellStock121
{
    public int MaxProfit(int[] prices) 
    {
        int maxProfit = 0;
        int left = 0;
        int right = 1;

        while (right < prices.Length)
        {
            int profit = prices[right] - prices[left];
            if (profit > 0)
                maxProfit = Math.Max(maxProfit, profit);
            else
                left = right;
            right++;
        }

        return maxProfit;
    }
    public int MaxProfit_SLOW(int[] prices) 
    {
        int maxProfit = 0;
        for (int i = 0; i < prices.Length - 1; ++i)
            for (int j = i + 1; j < prices.Length; ++j)            
                maxProfit = Math.Max(maxProfit, prices[j] - prices[i]);                    
        return maxProfit;
    }
    public void Test()
    {
        int [] prices1 = {7,1,5,3,6,4};
        var result1 = MaxProfit(prices1);

        int [] prices2 = {7,6,4,3,1};
        var result2 = MaxProfit(prices2);
    }
}

/*
121. Best Time to Buy and Sell Stock
Easy
29.5K
1K
Companies

You are given an array prices where prices[i] is the price of a given stock on the ith day.

You want to maximize your profit by choosing a single day to buy one stock and choosing a different day in the future to sell that stock.

Return the maximum profit you can achieve from this transaction. If you cannot achieve any profit, return 0.

 

Example 1:

Input: prices = [7,1,5,3,6,4]
Output: 5
Explanation: Buy on day 2 (price = 1) and sell on day 5 (price = 6), profit = 6-1 = 5.
Note that buying on day 2 and selling on day 1 is not allowed because you must buy before you sell.

Example 2:

Input: prices = [7,6,4,3,1]
Output: 0
Explanation: In this case, no transactions are done and the max profit = 0.

 

Constraints:

    1 <= prices.length <= 105
    0 <= prices[i] <= 104


*/