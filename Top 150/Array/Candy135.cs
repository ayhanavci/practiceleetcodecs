namespace Top150;

internal class Candy135
{
    public int Candy(int[] ratings) 
    {
        int[] candies = new int[ratings.Length];
        Array.Fill(candies, 1);

        bool modified = true;

        while (modified)
        {
            modified = false;
            for (int i = 1; i < ratings.Length; ++i)
            {            
                if (ratings[i] > ratings[i-1])
                {                    
                    int candyDiff = candies[i] - candies[i-1];
                    if (candyDiff <= 0)                    
                    {
                        candies[i] += (-candyDiff) + 1;
                        modified = true;                    
                    }
                        
                    
                }
                else if (ratings[i-1] > ratings[i])
                {                    
                    int candyDiff = candies[i-1] - candies[i];
                    if (candyDiff <= 0) 
                    {
                        candies[i-1] += (-candyDiff) + 1;
                        modified = true;
                    }                        
                }        
            }
        }                
        
        return candies.Sum();
            
    }
    public void Test()
    {
        int[] nums1 = {1,0,2};
        var result1 = Candy(nums1);

        int[] nums2 = {1,2,2};
        var result2 = Candy(nums2);
    }
}
/*
135. Candy
Hard
7.4K
578
Companies

There are n children standing in a line. Each child is assigned a rating value given in the integer array ratings.

You are giving candies to these children subjected to the following requirements:

    Each child must have at least one candy.
    Children with a higher rating get more candies than their neighbors.

Return the minimum number of candies you need to have to distribute the candies to the children.

 

Example 1:

Input: ratings = [1,0,2]
Output: 5
Explanation: You can allocate to the first, second and third child with 2, 1, 2 candies respectively.

Example 2:

Input: ratings = [1,2,2]
Output: 4
Explanation: You can allocate to the first, second and third child with 1, 2, 1 candies respectively.
The third child gets 1 candy because it satisfies the above two conditions.

 

Constraints:

    n == ratings.length
    1 <= n <= 2 * 104
    0 <= ratings[i] <= 2 * 104


*/