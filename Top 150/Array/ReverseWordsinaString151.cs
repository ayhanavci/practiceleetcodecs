using System.Text;

namespace Top150;

internal class ReverseWordsinaString151
{
    public string ReverseWords(string s) 
    {
        StringBuilder retVal = new StringBuilder();
        string[] words = s.Split(' ', StringSplitOptions.RemoveEmptyEntries);
        
        for (int i = words.Length - 1; i >= 0; --i)
        {
            retVal.Append(words[i]);            
            retVal.Append(' ');
        }
        retVal.Remove(retVal.Length - 1, 1);

        return retVal.ToString();
    }
    public void Test()
    {
        var result1 = ReverseWords("the sky is blue");
        var result2 = ReverseWords("  hello world  ");
        var result3 = ReverseWords("a good   example");
    }
}

/*
151. Reverse Words in a String
Medium
7.8K
5K
Companies

Given an input string s, reverse the order of the words.

A word is defined as a sequence of non-space characters. The words in s will be separated by at least one space.

Return a string of the words in reverse order concatenated by a single space.

Note that s may contain leading or trailing spaces or multiple spaces between two words. The returned string should only have a single space separating the words. Do not include any extra spaces.

 

Example 1:

Input: s = "the sky is blue"
Output: "blue is sky the"

Example 2:

Input: s = "  hello world  "
Output: "world hello"
Explanation: Your reversed string should not contain leading or trailing spaces.

Example 3:

Input: s = "a good   example"
Output: "example good a"
Explanation: You need to reduce multiple spaces between two words to a single space in the reversed string.

 

Constraints:

    1 <= s.length <= 104
    s contains English letters (upper-case and lower-case), digits, and spaces ' '.
    There is at least one word in s.

 

Follow-up: If the string data type is mutable in your language, can you solve it in-place with O(1) extra space?

*/