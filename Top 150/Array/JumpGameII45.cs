namespace Top150;

public class JumpGameII45
{
    //TODO: Tekrar bak BFS, DP
    public int Jump(int[] nums) 
    {        
        int steps = 0;

        for (int i = 1; i < nums.Length; ++i)        
            nums[i] = Math.Max(nums[i] + i, nums[i-1]);
        
        for (int i = 0; i < nums.Length - 1; i = nums[i])
            steps++;
        
        return steps;
    }

    public int Jump_SLOW(int[] nums) 
    {
        int steps = 1;
        int i = 0;
        
        if (nums.Length == 1) return 0;
        while (i + nums[i] < nums.Length - 1)
        {                                    
            int maxJumpIndex = 0;

            for (int j = 1; j <= nums[i]; ++j)            
                maxJumpIndex = Math.Max(maxJumpIndex, nums[i+j]);
            
            i = maxJumpIndex;                        
            steps++;
            if (i >= nums.Length) break;
        }
        return steps;
    }
    /*
    int jump(vector<int>& nums) {
        int last = 0;
        int left = 0, right = 0;
        int jumps = 0;
        while(last < nums.size()-1) {
            jumps++;
            for(int i=left;i<=right;i++) {
                last = max(last, i+nums[i]);
            }
            left = right+1;
            right = last;
        }
        return jumps;
    }
    */
    public void Test()
    {
        /*
        for (int i = 1; i < nums.Length; ++i)        
            nums[i] = Math.Max(nums[i] + i, nums[i-1]);
        */
        int[] nums50 = {4, 2, 3, 4, 20, 1, 2, 5, 1};        
        //int[] nums50 = {4, (3vs4), (5v4), (7v5), (24v7), (6v24), (8v24)};

        var result50 = Jump(nums50);

        int[] nums5 = {1, 1, 1, 1};        
        var result5 = Jump(nums5);

        int[] nums4 = {1};        
        var result4 = Jump(nums4);

        int[] nums3 = {1,3,2};        
        var result3 = Jump(nums3);

        int[] nums1 = {2,3,1,1,4};        
        var result1 = Jump(nums1);

        int[] nums2 = {2,3,0,1,4};
        var result2 = Jump(nums2);
    }
}

/*
45. Jump Game II
Medium
13.9K
509
Companies

You are given a 0-indexed array of integers nums of length n. You are initially positioned at nums[0].

Each element nums[i] represents the maximum length of a forward jump from index i. In other words, if you are at nums[i], you can jump to any nums[i + j] where:

    0 <= j <= nums[i] and
    i + j < n

Return the minimum number of jumps to reach nums[n - 1]. The test cases are generated such that you can reach nums[n - 1].

 

Example 1:

Input: nums = [2,3,1,1,4]
Output: 2
Explanation: The minimum number of jumps to reach the last index is 2. Jump 1 step from index 0 to 1, then 3 steps to the last index.

Example 2:

Input: nums = [2,3,0,1,4]
Output: 2

 

Constraints:

    1 <= nums.length <= 104
    0 <= nums[i] <= 1000
    It's guaranteed that you can reach nums[n - 1].


*/