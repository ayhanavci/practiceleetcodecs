using System.Text;

namespace Top150;

internal class TextJustification68
{
    public IList<string> FullJustify(string[] words, int maxWidth)
    {
        IList<string> result = new List<string>();
        List<string> wordsList = new List<string>(words);


        //List<List<string>> items = new List<List<string>>();

        //Create 
        while (wordsList.Count > 0)
        {
            int lineLength = 0;
            List<string> newLine = new List<string>();

            while (wordsList.Count > 0)
            {
                if (maxWidth >= lineLength + wordsList[0].Length)
                {
                    newLine.Add(wordsList[0]);
                    lineLength += wordsList[0].Length + 1;
                    wordsList.RemoveAt(0);
                }
                else
                    break;

            }
            if (newLine.Count > 0)
            {
                if (wordsList.Count == 0)
                    result.Add(JustifyLastLine(newLine, maxWidth));
                else
                    result.Add(JustifyLine(newLine, maxWidth));
            }

        }

        return result;
    }
    public string JustifyLine(List<string> newLine, int maxWidth)
    {
        StringBuilder retVal = new StringBuilder();
        int spaceCount = maxWidth;

        if (newLine.Count == 1)
        {
            spaceCount = maxWidth - newLine[0].Length;
            retVal.Append(newLine[0]);
            retVal.Append(' ', spaceCount);
            return retVal.ToString();
        }
        else if (newLine.Count == 2)
        {
            spaceCount = maxWidth - newLine[0].Length - newLine[1].Length;
            retVal.Append(newLine[0]);
            retVal.Append(' ', spaceCount);
            retVal.Append(newLine[1]);
            return retVal.ToString();
        }
        retVal.Append(newLine[0]);
        spaceCount = maxWidth - newLine[0].Length - newLine[newLine.Count - 1].Length;
        for (int i = 1; i < newLine.Count - 1; ++i)
        {
            spaceCount -= newLine[i].Length;
        }

        int spaceChunksBetweenWords = newLine.Count - 1;
        int spaceChunkSize = spaceCount / spaceChunksBetweenWords;
        int nonDivisibleSpaces = spaceCount % spaceChunksBetweenWords;

        for (int i = 1; i < newLine.Count; ++i)
        {            
            retVal.Append(' ', spaceChunkSize);            
            if (nonDivisibleSpaces > 0)
            {
                retVal.Append(' ');
                nonDivisibleSpaces--;
            }
            retVal.Append(newLine[i]);
                
        }        

        return retVal.ToString();
    }
    public string JustifyLastLine(List<string> newLine, int maxWidth)
    {
        StringBuilder retVal = new StringBuilder();
        int spaceCount = maxWidth;

        for (int i = 0; i < newLine.Count; ++i)
        {
            retVal.Append(newLine[i]);
            spaceCount -= newLine[i].Length;
            if (i < newLine.Count - 1) 
            {
                retVal.Append(' ');
                spaceCount--;
            }                
        }

        retVal.Append(' ', spaceCount);
        return retVal.ToString();
    }
    public void Test()
    {
        string[] words1 = { "This", "is", "an", "example", "of", "text", "justification." };
        var result1 = FullJustify(words1, 16);

        string[] words2 = { "What","must","be","acknowledgment","shall","be" };
        var result2 = FullJustify(words2, 16);

        string[] words3 = { "Science","is","what","we","understand","well","enough","to","explain","to","a","computer.","Art","is","everything","else","we","do" };
        var result3 = FullJustify(words3, 20);
    }
}
/*


68. Text Justification
Hard
3.4K
4.4K
Companies

Given an array of strings words and a width maxWidth, format the text such that each line has exactly maxWidth characters and is fully (left and right) justified.

You should pack your words in a greedy approach; that is, pack as many words as you can in each line. Pad extra spaces ' ' when necessary so that each line has exactly maxWidth characters.

Extra spaces between words should be distributed as evenly as possible. If the number of spaces on a line does not divide evenly between words, the empty slots on the left will be assigned more spaces than the slots on the right.

For the last line of text, it should be left-justified, and no extra space is inserted between words.

Note:

    A word is defined as a character sequence consisting of non-space characters only.
    Each word's length is guaranteed to be greater than 0 and not exceed maxWidth.
    The input array words contains at least one word.

 

Example 1:

Input: words = ["This", "is", "an", "example", "of", "text", "justification."], maxWidth = 16
Output:
[
   "This    is    an",
   "example  of text",
   "justification.  "
]

Example 2:

Input: words = ["What","must","be","acknowledgment","shall","be"], maxWidth = 16
Output:
[
  "What   must   be",
  "acknowledgment  ",
  "shall be        "
]
Explanation: Note that the last line is "shall be    " instead of "shall     be", because the last line must be left-justified instead of fully-justified.
Note that the second line is also left-justified because it contains only one word.

Example 3:

Input: words = ["Science","is","what","we","understand","well","enough","to","explain","to","a","computer.","Art","is","everything","else","we","do"], maxWidth = 20
Output:
[
  "Science  is  what we",
  "understand      well",
  "enough to explain to",
  "a  computer.  Art is",
  "everything  else  we",
  "do                  "
]

 

Constraints:

    1 <= words.length <= 300
    1 <= words[i].length <= 20
    words[i] consists of only English letters and symbols.
    1 <= maxWidth <= 100
    words[i].length <= maxWidth


while (wordsList.Count > 0)
        {
            StringBuilder nextItem = new StringBuilder();
            while (wordsList.Count > 0)
            {       
                int spaceRoom = nextItem.Length > 0 ? 1 : 0;                                                        
                if (maxWidth >= nextItem.Length + spaceRoom + wordsList[0].Length)
                {                    
                    nextItem.Append(' ', spaceRoom);
                    nextItem.Append(wordsList[0]);                    
                    wordsList.RemoveAt(0);
                }
                else
                    break;
                    
            }
            if (nextItem.Length > 0)
                result.Add(nextItem.ToString());
        }

*/