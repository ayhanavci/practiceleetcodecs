namespace Top150;

internal class ProductofArrayExceptSelf238
{
    public int[] ProductExceptSelf(int[] nums) 
    {
        int [] answer = new int[nums.Length];

        answer[0] = 1;

        for (int i = 1; i < nums.Length; ++i)
        {
            answer[i] = answer[i - 1] * nums[i - 1]; //prefix
        }

        int suffix = 1;
        for (int i = nums.Length - 1; i >= 0; --i)
        {
            answer[i] *= suffix;
            suffix *= nums[i];
        }

        return answer;
    }
    public void Test()
    {

    }
}

/*
Intuition: The problem requires computing the product of all elements in an array except the element at the current index. A common approach is to use prefix and suffix products. The prefix product for an index i is the product of all elements before i, and the suffix product is the product of all elements after i.

Approach:

    Initialize an array result to store the final product for each index.
    Compute the prefix products and store them in the result array. Start from the leftmost element and multiply the running product by the element at the current index.
    Compute the suffix product while updating the result array. Start from the rightmost element, multiply the running product by the element at the current index, and update the result array by multiplying it with the current suffix product.

*/
/*
238. Product of Array Except Self
Medium
21K
1.2K
Companies

Given an integer array nums, return an array answer such that answer[i] is equal to the product of all the elements of nums except nums[i].

The product of any prefix or suffix of nums is guaranteed to fit in a 32-bit integer.

You must write an algorithm that runs in O(n) time and without using the division operation.

 

Example 1:

Input: nums = [1,2,3,4]
Output: [24,12,8,6]

Example 2:

Input: nums = [-1,1,0,-3,3]
Output: [0,0,9,0,0]

Constraints:

    2 <= nums.length <= 105
    -30 <= nums[i] <= 30
    The product of any prefix or suffix of nums is guaranteed to fit in a 32-bit integer.

 

Follow up: Can you solve the problem in O(1) extra space complexity? (The output array does not count as extra space for space complexity analysis.)

*/