namespace Top150;

public class MajorityElement169
{
    public int MajorityElement(int[] nums) 
    {
        Dictionary<int, int> occurances = new Dictionary<int, int>();
        int majElement = nums[0];
        int majOccurance = 1;

        for (int i = 0; i < nums.Length; ++i)
        {
            if (occurances.ContainsKey(nums[i]))
            {
                int occurance = ++occurances[nums[i]];
                if (occurance > majOccurance)
                {
                    majOccurance = occurance;
                    majElement = nums[i];
                }
            }
            else
                occurances.Add(nums[i], 1);
        }
        return majElement;
    }
    public void Test()
    {

    }
}
/*
169. Majority Element
Easy
17.8K
543
Companies

Given an array nums of size n, return the majority element.

The majority element is the element that appears more than ⌊n / 2⌋ times. You may assume that the majority element always exists in the array.

 

Example 1:

Input: nums = [3,2,3]
Output: 3

Example 2:

Input: nums = [2,2,1,1,1,2,2]
Output: 2

 

Constraints:

    n == nums.length
    1 <= n <= 5 * 104
    -109 <= nums[i] <= 109

 
Follow-up: Could you solve the problem in linear time and in O(1) space?
*/