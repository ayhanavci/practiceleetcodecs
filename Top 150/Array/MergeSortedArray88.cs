namespace Top150;

public class MergeSortedArray88
{
    public void Merge(int[] nums1, int m, int[] nums2, int n) 
    {
        int firstArrPos = 0;
        int secondArrPos = 0;
                
        List<int> merged = new List<int>();
        for (int i = 0; i < m + n; ++i)
        {
            if (firstArrPos == m)
            {
                for (int j = secondArrPos; j < n; ++j)                
                    merged.Add(nums2[j]);
                break;
            }
            else if (secondArrPos == n)
            {
                for (int j = firstArrPos; j < m; ++j)                
                    merged.Add(nums1[j]);
                break;
            }
            else
            {
                if (nums1[firstArrPos] <= nums2[secondArrPos])                
                    merged.Add(nums1[firstArrPos++]);
                else
                    merged.Add(nums2[secondArrPos++]);
            }            
        }
        for (int i = 0; i < merged.Count; ++i)
            nums1[i] = merged[i];
    }
    public void Test()
    {
        int[] nums1 = {1,2,3,0,0,0};
        int[] nums2 = {2,5,6};
        Merge(nums1, 3, nums2, 3);

        int[] nums21 = {1};
        int[] nums22 = Array.Empty<int>();
        Merge(nums21, 1, nums22, 0);


        int[] nums31 = {0};
        int[] nums32 = {1};
        Merge(nums31, 0, nums32, 1);

    }
}

/*
88. Merge Sorted Array
Easy
13.6K
1.6K
Companies

You are given two integer arrays nums1 and nums2, sorted in non-decreasing order, and two integers m and n, representing the number of elements in nums1 and nums2 respectively.

Merge nums1 and nums2 into a single array sorted in non-decreasing order.

The final sorted array should not be returned by the function, but instead be stored inside the array nums1. To accommodate this, nums1 has a length of m + n, where the first m elements denote the elements that should be merged, and the last n elements are set to 0 and should be ignored. nums2 has a length of n.

 

Example 1:

Input: nums1 = [1,2,3,0,0,0], m = 3, nums2 = [2,5,6], n = 3
Output: [1,2,2,3,5,6]
Explanation: The arrays we are merging are [1,2,3] and [2,5,6].
The result of the merge is [1,2,2,3,5,6] with the underlined elements coming from nums1.

Example 2:

Input: nums1 = [1], m = 1, nums2 = [], n = 0
Output: [1]
Explanation: The arrays we are merging are [1] and [].
The result of the merge is [1].

Example 3:

Input: nums1 = [0], m = 0, nums2 = [1], n = 1
Output: [1]
Explanation: The arrays we are merging are [] and [1].
The result of the merge is [1].
Note that because m = 0, there are no elements in nums1. The 0 is only there to ensure the merge result can fit in nums1.

 

Constraints:

    nums1.length == m + n
    nums2.length == n
    0 <= m, n <= 200
    1 <= m + n <= 200
    -109 <= nums1[i], nums2[j] <= 109

 

Follow up: Can you come up with an algorithm that runs in O(m + n) time?

*/