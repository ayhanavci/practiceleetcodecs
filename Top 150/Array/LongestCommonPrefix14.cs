using System.Text;

namespace Top150;

internal class LongestCommonPrefix14
{
    public string LongestCommonPrefix(string[] strs) 
    {
        StringBuilder longest = new StringBuilder();

        string firstWord = strs[0];
        for (int i = 0; i < firstWord.Length; ++i)
        {
            foreach (var item in strs)
            {                
                if (item.Length <= i || firstWord[i] != item[i])                
                    return longest.ToString();
                
            }
            longest.Append(firstWord[i]);
        }
        

        return longest.ToString();
    }
    public void Test()
    {

    }
}
/*
14. Longest Common Prefix
Easy
16.6K
4.4K
Companies

Write a function to find the longest common prefix string amongst an array of strings.

If there is no common prefix, return an empty string "".

 

Example 1:

Input: strs = ["flower","flow","flight"]
Output: "fl"

Example 2:

Input: strs = ["dog","racecar","car"]
Output: ""
Explanation: There is no common prefix among the input strings.

 

Constraints:

    1 <= strs.length <= 200
    0 <= strs[i].length <= 200
    strs[i] consists of only lowercase English letters.


*/