using System.Text;

namespace Top150;

internal class IntegertoRoman12
{
    public string IntToRoman(int num)
    {
        string[] M = {"", "M", "MM", "MMM"};
        string[] C = {"", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"};
        string[] X = {"", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC"};
        string[] I = {"", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"};
        return M[num/1000] + C[(num%1000)/100] + X[(num%100)/10] + I[num%10];
    }
    public string IntToRoman2(int num)
    {
        StringBuilder roman = new StringBuilder();
        if (num >= 1000)
        {
            int mCount = num / 1000;
            roman.Append('M', mCount);
            num -= mCount * 1000;
        }
        if (num >= 900)
        {
            roman.Append("CM");
            num -= 900;
        }
        else if (num >= 500)
        {
            roman.Append("D");
            num -= 500;
        }
        else if (num >= 400)
        {
            roman.Append("CD");
            num -= 400;
        }
        if (num >= 100)
        {
            int cCount = num / 100;
            roman.Append('C', cCount);
            num -= cCount * 100;
        }
        if (num >= 90)
        {
            roman.Append("XC");
            num -= 90;
        }
        else if (num >= 50)
        {
            roman.Append("L");
            num -= 50;
        }
        else if (num >= 40)
        {
            roman.Append("XL");
            num -= 40;
        }
        if (num >= 10)
        {
            int xCount = num / 10;
            roman.Append('X', xCount);
            num -= xCount * 10;
        }
        if (num >= 9)
        {
            roman.Append("IX");
            num -= 9;
        }
        else if (num >= 5)
        {
            roman.Append("V");
            num -= 5;
        }
        else if (num >= 4)
        {
            roman.Append("IV");
            num -= 4;
        }
        if (num > 0)
        {            
            roman.Append('I', num);            
        }
        return roman.ToString();
    }
    public void Test()
    {
        var result1 = IntToRoman(3);//("III");
        var result2 = IntToRoman(58);//("LVIII");
        var result3 = IntToRoman(1994);//("MCMXCIV");        
        var result4 = IntToRoman(1944);//("MCMXLIV");       
        var result5 = IntToRoman(9);//("IX");       
    }
}
/*

    string M[] = {"", "M", "MM", "MMM"};
    string C[] = {"", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"};
    string X[] = {"", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC"};
    string I[] = {"", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"};
    return M[num/1000] + C[(num%1000)/100] + X[(num%100)/10] + I[num%10];
*/
/*
12. Integer to Roman
Medium
6.7K
5.4K
Companies

Roman numerals are represented by seven different symbols: I, V, X, L, C, D and M.

Symbol       Value
I             1
V             5
X             10
L             50
C             100
D             500
M             1000

For example, 2 is written as II in Roman numeral, just two one's added together. 12 is written as XII, which is simply X + II. The number 27 is written as XXVII, which is XX + V + II.

Roman numerals are usually written largest to smallest from left to right. However, the numeral for four is not IIII. Instead, the number four is written as IV. Because the one is before the five we subtract it making four. The same principle applies to the number nine, which is written as IX. There are six instances where subtraction is used:

    I can be placed before V (5) and X (10) to make 4 and 9. 
    X can be placed before L (50) and C (100) to make 40 and 90. 
    C can be placed before D (500) and M (1000) to make 400 and 900.

Given an integer, convert it to a roman numeral.

 

Example 1:

Input: num = 3
Output: "III"
Explanation: 3 is represented as 3 ones.

Example 2:

Input: num = 58
Output: "LVIII"
Explanation: L = 50, V = 5, III = 3.

Example 3:

Input: num = 1994
Output: "MCMXCIV"
Explanation: M = 1000, CM = 900, XC = 90 and IV = 4.

 

Constraints:

    1 <= num <= 3999


*/