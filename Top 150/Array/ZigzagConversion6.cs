using System.Text;

namespace Top150;

internal class ZigzagConversion6
{   
    public string Convert(string s, int numRows) 
    {
        StringBuilder retVal = new StringBuilder();

        List<StringBuilder> rows = new List<StringBuilder>();

        for (int i = 0; i < numRows; ++i)
            rows.Add(new StringBuilder());

        //Special case of 1
        if (numRows == 1) return s;
        
        //Special case of 2
        if (numRows == 2)
        {
            for (int i = 0; i < s.Length; ++i)            
                rows[i % 2 == 0 ? 0 : 1].Append(s[i]);                            
        }
        else
        {
            for (int i = 0; i < s.Length; ++i)
            {
                //Iterate through column. Add each char in the column to its respective row
                for (int j = 0; j < numRows && i + j < s.Length; ++j)                
                    rows[j].Append(s[i + j]);            
                    
                //Move pointer
                i += numRows;

                //The zigzag size between the columns is always at the size of (numRows - 2)
                //Add zigzag elements to their respective row
                for (int j = 0; j < numRows - 2 && i + j < s.Length; ++j)                
                    rows[numRows - j - 2].Append(s[i + j]);

                //Move pointer
                i += numRows - 3;
            }
        }        
        
        //Merge rows
        for (int i = 0; i < numRows; ++i)        
            retVal.Append(rows[i]);
        
        
        return retVal.ToString();
    }
    public void Test()
    {
        var result1 = Convert("PAYPALISHIRING", 3);     
        var result2 = Convert("PAYPALISHIRING", 4);
    }
}

/*
6. Zigzag Conversion
Medium
7.1K
13.9K
Companies

The string "PAYPALISHIRING" is written in a zigzag pattern on a given number of rows like this: (you may want to display this pattern in a fixed font for better legibility)

P   A   H   N
A P L S I I G
Y   I   R

And then read line by line: "PAHNAPLSIIGYIR"

Write the code that will take a string and make this conversion given a number of rows:

string convert(string s, int numRows);

 

Example 1:

Input: s = "PAYPALISHIRING", numRows = 3
Output: "PAHNAPLSIIGYIR"

Example 2:

Input: s = "PAYPALISHIRING", numRows = 4
Output: "PINALSIGYAHRPI"
Explanation:
P     I    N
A   L S  I G
Y A   H R
P     I

Example 3:

Input: s = "A", numRows = 1
Output: "A"

 

Constraints:

    1 <= s.length <= 1000
    s consists of English letters (lower-case and upper-case), ',' and '.'.
    1 <= numRows <= 1000


*/