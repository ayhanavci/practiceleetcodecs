namespace Top150;

internal class SingleNumberII137
{
   
    public int SingleNumber(int[] nums) 
    {
        int result = 0;        
        int controller = 1;

        for (int i = 0; i < 32; ++i)
        {
            long count = 0;
            for (int j = 0; j < nums.Length; ++j)
            {
                count += nums[j] & controller;                
            }            
            if (count % 3 != 0)
                result |= controller;
            controller <<= 1;
        }
        
        return result;
    }
    public void Test()
    {                
        //0 0 0 0   0 0 0 0   0 0 0 0    0 0 0 0    0 0 0 0   0 0 0 0   0 0 0 0    1 1 0 0 
        
        //00111111 11111111 11111111 11111100 
        //11111111 11111111 11111111 11111100 
        var result3 = SingleNumber(new int[] {-2,-2,1,1,4,1,4,4,-4,-2});
        var result1 = SingleNumber(new int[] {2,2,3,2});
        var result2 = SingleNumber(new int[] {0,1,0,1,0,1,99});
    }  
}

/*
137. Single Number II
Medium
Topics
Companies

Given an integer array nums where every element appears three times except for one, which appears exactly once. Find the single element and return it.

You must implement a solution with a linear runtime complexity and use only constant extra space.

 

Example 1:

Input: nums = [2,2,3,2]
Output: 3

Example 2:

Input: nums = [0,1,0,1,0,1,99]
Output: 99

 

Constraints:

    1 <= nums.length <= 3 * 104
    -231 <= nums[i] <= 231 - 1
    Each element in nums appears exactly three times except for one element which appears once.


*/