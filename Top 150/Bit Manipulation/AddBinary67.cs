using System.Text;

namespace Top150;

internal class AddBinary67
{
    public string AddBinary(string a, string b) 
    {
        StringBuilder result = new StringBuilder();

        int aLength = a.Length;
        int bLength = b.Length;
        
        int i = aLength - 1;
        int j = bLength - 1;
        bool flow = false;

        while (i >= 0 && j >= 0)
        {
            if (a[i] == '0' && b[j] == '0')
            {
                if (flow) {
                    result.Append('1');
                    flow = false;
                }
                else
                    result.Append('0');
                    
            }
            else if ((a[i] == '1' && b[j] == '0') || (a[i] == '0' && b[j] == '1'))
            {
                if (flow)                
                    result.Append('0');
                else
                    result.Append('1');
                
            }            
            else //if (a[i] == '1' && b[j] == '1')
            {
                if (flow)
                    result.Append('1');
                else
                {
                    result.Append('0');
                    flow = true;
                }
            }
            i--;
            j--;
        }
        while (i >= 0)
        {
            if (a[i] == '0')
            {
                if (flow) {
                    result.Append('1');
                    flow = false;
                }
                else
                    result.Append('0');

            }
            else //if (a[i] == '1')
            {
                if (flow) 
                    result.Append('0');                                    
                else
                    result.Append('1');
            }
            i--;
        }
        while (j >= 0)
        {
            if (b[j] == '0')
            {
                if (flow) {
                    result.Append('1');
                    flow = false;
                }
                else
                    result.Append('0');

            }
            else //if (b[j] == '1')
            {
                if (flow) 
                    result.Append('0');                                    
                else
                    result.Append('1');
            }
            j--;
        }
        if (flow)
            result.Append('1');
        
        char[] arr = result.ToString().ToCharArray();
        Array.Reverse(arr);
        return new string(arr);
    }
    public void Test()
    {

    }
}

/*
67. Add Binary
Solved
Easy
Topics
Companies

Given two binary strings a and b, return their sum as a binary string.

 

Example 1:

Input: a = "11", b = "1"
Output: "100"

Example 2:

Input: a = "1010", b = "1011"
Output: "10101"

 

Constraints:

    1 <= a.length, b.length <= 104
    a and b consist only of '0' or '1' characters.
    Each string does not contain leading zeros except for the zero itself.


*/