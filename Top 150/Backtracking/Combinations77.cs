namespace Top150;

internal class Combinations77
{    
    IList<IList<int>> result;
    int k;
    int n;
    public IList<IList<int>> Combine(int n, int k) 
    {     
        this.k = k;           
        this.n = n;
        result = new List<IList<int>>();
        BackTrack(1, new List<int>());

        return result;
    }
    public void BackTrack(int start, List<int> combination)
    {
        if (combination.Count == k)
        {
            result.Add(new List<int>(combination));
            return;
        }

        for (int i = start; i < n + 1; ++i)
        {
            combination.Add(i);
            
            BackTrack(i + 1, combination);

            combination.RemoveAt(combination.Count - 1);
        }
    }
    //TODO: Slow 8%
    /*public IList<IList<int>> Combine(int n, int k) 
    {                
        Queue<int> numbers = new Queue<int>();
        for (int i = 1; i <= n; ++i)
            numbers.Enqueue(i);
        
        return Dfs(numbers, k);

    }
    public IList<IList<int>> Dfs(Queue<int> numbers, int k) 
    {        
        IList<IList<int>> result = new List<IList<int>>();
        if (k == 1)
        {   
            while (numbers.Count > 0)
                result.Add(new List<int>(){numbers.Dequeue()});
                        
            return result;
        }      
            
        while (numbers.Count > 0)
        {
            int number = numbers.Dequeue();
            
            Queue<int> newNumbers = new Queue<int>(numbers);

            var childArrays = Dfs(newNumbers, k-1);                            
            foreach (var arr in childArrays)
            {
                arr.Add(number);
                result.Add(arr);       
            }            
        }        

        return result;
    }*/
    /*public IList<IList<int>> Combine(int n, int k) 
    {                
        List<int> numbers = new List<int>();
        for (int i = 1; i <= n; ++i)
            numbers.Add(i);
        
        return Dfs(numbers, k);

    }
    public IList<IList<int>> Dfs(List<int> numbers, int k) 
    {        
        IList<IList<int>> result = new List<IList<int>>();
        if (k == 1)
        {   
            for (int i = 0; i < numbers.Count; ++i)               
                result.Add(new List<int>(){numbers[i]});
                        
            return result;
        }      
            
        while (numbers.Count > 0)
        {
            int number = numbers[0];            
            numbers.RemoveAt(0);
            List<int> newNumbers = new List<int>(numbers);

            var childArrays = Dfs(newNumbers, k-1);                            
            foreach (var arr in childArrays)
            {
                arr.Add(number);
                result.Add(arr);       
            }            
        }        

        return result;
    }*/
    public void Test()
    {
        var result1 = Combine(4, 2);
        var result2 = Combine(1, 1);
    }
}
/*
77. Combinations
Medium
Topics
Companies

Given two integers n and k, return all possible combinations of k numbers chosen from the range [1, n].

You may return the answer in any order.

 

Example 1:

Input: n = 4, k = 2
Output: [[1,2],[1,3],[1,4],[2,3],[2,4],[3,4]]
Explanation: There are 4 choose 2 = 6 total combinations.
Note that combinations are unordered, i.e., [1,2] and [2,1] are considered to be the same combination.

Example 2:

Input: n = 1, k = 1
Output: [[1]]
Explanation: There is 1 choose 1 = 1 total combination.

 

Constraints:

    1 <= n <= 20
    1 <= k <= n


*/