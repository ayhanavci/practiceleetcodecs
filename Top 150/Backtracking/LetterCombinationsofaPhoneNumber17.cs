using System.Text;

namespace Top150;

internal class LetterCombinationsofaPhoneNumber17
{
    public IList<string> LetterCombinations(string digits) 
    {
        IList<string> result = new List<string>();
        if (digits == string.Empty) return result;

        char ch = digits[0];        
        if (digits.Length == 1)
        {
            if (ch == '2')
            {                                
                result.Add("a");
                result.Add("b");                                                
                result.Add("c");
            }
            else if (ch == '3')
            {
                result.Add("d");
                result.Add("e");                                                
                result.Add("f");
            }
            else if (ch == '4')
            {
                result.Add("g");
                result.Add("h");                                                
                result.Add("i");
            }
            else if (ch == '5')
            {
                result.Add("j");
                result.Add("k");                                                
                result.Add("l");
            }
            else if (ch == '6')
            {
                result.Add("m");
                result.Add("n");                                                
                result.Add("o");
            }
            else if (ch == '7')
            {
                result.Add("p");
                result.Add("q");                                                
                result.Add("r");
                result.Add("s");
            }
            else if (ch == '8')
            {
                result.Add("t");
                result.Add("u");                                                
                result.Add("v");
            }
            else if (ch == '9')
            {
                result.Add("w");
                result.Add("x");                                                
                result.Add("y");
                result.Add("z");
            }
            
            return result;
        }
        
        digits = digits.Remove(0, 1);
        IList<string> children = LetterCombinations(digits);
        
        foreach (string child in children)
        {
            StringBuilder builder = new StringBuilder();
            if (ch == '2')
            {                
                builder.Append('a');
                builder.Append(child);
                result.Add(builder.ToString());
                
                builder.Clear();
                builder.Append('b');
                builder.Append(child);
                result.Add(builder.ToString());

                builder.Clear();
                builder.Append('c');
                builder.Append(child);
                result.Add(builder.ToString());
            }
            else if (ch == '3')
            {
                builder.Append('d');
                builder.Append(child);
                result.Add(builder.ToString());
                
                builder.Clear();
                builder.Append('e');
                builder.Append(child);
                result.Add(builder.ToString());

                builder.Clear();
                builder.Append('f');
                builder.Append(child);
                result.Add(builder.ToString());
            }
            else if (ch == '4')
            {
                builder.Append('g');
                builder.Append(child);
                result.Add(builder.ToString());
                
                builder.Clear();
                builder.Append('h');
                builder.Append(child);
                result.Add(builder.ToString());

                builder.Clear();
                builder.Append('i');
                builder.Append(child);
                result.Add(builder.ToString());
            }
            else if (ch == '5')
            {
                builder.Append('j');
                builder.Append(child);
                result.Add(builder.ToString());
                
                builder.Clear();
                builder.Append('k');
                builder.Append(child);
                result.Add(builder.ToString());

                builder.Clear();
                builder.Append('l');
                builder.Append(child);
                result.Add(builder.ToString());
            }
            else if (ch == '6')
            {
                builder.Append('m');
                builder.Append(child);
                result.Add(builder.ToString());
                
                builder.Clear();
                builder.Append('n');
                builder.Append(child);
                result.Add(builder.ToString());

                builder.Clear();
                builder.Append('o');
                builder.Append(child);
                result.Add(builder.ToString());
            }
            else if (ch == '7')
            {
                builder.Append('p');
                builder.Append(child);
                result.Add(builder.ToString());
                
                builder.Clear();
                builder.Append('q');
                builder.Append(child);
                result.Add(builder.ToString());

                builder.Clear();
                builder.Append('r');
                builder.Append(child);
                result.Add(builder.ToString());

                builder.Clear();
                builder.Append('s');
                builder.Append(child);
                result.Add(builder.ToString());
            }
            else if (ch == '8')
            {
                builder.Append('t');
                builder.Append(child);
                result.Add(builder.ToString());
                
                builder.Clear();
                builder.Append('u');
                builder.Append(child);
                result.Add(builder.ToString());

                builder.Clear();
                builder.Append('v');
                builder.Append(child);
                result.Add(builder.ToString());
            }
            else if (ch == '9')
            {
                builder.Append('w');
                builder.Append(child);
                result.Add(builder.ToString());
                
                builder.Clear();
                builder.Append('x');
                builder.Append(child);
                result.Add(builder.ToString());

                builder.Clear();
                builder.Append('y');
                builder.Append(child);
                result.Add(builder.ToString());

                builder.Clear();
                builder.Append('z');
                builder.Append(child);
                result.Add(builder.ToString());
            }
        }
        digits.Insert(0, ch.ToString());
        return result;
    }
    public void Test()
    {
        var result1 = LetterCombinations("23");
        var result2 = LetterCombinations("");
        var result3 = LetterCombinations("2");
    }
    
}

/*
17. Letter Combinations of a Phone Number
Solved
Medium
Topics
Companies

Given a string containing digits from 2-9 inclusive, return all possible letter combinations that the number could represent. Return the answer in any order.

A mapping of digits to letters (just like on the telephone buttons) is given below. Note that 1 does not map to any letters.

 

Example 1:

Input: digits = "23"
Output: ["ad","ae","af","bd","be","bf","cd","ce","cf"]

Example 2:

Input: digits = ""
Output: []

Example 3:

Input: digits = "2"
Output: ["a","b","c"]

 

Constraints:

    0 <= digits.length <= 4
    digits[i] is a digit in the range ['2', '9'].


*/