namespace Top150;

internal class Permutations46
{
    public IList<IList<int>> Permute(int[] nums) 
    {        
        return BackTrack(new List<int>(nums));
    }
    public IList<IList<int>> BackTrack(List<int> numbers)
    {
        IList<IList<int>> result = new List<IList<int>>();
        if (numbers.Count == 1)
        {
            result.Add(new List<int>() {numbers[0]});
            return result;
        }

        for (int i = 0; i < numbers.Count; ++i)
        {
            int number = numbers[i]; 
            numbers.RemoveAt(i);

            var branches = BackTrack(numbers);

            foreach (var branch in branches)
            {
                branch.Add(number);
                result.Add(branch);
            }
            
            numbers.Insert(i, number);
        }

        return result;
    }
    public void Test()
    {
        Test1();
    }
    public void Test1()
    {
        int[] nums = {1,2,3};
        var result = Permute(nums);
    }
    public void Test2()
    {
        int[] nums = {};
        var result = Permute(nums);
    }
}

/*
46. Permutations
Medium
Topics
Companies

Given an array nums of distinct integers, return all the possible permutations. You can return the answer in any order.

 

Example 1:

Input: nums = [1,2,3]
Output: [[1,2,3],[1,3,2],[2,1,3],[2,3,1],[3,1,2],[3,2,1]]

Example 2:

Input: nums = [0,1]
Output: [[0,1],[1,0]]

Example 3:

Input: nums = [1]
Output: [[1]]

 

Constraints:

    1 <= nums.length <= 6
    -10 <= nums[i] <= 10
    All the integers of nums are unique.


*/