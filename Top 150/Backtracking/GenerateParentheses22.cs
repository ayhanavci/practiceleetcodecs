using System.Text;

namespace Top150;

internal class GenerateParentheses22
{
    IList<string> result;
    int n;
    public IList<string> GenerateParenthesis(int n) 
    {
        this.n = n;
        result = new List<string>();

        BackTrack(0, 0, new StringBuilder());
        return result;
    }
    public void BackTrack(int open, int close, StringBuilder current)
    {
        if (open > n || close > n || close > open) return;
        if (open == n && open == close)
            result.Add(current.ToString());
        
        current.Append('(');
        BackTrack(open + 1, close, current);
        current.Remove(current.Length - 1, 1);
        
        current.Append(')');
        BackTrack(open, close + 1, current);
        current.Remove(current.Length - 1, 1);    
        
    }

    public void Test()
    {
        var result1 = GenerateParenthesis(3);//["((()))","(()())","(())()","()(())","()()()"]
        var result2 = GenerateParenthesis(1);//["()"]

    }
}
/*
22. Generate Parentheses
Medium
Topics
Companies

Given n pairs of parentheses, write a function to generate all combinations of well-formed parentheses.

 

Example 1:

Input: n = 3
Output: ["((()))","(()())","(())()","()(())","()()()"]

Example 2:

Input: n = 1
Output: ["()"]

 

Constraints:

    1 <= n <= 8


*/