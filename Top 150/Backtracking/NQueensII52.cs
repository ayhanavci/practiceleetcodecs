namespace Top150;

internal class NQueensII52
{
    int[][] board;
    int n;
    public int TotalNQueens(int n) 
    {
        this.n = n;
        board = new int[n][];
        for (int i = 0; i < n; ++i)        
            board[i] = new int[n];
        
        int count = 0;
        for (int col = 0; col < n; ++col)
        {                   
            int ret = BackTrack(0, col);                
            count += ret;
        }
        return count;
    }
    public int BackTrack(int row, int col)
    {
        if (row >= n || col >= n || board[row][col] != 0) 
            return 0;
        if (row == n - 1 && board[row][col] == 0) 
            return 1;
        
        Mark(row, col);
        
        int count = 0;        
        for (int j = 0; j < n; ++j)
        {                
            count += BackTrack(row + 1, j);
        }        

        UnMark(row, col);
       
        return count;
    }
  
    public void Mark(int row, int col)
    {
        //Mark queen diagonals
        int downRightCol = col + 1;
        int downLeftCol = col - 1;
        int downRow = row + 1;
        board[row][col] = 2;

        while (downRow < n)
        {
            if (downRightCol < n) 
            {
                board[downRow][downRightCol] += 1;
                downRightCol += 1;      
            }                
            if (downLeftCol >= 0)
            {
                board[downRow][downLeftCol] += 1;
                downLeftCol -= 1;
            }
            board[downRow][col] += 1;
            downRow += 1;      
        }
    }
    public void UnMark(int row, int col)
    {
        //Take back
        int downRightCol = col + 1;
        int downLeftCol = col - 1;
        int downRow = row + 1;
        board[row][col] = 0;
        while (downRow < n)
        {           
            if (downRightCol < n) 
            {
                board[downRow][downRightCol] -= 1;
                downRightCol += 1;      
            }                
            if (downLeftCol >= 0)
            {
                board[downRow][downLeftCol] -= 1;
                downLeftCol -= 1;
            }
            board[downRow][col] -= 1;
            
            downRow += 1;      
        }
        
    }
    public void Test()
    {
        //var result4 = TotalNQueens(4);
        var result1 = TotalNQueens(5);//o: 15 e:10
        //var result2 = TotalNQueens(1);
        //var result3 = TotalNQueens(3);
    }
}

/*
52. N-Queens II
Hard
Topics
Companies

The n-queens puzzle is the problem of placing n queens on an n x n chessboard such that no two queens attack each other.

Given an integer n, return the number of distinct solutions to the n-queens puzzle.

 

Example 1:

Input: n = 4
Output: 2
Explanation: There are two distinct solutions to the 4-queens puzzle as shown.

Example 2:

Input: n = 1
Output: 1

 

Constraints:

    1 <= n <= 9


*/