namespace Top150;

internal class CombinatioSum39
{
    public IList<IList<int>> CombinationSum(int[] candidates, int target) 
    {        
        IList<IList<int>> [] sums = new List<IList<int>>[target + 1];
        Array.Fill(sums, null);
        sums[0] = new List<IList<int>>
        {
            new List<int>()
        };        

        for (int i = 0; i < target + 1; ++i)
        {
            if (sums[i] == null)
                continue;
            for (int j = 0; j < candidates.Length; ++j)
            {
                if (i + candidates[j] <= target)
                {
                    if (sums[i + candidates[j]] == null)
                    {
                        sums[i + candidates[j]] = new List<IList<int>>();                        
                    }                    
                    foreach (var prev in sums[i])
                    {
                        List<int> newArr = new List<int>(prev);                        
                        if (prev.Count == 0 || prev.Last() <= candidates[j])
                        {
                            newArr.Add(candidates[j]);                        
                            //newArr.Sort();
                            sums[i + candidates[j]].Add(newArr);
                        }
                        
                    }
                }                
            }
        }
        if (sums[target] == null)
            return new List<IList<int>>();
        return sums[target];
    }
    public void Test()
    {
        Test1();
    }
    public void Test1()
    {
        int [] nums = {2,3,6,7};
        var result = CombinationSum(nums, 7);//[[2,2,3],[7]]
    }
    public void Test2()
    {
        int [] nums = {2};
        var result = CombinationSum(nums, 1);//[[2,2,3],[7]]
    }
}


/*
39. Combination Sum
Medium
Topics
Companies

Given an array of distinct integers candidates and a target integer target, return a list of all unique combinations of candidates where the chosen numbers sum to target. You may return the combinations in any order.

The same number may be chosen from candidates an unlimited number of times. Two combinations are unique if the
frequency
of at least one of the chosen numbers is different.

The test cases are generated such that the number of unique combinations that sum up to target is less than 150 combinations for the given input.

 

Example 1:

Input: candidates = [2,3,6,7], target = 7
Output: [[2,2,3],[7]]
Explanation:
2 and 3 are candidates, and 2 + 2 + 3 = 7. Note that 2 can be used multiple times.
7 is a candidate, and 7 = 7.
These are the only two combinations.

Example 2:

Input: candidates = [2,3,5], target = 8
Output: [[2,2,2,2],[2,3,3],[3,5]]

Example 3:

Input: candidates = [2], target = 1
Output: []

 

Constraints:

    1 <= candidates.length <= 30
    2 <= candidates[i] <= 40
    All elements of candidates are distinct.
    1 <= target <= 40


*/