using System.Text;

namespace Top150;

internal class WordSearch79
{
    char[][] board;
    int rowCount;
    int colCount;
    string word;
    public bool Exist(char[][] board, string word) 
    {
        this.board = board;
        this.word = word;
        rowCount = board.Length;
        colCount = board[0].Length;

        for (int i = 0; i < board.Length; ++i)        
            for (int j = 0; j < colCount; ++j)
                if (Dfs(i, j, new StringBuilder())) return true;
        
        return false;
    }
    public bool Dfs(int row, int col, StringBuilder current)
    {
        if (row < 0 || col < 0 || row >= rowCount || col >= colCount || board[row][col] == '#') return false;        
        if (current.Equals(word)) 
            return true;
        if (current.Length >= word.Length) 
            return false;

        char ch = board[row][col];
        current.Append(ch);
        if (current.Equals(word))
            return true;
        board[row][col] = '#';

        if (Dfs(row + 1, col, current))
            return true;
        if (Dfs(row - 1, col, current))
            return true;
        if (Dfs(row, col + 1, current))
            return true;
        if (Dfs(row, col - 1, current))
            return true; 
        
        current.Remove(current.Length - 1, 1);
        board[row][col] = ch;

        return false;
    }
    public void Test()
    {
        //Test1();
        //Test2();
        //Test3();
        Test4();
    }
    public void Test1()
    {
        char[][]board = new char[3][];
        board[0] = new char[]{'A','B','C','E'};//['S','F','C','S'],['A','D','E','E']]
        board[1] = new char[]{'S','F','C','S'};
        board[2] = new char[]{'A','D','E','E'};

        var result = Exist(board, "ABCCED");
    }
    public void Test2()//board = [["A","B","C","E"],["S","F","C","S"],["A","D","E","E"]], word = "SEE"
    {
        char[][]board = new char[3][];
        board[0] = new char[]{'A','B','C','E'};//['S','F','C','S'],['A','D','E','E']]
        board[1] = new char[]{'S','F','C','S'};
        board[2] = new char[]{'A','D','E','E'};

        var result = Exist(board, "SEE");
    }
    public void Test3()//board = [["A","B","C","E"],["S","F","C","S"],["A","D","E","E"]], word = "SEE"
    {
        char[][]board = new char[3][];
        board[0] = new char[]{'A','B','C','E'};//['S','F','C','S'],['A','D','E','E']]
        board[1] = new char[]{'S','F','C','S'};
        board[2] = new char[]{'A','D','E','E'};

        var result = Exist(board, "ABCB");
    }
     public void Test4()//board = [["A","B","C","E"],["S","F","C","S"],["A","D","E","E"]], word = "SEE"
    {
        char[][]board = new char[1][];
        board[0] = new char[]{'a'};//['S','F','C','S'],['A','D','E','E']]
        

        var result = Exist(board, "a");
    }
}
/*
79. Word Search
Medium
Topics
Companies

Given an m x n grid of characters board and a string word, return true if word exists in the grid.

The word can be constructed from letters of sequentially adjacent cells, where adjacent cells are horizontally or vertically neighboring. The same letter cell may not be used more than once.

 

Example 1:

Input: board = [["A","B","C","E"],["S","F","C","S"],["A","D","E","E"]], word = "ABCCED"
Output: true

Example 2:

Input: board = [["A","B","C","E"],["S","F","C","S"],["A","D","E","E"]], word = "SEE"
Output: true

Example 3:

Input: board = [["A","B","C","E"],["S","F","C","S"],["A","D","E","E"]], word = "ABCB"
Output: false

 

Constraints:

    m == board.length
    n = board[i].length
    1 <= m, n <= 6
    1 <= word.length <= 15
    board and word consists of only lowercase and uppercase English letters.

 

Follow up: Could you use search pruning to make your solution faster with a larger board?

*/