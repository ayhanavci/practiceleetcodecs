namespace Top150;

internal class RansomNote383
{
    public bool CanConstruct(string ransomNote, string magazine) 
    {
        Dictionary<char, int> ransomFrequency = new Dictionary<char, int>();
        Dictionary<char, int> magazineFrequency = new Dictionary<char, int>();
        
        for (int i = 0; i < ransomNote.Length; ++i)
            if (ransomFrequency.ContainsKey(ransomNote[i]))
                ransomFrequency[ransomNote[i]]++;
            else
                ransomFrequency.Add(ransomNote[i], 1);
        
        for (int i = 0; i < magazine.Length; ++i)
            if (magazineFrequency.ContainsKey(magazine[i]))
                magazineFrequency[magazine[i]]++;
            else
                magazineFrequency.Add(magazine[i], 1);

        foreach (var letter in ransomFrequency)
        {
            int value = 0;
            if (!magazineFrequency.TryGetValue(letter.Key, out value))
                return false;
            if (value > letter.Value)
                return false;
        }

        return true;
    }
    public void Test()
    {
        var result1 = CanConstruct("bg", "efjbdfbdgfjhhaiigfhbaejahgfbbgbjagbddfgdiaigdadhcfcj");
    }
}
/*
383. Ransom Note
Easy
4.7K
482
Companies

Given two strings ransomNote and magazine, return true if ransomNote can be constructed by using the letters from magazine and false otherwise.

Each letter in magazine can only be used once in ransomNote.

 

Example 1:

Input: ransomNote = "a", magazine = "b"
Output: false

Example 2:

Input: ransomNote = "aa", magazine = "ab"
Output: false

Example 3:

Input: ransomNote = "aa", magazine = "aab"
Output: true

 

Constraints:

    1 <= ransomNote.length, magazine.length <= 105
    ransomNote and magazine consist of lowercase English letters.


*/