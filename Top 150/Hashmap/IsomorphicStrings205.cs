namespace Top150;

internal class IsomorphicStrings205
{
    public bool IsIsomorphic(string s, string t) 
    {
        char[] sMap = new char[128];
        char[] tMap = new char[128];

        for (int i = 0; i < s.Length; ++i)
        {
            if (sMap[s[i]] > 0)
            {
                var sChar = sMap[s[i]];
                if (t[i] != sChar)
                    return false;
                
            }
            if (tMap[t[i]] > 0)
            {
                var tChar = tMap[t[i]];
                if (s[i] != tChar)
                    return false;
            }

            sMap[s[i]] = t[i];
            tMap[t[i]] = s[i];
            
        }

        return true;
    }
    public void Test()
    {
        var result1 = IsIsomorphic("egg", "add");
        var result2 = IsIsomorphic("foo", "bar");
        var result3 = IsIsomorphic("paper", "title");
        var result4 = IsIsomorphic("badc", "baba");
        var result5 = IsIsomorphic("badc", "ckca");
    }
}

/*
205. Isomorphic Strings
Easy
8K
1.9K
Companies

Given two strings s and t, determine if they are isomorphic.

Two strings s and t are isomorphic if the characters in s can be replaced to get t.

All occurrences of a character must be replaced with another character while preserving the order of characters. No two characters may map to the same character, but a character may map to itself.

 

Example 1:

Input: s = "egg", t = "add"
Output: true

Example 2:

Input: s = "foo", t = "bar"
Output: false

Example 3:

Input: s = "paper", t = "title"
Output: true

 

Constraints:

    1 <= s.length <= 5 * 104
    t.length == s.length
    s and t consist of any valid ascii character.


*/