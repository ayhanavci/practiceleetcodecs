namespace Top150;

internal class ValidAnagram242
{
    public bool IsAnagram(string s, string t) 
    {
        if (s.Length != t.Length) return false;

        Dictionary<char, int> sMap = new Dictionary<char, int>();
        Dictionary<char, int> tMap = new Dictionary<char, int>();

        for (int i = 0; i < s.Length; ++i)
        {
            if (sMap.ContainsKey(s[i])) sMap[s[i]]++;
            else sMap.Add(s[i], 1);
            
            if (tMap.ContainsKey(t[i])) tMap[t[i]]++;
            else tMap.Add(t[i], 1);
        }

        foreach (var pair in sMap)
        {
            if (tMap.ContainsKey(pair.Key)) 
            {
                if (tMap[pair.Key] != pair.Value) 
                    return false;            
            }     
                
            else 
                return false;
        }

        foreach (var pair in tMap)
        {
            if (sMap.ContainsKey(pair.Key))            
            {
                if (sMap[pair.Key] != pair.Value) 
                    return false;            
            }
                
            else 
                return false;
        }
        return true;
    }
    public void Test()
    {
        var result1 = IsAnagram("anagram", "nagaram");
    }
}
/*
242. Valid Anagram
Easy
11.5K
364
Companies

Given two strings s and t, return true if t is an anagram of s, and false otherwise.

An Anagram is a word or phrase formed by rearranging the letters of a different word or phrase, typically using all the original letters exactly once.

 

Example 1:

Input: s = "anagram", t = "nagaram"
Output: true

Example 2:

Input: s = "rat", t = "car"
Output: false

 

Constraints:

    1 <= s.length, t.length <= 5 * 104
    s and t consist of lowercase English letters.

 

Follow up: What if the inputs contain Unicode characters? How would you adapt your solution to such a case?

*/