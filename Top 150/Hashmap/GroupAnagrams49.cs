using System.Collections;

namespace Top150;

internal class GroupAnagrams49
{   
    public IList<IList<string>> GroupAnagrams(string[] strs) 
    {
        IList<IList<string>> retval = new List<IList<string>>();
        Dictionary<string, List<string>> map = new Dictionary<string, List<string>>();

        foreach (string word in strs)
        {
            char[] chars = word.ToCharArray();
            Array.Sort(chars);

            string sortedWord = new(chars);
            if (!map.ContainsKey(sortedWord))                            
                map.Add(sortedWord, new List<string>());
            
            map[sortedWord].Add(word);            
        }
        
        foreach (var pair in map)
            retval.Add(pair.Value);
        return retval;
    }
    
    public IList<IList<string>> GroupAnagrams_PASSES_BUT_STILLSLOW(string[] strs) 
    {
        IList<IList<string>> retval = new List<IList<string>>();

        List<string> strings = new List<string>(strs);

        while (strings.Count > 0)
        {
            List<string> nextAnagram = new List<string>();
            string reference = strings[0];            
            strings.RemoveAt(0);            
            nextAnagram.Add(reference);
            Dictionary<char, int> referenceMap = new Dictionary<char, int>();
            int referenceLength = reference.Length;

            for (int i = 0; i < referenceLength; ++i)
            {
                if (referenceMap.ContainsKey(reference[i]))
                    referenceMap[reference[i]]++;
                else
                    referenceMap.Add(reference[i], 1);
            }
            

            for (int i = 0; i < strings.Count; ++i)
            {
                string testItem = strings[i];
                Dictionary<char, int> testMap = new Dictionary<char, int>();
                int testLength = testItem.Length;
                if (testLength != referenceLength)
                    continue;

                for (int j = 0; j < testLength; ++j)
                {
                    if (testMap.ContainsKey(testItem[j]))
                        testMap[testItem[j]]++;
                    else
                        testMap.Add(testItem[j], 1);
                }
                bool match = true;
                foreach (var pair in referenceMap)
                {
                    if (!testMap.ContainsKey(pair.Key) || testMap[pair.Key] != pair.Value)
                    {
                        match = false;
                        break;    
                    }                                            
                }
                if (match)
                {
                    nextAnagram.Add(testItem);
                    strings.RemoveAt(i);
                    i--;
                }
                
            }
            retval.Add(nextAnagram);
        }

        return retval;
    }
    
    public void Test()
    {
        string[] strs1 = {"eat","tea","tan","ate","nat","bat"};
        var result1 = GroupAnagrams(strs1);
    }
    public IList<IList<string>> GroupAnagrams_TIMEOUT(string[] strs) 
    {
        IList<IList<string>> retval = new List<IList<string>>();
        List<string> strings = new List<string>(strs);

        while (strings.Count > 0)
        {
            List<string> nextAnagram = new List<string>();
            string reference = strings[0];            
            strings.RemoveAt(0);            
            nextAnagram.Add(reference);
            reference = Sort(reference);

            for (int i = 0; i < strings.Count; ++i)
            {
                string nextItem = strings[i];
                if (reference.Equals(Sort(nextItem)))
                {
                    nextAnagram.Add(nextItem);
                    strings.RemoveAt(i);
                    i--;
                }
            }
            retval.Add(nextAnagram);
        }


        return retval;
    }
    private string Sort(string input)
    {
        char[] chars = input.ToCharArray();
        Array.Sort(chars);
        return new string(chars);
    }
}
/*
49. Group Anagrams
Medium
18K
538
Companies

Given an array of strings strs, group the anagrams together. You can return the answer in any order.

An Anagram is a word or phrase formed by rearranging the letters of a different word or phrase, typically using all the original letters exactly once.

 

Example 1:

Input: strs = ["eat","tea","tan","ate","nat","bat"]
Output: [["bat"],["nat","tan"],["ate","eat","tea"]]

Example 2:

Input: strs = [""]
Output: [[""]]

Example 3:

Input: strs = ["a"]
Output: [["a"]]

 

Constraints:

    1 <= strs.length <= 104
    0 <= strs[i].length <= 100
    strs[i] consists of lowercase English letters.


*/