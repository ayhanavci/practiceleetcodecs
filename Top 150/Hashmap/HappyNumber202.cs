namespace Top150;

internal class HappyNumber202
{
    public bool IsHappy(int n) 
    {
        HashSet<int> visits = new HashSet<int>();

        while (!visits.Contains(n))
        {
            visits.Add(n);
            int next = 0;
            
            while (n > 0)
            {
                int digit = n % 10;
                n /= 10;
                next += digit * digit;
            }
            if (next == 1) return true;
            n = next;            
        }

        return false;
    }
    public bool IsHappy2(int n)
    {
        int slow = n;
        int fast = n;

        do
        {
            slow = getDigitSquare(slow);
            fast = getDigitSquare(getDigitSquare(fast));            
            if (fast == 1) return true;
        } while (slow != fast);

        return false;
    }
    public int getDigitSquare(int n)
    {
        int retVal = 0;

        while (n > 0)
        {
            int digit = n % 10;
            retVal += digit * digit;
            n /= 10;
        }

        return retVal;
    }

    public void Test()
    {
        var result1 = IsHappy2(19);
        var result2 = IsHappy2(2);
    }
}
/*
202. Happy Number
Easy
9.9K
1.3K
Companies

Write an algorithm to determine if a number n is happy.

A happy number is a number defined by the following process:

    Starting with any positive integer, replace the number by the sum of the squares of its digits.
    Repeat the process until the number equals 1 (where it will stay), or it loops endlessly in a cycle which does not include 1.
    Those numbers for which this process ends in 1 are happy.

Return true if n is a happy number, and false if not.

 

Example 1:

Input: n = 19
Output: true
Explanation:
12 + 92 = 82
82 + 22 = 68
62 + 82 = 100
12 + 02 + 02 = 1

Example 2:

Input: n = 2
Output: false

 

Constraints:

    1 <= n <= 231 - 1


*/