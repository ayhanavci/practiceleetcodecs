namespace Top150;

internal class ContainsDuplicateII219
{
    //As you loop upwards, remove the items that cannot satisfy Abs(i - j) <= k anymore. Because i(current index) became too large for the early occurance (j).
    public bool ContainsNearbyDuplicate(int[] nums, int k) 
    {
        HashSet<int> items = new HashSet<int>();

        int minimumIndex = 0;
        for (int i = 0; i < nums.Length; ++i)
        {
            if (items.Contains(nums[i]))
                return true;
            items.Add(nums[i]);

            if (i > k)                            
                items.Remove(minimumIndex++);            
            
        }

        return false;
    }
    //That's my original solution.
    public bool ContainsNearbyDuplicate2(int[] nums, int k) 
    {
        Dictionary<int, List<int>> numberDict = new Dictionary<int, List<int>>();

        for (int i = 0; i < nums.Length; ++i)
        {
            if (numberDict.ContainsKey(nums[i]))
            {
                List<int> indices = numberDict[nums[i]];
                foreach (int j in indices)
                {
                    if (Math.Abs(i - j) <= k)
                        return true;
                }
                indices.Add(i);
            }
            else
            {
                List<int> indeces = new List<int> { i };
                numberDict.Add(nums[i], indeces);
            }
        }

        return false;
    }
    public void Test()
    {

    }
}
/*
219. Contains Duplicate II
Easy
5.8K
3K
Companies

Given an integer array nums and an integer k, return true if there are two distinct indices i and j in the array such that nums[i] == nums[j] and abs(i - j) <= k.

 

Example 1:

Input: nums = [1,2,3,1], k = 3
Output: true

Example 2:

Input: nums = [1,0,1,1], k = 1
Output: true

Example 3:

Input: nums = [1,2,3,1,2,3], k = 2
Output: false

 

Constraints:

    1 <= nums.length <= 105
    -109 <= nums[i] <= 109
    0 <= k <= 105


*/