namespace Top150;

internal class WordPattern290
{
    public bool WordPattern(string pattern, string s) 
    {
        Dictionary<char, string> patternMap = new Dictionary<char, string>();        
        Dictionary<string, char> wordMap = new Dictionary<string, char>();

        string[] words = s.Split(' ');

        int patternLength = pattern.Length;
        if (patternLength != words.Length) return false;

        for (int i = 0; i < patternLength; ++i)
        {
            if (patternMap.ContainsKey(pattern[i]))
            {
                string word = patternMap[pattern[i]];
                if (word != words[i])
                    return false;                
            }
            else if (wordMap.ContainsKey(words[i]))
            {
                return false;
            }
            else
            {
                patternMap.Add(pattern[i], words[i]);
                wordMap.Add(words[i], pattern[i]);
            }
        }

        return true;
    }
    public void Test()
    {  
        var result1 = WordPattern("abba", "dog cat cat dog");
        var result2 = WordPattern("abba", "dog cat cat fish");
        var result3 = WordPattern("aaaa", "dog cat cat dog");
    }
}
/*
290. Word Pattern
Easy
7K
919
Companies

Given a pattern and a string s, find if s follows the same pattern.

Here follow means a full match, such that there is a bijection between a letter in pattern and a non-empty word in s.

 

Example 1:

Input: pattern = "abba", s = "dog cat cat dog"
Output: true

Example 2:

Input: pattern = "abba", s = "dog cat cat fish"
Output: false

Example 3:

Input: pattern = "aaaa", s = "dog cat cat dog"
Output: false

 

Constraints:

    1 <= pattern.length <= 300
    pattern contains only lower-case English letters.
    1 <= s.length <= 3000
    s contains only lowercase English letters and spaces ' '.
    s does not contain any leading or trailing spaces.
    All the words in s are separated by a single space.


*/