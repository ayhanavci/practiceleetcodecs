namespace Top150;

internal class TwoSum1
{
    public int[] TwoSum(int[] nums, int target) 
    {
        int[] retval = new int[2];
        Dictionary<int, int> numbers = new Dictionary<int, int>();

        for (int i = 0; i < nums.Length; ++i)
        {
            int partner = target - nums[i];
            if (numbers.ContainsKey(partner))
            {
                retval[0] = i;
                retval[1] = numbers[partner];
                return retval;
            } 
            if (!numbers.ContainsKey(nums[i]))
                numbers.Add(nums[i], i);
        }
        
        return retval;
    }
    public void Test()
    {
        int [] nums1 = {1,1,1,1,1,4,1,1,1,1,1,7,1,1,1,1,1};
        var result1 = TwoSum(nums1, 11);
    }
}

/*
1. Two Sum
Easy
54.3K
1.8K
Companies

Given an array of integers nums and an integer target, return indices of the two numbers such that they add up to target.

You may assume that each input would have exactly one solution, and you may not use the same element twice.

You can return the answer in any order.

 

Example 1:

Input: nums = [2,7,11,15], target = 9
Output: [0,1]
Explanation: Because nums[0] + nums[1] == 9, we return [0, 1].

Example 2:

Input: nums = [3,2,4], target = 6
Output: [1,2]

Example 3:

Input: nums = [3,3], target = 6
Output: [0,1]

 

Constraints:

    2 <= nums.length <= 104
    -109 <= nums[i] <= 109
    -109 <= target <= 109
    Only one valid answer exists.

 
Follow-up: Can you come up with an algorithm that is less than O(n2) time complexity?
*/