namespace Top150;

internal class BinaryTreeRightSideView199
{
    //Definition for a binary tree node.
    public class TreeNode
    {
        public int val;
        public TreeNode left;
        public TreeNode right;
        public TreeNode(int val = 0, TreeNode left = null, TreeNode right = null)
        {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }

    Dictionary<int, int> depthMap;
    IList<int> result;   
    public IList<int> RightSideView(TreeNode root) 
    {                     
        depthMap = new Dictionary<int, int>();
        result = new List<int>();   
        PopulateRightSide(root, 1);
       
        return result;
    }
    public void PopulateRightSide(TreeNode root, int depth)
    {
        if (root == null) return;
        if (!depthMap.ContainsKey(depth))   
        {
            depthMap.Add(depth, root.val);
            result.Add(root.val);
        }                 
        
        PopulateRightSide(root.right, depth + 1);
        PopulateRightSide(root.left, depth + 1);        
    }

    public void Test()
    {
        Test1();
        Test2();
        Test3();
    }

    public void Test1()
    {                
        TreeNode l = new TreeNode(2)
        {
            right = new TreeNode(5),            
        };
        
        TreeNode r = new TreeNode(3)
        {
            right = new TreeNode(4)
        };


        TreeNode root = new TreeNode(1)
        {
            right = r,
            left = l
        };

        var result = RightSideView(root);
    }
    public void Test2()
    {        
        

        
        TreeNode r = new TreeNode(3)
        {
           
        };


        TreeNode root = new TreeNode(1)
        {
            right = r,
           
        };

        var result = RightSideView(root);
    }
     public void Test3()
    {        


        var result = RightSideView(null);
    }
}

/*

199. Binary Tree Right Side View
Medium
Topics
Companies

Given the root of a binary tree, imagine yourself standing on the right side of it, return the values of the nodes you can see ordered from top to bottom.

 

Example 1:

Input: root = [1,2,3,null,5,null,4]
Output: [1,3,4]

Example 2:

Input: root = [1,null,3]
Output: [1,3]

Example 3:

Input: root = []
Output: []

 

Constraints:

    The number of nodes in the tree is in the range [0, 100].
    -100 <= Node.val <= 100

*/