namespace Top150;

internal class BinaryTreeLevelOrderTraversal102
{
    //TODO: SUBMIT
    //Definition for a binary tree node.
    public class TreeNode
    {
        public int val;
        public TreeNode left;
        public TreeNode right;
        public TreeNode(int val = 0, TreeNode left = null, TreeNode right = null)
        {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }
    IList<IList<int>> result;
    public IList<IList<int>> LevelOrder(TreeNode root) 
    {
        result = new List<IList<int>>();

        Traverse(root, 1);
        
        return result;
    }   
    public void Traverse(TreeNode root, int level)
    {
        if (root == null) return;

        if (result.Count < level)        
            result.Add(new List<int>() { root.val });
        else
            result[level-1].Add(root.val);

        Traverse(root.left, level + 1);
        Traverse(root.right, level + 1);
    }

    public void Test()
    {

    }
}

/*
102. Binary Tree Level Order Traversal
Medium
Topics
Companies

Given the root of a binary tree, return the level order traversal of its nodes' values. (i.e., from left to right, level by level).

 

Example 1:

Input: root = [3,9,20,null,null,15,7]
Output: [[3],[9,20],[15,7]]

Example 2:

Input: root = [1]
Output: [[1]]

Example 3:

Input: root = []
Output: []

 

Constraints:

    The number of nodes in the tree is in the range [0, 2000].
    -1000 <= Node.val <= 1000


*/