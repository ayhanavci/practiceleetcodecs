namespace Top150;

internal class BinaryTreeZigzagLevelOrderTraversal103
{
    //Definition for a binary tree node.
    public class TreeNode
    {
        public int val;
        public TreeNode left;
        public TreeNode right;
        public TreeNode(int val = 0, TreeNode left = null, TreeNode right = null)
        {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }
    List<IList<int>> retVal;
    public IList<IList<int>> ZigzagLevelOrder(TreeNode root) 
    {
        retVal = new List<IList<int>>();
        
        Traverse(root, 0, true);

        return retVal;
    }
    public void Traverse(TreeNode root, int level, bool leftToRight)
    {
        if (root == null) return;
        IList<int> currentLevel;
        if (retVal.Count <= level)
        {
            currentLevel = new List<int>();
            retVal.Add(currentLevel);
        }
        else
        {
            currentLevel = retVal[level];
        }
        if (leftToRight)
        {
            currentLevel.Add(root.val);
        }
        else
        {
            if (currentLevel.Count == 0)
                currentLevel.Add(root.val);
            else
                currentLevel.Insert(0, root.val);
        }
        

        leftToRight = !leftToRight;
        Traverse(root.left, level + 1, leftToRight);
        Traverse(root.right, level + 1, leftToRight);

    }
    public void Test()
    {
        Test1();
        Test2();
    }
    public void Test1()
    {                
        TreeNode l = new TreeNode(9);
        
        TreeNode r = new TreeNode(20)
        {
            right = new TreeNode(7),
            left = new TreeNode(15)
            
        };


        TreeNode root = new TreeNode(3)
        {
            right = r,
            left = l
        };

        var result = ZigzagLevelOrder(root);
    }
    public void Test2()
    {                
        TreeNode l = new TreeNode(2)
        {            
            left = new TreeNode(4)            
        };
        
        TreeNode r = new TreeNode(3)
        {
            right = new TreeNode(5),                    
        };


        TreeNode root = new TreeNode(1)
        {
            right = r,
            left = l
        };

        var result = ZigzagLevelOrder(root);
    }
}

/*
103. Binary Tree Zigzag Level Order Traversal
Medium
Topics
Companies

Given the root of a binary tree, return the zigzag level order traversal of its nodes' values. (i.e., from left to right, then right to left for the next level and alternate between).

 

Example 1:

Input: root = [3,9,20,null,null,15,7]
Output: [[3],[20,9],[15,7]]

Example 2:

Input: root = [1]
Output: [[1]]

Example 3:

Input: root = []
Output: []

 

Constraints:

    The number of nodes in the tree is in the range [0, 2000].
    -100 <= Node.val <= 100


*/