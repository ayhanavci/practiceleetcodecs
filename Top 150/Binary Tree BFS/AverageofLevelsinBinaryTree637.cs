namespace Top150;

internal class AverageofLevelsinBinaryTree637
{    
    //Definition for a binary tree node.
    public class TreeNode
    {
        public int val;
        public TreeNode left;
        public TreeNode right;
        public TreeNode(int val = 0, TreeNode left = null, TreeNode right = null)
        {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }
    
    Dictionary<int, List<int>> levelMap;
    public IList<double> AverageOfLevels(TreeNode root) 
    {
        IList<double> result = new List<double>();
        levelMap = new Dictionary<int, List<int>>();
        Traverse(root, 0);

        for (int i = 0; i < levelMap.Count; ++i)
        {
            var valList = levelMap[i];
            double sum = 0;
            foreach (var val in valList)
                sum += val;
            result.Add(sum / valList.Count);
        }
        return result;
    }
    public void Traverse(TreeNode root, int level)
    {
        if (root == null) return;

        List<int> valList;
        if (levelMap.TryGetValue(level, out valList))        
            valList.Add(root.val);        
        else        
            levelMap.Add(level, new List<int>() {root.val});

        Traverse(root.left, level + 1);        
        Traverse(root.right, level + 1);
    }
    public void Test()
    {

    }
}

/*
637. Average of Levels in Binary Tree
Solved
Easy
Topics
Companies
Given the root of a binary tree, return the average value of the nodes on each level in the form of an array. Answers within 10-5 of the actual answer will be accepted.

 

Example 1:

Input: root = [3,9,20,null,null,15,7]
Output: [3.00000,14.50000,11.00000]
Explanation: The average value of nodes on level 0 is 3, on level 1 is 14.5, and on level 2 is 11.
Hence return [3, 14.5, 11].

Example 2:

Input: root = [3,9,20,15,7]
Output: [3.00000,14.50000,11.00000]

 

Constraints:

    The number of nodes in the tree is in the range [1, 104].
    -231 <= Node.val <= 231 - 1


*/