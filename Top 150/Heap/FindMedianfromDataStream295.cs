namespace Top150;

internal class FindMedianfromDataStream295
{
    
    public class MedianFinder
    {
        PriorityQueue<int, int> smallHalfHeap;
        PriorityQueue<int, int> bigHalfHeap;
        public MedianFinder()
        {
            smallHalfHeap = new PriorityQueue<int, int>(Comparer<int>.Create((x, y) => y - x)); //Max heap for small part
            bigHalfHeap = new PriorityQueue<int, int>();//Min heap for big part
        }

        public void AddNum(int num)
        {
            if (smallHalfHeap.Count == 0)
            {
                smallHalfHeap.Enqueue(num, num);
                return;
            }
            int maxOfSmalls;
            if (bigHalfHeap.Count == 0)
            {
                maxOfSmalls = smallHalfHeap.Peek();    
                if (maxOfSmalls > num)
                {
                    int temp = smallHalfHeap.Dequeue();
                    bigHalfHeap.Enqueue(temp, temp);
                    smallHalfHeap.Enqueue(num, num);
                }
                else
                    bigHalfHeap.Enqueue(num, num);
                return;
            }

            int minOfBigs = bigHalfHeap.Peek();
            maxOfSmalls = smallHalfHeap.Peek();


            if (num >= maxOfSmalls)//ie. for 5: smalls:1,2 
            {
                if (num >= minOfBigs)//ie. for 5: smalls:1,2 bigs:3,4
                {
                    if (bigHalfHeap.Count > smallHalfHeap.Count)
                    {
                        int temp = bigHalfHeap.Dequeue();
                        smallHalfHeap.Enqueue(temp, temp);
                        bigHalfHeap.Enqueue(num, num);
                    }
                    else
                    {
                        bigHalfHeap.Enqueue(num, num);
                    }
                }
                else //num < minOfBigs.ie. for 5: smalls:1,2 bigs:10,11
                {
                    if (smallHalfHeap.Count > bigHalfHeap.Count) //ie. for 5: smalls:1,2,3 bigs:10,11
                    {
                        bigHalfHeap.Enqueue(num, num);
                    }
                    else //ie. for 5: smalls:1,2 bigs:10,11,12 OR smalls:1,2,3 bigs:10,11,12
                    {
                        smallHalfHeap.Enqueue(num, num);
                    }
                }
            }    
            else //num < maxOfSmalls. ie. for 5: smalls:3,4,6 bigs:10,11,12. num should always be inserted into smalls
            {
                if (bigHalfHeap.Count >= smallHalfHeap.Count)//ie. for 5: smalls:3,4,6 bigs:10,11,12 OR smalls:3,6 bigs:10,11,12
                {
                    smallHalfHeap.Enqueue(num, num);
                }
                else //ie. for 5: smalls:1,3,4,6 bigs:10,11,12 
                {
                    int temp = smallHalfHeap.Dequeue();
                    bigHalfHeap.Enqueue(temp, temp);
                    smallHalfHeap.Enqueue(num, num);
                }
            }        
            
        }

        public double FindMedian()
        {
            int smallHalfCount = smallHalfHeap.Count;
            int bigHalfCount = bigHalfHeap.Count;
            int total = smallHalfCount + bigHalfCount;

            if (bigHalfCount == smallHalfCount)//even            
                return ((double) bigHalfHeap.Peek() + smallHalfHeap.Peek()) / 2;            
            
            //odd
            return bigHalfCount > smallHalfCount ? bigHalfHeap.Peek() : smallHalfHeap.Peek();
        }
    }
    public void Test()
    {
        Test1();
        Test2();
    }
    public void Test1()
    {
        MedianFinder medianFinder = new MedianFinder();
        medianFinder.AddNum(1);    // arr = [1]
        medianFinder.AddNum(2);    // arr = [1, 2]
        var result1 = medianFinder.FindMedian(); // return 1.5 (i.e., (1 + 2) / 2)
        medianFinder.AddNum(3);    // arr[1, 2, 3]
        var result2 = medianFinder.FindMedian(); // return 2.0

        //[null,null,null,1.00000,null,1.00000]
    }
    public void Test2()
    {
        MedianFinder medianFinder = new MedianFinder();
        medianFinder.AddNum(-1);    // arr = [-1]
        var result1 = medianFinder.FindMedian(); //-1 
        medianFinder.AddNum(-2);    // arr = [-2, -1]        
        var result2 = medianFinder.FindMedian(); //-1.5
        medianFinder.AddNum(-3);    // arr[-3, -2, -1]
        var result3 = medianFinder.FindMedian(); //-2
        medianFinder.AddNum(-4);    // arr[-4, -3, -2, -1]
        var result4 = medianFinder.FindMedian(); //-2.5
        medianFinder.AddNum(-5);    // arr[-5, -4, -3, -2, -1]
        var result5 = medianFinder.FindMedian(); //-3

        //o[null,null,-1.00000,null,-1.50000,null,-1.00000,null,-2.50000,null,-3.00000]
        //e[null,null,-1.00000,null,-1.50000,null,-2.00000,null,-2.50000,null,-3.00000]
        //["MedianFinder","addNum","findMedian","addNum","findMedian","addNum","findMedian","addNum","findMedian","addNum","findMedian"]
        //[[],[-1],[],[-2],[],[-3],[],[-4],[],[-5],[]]
    }
}
/*
295. Find Median from Data Stream
Hard
Topics
Companies

The median is the middle value in an ordered integer list. If the size of the list is even, there is no middle value, and the median is the mean of the two middle values.

    For example, for arr = [2,3,4], the median is 3.
    For example, for arr = [2,3], the median is (2 + 3) / 2 = 2.5.

Implement the MedianFinder class:

    MedianFinder() initializes the MedianFinder object.
    void addNum(int num) adds the integer num from the data stream to the data structure.
    double findMedian() returns the median of all elements so far. Answers within 10-5 of the actual answer will be accepted.

 

Example 1:

Input
["MedianFinder", "addNum", "addNum", "findMedian", "addNum", "findMedian"]
[[], [1], [2], [], [3], []]
Output
[null, null, null, 1.5, null, 2.0]

Explanation
MedianFinder medianFinder = new MedianFinder();
medianFinder.addNum(1);    // arr = [1]
medianFinder.addNum(2);    // arr = [1, 2]
medianFinder.findMedian(); // return 1.5 (i.e., (1 + 2) / 2)
medianFinder.addNum(3);    // arr[1, 2, 3]
medianFinder.findMedian(); // return 2.0

 

Constraints:

    -105 <= num <= 105
    There will be at least one element in the data structure before calling findMedian.
    At most 5 * 104 calls will be made to addNum and findMedian.

 

Follow up:

    If all integer numbers from the stream are in the range [0, 100], how would you optimize your solution?
    If 99% of all integer numbers from the stream are in the range [0, 100], how would you optimize your solution?


*/