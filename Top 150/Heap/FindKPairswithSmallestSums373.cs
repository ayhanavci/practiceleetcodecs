namespace Top150;

internal class FindKPairswithSmallestSums373
{
    //TODO: Slow 17%
    public IList<IList<int>> KSmallestPairs(int[] nums1, int[] nums2, int k) 
    {
        PriorityQueue<Tuple<int, int>, int> minHeap = new PriorityQueue<Tuple<int, int>, int>();
        HashSet<Tuple<int, int>> visited = new HashSet<Tuple<int, int>>();

        List<IList<int>> result = new List<IList<int>>();

        minHeap.Enqueue(new Tuple<int, int>(0, 0), nums1[0] + nums2[0]);
        visited.Add(new Tuple<int, int>(0, 0));

        while (k > 0 && minHeap.Count > 0)
        {
            var tp = minHeap.Dequeue();
            int n1 = tp.Item1;
            int n2 = tp.Item2;
            List<int> nextList = new List<int>()
            {
                nums1[n1],
                nums2[n2]
            }; 

            result.Add(nextList);
            k--;

            int nextn1Index = n1 + 1;
            int nextn2Index = n2 + 1;
            
            if (nextn1Index < nums1.Length && !visited.Contains(new Tuple<int, int>(nextn1Index, n2)))
            {
                minHeap.Enqueue(new Tuple<int, int>(nextn1Index, n2), nums1[nextn1Index] + nums2[n2]);
                visited.Add(new Tuple<int, int>(nextn1Index, n2));
            }
            if (nextn2Index < nums2.Length && !visited.Contains(new Tuple<int, int>(n1, nextn2Index)))
            {
                minHeap.Enqueue(new Tuple<int, int>(n1, nextn2Index), nums1[n1] + nums2[nextn2Index]);
                visited.Add(new Tuple<int, int>(n1, nextn2Index));
            }
        }

        return result;
    }

    /*public IList<IList<int>> KSmallestPairs(int[] nums1, int[] nums2, int k) 
    {
        PriorityQueue<Tuple<int, int>, int> minHeap = new PriorityQueue<Tuple<int, int>, int>();
        List<IList<int>> result = new List<IList<int>>();

        for (int i = 0; i < nums1.Length; ++i)
            for (int j = 0; j < nums2.Length; ++j)
                minHeap.Enqueue(new Tuple<int, int>(nums1[i], nums2[j]), nums1[i] + nums2[j]);

        for (int i = 0; i < k; ++i)
        {
            var tp = minHeap.Dequeue();
            List<int> nextList = new List<int>()
            {
                tp.Item1,
                tp.Item2
            }; 
            result.Add(nextList);
            if (minHeap.Count == 0)
                break;
        }
        return result;
    }*/
    /*public IList<IList<int>> KSmallestPairs(int[] nums1, int[] nums2, int k) 
    {
        PriorityQueue<int, int> firstMinHeap = new PriorityQueue<int, int>();
        PriorityQueue<int, int> secondMinHeap = new PriorityQueue<int, int>();

        for (int i = 0; i < nums1.Length; ++i)
            firstMinHeap.Enqueue(nums1[i], nums1[i]);
        
        for (int i = 0; i < nums2.Length; ++i)
            secondMinHeap.Enqueue(nums2[i], nums2[i]);

        List<IList<int>> result = new List<IList<int>>();

        int minFirst = firstMinHeap.Dequeue();
        int minSecond = secondMinHeap.Dequeue();
        List<int> nextList = new List<int>()
        {
            minFirst,
            minSecond
        };            
        result.Add(nextList);
        k--;
     
        while (k > 0 && firstMinHeap.Count > 0 && secondMinHeap.Count > 0)
        {                        
            int nextFirst = firstMinHeap.Peek();
            int nextSecond = secondMinHeap.Peek();

            if (minFirst + nextSecond < minSecond + nextFirst)
            {
                minSecond = secondMinHeap.Dequeue();
            }
            else
            {
                minFirst = firstMinHeap.Dequeue();
            }
            
            nextList = new List<int>()
            {
                minFirst,
                minSecond
            };            
            result.Add(nextList);
            k--;
        }        
        if (k > 0 && firstMinHeap.Count == 0 && secondMinHeap.Count > 0)
        {
            while (k > 0 && secondMinHeap.Count > 0)
            {
                minSecond = secondMinHeap.Dequeue();
                nextList = new List<int>()
                {
                    minFirst,
                    minSecond
                };            
                result.Add(nextList);
                k--;
            }
        }
        else if (k > 0 && secondMinHeap.Count == 0 && firstMinHeap.Count > 0)
        {   
            while (k > 0 && firstMinHeap.Count > 0)
            {
                minFirst = firstMinHeap.Dequeue();
                nextList = new List<int>()
                {
                    minFirst,
                    minSecond
                };            
                result.Add(nextList);
                k--;
            }
        }
        
        return result;
    }*/
    public void Test()
    {
        Test5();
        /*Test4();
        Test1();
        Test2();
        Test3();*/
    }
    public void Test1()
    {
        int[] nums1 = {1,7,11};
        int[] nums2 = {2,4,6};

        var result = KSmallestPairs(nums1, nums2, 3);// [[1,2],[1,4],[1,6]]
    }
    public void Test2()
    {
        int[] nums1 = {1,1,2};
        int[] nums2 = {1,2,3};

        var result = KSmallestPairs(nums1, nums2, 2);//[[1,1],[1,1]]
    }
    public void Test3()
    {
        int[] nums1 = {1,2};
        int[] nums2 = {3};

        var result = KSmallestPairs(nums1, nums2, 3);//[[1,3],[2,3]]
    }
    public void Test4()
    {
        int[] nums1 = {1,2,4,5,6};
        int[] nums2 = {3,5,7,9};

        var result = KSmallestPairs(nums1, nums2, 3);//[[1,3],[2,3]]
        //[[1,3],[1,5],[1,7]] o
        //[[1,3],[2,3],[1,5]] e
    }
      public void Test5()
    {
        int[] nums1 = {1,2,4,5,6};
        int[] nums2 = {3,5,7,9};

        var result = KSmallestPairs(nums1, nums2, 20);//[[1,3],[2,3]]
        //[[1,3],[2,3],[1,5],[2,5],[4,3],[1,7],[5,3],[2,7],[4,5],[6,3],[1,9],[5,5],[6,5],[2,9]] o
        //[[1,3],[2,3],[1,5],[2,5],[4,3],[1,7],[5,3],[2,7],[4,5],[6,3],[1,9],[5,5],[2,9],[4,7],[6,5],[5,7],[4,9],[6,7],[5,9],[6,9]] e
    }
}
/*
373. Find K Pairs with Smallest Sums
Medium
Topics
Companies

You are given two integer arrays nums1 and nums2 sorted in non-decreasing order and an integer k.

Define a pair (u, v) which consists of one element from the first array and one element from the second array.

Return the k pairs (u1, v1), (u2, v2), ..., (uk, vk) with the smallest sums.

 

Example 1:

Input: nums1 = [1,7,11], nums2 = [2,4,6], k = 3
Output: [[1,2],[1,4],[1,6]]
Explanation: The first 3 pairs are returned from the sequence: [1,2],[1,4],[1,6],[7,2],[7,4],[11,2],[7,6],[11,4],[11,6]

Example 2:

Input: nums1 = [1,1,2], nums2 = [1,2,3], k = 2
Output: [[1,1],[1,1]]
Explanation: The first 2 pairs are returned from the sequence: [1,1],[1,1],[1,2],[2,1],[1,2],[2,2],[1,3],[1,3],[2,3]

Example 3:

Input: nums1 = [1,2], nums2 = [3], k = 3
Output: [[1,3],[2,3]]
Explanation: All possible pairs are returned from the sequence: [1,3],[2,3]

 

Constraints:

    1 <= nums1.length, nums2.length <= 105
    -109 <= nums1[i], nums2[i] <= 109
    nums1 and nums2 both are sorted in non-decreasing order.
    1 <= k <= 104
    k <= nums1.length * nums2.length


*/