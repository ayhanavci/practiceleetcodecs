namespace Top150;

internal class KthLargestElementinanArray215
{
    public int FindKthLargest(int[] nums, int k) 
    {
        PriorityQueue<int, int> maxHeap = new PriorityQueue<int, int>(Comparer<int>.Create((x,y) => y - x));
        foreach (int num in nums)
            maxHeap.Enqueue(num, num);

        for (int i = 0; i < k - 1; ++i)
            maxHeap.Dequeue();
        return maxHeap.Dequeue();
    }
    public void Test()
    {
        int[] nums1 = {3,2,1,5,6,4};
        var result1 = FindKthLargest(nums1, 2);

        int[] nums2 = {3,2,3,1,2,4,5,5,6};
        var result2 = FindKthLargest(nums2, 4);
    }
}

/*
215. Kth Largest Element in an Array
Medium
Topics
Companies

Given an integer array nums and an integer k, return the kth largest element in the array.

Note that it is the kth largest element in the sorted order, not the kth distinct element.

Can you solve it without sorting?

 

Example 1:

Input: nums = [3,2,1,5,6,4], k = 2
Output: 5

Example 2:

Input: nums = [3,2,3,1,2,4,5,5,6], k = 4
Output: 4

 

Constraints:

    1 <= k <= nums.length <= 105
    -104 <= nums[i] <= 104


*/