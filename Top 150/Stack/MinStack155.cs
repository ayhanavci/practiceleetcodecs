namespace Top150;

internal class MinStack155
{

    public class MinStack
    {
        Stack<Tuple<int, int>> _stack;                
        int _min = int.MaxValue;

        public MinStack()
        {
            _stack = new Stack<Tuple<int, int>>();
        }

        public void Push(int val)
        {
            _min = Math.Min(_min, val);
            _stack.Push(new Tuple<int, int>(val, _min));
        }

        public void Pop()
        {            
            _stack.Pop();
            _min = _stack.Count > 0 ? GetMin() : int.MaxValue;
        }

        public int Top()
        {
            return _stack.Peek().Item1;
        }

        public int GetMin()
        {
            return _stack.Peek().Item2;
        }
    }
    public void Test()
    {
        /*MinStack minStack = new MinStack();
        minStack.Push(-2);
        minStack.Push(0);
        minStack.Push(-3);
        var result1 = minStack.GetMin(); // return -3
        minStack.Pop();
        var result2 = minStack.Top();    // return 0
        var result3 = minStack.GetMin(); // return -2*/
        
        /*MinStack minStack = new MinStack();
        minStack.Push(2);
        minStack.Push(0);
        minStack.Push(3);
        minStack.Push(0);
        
        var result1 = minStack.GetMin(); 
        minStack.Pop();
        var result2 = minStack.GetMin(); 
        minStack.Pop();
        var result3 = minStack.GetMin(); 
        minStack.Pop();
                
        var result4 = minStack.GetMin(); // return -2*/

        MinStack minStack = new MinStack();
        minStack.Push(2147483646);
        minStack.Push(2147483646);
        minStack.Push(2147483647);        
        
        var result1 = minStack.Top();
        minStack.Pop();        
        
        var result2 = minStack.GetMin(); 
        minStack.Pop();        
        var result3 = minStack.GetMin(); 
        minStack.Pop();        
        
        minStack.Push(2147483647);   
        var result4 = minStack.Top();     
        
        var result5 = minStack.GetMin(); 
        minStack.Push(-2147483648);   

        var result6 = minStack.Top();    
        var result7 = minStack.GetMin();  
        var result8 = minStack.Top();     
        var result9 = minStack.GetMin();  

    }
}
/*
155. Min Stack
Medium
Topics
Companies
Hint

Design a stack that supports push, pop, top, and retrieving the minimum element in constant time.

Implement the MinStack class:

    MinStack() initializes the stack object.
    void push(int val) pushes the element val onto the stack.
    void pop() removes the element on the top of the stack.
    int top() gets the top element of the stack.
    int getMin() retrieves the minimum element in the stack.

You must implement a solution with O(1) time complexity for each function.

 

Example 1:

Input
["MinStack","push","push","push","getMin","pop","top","getMin"]
[[],[-2],[0],[-3],[],[],[],[]]

Output
[null,null,null,null,-3,null,0,-2]

Explanation
MinStack minStack = new MinStack();
minStack.push(-2);
minStack.push(0);
minStack.push(-3);
minStack.getMin(); // return -3
minStack.pop();
minStack.top();    // return 0
minStack.getMin(); // return -2

 

Constraints:

    -231 <= val <= 231 - 1
    Methods pop, top and getMin operations will always be called on non-empty stacks.
    At most 3 * 104 calls will be made to push, pop, top, and getMin.


*/