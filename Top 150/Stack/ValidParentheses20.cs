namespace Top150;

internal class ValidParentheses20
{
    public bool IsValid(string s) 
    {
        //"[{()[]()}]"
        Stack<char> items = new Stack<char>();

        for (int i = 0; i < s.Length; ++i)
        {
            if (s[i] == '(' || s[i] == '{' || s[i] == '[')
            {
                items.Push(s[i]);
            }
            else if (items.Count == 0)
            {
                return false;
            }
            else
            {
                var lastOpen = items.Pop();
                if (s[i] == ')' && lastOpen != '(') return false;
                else if (s[i] == '}' && lastOpen != '{') return false;
                else if (s[i] == ']' && lastOpen != '[') return false;

            }
        }
        return items.Count == 0;        
    }
    public void Test()
    {
        var result1 = IsValid("()");
        var result2 = IsValid("()[]{}");
        var result3 = IsValid("(]");
        var result4 = IsValid("[{()[]()}]");
        var result5 = IsValid("[{()[](()}]");
    }
}
/*
20. Valid Parentheses
Easy
23.1K
1.6K
Companies

Given a string s containing just the characters '(', ')', '{', '}', '[' and ']', determine if the input string is valid.

An input string is valid if:

    Open brackets must be closed by the same type of brackets.
    Open brackets must be closed in the correct order.
    Every close bracket has a corresponding open bracket of the same type.

 

Example 1:

Input: s = "()"
Output: true

Example 2:

Input: s = "()[]{}"
Output: true

Example 3:

Input: s = "(]"
Output: false

 

Constraints:

    1 <= s.length <= 104
    s consists of parentheses only '()[]{}'.


*/