using System.Text;

namespace Top150;

//TODO: Tekrar bak
internal class BasicCalculator224
{
    public void Operate(Stack<int> values, char op, int number)
    {
        switch (op)
        {
            case '+':
                values.Push(number);
            break;
            case '-':
                values.Push(-number);
            break;
            case '*':
                values.Push(values.Pop() * number);
            break;
            case '/':
                values.Push(values.Pop() / number);
            break;
        }
    }
    public int Calculate(string s) 
    {
        int result = 0;

        int number = 0;
        Stack<int> values = new Stack<int>();
        
        char lastOp = '+';
        for (int i = 0; i < s.Length; ++i)
        {
            char ch = s[i];
            if (ch == ' ') continue;
            
            if (char.IsDigit(ch))
            {
                int tmp = (ch - '0');
                number = (number * 10) + tmp;
            }         
            else if (ch == '-' || ch == '+' || ch == '*' || ch == '/')
            {
                Operate(values, lastOp, number);
                number = 0;
                lastOp = ch;
            }            
            else if (ch == '(') //Start collecting inner expression
            {                
                StringBuilder inner = new StringBuilder();
                int openCount = 1;

                i++;//Skip (
                while (i < s.Length)
                {                    
                    if (s[i] == '(') 
                        openCount++;
                    else if (s[i] == ')') 
                        openCount--;
                    
                    if (openCount == 0)
                        break;
                    
                    inner.Append(s[i++]);
                }                                                      
                
                Operate(values, lastOp, Calculate(inner.ToString()));
            }
            
        }    
        Operate(values, lastOp, number);
        while (values.Count > 0)
            result += values.Pop();
        return result;
    }
    public void Test()
    {
        var result1 = Calculate("(1+(4+5+2)-3)+(6+8)");
        var result2 = Calculate("1 + 1");
        var result3 = Calculate(" 2-1 + 2 ");
        var result4 = Calculate("2147483647");
    }
}
/*
224. Basic Calculator
Hard
6K
447
Companies

Given a string s representing a valid expression, implement a basic calculator to evaluate it, and return the result of the evaluation.

Note: You are not allowed to use any built-in function which evaluates strings as mathematical expressions, such as eval().

 

Example 1:

Input: s = "1 + 1"
Output: 2

Example 2:

Input: s = " 2-1 + 2 "
Output: 3

Example 3:

Input: s = "(1+(4+5+2)-3)+(6+8)"
Output: 23

 

Constraints:

    1 <= s.length <= 3 * 105
    s consists of digits, '+', '-', '(', ')', and ' '.
    s represents a valid expression.
    '+' is not used as a unary operation (i.e., "+1" and "+(2 + 3)" is invalid).
    '-' could be used as a unary operation (i.e., "-1" and "-(2 + 3)" is valid).
    There will be no two consecutive operators in the input.
    Every number and running calculation will fit in a signed 32-bit integer.


*/