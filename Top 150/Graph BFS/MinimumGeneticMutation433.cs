namespace Top150;

internal class MinimumGeneticMutation433
{
    Dictionary<string, List<string>> mutationGraph;
    public int MinMutation(string startGene, string endGene, string[] bank) 
    {
        mutationGraph = new Dictionary<string, List<string>>();
        CreateGraph(startGene, bank);
        if (!mutationGraph.ContainsKey(endGene))
            return -1;        

        Queue<Tuple<string, int>> queue = new Queue<Tuple<string, int>>();
        HashSet<string> visited = new HashSet<string>();
        queue.Enqueue(new Tuple<string, int>(startGene, 1));
        visited.Add(startGene);

        while (queue.Count > 0)
        {
            var item = queue.Dequeue();                        
            var neighbours = mutationGraph[item.Item1];
            int distance = item.Item2;

            foreach (string neighbour in neighbours)
            {
                if (neighbour.Equals(endGene))
                    return distance;
                if (!visited.Contains(neighbour))
                {
                    queue.Enqueue(new Tuple<string, int>(neighbour, distance + 1));
                    visited.Add(neighbour);
                }
                    
            }
        }

        return -1;
    }
    private void CreateGraph(string startGene, string[] bank)
    {
        List<string> startNeighbours = new List<string>();
        mutationGraph.Add(startGene, startNeighbours);
        for (int i = 0; i < bank.Length; ++i)
        {
            if (IsOneDistantMutation(startGene, bank[i]))
                startNeighbours.Add(bank[i]);
        }

        for (int i = 0; i < bank.Length; ++i)
        {
            string gene1 = bank[i];
            List<string> neighbours;
            if (!mutationGraph.TryGetValue(gene1, out neighbours))
            {
                neighbours = new List<string>();
                mutationGraph.Add(gene1, neighbours);
            }
            
            for (int j = i + 1; j < bank.Length; ++j)
            {
                if (IsOneDistantMutation(gene1, bank[j]))
                {
                    string gene2 = bank[j];
                    neighbours.Add(gene2);  
                    List<string> neighbours2;
                    if (!mutationGraph.TryGetValue(gene2, out neighbours2))
                    {
                        neighbours2 = new List<string>();
                        mutationGraph.Add(gene2, neighbours2);
                    }
                    neighbours2.Add(gene1);
                }
            }
        }

    }
    public bool IsOneDistantMutation(string gen1, string gen2)
    {
        int count = 0;

        for (int i = 0; i < gen1.Length; ++i)
        {
            if (gen1[i] != gen2[i])            
                if (++count > 1) 
                    return false;
            
        }

        return count == 1;
    }
    public void Test()
    {
        Test1();
    }
    public void Test1()
    {
        string[] bank = new string[] {"AACCGGTA","AACCGCTA","AAACGGTA"};
        var result = MinMutation("AACCGGTT", "AAACGGTA", bank);
    }
}
/*
433. Minimum Genetic Mutation
Medium
Topics
Companies

A gene string can be represented by an 8-character long string, with choices from 'A', 'C', 'G', and 'T'.

Suppose we need to investigate a mutation from a gene string startGene to a gene string endGene where one mutation is defined as one single character changed in the gene string.

    For example, "AACCGGTT" --> "AACCGGTA" is one mutation.

There is also a gene bank bank that records all the valid gene mutations. A gene must be in bank to make it a valid gene string.

Given the two gene strings startGene and endGene and the gene bank bank, return the minimum number of mutations needed to mutate from startGene to endGene. If there is no such a mutation, return -1.

Note that the starting point is assumed to be valid, so it might not be included in the bank.

 

Example 1:

Input: startGene = "AACCGGTT", endGene = "AACCGGTA", bank = ["AACCGGTA"]
Output: 1

Example 2:

Input: startGene = "AACCGGTT", endGene = "AAACGGTA", bank = ["AACCGGTA","AACCGCTA","AAACGGTA"]
Output: 2

 

Constraints:

    0 <= bank.length <= 10
    startGene.length == endGene.length == bank[i].length == 8
    startGene, endGene, and bank[i] consist of only the characters ['A', 'C', 'G', 'T'].


*/