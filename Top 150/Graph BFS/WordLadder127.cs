namespace Top150;

internal class WorldLadder127
{    
    Dictionary<string, List<string>> wordGraph;
    public int LadderLength(string beginWord, string endWord, IList<string> wordList) 
    {        
        wordGraph  = new Dictionary<string, List<string>>();
        CreateGraph(beginWord, wordList);
        if (!wordGraph.ContainsKey(endWord))
            return 0;

        Queue<Tuple<string, int>> queue = new Queue<Tuple<string, int>>();

        queue.Enqueue(new Tuple<string, int>(beginWord, 1));
        HashSet<string> visited = new HashSet<string>();
        while (queue.Count > 0)
        {
            var wordAndDist = queue.Dequeue();
            visited.Add(wordAndDist.Item1);

            if (wordAndDist.Item1 == endWord)
                return wordAndDist.Item2;
            
            var neighbours = wordGraph[wordAndDist.Item1];

            foreach (var neighbour in neighbours)
            {
                if (!visited.Contains(neighbour))
                    queue.Enqueue(new Tuple<string, int>(neighbour, wordAndDist.Item2 + 1));
            }
        }
        
        return 0;
    }
    private void CreateGraph(string beginWord, IList<string> wordList)
    {

        for (int i = 0; i < wordList.Count; ++i)
        {
            string first = wordList[i];

            List<string> neighbours;

            if (!wordGraph.TryGetValue(first, out neighbours))
            {
                neighbours = new List<string> ();
                wordGraph.Add(first, neighbours);
            }
            

            for (int j = i + 1; j < wordList.Count; ++j)
            {
                string second = wordList[j];
                if (IsOneCharDistant(first, second))
                {                    
                    neighbours.Add(second);
                    if (wordGraph.ContainsKey(second))                    
                        wordGraph[second].Add(first);                    
                    else                    
                        wordGraph.Add(second, new List<string> () { first });                    
                }
                
            }
        }
        if (!wordGraph.ContainsKey(beginWord))
        {
            List<string> neighbours = new List<string>();
            wordGraph.Add(beginWord, neighbours);
            for (int i = 0; i < wordList.Count; ++i)
            {
                if (IsOneCharDistant(beginWord, wordList[i]))                
                    neighbours.Add(wordList[i]);
                
            }
        }
        
    }
    private bool IsOneCharDistant(string first, string second)
    {
        int count = 0;

        for (int i = 0; i < first.Length; ++i)
        {
            if (first[i] != second[i])
                if (++count > 1) return false;
            
        }

        return count == 1;
    }
    public void Test()
    {
        //Test1();
        //Test2();
        //Test3();
        Test4();
    }
    public void Test1()
    {
        string[] wordList = new string[] {"hot","dot","dog","lot","log","cog"};
        var result = LadderLength("hit", "cog", wordList);
    }
    public void Test2()
    {
        string[] wordList = new string[] {"hot","dot","dog","lot","log"};
        var result = LadderLength("hit", "cog", wordList);
    }

    public void Test3()
    {
        string[] wordList = new string[] {"b"};
        var result = LadderLength("a", "z", wordList);
    }
     public void Test4()
    {
        string[] wordList = new string[] {"cog"};
        var result = LadderLength("hog", "cog", wordList);
    }
}

/*
class Solution {
    public int ladderLength(String beginWord, String endWord, List<String> wordList) {
        Set<String> set = new HashSet<>(wordList);
        if(!set.contains(endWord)) return 0;
        
        Queue<String> queue = new LinkedList<>();
        queue.add(beginWord);
        
        Set<String> visited = new HashSet<>();
        queue.add(beginWord);
        
        int changes = 1;
        
        while(!queue.isEmpty()){
            int size = queue.size();
            for(int i = 0; i < size; i++){
                String word = queue.poll();
                if(word.equals(endWord)) return changes;
                
                for(int j = 0; j < word.length(); j++){
                    for(int k = 'a'; k <= 'z'; k++){
                        char arr[] = word.toCharArray();
                        arr[j] = (char) k;
                        
                        String str = new String(arr);
                        if(set.contains(str) && !visited.contains(str)){
                            queue.add(str);
                            visited.add(str);
                        }
                    }
                }
            }
            ++changes;
        }
        return 0;
    }
}
*/
/*
127. Word Ladder
Hard
Topics
Companies

A transformation sequence from word beginWord to word endWord using a dictionary wordList is a sequence of words beginWord -> s1 -> s2 -> ... -> sk such that:

    Every adjacent pair of words differs by a single letter.
    Every si for 1 <= i <= k is in wordList. Note that beginWord does not need to be in wordList.
    sk == endWord

Given two words, beginWord and endWord, and a dictionary wordList, return the number of words in the shortest transformation sequence from beginWord to endWord, or 0 if no such sequence exists.

 

Example 1:

Input: beginWord = "hit", endWord = "cog", wordList = ["hot","dot","dog","lot","log","cog"]
Output: 5
Explanation: One shortest transformation sequence is "hit" -> "hot" -> "dot" -> "dog" -> cog", which is 5 words long.

Example 2:

Input: beginWord = "hit", endWord = "cog", wordList = ["hot","dot","dog","lot","log"]
Output: 0
Explanation: The endWord "cog" is not in wordList, therefore there is no valid transformation sequence.

 

Constraints:

    1 <= beginWord.length <= 10
    endWord.length == beginWord.length
    1 <= wordList.length <= 5000
    wordList[i].length == beginWord.length
    beginWord, endWord, and wordList[i] consist of lowercase English letters.
    beginWord != endWord
    All the words in wordList are unique.


*/