﻿namespace PracticeLeetCodeCS.Easy;

internal class IncreasingOrderSearchTree897
{

    //Definition for a binary tree node.
    public class TreeNode
    {
        public int val;
        public TreeNode left;
        public TreeNode right;
        public TreeNode(int val = 0, TreeNode left = null, TreeNode right = null)
        {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }

    TreeNode nextNode = null;
    TreeNode startNode = null;
    public TreeNode IncreasingBST(TreeNode root)
    {
        if (root == null) return null;

        IncreasingBST(root.left);
        if (startNode == null)
        {
            startNode = new TreeNode(root.val);
            nextNode = startNode;
        }

        else
        {
            nextNode.right = new TreeNode(root.val);
            nextNode = nextNode.right;
        }

        IncreasingBST(root.right);
        return startNode;
    }
    public void Test()
    {
        TreeNode root = new TreeNode(5,
            
            new TreeNode(3, 
                new TreeNode(2, new TreeNode(1), null), 
                new TreeNode(4)),

            new TreeNode(6, 
                null, 
                new TreeNode(8, new TreeNode(7), new TreeNode(9))));

        var retVal = IncreasingBST(root);
    }
}

/*
 897. Increasing Order Search Tree
Easy
3.7K
640
Companies

Given the root of a binary search tree, rearrange the tree in in-order so that the leftmost node in the tree is now the root of the tree, and every node has no left child and only one right child.

 

Example 1:

Input: root = [5,3,6,2,4,null,8,1,null,null,null,7,9]
Output: [1,null,2,null,3,null,4,null,5,null,6,null,7,null,8,null,9]

Example 2:

Input: root = [5,1,7]
Output: [1,null,5,null,7]

 

Constraints:

    The number of nodes in the given tree will be in the range [1, 100].
    0 <= Node.val <= 1000


 */