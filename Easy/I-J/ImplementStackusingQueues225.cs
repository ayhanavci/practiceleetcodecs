﻿namespace PracticeLeetCodeCS.Easy;

internal class ImplementStackusingQueues225
{
    public class MyStack
    {
        Queue<int> queue1 = new Queue<int>();
        Queue<int> queue2 = new Queue<int>();
        public MyStack()
        {

        }

        public void Push(int x)
        {
            if (queue1.Count > 0)
                queue1.Enqueue(x);
            else 
                queue2.Enqueue(x);
        }

        public int Pop()
        {            
            Queue<int> fullQueue;
            Queue<int> emptyQueue;
            if (queue1.Count > 0)
            {
                fullQueue = queue1;
                emptyQueue = queue2;
            }
            else
            {
                fullQueue = queue2;
                emptyQueue = queue1;
            }

            while (fullQueue.Count > 1)            
                emptyQueue.Enqueue(fullQueue.Dequeue());
            
            return fullQueue.Dequeue();
        }

        public int Top()
        {
            Queue<int> fullQueue;
            Queue<int> emptyQueue;
            if (queue1.Count > 0)
            {
                fullQueue = queue1;
                emptyQueue = queue2;
            }
            else
            {
                fullQueue = queue2;
                emptyQueue = queue1;
            }

            while (fullQueue.Count > 1)
                emptyQueue.Enqueue(fullQueue.Dequeue());
            int top = fullQueue.Dequeue();
            emptyQueue.Enqueue(top); 
            return top;
        }

        public bool Empty()
        {
            return (queue1.Count == 0 && queue2.Count == 0);
        }
    }

    public void Test()
    {
        MyStack stack = new MyStack();
        stack.Push(1);
        stack.Push(2);
        stack.Push(3);
        int val;
        val = stack.Top();
        val = stack.Pop();

        val = stack.Top();
        val = stack.Pop();

        val = stack.Top();
        val = stack.Pop();


        //Stack<int> test= new Stack<int>();
        //test.Pop();

    }

    /**
     * Your MyStack object will be instantiated and called as such:
     * MyStack obj = new MyStack();
     * obj.Push(x);
     * int param_2 = obj.Pop();
     * int param_3 = obj.Top();
     * bool param_4 = obj.Empty();
     */
}

/*
 225. Implement Stack using Queues
Easy

Implement a last-in-first-out (LIFO) stack using only two queues. The implemented stack should support all the functions of a normal stack (push, top, pop, and empty).

Implement the MyStack class:

    void push(int x) Pushes element x to the top of the stack.
    int pop() Removes the element on the top of the stack and returns it.
    int top() Returns the element on the top of the stack.
    boolean empty() Returns true if the stack is empty, false otherwise.

Notes:

    You must use only standard operations of a queue, which means that only push to back, peek/pop from front, size and is empty operations are valid.
    Depending on your language, the queue may not be supported natively. You may simulate a queue using a list or deque (double-ended queue) as long as you use only a queue"s standard operations.

 

Example 1:

Input
["MyStack", "push", "push", "top", "pop", "empty"]
[[], [1], [2], [], [], []]
Output
[null, null, null, 2, 2, false]

Explanation
MyStack myStack = new MyStack();
myStack.push(1);
myStack.push(2);
myStack.top(); // return 2
myStack.pop(); // return 2
myStack.empty(); // return False

 

Constraints:

    1 <= x <= 9
    At most 100 calls will be made to push, pop, top, and empty.
    All the calls to pop and top are valid.

 

Follow-up: Can you implement the stack using only one queue?

 */