﻿namespace PracticeLeetCodeCS.Easy;

internal class InvertBinaryTree226
{
    //Definition for a binary tree node.
    public class TreeNode
    {
        public int val;
        public TreeNode left;
        public TreeNode right;
        public TreeNode(int val = 0, TreeNode left = null, TreeNode right = null)
        {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }
        
    public TreeNode InvertTree(TreeNode root)
    {
        if (root == null)
            return null;
        TreeNode temp = root.left;
        root.left = root.right;
        root.right = temp;

        InvertTree(root.left);
        InvertTree(root.right);
        return root;
    }
    public void Test()
    {
        TreeNode left_right = new TreeNode(3);
        TreeNode left_left = new TreeNode(1);

        TreeNode right_right = new TreeNode(9);
        TreeNode right_left = new TreeNode(6);

        TreeNode left = new TreeNode(2, left_left, left_right);
        TreeNode right = new TreeNode(7, right_left, right_right);

        TreeNode root = new TreeNode(4, left, right);

        TreeNode head = InvertTree(root);
    }
}

/*
 226. Invert Binary Tree
Easy

Given the root of a binary tree, invert the tree, and return its root.

 

Example 1:

Input: root = [4,2,7,1,3,6,9]
Output: [4,7,2,9,6,3,1]

Example 2:

Input: root = [2,1,3]
Output: [2,3,1]

Example 3:

Input: root = []
Output: []

 

Constraints:

    The number of nodes in the tree is in the range [0, 100].
    -100 <= Node.val <= 100


 */