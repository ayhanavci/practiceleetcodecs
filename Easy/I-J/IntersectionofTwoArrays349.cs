﻿namespace PracticeLeetCodeCS.Easy;

internal class IntersectionofTwoArrays349
{
    public int[] Intersection(int[] nums1, int[] nums2)
    {
        List<int> items = new List<int>();
        
        for (int i = 0; i < nums1.Length; ++i)
        {
            if (!items.Contains(nums1[i]))
            {
                for (int j = 0; j < nums2.Length; ++j)
                {
                    if (nums1[i] == nums2[j])
                    {
                        items.Add(nums1[i]);
                        break;
                    }                        
                }
            }
            
        }              
        return items.ToArray();
    }
    public void Test()
    {
        int[] nums1 = { 4, 9, 5 };
        int[] nums2 = { 9, 4, 9, 8, 4 };

        int[] result = Intersection(nums1, nums2);
    }
}

/*
 349. Intersection of Two Arrays
Easy

Given two integer arrays nums1 and nums2, return an array of their intersection. Each element in the result must be unique and you may return the result in any order.

 

Example 1:

Input: nums1 = [1,2,2,1], nums2 = [2,2]
Output: [2]

Example 2:

Input: nums1 = [4,9,5], nums2 = [9,4,9,8,4]
Output: [9,4]
Explanation: [4,9] is also accepted.

 

Constraints:

    1 <= nums1.length, nums2.length <= 1000
    0 <= nums1[i], nums2[i] <= 1000


 */