﻿using System.Text;

namespace PracticeLeetCodeCS.Easy;

internal class IsomorphicStrings205
{
    public bool IsIsomorphic2(string s, string t)
    {
        StringBuilder convertedString = new StringBuilder();
        if (s.Length != t.Length) return false;

        Dictionary<char, char> chars = new Dictionary<char, char>();
        for (int i = 0; i < s.Length; i++)
        {
            char ch;
            if (chars.TryGetValue(s[i], out ch))
            {
                convertedString.Append(ch);
            }
            else
            {
                chars.Add(s[i], t[i]);
                convertedString.Append(t[i]);
            }
                            
        }

        return convertedString.ToString().Equals(t);
    }
    public bool IsIsomorphic(string s, string t)
    {
        if (s.Length != t.Length) return false;
        Dictionary<char, char> sTot = new Dictionary<char, char>();
        Dictionary<char, char> tTos = new Dictionary<char, char>();
        for (int i = 0; i < s.Length; i++)
        {
            char ch;
            if (sTot.TryGetValue(s[i], out ch))
            {
                if (ch != t[i]) 
                    return false;
            }
            else
            {
                sTot[s[i]] = t[i];
            }

            if (tTos.TryGetValue(t[i], out ch))
            {
                if (ch != s[i])
                    return false;
            }
            else
            {
                tTos[t[i]] = s[i];
            }
        }

        return true;
    }

    public void Test()
    {
        string s = "badc";
        string t = "baba";
        bool retVal = IsIsomorphic(s, t);
    }
}

/*
 205. Isomorphic Strings
Easy

Given two strings s and t, determine if they are isomorphic.

Two strings s and t are isomorphic if the characters in s can be replaced to get t.

All occurrences of a character must be replaced with another character while preserving the order of characters. No two characters may map to the same character, but a character may map to itself.

 

Example 1:

Input: s = "egg", t = "add"
Output: true

Example 2:

Input: s = "foo", t = "bar"
Output: false

Example 3:

Input: s = "paper", t = "title"
Output: true

 

Constraints:

    1 <= s.length <= 5 * 104
    t.length == s.length
    s and t consist of any valid ascii character.


 
 */