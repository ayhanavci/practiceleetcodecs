﻿using System.Text;

namespace PracticeLeetCodeCS.Easy;

internal class IncreasingDecreasingString1370
{
    public string SortString(string s)
    {
        StringBuilder source = new StringBuilder(s);
        StringBuilder retVal = new StringBuilder();

        while (source.Length > 0)
        {                        
            //Find smallest index in remaining chars
            int smallestIndex = 0;            
            for (int i = 0; i < source.Length; i++)
            {
                if (source[smallestIndex] > source[i])                
                    smallestIndex = i;                
            }
            //Append smallest
            char smallestChar = source[smallestIndex];
            retVal.Append(smallestChar);
            source.Remove(smallestIndex, 1);

            //Find and append next smallest, if exists
            do
            {
                smallestIndex = -1;
                for (int i = 0; i < source.Length; ++i)
                {
                    if (smallestIndex == -1)
                    {
                        if (smallestChar < source[i])                        
                            smallestIndex = i;                        
                    }
                    else
                    {
                        if (source[smallestIndex] > source[i] 
                            && smallestChar < source[i]) 
                            smallestIndex = i;
                    }
                    
                }
                if (smallestIndex != -1)
                {
                    smallestChar = source[smallestIndex];
                    retVal.Append(smallestChar);
                    source.Remove(smallestIndex, 1);
                }
            } while (smallestIndex != -1);

            //Find largest index in remaining chars
            if (source.Length == 0)
                return retVal.ToString();
            int largestIndex = 0;
            for (int i = 0; i < source.Length; i++)
            {
                if (source[largestIndex] < source[i])
                    largestIndex = i;
            }

            //Append largest
            char largestChar = source[largestIndex];
            retVal.Append(largestChar);
            source.Remove(largestIndex, 1);

            //Find and append next largest, if exists
            do
            {
                largestIndex = -1;
                for (int i = 0; i < source.Length; ++i)
                {
                    if (largestIndex == -1)
                    {
                        if (largestChar > source[i])
                            largestIndex = i;
                    }
                    else
                    {
                        if (source[largestIndex] < source[i] 
                            && largestChar > source[i])
                            largestIndex = i;
                     }

                }
                if (largestIndex != -1)
                {
                    largestChar = source[largestIndex];
                    retVal.Append(largestChar);
                    source.Remove(largestIndex, 1);
                }
            } while (largestIndex != -1);
        }

        return retVal.ToString();
    }
    
    public void Test()
    {
        //string s = "aaaabbbbcccc";
        string s = "rat";
        var retVal = SortString(s);
    }
}
/*
 
    calculates freq of each character
    Step 1: If accumulated all characters, we are done, else
    Step 2: accumulate from 'a' -> 'z',
    Step 3: accumulate from 'z' -> 'a'
    Go to Step 1.

public String sortString(String s) {
        int len = s.length();
        int[] freq = new int[26];
        for (int index = 0; index < len; index++) {
            freq[s.charAt(index) - 'a']++;
        }
        StringBuilder sb = new StringBuilder(len);
        int count = 0;
        while (count < len) {
            // sorting up
            for (int i = 0; i < 26; i++) {
                if (freq[i] > 0) {
                    sb.append((char) (i + 'a'));
                    freq[i] = freq[i] - 1;
                    count++;
                }
            }
            // sorting down
            for (int i = 25; i >= 0; i--) {
                if (freq[i] > 0) {
                    sb.append((char) (i + 'a'));
                    freq[i] = freq[i] - 1;
                    count++;
                }
            }
        }
        return sb.toString();
    }
 
 */
/*
 class Solution {
public:
    string sortString(string s) {
        string res="";
        int a[26]={0},k=0;
        for(int i=0;i<s.length();i++)
        {a[s[i]-'a']++;k++;}
        while(k)
        {
            for(int i=0;i<26;i++)
            {
                if(a[i]>0) { res+=i+'a';k--;a[i]--;}
            }
            for(int i=25;i>=0;i--)
            {
                if(a[i]>0) { res+=i+'a';k--;a[i]--;}
            }
        }
        
        
        return res;
        
    }
};
 */
/*
 1370. Increasing Decreasing String
Easy
616
729
Companies

You are given a string s. Reorder the string using the following algorithm:

    Pick the smallest character from s and append it to the result.
    Pick the smallest character from s which is greater than the last appended character to the result and append it.
    Repeat step 2 until you cannot pick more characters.
    Pick the largest character from s and append it to the result.
    Pick the largest character from s which is smaller than the last appended character to the result and append it.
    Repeat step 5 until you cannot pick more characters.
    Repeat the steps from 1 to 6 until you pick all characters from s.

In each step, If the smallest or the largest character appears more than once you can choose any occurrence and append it to the result.

Return the result string after sorting s with this algorithm.

 

Example 1:

Input: s = "aaaabbbbcccc"
Output: "abccbaabccba"
Explanation: After steps 1, 2 and 3 of the first iteration, result = "abc"
After steps 4, 5 and 6 of the first iteration, result = "abccba"
First iteration is done. Now s = "aabbcc" and we go back to step 1
After steps 1, 2 and 3 of the second iteration, result = "abccbaabc"
After steps 4, 5 and 6 of the second iteration, result = "abccbaabccba"

Example 2:

Input: s = "rat"
Output: "art"
Explanation: The word "rat" becomes "art" after re-ordering it with the mentioned algorithm.

 

Constraints:

    1 <= s.length <= 500
    s consists of only lowercase English letters.


 */