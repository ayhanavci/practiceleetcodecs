﻿namespace PracticeLeetCodeCS.Easy;

internal class FirstUniqueCharacterinaString387
{
    public int FirstUniqChar(string s)
    {
        Dictionary<char, int> items = new Dictionary<char, int>();
        List<char> dumpster = new List<char>();
        
        for (int i = 0; i < s.Length; i++)
        {
            int position;
            if (items.TryGetValue(s[i], out position))
            {
                items.Remove(s[i]);
                dumpster.Add(s[i]);
            }
            else if (!dumpster.Contains(s[i]))
            {
                items.Add(s[i], i);
            }            
        }

        if (items.Count == 0)
            return -1;        
        int smallestIndex = 100000;
        foreach (var item in items)
        {
            if (item.Value < smallestIndex) 
                smallestIndex = item.Value;
        }
        return smallestIndex;
    }
    public void Test()
    {
        string s = "loveleetcode";
        int retVal = FirstUniqChar(s);
    }
}
/*
 387. First Unique Character in a String
Easy

Given a string s, find the first non-repeating character in it and return its index. If it does not exist, return -1.

 

Example 1:

Input: s = "leetcode"
Output: 0

Example 2:

Input: s = "loveleetcode"
Output: 2

Example 3:

Input: s = "aabb"
Output: -1

 

Constraints:

    1 <= s.length <= 105
    s consists of only lowercase English letters.


 */