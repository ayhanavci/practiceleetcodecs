﻿namespace PracticeLeetCodeCS.Easy;

internal class FindNUniqueIntegersSumuptoZero1304
{
    public int[] SumZero(int n)
    {
        int[] result = new int[n];
        for (int i = 0; i < n / 2; i++)
        {
            result[i] = - n + i;
            result[n - 1 - i] = n - i;
        }
        if (n % 2 == 1)
            result[n / 2] = 0;

        return result;
    }
    public void Test()
    {
        var retVal = SumZero(5);
    }
}

/*
 1304. Find N Unique Integers Sum up to Zero
Easy
1.5K
535
Companies

Given an integer n, return any array containing n unique integers such that they add up to 0.

 

Example 1:

Input: n = 5
Output: [-7,-1,1,3,4]
Explanation: These arrays also are accepted [-5,-1,1,2,3] , [-3,-1,2,-2,4].

Example 2:

Input: n = 3
Output: [-1,0,1]

Example 3:

Input: n = 1
Output: [0]

 

Constraints:

    1 <= n <= 1000


 */