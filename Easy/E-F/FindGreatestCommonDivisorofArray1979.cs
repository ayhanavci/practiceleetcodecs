namespace PracticeLeetCodeCS.Easy;

internal class FindGreatestCommonDivisorofArray1979
{
    public int FindGCD(int[] nums) 
    {
        int smallest = int.MaxValue;
        int largest = int.MinValue;

        for (int i = 0; i < nums.Length; ++i)
        {
            if (nums[i] < smallest)
                smallest = nums[i];
            if (nums[i] > largest)
                largest = nums[i];
        }

        while (smallest != 0 && largest != 0)
        {
            if (largest > smallest)
                largest %= smallest;
            else
                smallest %= largest;
        }

        return smallest | largest;
    }
    static int GCD1(int num1, int num2)
    {
        int Remainder;
 
        while (num2 != 0)
        {
            Remainder = num1 % num2;
            num1 = num2;
            num2 = Remainder;
        }
 
        return num1;
    }
    public int GCD2(int a, int b)
    {
        int Remainder;
    
        while( b != 0 )
        {
            Remainder = a % b;
            a = b;
            b = Remainder;
        }
        return a;
    }
    public void Test()
    {

    }
}
/*
1979. Find Greatest Common Divisor of Array
Easy
818
33
Companies

Given an integer array nums, return the greatest common divisor of the smallest number and largest number in nums.

The greatest common divisor of two numbers is the largest positive integer that evenly divides both numbers.

 

Example 1:

Input: nums = [2,5,6,9,10]
Output: 2
Explanation:
The smallest number in nums is 2.
The largest number in nums is 10.
The greatest common divisor of 2 and 10 is 2.

Example 2:

Input: nums = [7,5,6,8,3]
Output: 1
Explanation:
The smallest number in nums is 3.
The largest number in nums is 8.
The greatest common divisor of 3 and 8 is 1.

Example 3:

Input: nums = [3,3]
Output: 3
Explanation:
The smallest number in nums is 3.
The largest number in nums is 3.
The greatest common divisor of 3 and 3 is 3.

 

Constraints:

    2 <= nums.length <= 1000
    1 <= nums[i] <= 1000


*/