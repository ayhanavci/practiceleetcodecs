﻿using System.Text;

namespace PracticeLeetCodeCS.Easy;

internal class FindCommonCharacters1002
{
    public IList<string> CommonChars(string[] words)
    {
        List<string> commonChars = new List<string>();
        
        for (int i = 0; i < words[0].Length; i++)         
            commonChars.Add(words[0][i].ToString());
        
        for (int i = 1; i < words.Length; ++i)
        {
            List<char> word = new List<char>(words[i]);

            for (int k = 0; k < commonChars.Count; ++k)
            {
                if (word.Contains(commonChars[k][0]))                
                    word.Remove(commonChars[k][0]);
                else                
                    commonChars.Remove(commonChars[k--]);
                if (commonChars.Count == 0)
                    return commonChars;
            }
            
        }

        return commonChars;
    }

    public void Test()
    {
        string[] words1 = { "bella", "label", "roller" };
        var retval = CommonChars(words1);

        string[] words2 = { "cool", "lock", "cook" };
        var retval2 = CommonChars(words2);
    }
}

/*
 1002. Find Common Characters
Easy
2.8K
230
Companies

Given a string array words, return an array of all characters that show up in all strings within the words (including duplicates). You may return the answer in any order.

 

Example 1:

Input: words = ["bella","label","roller"]
Output: ["e","l","l"]

Example 2:

Input: words = ["cool","lock","cook"]
Output: ["c","o"]

 

Constraints:

    1 <= words.length <= 100
    1 <= words[i].length <= 100
    words[i] consists of lowercase English letters.


 */