﻿namespace PracticeLeetCodeCS.Easy;

internal class FairCandySwap888
{
    public int[] FairCandySwap(int[] aliceSizes, int[] bobSizes)
    {
        int[] result = new int[2];

        int aliceTotal = 0;
        for (int i = 0; i < aliceSizes.Length; ++i)
            aliceTotal += aliceSizes[i];

        int bobTotal = 0;
        for (int i = 0; i < bobSizes.Length; ++i)
            bobTotal += bobSizes[i];       

        for (int i = 0; i < aliceSizes.Length; ++i)
            for (int j = 0; j < bobSizes.Length; ++j)
            {
                if ((bobTotal - bobSizes[j] + aliceSizes[i]) == 
                    (aliceTotal - aliceSizes[i] + bobSizes[j]))
                {
                    result[0] = aliceSizes[i];
                    result[1] = bobSizes[j];
                    return result;
                }
            }

        return result;
    }
    public void Test()
    {
        int[] aliceSizes = { 1, 1 };
        int[] bobSizes = { 2, 2 };

        var retVal = FairCandySwap(aliceSizes, bobSizes);


        int[] aliceSizes2 = { 1, 2 };
        int[] bobSizes2 = { 2, 3 };

        var retVal2 = FairCandySwap(aliceSizes2, bobSizes2);


        int[] aliceSizes3 = { 2 };
        int[] bobSizes3 = { 1, 3 };

        var retVal3 = FairCandySwap(aliceSizes3, bobSizes3);
    }
}

/*
 888. Fair Candy Swap
Easy
1.6K
298
Companies

Alice and Bob have a different total number of candies. You are given two integer arrays aliceSizes and bobSizes where aliceSizes[i] is the number of candies of the ith box of candy that Alice has and bobSizes[j] is the number of candies of the jth box of candy that Bob has.

Since they are friends, they would like to exchange one candy box each so that after the exchange, they both have the same total amount of candy. The total amount of candy a person has is the sum of the number of candies in each box they have.

Return an integer array answer where answer[0] is the number of candies in the box that Alice must exchange, and answer[1] is the number of candies in the box that Bob must exchange. If there are multiple answers, you may return any one of them. It is guaranteed that at least one answer exists.

 

Example 1:

Input: aliceSizes = [1,1], bobSizes = [2,2]
Output: [1,2]

Example 2:

Input: aliceSizes = [1,2], bobSizes = [2,3]
Output: [1,2]

Example 3:

Input: aliceSizes = [2], bobSizes = [1,3]
Output: [2,3]

 

Constraints:

    1 <= aliceSizes.length, bobSizes.length <= 104
    1 <= aliceSizes[i], bobSizes[j] <= 105
    Alice and Bob have a different total number of candies.
    There will be at least one valid answer for the given input.


 */