﻿using System.Diagnostics.SymbolStore;

namespace PracticeLeetCodeCS.Easy;

internal class FirstBadVersion278
{
    /* The isBadVersion API is defined in the parent class VersionControl.
      bool IsBadVersion(int version); */    
    public int FirstBadVersion(int n)
    {
        uint upperLimit = (uint)n;
        uint lowerLimit = 0;

        uint searchIndex;                
        
        while (true)
        {
            searchIndex = (upperLimit + lowerLimit) / 2;
            if (IsBadVersion((int)searchIndex))
                upperLimit = searchIndex;            
            else
                lowerLimit = searchIndex;                
            
            if (upperLimit - lowerLimit <= 1)
                return IsBadVersion((int)upperLimit) ? (int)upperLimit : (int)lowerLimit;
        }        
    }
    bool IsBadVersion(int version)
    {
        return version >= 1702766719;
    }
    public void Test()
    {        

        int retVal = FirstBadVersion(2126753390);
    }
}
/*
 278. First Bad Version
Easy

You are a product manager and currently leading a team to develop a new product. Unfortunately, the latest version of your product fails the quality check. Since each version is developed based on the previous version, all the versions after a bad version are also bad.

Suppose you have n versions [1, 2, ..., n] and you want to find out the first bad one, which causes all the following ones to be bad.

You are given an API bool isBadVersion(version) which returns whether version is bad. Implement a function to find the first bad version. You should minimize the number of calls to the API.

 

Example 1:

Input: n = 5, bad = 4
Output: 4
Explanation:
call isBadVersion(3) -> false
call isBadVersion(5) -> true
call isBadVersion(4) -> true
Then 4 is the first bad version.

Example 2:

Input: n = 1, bad = 1
Output: 1

 

Constraints:

    1 <= bad <= n <= 231 - 1


 */