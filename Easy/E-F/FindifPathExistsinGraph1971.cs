namespace PracticeLeetCodeCS.Easy;

internal class FindifPathExistsinGraph1971
{
    //TODO Graph BFS 
    public bool ValidPath(int n, int[][] edges, int source, int destination) 
    {
        
        return false;
    }
    public void Test()
    {

        int[][] edges1 = new int[10][];
        edges1[0] = new int[]{4,3};
        edges1[1] = new int[]{1,4};
        edges1[2] = new int[]{4,8};
        edges1[3] = new int[]{1,7};
        edges1[4] = new int[]{6,4};
        edges1[5] = new int[]{4,2};
        edges1[6] = new int[]{7,4};
        edges1[7] = new int[]{4,0};
        edges1[8] = new int[]{0,9};
        edges1[9] = new int[]{5,4};        

        var retVal1 = ValidPath(10, edges1, 5, 9);
        //[[0,7],[0,8],[6,1],[2,0],[0,4],[5,8],[4,7],[1,3],[3,5],[6,5]]
        //10,7,5

        /*int[][] edges1 = new int[10][];
        edges1[0] = new int[]{4,3};
        edges1[1] = new int[]{1,4};
        edges1[2] = new int[]{4,8};
        edges1[3] = new int[]{1,7};
        edges1[4] = new int[]{6,4};
        edges1[5] = new int[]{4,2};
        edges1[6] = new int[]{7,4};
        edges1[7] = new int[]{4,0};
        edges1[8] = new int[]{0,9};
        edges1[9] = new int[]{5,4};        

        var retVal1 = ValidPath(10, edges1, 5, 9);*/

        //[4,3],[1,4],[4,8],[1,7],[6,4],[4,2],[7,4],[4,0],[0,9],[5,4]
        //10,5,9

        //5->4
        //4->3,8,2,0
            //3->
            //8->
            //2->
            //0->9
    }
}

/*
1971. Find if Path Exists in Graph
Easy
2.8K
143
Companies

There is a bi-directional graph with n vertices, where each vertex is labeled from 0 to n - 1 (inclusive). The edges in the graph are represented as a 2D integer array edges, where each edges[i] = [ui, vi] denotes a bi-directional edge between vertex ui and vertex vi. Every vertex pair is connected by at most one edge, and no vertex has an edge to itself.

You want to determine if there is a valid path that exists from vertex source to vertex destination.

Given edges and the integers n, source, and destination, return true if there is a valid path from source to destination, or false otherwise.

 

Example 1:

Input: n = 3, edges = [[0,1],[1,2],[2,0]], source = 0, destination = 2
Output: true
Explanation: There are two paths from vertex 0 to vertex 2:
- 0 → 1 → 2
- 0 → 2

Example 2:

Input: n = 6, edges = [[0,1],[0,2],[3,5],[5,4],[4,3]], source = 0, destination = 5
Output: false
Explanation: There is no path from vertex 0 to vertex 5.

 

Constraints:

    1 <= n <= 2 * 105
    0 <= edges.length <= 2 * 105
    edges[i].length == 2
    0 <= ui, vi <= n - 1
    ui != vi
    0 <= source, destination <= n - 1
    There are no duplicate edges.
    There are no self edges.


*/