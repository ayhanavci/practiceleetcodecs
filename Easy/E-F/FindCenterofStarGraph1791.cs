﻿namespace PracticeLeetCodeCS.Easy;
internal class FindCenterofStarGraph1791
{
    public int FindCenter(int[][] edges)
    {
        Dictionary<int, int> points = new Dictionary<int, int>();

        foreach (var edge in edges)
        {
            int count;
            if (points.TryGetValue(edge[0], out count))
            {
                if (++points[edge[0]] == edges.Length) return edge[0];
            }
            else
            {
                points.Add(edge[0], 1);
            }

            if (points.TryGetValue(edge[1], out count))
            {                
                if (++points[edge[1]] == edges.Length) return edge[1];
            }
            else
            {
                points.Add(edge[1], 1);
            }
        }

        return 0;
    }
    public void Test()
    {
        int[][] edges = new int[4][];
        edges[0] = new int[] { 1, 2 };
        edges[1] = new int[] { 5, 1 };
        edges[2] = new int[] { 1, 3 };
        edges[3] = new int[] { 1, 4 };
        var retVal = FindCenter(edges);
    }
}

/*
 1791. Find Center of Star Graph
Easy
1K
130
Companies

There is an undirected star graph consisting of n nodes labeled from 1 to n. A star graph is a graph where there is one center node and exactly n - 1 edges that connect the center node with every other node.

You are given a 2D integer array edges where each edges[i] = [ui, vi] indicates that there is an edge between the nodes ui and vi. Return the center of the given star graph.

 

Example 1:

Input: edges = [[1,2],[2,3],[4,2]]
Output: 2
Explanation: As shown in the figure above, node 2 is connected to every other node, so 2 is the center.

Example 2:

Input: edges = [[1,2],[5,1],[1,3],[1,4]]
Output: 1

 

Constraints:

    3 <= n <= 105
    edges.length == n - 1
    edges[i].length == 2
    1 <= ui, vi <= n
    ui != vi
    The given edges represent a valid star graph.


 */