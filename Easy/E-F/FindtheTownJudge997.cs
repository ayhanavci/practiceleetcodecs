﻿namespace PracticeLeetCodeCS.Easy;

internal class FindtheTownJudge997
{
    public int FindJudge(int n, int[][] trust)
    {
        if (n == 1 && trust.Length == 0) return 1;

        Dictionary<int, int> trustCount = new Dictionary<int, int>();
        HashSet<int> trustingPeople = new HashSet<int>();

        for (int i = 0; i < trust.Length; ++i)
        {
            trustingPeople.Add(trust[i][0]);
            if (trustCount.ContainsKey(trust[i][1]))
                trustCount[trust[i][1]]++;
            else
                trustCount[trust[i][1]] = 1;
        }

        for (int i = 0; i < trustCount.Count; ++i)
        {
            int person = trustCount.ElementAt(i).Key;
            int count = trustCount.ElementAt(i).Value;

            if (count == n - 1 && !trustingPeople.Contains(person))
                return person;

        }

        return -1;
    }
    public void Test()
    {
        
    }
}

/*
 997. Find the Town Judge
Easy
4.2K
308
Companies

In a town, there are n people labeled from 1 to n. There is a rumor that one of these people is secretly the town judge.

If the town judge exists, then:

    The town judge trusts nobody.
    Everybody (except for the town judge) trusts the town judge.
    There is exactly one person that satisfies properties 1 and 2.

You are given an array trust where trust[i] = [ai, bi] representing that the person labeled ai trusts the person labeled bi.

Return the label of the town judge if the town judge exists and can be identified, or return -1 otherwise.

 

Example 1:

Input: n = 2, trust = [[1,2]]
Output: 2

Example 2:

Input: n = 3, trust = [[1,3],[2,3]]
Output: 3

Example 3:

Input: n = 3, trust = [[1,3],[2,3],[3,1]]
Output: -1

 

Constraints:

    1 <= n <= 1000
    0 <= trust.length <= 104
    trust[i].length == 2
    All the pairs of trust are unique.
    ai != bi
    1 <= ai, bi <= n


 */