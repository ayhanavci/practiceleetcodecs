﻿using System.Text;

namespace PracticeLeetCodeCS.Easy;

internal class ExcelSheetColumnTitle168
{
    public string ConvertToTitle(int columnNumber)
    {        
        StringBuilder title = new StringBuilder();   
        int cycle = columnNumber;
        int remainder = 0;
        char ch;

        while (cycle > 0)
        {
            remainder = cycle % 26;            
            if (remainder == 0)
            {
                ch = 'Z';
                cycle = (cycle / 26) - 1;
            }
            else
            {
                ch = (char)(remainder + 'A' - 1);
                cycle = cycle / 26;
            }
            title.Insert(0, ch);                                    
        }

        return title.ToString();
    }
    public void Test()
    {
        int columnNumber = 701;
        string s = ConvertToTitle(columnNumber);
        return;
        //string s = '';        
        //string s = ConvertToTitle(columNumber);
        int remainder = columnNumber % 26;
        
        if (remainder == 0)        
            s += 'Z';        
        else        
            s += (char)((char)remainder + 'A' - 1);

        if (columnNumber < 26)
        {
            return;
        }
        columnNumber = columnNumber / 26;
        remainder = columnNumber % 26;

        if (remainder == 0)
            s += 'Z';
        else
            s += (char)((char)remainder + 'A' - 1);

        if (columnNumber < 26)
        {
            return;
        }

        columnNumber = columnNumber / 26;
        remainder = columnNumber % 26;

        if (remainder == 0)
            s += 'Z';
        else
            s += (char)((char)remainder + 'A' - 1);

    }
}

/*
 168. Excel Sheet Column Title
Easy

Given an integer columnNumber, return its corresponding column title as it appears in an Excel sheet.

For example:

A -> 1
B -> 2
C -> 3
...
Z -> 26
AA -> 27
AB -> 28 
...

 

Example 1:

Input: columnNumber = 1
Output: "A"

Example 2:

Input: columnNumber = 28
Output: "AB"

Example 3:

Input: columnNumber = 701
Output: "ZY"

 

Constraints:

    1 <= columnNumber <= 231 - 1


 
 */