﻿namespace PracticeLeetCodeCS.Easy;

internal class FlippinganImage832
{
    public int[][] FlipAndInvertImage(int[][] image)
    {
        int[][] retVal = new int[image.Length][];

        for (int i = 0; i < image.Length; ++i)
        {
            retVal[i] = new int[image[i].Length];
            for (int j = 0; j < image[i].Length / 2; ++j)
            {
                int bitFromStart = image[i][j];
                int bitFromEnd = image[i][image[i].Length - j - 1];

                bitFromStart = bitFromStart == 0 ? 1 : 0;
                bitFromEnd = bitFromEnd == 0 ? 1 : 0;

                retVal[i][j] = bitFromEnd;
                retVal[i][image[i].Length - j - 1] = bitFromStart;
            }
            if (image[i].Length % 2 == 1)
                retVal[i][image[i].Length / 2] = image[i][image[i].Length / 2] == 0 ? 1 : 0;
        }

        return retVal;

    }
    public void Test()
    {
        int[][] image = new int[4][];
        image[0] = new int[] { 1, 1, 0, 0 };
        image[1] = new int[] { 1, 0, 0, 1 };
        image[2] = new int[] { 0, 1, 1, 1 };
        image[3] = new int[] { 1, 0, 1, 0 };

        var retVal = FlipAndInvertImage(image);

        //[[1,1,0,0],[1,0,0,1],[0,1,1,1],[1,0,1,0]]

        //rev [[0,0,1,1],[1,0,0,1],[1,1,1,0],[0,1,0,1]].
        //inv [[1,1,0,0],[0,1,1,0],[0,0,0,1],[1,0,1,0]]
    }
}

/*
 832. Flipping an Image
Easy
2.7K
216
Companies

Given an n x n binary matrix image, flip the image horizontally, then invert it, and return the resulting image.

To flip an image horizontally means that each row of the image is reversed.

    For example, flipping [1,1,0] horizontally results in [0,1,1].

To invert an image means that each 0 is replaced by 1, and each 1 is replaced by 0.

    For example, inverting [0,1,1] results in [1,0,0].

 

Example 1:

Input: image = [[1,1,0],[1,0,1],[0,0,0]]
Output: [[1,0,0],[0,1,0],[1,1,1]]
Explanation: First reverse each row: [[0,1,1],[1,0,1],[0,0,0]].
Then, invert the image: [[1,0,0],[0,1,0],[1,1,1]]

Example 2:

Input: image = [[1,1,0,0],[1,0,0,1],[0,1,1,1],[1,0,1,0]]
Output: [[1,1,0,0],[0,1,1,0],[0,0,0,1],[1,0,1,0]]
Explanation: First reverse each row: [[0,0,1,1],[1,0,0,1],[1,1,1,0],[0,1,0,1]].
Then invert the image: [[1,1,0,0],[0,1,1,0],[0,0,0,1],[1,0,1,0]]

 

Constraints:

    n == image.length
    n == image[i].length
    1 <= n <= 20
    images[i][j] is either 0 or 1.


 */