using System.Text;
namespace PracticeLeetCodeCS.Easy;

internal class FindResultantArrayAfterRemovingAnagrams2273
{
    public IList<string> RemoveAnagrams(string[] words) 
    {
        List<string> wordsList = new List<string>(words);

        for (int i = 1; i < wordsList.Count; ++i)
        {
            StringBuilder refWord = new StringBuilder(wordsList[i]);
            StringBuilder prevWord = new StringBuilder(wordsList[i-1]);
            
            if (refWord.Length != prevWord.Length) continue;
            
            for (int j = 0; j < wordsList[i].Length; ++j)
            {
                int index = prevWord.ToString().IndexOf(refWord[0]);
                if (index == -1) break;                
                refWord.Remove(0, 1);
                prevWord.Remove(index, 1);
            }
            if (refWord.Length == 0 && prevWord.Length == 0)                                
                wordsList.RemoveAt(i--);                    
            
        }

        return wordsList;
    }
    public void Test()
    {
        //string[] words = {"abba","baba","bbaa","cd","cd"};
        string[] words = {"a","b","c","d","e"};
        var retVal = RemoveAnagrams(words);
    }
}
/*
for (int j = i + 1; j < wordsList.Count; ++j)
            {
                StringBuilder nextWord = new StringBuilder(wordsList[j]);
                bool fMatch = true;
                for (int k = 0; k < refWord.Length; ++k)
                {
                    int index = nextWord.ToString().IndexOf(refWord[i]);
                    if (index == -1) 
                    {
                        fMatch = false;
                        break;
                    }                        
                    nextWord.Remove(index, 1);                    
                }
                if (fMatch && nextWord.Length == 0)
                {
                    wordsList.RemoveAt(j);
                    i--;
                    break;
                }
            }
*/
/*
2273. Find Resultant Array After Removing Anagrams
Easy
508
128
Companies

You are given a 0-indexed string array words, where words[i] consists of lowercase English letters.

In one operation, select any index i such that 0 < i < words.length and words[i - 1] and words[i] are anagrams, and delete words[i] from words. Keep performing this operation as long as you can select an index that satisfies the conditions.

Return words after performing all operations. It can be shown that selecting the indices for each operation in any arbitrary order will lead to the same result.

An Anagram is a word or phrase formed by rearranging the letters of a different word or phrase using all the original letters exactly once. For example, "dacb" is an anagram of "abdc".

 

Example 1:

Input: words = ["abba","baba","bbaa","cd","cd"]
Output: ["abba","cd"]
Explanation:
One of the ways we can obtain the resultant array is by using the following operations:
- Since words[2] = "bbaa" and words[1] = "baba" are anagrams, we choose index 2 and delete words[2].
  Now words = ["abba","baba","cd","cd"].
- Since words[1] = "baba" and words[0] = "abba" are anagrams, we choose index 1 and delete words[1].
  Now words = ["abba","cd","cd"].
- Since words[2] = "cd" and words[1] = "cd" are anagrams, we choose index 2 and delete words[2].
  Now words = ["abba","cd"].
We can no longer perform any operations, so ["abba","cd"] is the final answer.

Example 2:

Input: words = ["a","b","c","d","e"]
Output: ["a","b","c","d","e"]
Explanation:
No two adjacent strings in words are anagrams of each other, so no operations are performed.

 

Constraints:

    1 <= words.length <= 100
    1 <= words[i].length <= 10
    words[i] consists of lowercase English letters.


*/