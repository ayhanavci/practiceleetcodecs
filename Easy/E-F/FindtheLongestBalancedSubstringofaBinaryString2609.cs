namespace PracticeLeetCodeCS.Easy;

internal class FindtheLongestBalancedSubstringofaBinaryString2609
{
    public int FindTheLongestBalancedSubstring(string s) 
    {
        int longest = 0;
        int currentZeros = 0;
        int currentOnes = 0;
        
        if (s[0] == '0') currentZeros++;

        for (int i = 1; i < s.Length; ++i)
        {
            if (s[i] == '0') 
            {
                if (currentOnes > 0)
                {
                    int minDigits = Math.Min(currentOnes, currentZeros);
                    longest = Math.Max(longest, minDigits * 2);
                    currentZeros = 1;
                    currentOnes = 0;
                }
                else
                {
                    currentZeros++;
                }             
            }                
            else 
            {    
                if (++currentOnes <= currentZeros)                
                    longest = Math.Max(longest, currentOnes * 2);                                
            }            
        }
        return longest;
    }
    public void Test()
    {
        var retVal1 = FindTheLongestBalancedSubstring("001");
        //var retVal2 = FindTheLongestBalancedSubstring("00111");
        //var retVal3 = FindTheLongestBalancedSubstring("111");
    }
}
/*
2609. Find the Longest Balanced Substring of a Binary String
Easy
285
14
Companies

You are given a binary string s consisting only of zeroes and ones.

A substring of s is considered balanced if all zeroes are before ones and the number of zeroes is equal to the number of ones inside the substring. Notice that the empty substring is considered a balanced substring.

Return the length of the longest balanced substring of s.

A substring is a contiguous sequence of characters within a string.

 

Example 1:

Input: s = "01000111"
Output: 6
Explanation: The longest balanced substring is "000111", which has length 6.

Example 2:

Input: s = "00111"
Output: 4
Explanation: The longest balanced substring is "0011", which has length 4. 

Example 3:

Input: s = "111"
Output: 0
Explanation: There is no balanced substring except the empty substring, so the answer is 0.

 

Constraints:

    1 <= s.length <= 50
    '0' <= s[i] <= '1'


*/
