﻿namespace PracticeLeetCodeCS.Easy;

internal class ElementAppearingMoreThan25InSortedArray1287
{
    public int FindSpecialInteger(int[] arr)
    {
        int percent = (int)MathF.Ceiling(arr.Length / 4);
                
        int currentNo = arr[0];        
        int currentCount = 1;

        for (int i = 1; i < arr.Length; i++)
        {
            if (arr[i] == currentNo)
            {
                currentCount++;
                if (currentCount > percent)
                    break;
            }                
            else
            {
                currentNo = arr[i];
                currentCount = 1;
            }
        }
        return currentNo;
    }
    public void Test()
    {
        int[] arr = { 1, 2, 3, 3 };
        var retVal = FindSpecialInteger(arr);
    }
}

/*
 1287. Element Appearing More Than 25% In Sorted Array
Easy
774
41
Companies

Given an integer array sorted in non-decreasing order, there is exactly one integer in the array that occurs more than 25% of the time, return that integer.

 

Example 1:

Input: arr = [1,2,2,6,6,6,6,7,10]
Output: 6

Example 2:

Input: arr = [1,1]
Output: 1

 

Constraints:

    1 <= arr.length <= 104
    0 <= arr[i] <= 105


 */