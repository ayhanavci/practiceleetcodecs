namespace PracticeLeetCodeCS.Easy;
//TODO: Tekrar üzerinden geç
internal class FindSubsequenceofLengthKWiththeLargestSum2099
{
    public int[] MaxSubsequence(int[] nums, int k) 
    {        
        int length = nums.Length;
        int[] sortedNums = new int[length];                
        
        Array.Copy(nums, sortedNums, length);
        Array.Sort(sortedNums);    
                        
        List<int> excludes = new List<int>();
        for (int i = 0; i < length - k; ++i) excludes.Add(sortedNums[i]);    

        int index = 0;
        int[] maxSeq = new int[k];
        foreach (int number in nums)
        {
            if (excludes.Contains(number)) excludes.Remove(number);
            else maxSeq[index++] = number;
        }    
        
        return maxSeq;
    }
    
    public void Test()
    {
        int[] nums0 = {2,1,3,3};
        var retVal0 = MaxSubsequence(nums0, 2);

        int[] nums1 = {-1,-2,3,4};
        var retVal1 = MaxSubsequence(nums1, 3);

        int[] nums2 = {3, 4, 3, 3};
        var retVal2 = MaxSubsequence(nums2, 2);
    }
}
//Keep a copy of original array. Then sort original array and take first nums.Length-k numbers. Remove them one by one from the copy array of original array. Then we get the result.
/*
2099. Find Subsequence of Length K With the Largest Sum
Easy
992
92
Companies

You are given an integer array nums and an integer k. You want to find a subsequence of nums of length k that has the largest sum.

Return any such subsequence as an integer array of length k.

A subsequence is an array that can be derived from another array by deleting some or no elements without changing the order of the remaining elements.

 

Example 1:

Input: nums = [2,1,3,3], k = 2
Output: [3,3]
Explanation:
The subsequence has the largest sum of 3 + 3 = 6.

Example 2:

Input: nums = [-1,-2,3,4], k = 3
Output: [-1,3,4]
Explanation: 
The subsequence has the largest sum of -1 + 3 + 4 = 6.

Example 3:

Input: nums = [3,4,3,3], k = 2
Output: [3,4]
Explanation:
The subsequence has the largest sum of 3 + 4 = 7. 
Another possible subsequence is [4, 3].

 

Constraints:

    1 <= nums.length <= 1000
    -105 <= nums[i] <= 105
    1 <= k <= nums.length


*/