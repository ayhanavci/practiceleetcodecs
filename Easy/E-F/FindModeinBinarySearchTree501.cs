﻿namespace PracticeLeetCodeCS.Easy;

internal class FindModeinBinarySearchTree501
{
    //Definition for a binary tree node.
    public class TreeNode
    {
        public int val;
        public TreeNode left;
        public TreeNode right;
        public TreeNode(int val = 0, TreeNode left = null, TreeNode right = null)
        {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }
    Dictionary<int, int> frequency = new Dictionary<int, int>();
    public int[] FindMode(TreeNode root)
    {
        if (root == null) return null;
        if (frequency.ContainsKey(root.val))        
            frequency[root.val]++;
        else
            frequency[root.val] = 1;
        FindMode(root.left);
        FindMode(root.right);

        List<int> result = new List<int>();
        int maxOccurance = 0;

        foreach (KeyValuePair<int, int> kvp in frequency)
        {
            if (kvp.Value > maxOccurance)
            {
                maxOccurance = kvp.Value;
                result.Clear();
                result.Add(kvp.Key);
            }            
            else if (kvp.Value == maxOccurance) 
            {
                result.Add(kvp.Key);
            }
        }
        return result.ToArray();
    }
    public void Test()
    {
        TreeNode left_right = new TreeNode(3);

        TreeNode left_left_right = new TreeNode(4);
        TreeNode left_left_left = new TreeNode(4);
        TreeNode left_left = new TreeNode(3, left_left_left, left_left_right);

        TreeNode left = new TreeNode(3, left_left, left_right);
        TreeNode right = new TreeNode(4);

        TreeNode root = new TreeNode(1, left, right);

        int[] result = FindMode(root);
    }
}
/*
 501. Find Mode in Binary Search Tree
Easy

Given the root of a binary search tree (BST) with duplicates, return all the mode(s) (i.e., the most frequently occurred element) in it.

If the tree has more than one mode, return them in any order.

Assume a BST is defined as follows:

    The left subtree of a node contains only nodes with keys less than or equal to the node"s key.
    The right subtree of a node contains only nodes with keys greater than or equal to the node"s key.
    Both the left and right subtrees must also be binary search trees.

 

Example 1:

Input: root = [1,null,2,2]
Output: [2]

Example 2:

Input: root = [0]
Output: [0]

 

Constraints:

    The number of nodes in the tree is in the range [1, 104].
    -105 <= Node.val <= 105

 */