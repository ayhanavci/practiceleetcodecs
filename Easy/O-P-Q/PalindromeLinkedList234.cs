﻿using System.Collections.Generic;

namespace PracticeLeetCodeCS.Easy;

internal class PalindromeLinkedList234
{
    //Definition for singly-linked list.
    public class ListNode
    {
        public int val;
        public ListNode? next;
        public ListNode(int val = 0, ListNode? next = null)
        {
            this.val = val;
            this.next = next;
        }
    }
    public bool IsPalindrome(ListNode head)
    {
        if (head.next == null)
            return true;
        Stack<int> stack = new Stack<int>();

        ListNode? current = head;
        while (current != null)
        {
            stack.Push(current.val);
            current = current.next;
        }       
        current = head;
        int center = stack.Count / 2;
        for (int i = 0; i < center; ++i)
        {
            int item = stack.Pop();
            if (current?.val != item)
                return false;
            current = current.next;
        }

        return true;
    }
    public bool IsPalindrome2(ListNode head)
    {
        if (head.next == null)
            return true;

        
        return true;
    }
    public void Test()
    {
        ListNode list13 = new ListNode(1);
        ListNode list12 = new ListNode(1, list13);
        ListNode list11 = new ListNode(2, list12);
        ListNode list1 = new ListNode(1, list11);

        bool retval = IsPalindrome2(list1);
    }
}

/*
 234. Palindrome Linked List
Easy

Given the head of a singly linked list, return true if it is a palindrome or false otherwise.

 

Example 1:

Input: head = [1,2,2,1]
Output: true

Example 2:

Input: head = [1,2]
Output: false

 

Constraints:

    The number of nodes in the list is in the range [1, 105].
    0 <= Node.val <= 9

 
Follow up: Could you do it in O(n) time and O(1) space?
 */