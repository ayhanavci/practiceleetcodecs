﻿namespace PracticeLeetCodeCS.Easy;

internal class PowerofTwo231
{
    public bool IsPowerOfTwo(int n)
    {        
        if (n <= 0) return false;

        while (n > 2)
        {
          
            if (n % 2 != 0) 
                return false;
            n /= 2;           
        }
        return true;
    }
    public void Test()
    {
        bool retVal = IsPowerOfTwo(32);
    }
}

/*
231. Power of Two
Easy

Given an integer n, return true if it is a power of two.Otherwise, return false.

An integer n is a power of two, if there exists an integer x such that n == 2x.



Example 1:

Input: n = 1
Output: true
Explanation: 20 = 1

Example 2:

Input: n = 16
Output: true
Explanation: 24 = 16

Example 3:

Input: n = 3
Output: false




Constraints:

    -231 <= n <= 231 - 1
*/