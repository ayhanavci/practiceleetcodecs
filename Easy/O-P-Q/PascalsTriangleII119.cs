﻿namespace PracticeLeetCodeCS.Easy;

internal class PascalsTriangleII119
{
    public IList<int> GetRow(int rowIndex)
    {
        List<int> nextRow = new List<int>();
        List<int> previousRow = new List<int>();
        previousRow.Add(1);

        for (int i = 1; i < rowIndex + 2; ++i)
        {
            nextRow = new List<int>();
            for (int j = 0; j < i; ++j)
            {
                int leftNumber = j > 0 ? previousRow[j - 1] : 0;
                int rightNumber = previousRow.Count > j ? previousRow[j] : 0;

                nextRow.Add(leftNumber + rightNumber);
            }
            
            previousRow = nextRow;
        }
        return nextRow;
    }
    public void Test()
    {
        IList<int> result = GetRow(3);
    }
}

/*
 119. Pascal's Triangle II
Easy

Given an integer rowIndex, return the rowIndexth (0-indexed) row of the Pascal's triangle.

In Pascal's triangle, each number is the sum of the two numbers directly above it as shown:

 

Example 1:

Input: rowIndex = 3
Output: [1,3,3,1]

Example 2:

Input: rowIndex = 0
Output: [1]

Example 3:

Input: rowIndex = 1
Output: [1,1]

 

Constraints:

    0 <= rowIndex <= 33

 

Follow up: Could you optimize your algorithm to use only O(rowIndex) extra space?

 */