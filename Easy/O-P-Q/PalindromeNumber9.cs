﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticeLeetCodeCS.Easy;

public class PalindromeNumber9
{
    private bool IsPalindrome(int x)
    {
        if (x < 0) return false;

        string s = x.ToString();
        for (int i = 0; i < s.Length / 2; ++i)
        {
            if (s[i] != s[s.Length - i - 1])
                return false;
        }

        return true;
    }

    public void Test()
    {
        int x = 121;
        Console.WriteLine($"PalindromeNumberP. Number:{x} Result:{IsPalindrome(x)}");

        x = -121;
        Console.WriteLine($"PalindromeNumberP. Number:{x} Result:{IsPalindrome(x)}");

        x = 10;
        Console.WriteLine($"PalindromeNumberP. Number:{x} Result:{IsPalindrome(x)}");

        x = 34543;
        Console.WriteLine($"PalindromeNumberP. Number:{x} Result:{IsPalindrome(x)}");

        x = 34533;
        Console.WriteLine($"PalindromeNumberP. Number:{x} Result:{IsPalindrome(x)}");

        x = 896698;
        Console.WriteLine($"PalindromeNumberP. Number:{x} Result:{IsPalindrome(x)}");
    }
}

/*
 9. Palindrome Number
Easy

Given an integer x, return true if x is a palindrome, and false otherwise.

 

Example 1:

Input: x = 121
Output: true
Explanation: 121 reads as 121 from left to right and from right to left.

Example 2:

Input: x = -121
Output: false
Explanation: From left to right, it reads -121. From right to left, it becomes 121-. Therefore it is not a palindrome.

Example 3:

Input: x = 10
Output: false
Explanation: Reads 01 from right to left. Therefore it is not a palindrome.

 

Constraints:

    -231 <= x <= 231 - 1

 
Follow up: Could you solve it without converting the integer to a string?
 
 */