﻿namespace PracticeLeetCodeCS.Easy;

internal class PrimeNumberofSetBitsinBinaryRepresentation762
{
    public int CountPrimeSetBits(int left, int right)
    {
        int totalPrimeBits = 0;
        for (int i = left; i <= right; ++i)
        {
            //Get number of set bits
            int setBitCount = 0;
            int current = i;
            while (current > 0)
            {
                if ((current & 1) == 1) setBitCount++;
                current >>= 1;
            }

            //Check if number of set bits is prime
            if (setBitCount <= 1) continue;            
            if (setBitCount == 2) { totalPrimeBits++; continue; }
            if (setBitCount % 2 == 0) continue;            

            bool isPrime = true;            
            for (int j = 2; j < setBitCount / 2; ++j)
            {
                if (setBitCount % j == 0)
                {
                    isPrime = false;
                    break;
                }

            }
            if (isPrime) totalPrimeBits++;

        }
        return totalPrimeBits;
    }
    public void Test()
    {
        var retVal = CountPrimeSetBits(6, 10);
        retVal = CountPrimeSetBits(990, 1048);//28
    }
}

/*
 762. Prime Number of Set Bits in Binary Representation
Easy
570
487
Companies

Given two integers left and right, return the count of numbers in the inclusive range [left, right] having a prime number of set bits in their binary representation.

Recall that the number of set bits an integer has is the number of 1's present when written in binary.

    For example, 21 written in binary is 10101, which has 3 set bits.

 

Example 1:

Input: left = 6, right = 10
Output: 4
Explanation:
6  -> 110 (2 set bits, 2 is prime)
7  -> 111 (3 set bits, 3 is prime)
8  -> 1000 (1 set bit, 1 is not prime)
9  -> 1001 (2 set bits, 2 is prime)
10 -> 1010 (2 set bits, 2 is prime)
4 numbers have a prime number of set bits.

Example 2:

Input: left = 10, right = 15
Output: 5
Explanation:
10 -> 1010 (2 set bits, 2 is prime)
11 -> 1011 (3 set bits, 3 is prime)
12 -> 1100 (2 set bits, 2 is prime)
13 -> 1101 (3 set bits, 3 is prime)
14 -> 1110 (3 set bits, 3 is prime)
15 -> 1111 (4 set bits, 4 is not prime)
5 numbers have a prime number of set bits.

 

Constraints:

    1 <= left <= right <= 106
    0 <= right - left <= 104


 */