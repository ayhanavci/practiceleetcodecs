﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticeLeetCodeCS.Easy;

public class PlusOne66
{
    public int[] PlusOne(int[] digits)
    {
        Stack<int> result = new Stack<int>();

        int index = digits.Length - 1;
        int sum = digits[index] + 1;
        result.Push(sum % 10);

        while (index > 0)
        {            
            sum = digits[--index] + (sum > 9 ? 1 : 0);
            result.Push(sum % 10);
        } 
        if (sum > 9) result.Push(1);

        int[] retVal = new int[result.Count];

        for (int i = 0; result.Count > 0; ++i)
        {
            retVal[i] = result.Pop();
        }

        return retVal;
    }
    public void Test()
    {
        //int[] digits = { 1, 2, 3 };
        //int[] digits = { 4,3,2,2 };
        //int[] digits = { 9 };
        int[] digits = { 9, 9 };

        int[] result = PlusOne(digits);
    }
}

/*
 66. Plus One
Easy

You are given a large integer represented as an integer array digits, where each digits[i] is the ith digit of the integer. The digits are ordered from most significant to least significant in left-to-right order. The large integer does not contain any leading 0's.

Increment the large integer by one and return the resulting array of digits.

 

Example 1:

Input: digits = [1,2,3]
Output: [1,2,4]
Explanation: The array represents the integer 123.
Incrementing by one gives 123 + 1 = 124.
Thus, the result should be [1,2,4].

Example 2:

Input: digits = [4,3,2,1]
Output: [4,3,2,2]
Explanation: The array represents the integer 4321.
Incrementing by one gives 4321 + 1 = 4322.
Thus, the result should be [4,3,2,2].

Example 3:

Input: digits = [9]
Output: [1,0]
Explanation: The array represents the integer 9.
Incrementing by one gives 9 + 1 = 10.
Thus, the result should be [1,0].

 

Constraints:

    1 <= digits.length <= 100
    0 <= digits[i] <= 9
    digits does not contain any leading 0's.


 */