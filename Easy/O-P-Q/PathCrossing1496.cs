﻿namespace PracticeLeetCodeCS.Easy;

internal class PathCrossing1496
{
    public bool IsPathCrossing(string path)
    {
        List<int> pastLats = new List<int>();
        List<int> pastLons = new List<int>();

        int lat = 0;
        int lon = 0;                
        pastLats.Add(lat);
        pastLons.Add(lon); 

        for (int i = 0; i < path.Length; i++)
        {
            if (path[i] == 'N')
                lat += 1;
            else if (path[i] == 'S')
                lat -= 1;
            else if (path[i] == 'E')
                lon += 1;
            else if (path[i] == 'W')
                lon -= 1;
            pastLats.Add(lat);
            pastLons.Add(lon);
        }
        for (int i = 0; i < pastLats.Count; ++i)
        {
            for (int j = i + 1; j < pastLats.Count; ++j)
            {
                if (pastLats[i] == pastLats[j] && pastLons[i] == pastLons[j])
                    return true;
            }
        }

        return false;
    }
    public void Test()
    {
        var retVal = IsPathCrossing("NESWW");
    }
}

/*
 1496. Path Crossing
Easy
591
13
Companies

Given a string path, where path[i] = 'N', 'S', 'E' or 'W', each representing moving one unit north, south, east, or west, respectively. You start at the origin (0, 0) on a 2D plane and walk on the path specified by path.

Return true if the path crosses itself at any point, that is, if at any time you are on a location you have previously visited. Return false otherwise.

 

Example 1:

Input: path = "NES"
Output: false 
Explanation: Notice that the path doesn't cross any point more than once.

Example 2:

Input: path = "NESWW"
Output: true
Explanation: Notice that the path visits the origin twice.

 

Constraints:

    1 <= path.length <= 104
    path[i] is either 'N', 'S', 'E', or 'W'.


 */