namespace PracticeLeetCodeCS.Easy;

internal class OddStringDifference2451
{
    public string OddString(string[] words) 
    {
        List<int> array0 = new List<int>();
        List<int> array1 = new List<int>();
        List<int> array2 = new List<int>();
        List<int> arrayDefault;

        for (int i = 0; i < words[0].Length - 1; ++i)        
            array0.Add((words[0][i+1] - 'a') - (words[0][i] - 'a'));

        for (int i = 0; i < words[1].Length - 1; ++i)        
            array1.Add((words[1][i+1] - 'a') - (words[1][i] - 'a'));

        for (int i = 0; i < words[2].Length - 1; ++i)        
            array2.Add((words[2][i+1] - 'a') - (words[2][i] - 'a'));
        
        for (int i = 0; i < array0.Count; ++i)
        {
            if (array0[i] != array1[i])
            {
                if (array0[i] != array2[i]) return words[0];
                else return words[1];
            }   
            else if (array0[i] != array2[i])
            {
                return words[2];
            }
        }
        arrayDefault = array0;        
        for (int i = 3; i < words.Length; ++i)
        {
            List<int> arrayCheck = new List<int>();
            for (int j = 0; j < words[i].Length - 1; ++j)
                arrayCheck.Add((words[i][j+1] - 'a') - (words[i][j] - 'a'));
            for (int j = 0; j < arrayDefault.Count; ++j)
                if (arrayDefault[j] != arrayCheck[j]) return words[i];

        }
        return "";    
    }
    public void Test()
    {
        string[] words = {"adc","wzy","adc", "wzy","abc", "adc"};
        var retVal1 = OddString(words);
    }
}
/*
2451. Odd String Difference
Easy
312
90
Companies

You are given an array of equal-length strings words. Assume that the length of each string is n.

Each string words[i] can be converted into a difference integer array difference[i] of length n - 1 where difference[i][j] = words[i][j+1] - words[i][j] where 0 <= j <= n - 2. Note that the difference between two letters is the difference between their positions in the alphabet i.e. the position of 'a' is 0, 'b' is 1, and 'z' is 25.

    For example, for the string "acb", the difference integer array is [2 - 0, 1 - 2] = [2, -1].

All the strings in words have the same difference integer array, except one. You should find that string.

Return the string in words that has different difference integer array.

 

Example 1:

Input: words = ["adc","wzy","abc"]
Output: "abc"
Explanation: 
- The difference integer array of "adc" is [3 - 0, 2 - 3] = [3, -1].
- The difference integer array of "wzy" is [25 - 22, 24 - 25]= [3, -1].
- The difference integer array of "abc" is [1 - 0, 2 - 1] = [1, 1]. 
The odd array out is [1, 1], so we return the corresponding string, "abc".

Example 2:

Input: words = ["aaa","bob","ccc","ddd"]
Output: "bob"
Explanation: All the integer arrays are [0, 0] except for "bob", which corresponds to [13, -13].

 

Constraints:

    3 <= words.length <= 100
    n == words[i].length
    2 <= n <= 20
    words[i] consists of lowercase English letters.


*/