﻿using System.Collections.Generic;

namespace PracticeLeetCodeCS.Easy;

internal class OccurrencesAfterBigram1078
{
    public string[] FindOcurrences(string text, string first, string second)
    {
        List<string> result = new List<string>();
        string[] sentence = text.Split(' ');
        for (int i = 0; i < sentence.Length - 2; i++)
        {
            if (sentence[i].Equals(first) && sentence[i + 1].Equals(second))
                result.Add(sentence[i+2]);
        }
        return result.ToArray();
    }
    public void Test()
    {
        string text = "alice is a good girl she is a good student";
        string first = "a";
        string second = "good";

        var retVal = FindOcurrences(text, first, second);
    }
}

/*
 1078. Occurrences After Bigram
Easy
395
318
Companies

Given two strings first and second, consider occurrences in some text of the form "first second third", where second comes immediately after first, and third comes immediately after second.

Return an array of all the words third for each occurrence of "first second third".

 

Example 1:

Input: text = "alice is a good girl she is a good student", first = "a", second = "good"
Output: ["girl","student"]

Example 2:

Input: text = "we will we will rock you", first = "we", second = "will"
Output: ["we","rock"]

 

Constraints:

    1 <= text.length <= 1000
    text consists of lowercase English letters and spaces.
    All the words in text a separated by a single space.
    1 <= first.length, second.length <= 10
    first and second consist of lowercase English letters.


 */