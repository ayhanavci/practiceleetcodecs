namespace PracticeLeetCodeCS.Easy;

internal class PercentageofLetterinString2278
{
    public int PercentageLetter(string s, char letter) 
    {
        int count = 0;
        foreach (var ch in s)        
            if (ch == letter) count++;
        double ratio = (double)count / s.Length;
        return (int)(ratio * 100);
    }
    public void Test()
    {
        var retVal = PercentageLetter("foobar", 'o');
    }
}
/*
2278. Percentage of Letter in String
Easy
396
44
Companies

Given a string s and a character letter, return the percentage of characters in s that equal letter rounded down to the nearest whole percent.

 

Example 1:

Input: s = "foobar", letter = "o"
Output: 33
Explanation:
The percentage of characters in s that equal the letter 'o' is 2 / 6 * 100% = 33% when rounded down, so we return 33.

Example 2:

Input: s = "jjjj", letter = "k"
Output: 0
Explanation:
The percentage of characters in s that equal the letter 'k' is 0%, so we return 0.

 

Constraints:

    1 <= s.length <= 100
    s consists of lowercase English letters.
    letter is a lowercase English letter.


*/