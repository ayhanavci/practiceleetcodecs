﻿namespace PracticeLeetCodeCS.Easy;

internal class PartitionArrayIntoThreePartsWithEqualSum1013
{
    public bool CanThreePartsEqualSum(int[] arr)
    {
        int sum1 = 0;
        int sum2 = 0;
        int sum3 = 0;
        for (int i = 0; i < arr.Length; i++)
        {
            sum1 += arr[i];
            sum2 = 0;

            for (int j = i + 1; j < arr.Length; ++j)
            {
                sum2 += arr[j];                
                if (sum1 == sum2)
                {
                    sum3 = 0;
                    if (j + 1 < arr.Length)
                    {
                        for (int k = j + 1; k < arr.Length; ++k)
                            sum3 += arr[k];
                        if (sum1 == sum3)
                            return true;
                    }
                    
                }
            }
        }

        return false;
    }
    public void Test()
    {
        int[] arr1 = { 0, 2, 1, -6, 6, -7, 9, 1, 2, 0, 1 };
        //var retval = CanThreePartsEqualSum(arr1);

        int[] arr2 = { 1, -1, 1, -1 };
        var retval2 = CanThreePartsEqualSum(arr2);
    }
}
/*
 
    Suppose the array was indeed breakable into 3 parts with equal sum. Then the array would look like S S S, where S represents the sum
    of each segment of the array. Hence, the entire sum would be S+S+S=3S.
    Assume that the array is breakable into such segments. Let us compute the prefix sum of the array. Since the array resembles S S S,
    therefore the prefix sum would resemble S 2S 3S.
    So we just need to check if the prefix sum contains S 2S & 3S (in the same order). Since the sum is already 3S, we do not need to
    worry about 3S. All that remains is to check whether the prefix sum contains S and 2S such that 2S is to the right of S.
    [Update ---- We do have to check for 3S.]

 */
/*
 1013. Partition Array Into Three Parts With Equal Sum
Easy
1.4K
134
Companies

Given an array of integers arr, return true if we can partition the array into three non-empty parts with equal sums.

Formally, we can partition the array if we can find indexes i + 1 < j with (arr[0] + arr[1] + ... + arr[i] == arr[i + 1] + arr[i + 2] + ... + arr[j - 1] == arr[j] + arr[j + 1] + ... + arr[arr.length - 1])

 

Example 1:

Input: arr = [0,2,1,-6,6,-7,9,1,2,0,1]
Output: true
Explanation: 0 + 2 + 1 = -6 + 6 - 7 + 9 + 1 = 2 + 0 + 1

Example 2:

Input: arr = [0,2,1,-6,6,7,9,-1,2,0,1]
Output: false

Example 3:

Input: arr = [3,3,6,5,-2,2,5,1,-9,4]
Output: true
Explanation: 3 + 3 = 6 = 5 - 2 + 2 + 5 + 1 - 9 + 4

 

Constraints:

    3 <= arr.length <= 5 * 104
    -104 <= arr[i] <= 104


 */