﻿namespace PracticeLeetCodeCS.Easy;

internal class MaximumProductofThreeNumbers628
{
    public int MaximumProduct(int[] nums)
    {
        Array.Sort(nums);
        return Math.Max(
            nums[0] * nums[1] * nums[nums.Length-1],
            nums[nums.Length - 1] * nums[nums.Length - 2] * nums[nums.Length - 3]);
    }
    public void Test()
    {

    }
}

/*
  If you will think for a second, you may realize that the maximum product of any three numbers would be the product of the greatest three numbers or the smallest two numbers with the greatest number from the nums array.

The reason why the maximum product of any triplet could have two smallest numbers because there could be negative numbers in the nums array and the product of the two smallest numbers could be largely positive if the numbers are greater than the 2nd and 3rd largest number of the array.

So if we will sort the array in ascending order, then the largest three numbers will be available at the end of the array. Also, the smallest two numbers will be available at the beginning of the array. 
[-5, -7, 4, 2, 1, 9]
 */
/*
 628. Maximum Product of Three Numbers
Easy
3.4K
574
Companies

Given an integer array nums, find three numbers whose product is maximum and return the maximum product.

 

Example 1:

Input: nums = [1,2,3]
Output: 6

Example 2:

Input: nums = [1,2,3,4]
Output: 24

Example 3:

Input: nums = [-1,-2,-3]
Output: -6

 

Constraints:

    3 <= nums.length <= 104
    -1000 <= nums[i] <= 1000


 */