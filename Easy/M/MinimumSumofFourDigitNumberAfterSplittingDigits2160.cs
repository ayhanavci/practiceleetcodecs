namespace PracticeLeetCodeCS.Easy;

internal class MinimumSumofFourDigitNumberAfterSplittingDigits2160
{
    public int MinimumSum(int num) 
    {
        List<int> digits = new List<int>();
        while (num != 0)
        {
            digits.Add(num % 10);
            num /= 10;
        }
        digits.Sort();
        int number1 = digits[0] * 10;
        int number2 = digits[1] * 10;
        number1 += digits[2];
        number2 += digits[3];

        return number1 + number2;
    }
    public void Test()
    {

    }
}
/*
2160. Minimum Sum of Four Digit Number After Splitting Digits
Easy
1.1K
117
Companies

You are given a positive integer num consisting of exactly four digits. Split num into two new integers new1 and new2 by using the digits found in num. Leading zeros are allowed in new1 and new2, and all the digits found in num must be used.

    For example, given num = 2932, you have the following digits: two 2's, one 9 and one 3. Some of the possible pairs [new1, new2] are [22, 93], [23, 92], [223, 9] and [2, 329].

Return the minimum possible sum of new1 and new2.

 

Example 1:

Input: num = 2932
Output: 52
Explanation: Some possible pairs [new1, new2] are [29, 23], [223, 9], etc.
The minimum sum can be obtained by the pair [29, 23]: 29 + 23 = 52.

Example 2:

Input: num = 4009
Output: 13
Explanation: Some possible pairs [new1, new2] are [0, 49], [490, 0], etc. 
The minimum sum can be obtained by the pair [4, 9]: 4 + 9 = 13.

 

Constraints:

    1000 <= num <= 9999


*/