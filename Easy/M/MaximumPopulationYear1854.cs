namespace PracticeLeetCodeCS.Easy;

internal class MaximumPopulationYear1854
{
    public int MaximumPopulation(int[][] logs) 
    {
        Dictionary<int, int> yearPopulation = new Dictionary<int, int>();        
        int maxPop = 0;
        int maxYear = 0;

        for (int y = 1950; y < 2050; ++y) 
        {
            yearPopulation.Add(y, 0);
            for (int i = 0; i < logs.Length; ++i)
            {
              
                if (logs[i][0] <= y && logs[i][1] > y)   
                {
                    int yPop = ++yearPopulation[y];
                    if (yPop > maxPop) 
                    {
                        maxPop = yPop;
                        maxYear = y;
                    }                    
                }                                                  
            }
        }
        return maxYear;
    }
    public void Test()
    {
        int[][] logs = new int[2][];
        logs[0] = new int[] {1993, 1999};
        logs[1] = new int[] {2000, 2010};

        var retVal = MaximumPopulation(logs);
    }
}
/*

1854. Maximum Population Year
Easy
937
155
Companies

You are given a 2D integer array logs where each logs[i] = [birthi, deathi] indicates the birth and death years of the ith person.

The population of some year x is the number of people alive during that year. The ith person is counted in year x's population if x is in the inclusive range [birthi, deathi - 1]. Note that the person is not counted in the year that they die.

Return the earliest year with the maximum population.

 

Example 1:

Input: logs = [[1993,1999],[2000,2010]]
Output: 1993
Explanation: The maximum population is 1, and 1993 is the earliest year with this population.

Example 2:

Input: logs = [[1950,1961],[1960,1971],[1970,1981]]
Output: 1960
Explanation: 
The maximum population is 2, and it had happened in years 1960 and 1970.
The earlier year between them is 1960.

 

Constraints:

    1 <= logs.length <= 100
    1950 <= birthi < deathi <= 2050


*/