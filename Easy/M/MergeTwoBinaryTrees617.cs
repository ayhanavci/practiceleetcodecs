﻿namespace PracticeLeetCodeCS.Easy;

internal class MergeTwoBinaryTrees617
{
    //Definition for a binary tree node.
    public class TreeNode
    {
        public int val;
        public TreeNode left;
        public TreeNode right;
        public TreeNode(int val = 0, TreeNode left = null, TreeNode right = null)
        {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }

    public TreeNode MergeTrees(TreeNode root1, TreeNode root2)
    {
        if (root1 == null && root2 == null) return null;
        
        TreeNode newNode = new TreeNode();

        newNode.val = (root1 == null ? 0 : root1.val) + (root2 == null ? 0 : root2.val);    
        newNode.left = MergeTrees((root1 == null ? null : root1.left), (root2 == null ? null : root2.left));
        newNode.right = MergeTrees((root1 == null ? null : root1.right), (root2 == null ? null : root2.right));

        return newNode;
    }
    public void Test()
    {

    }
}

/*
 617. Merge Two Binary Trees
Easy
7.7K
269
Companies

You are given two binary trees root1 and root2.

Imagine that when you put one of them to cover the other, some nodes of the two trees are overlapped while the others are not. You need to merge the two trees into a new binary tree. The merge rule is that if two nodes overlap, then sum node values up as the new value of the merged node. Otherwise, the NOT null node will be used as the node of the new tree.

Return the merged tree.

Note: The merging process must start from the root nodes of both trees.

 

Example 1:

Input: root1 = [1,3,2,5], root2 = [2,1,3,null,4,null,7]
Output: [3,4,5,5,4,null,7]

Example 2:

Input: root1 = [1], root2 = [1,2]
Output: [2,2]

 

Constraints:

    The number of nodes in both trees is in the range [0, 2000].
    -104 <= Node.val <= 104


 */