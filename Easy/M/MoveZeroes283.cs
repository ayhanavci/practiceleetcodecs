﻿namespace PracticeLeetCodeCS.Easy;

internal class MoveZeroes283
{
    public void MoveZeroes(int[] nums)
    {
        Queue<int> zeroPositions = new Queue<int>();
        for (int i = 0 ; i < nums.Length; i++)
        {
            if (nums[i] == 0)
            {
                zeroPositions.Enqueue(i);
                continue;
            }                            
            if (zeroPositions.Count > 0)
            {
                nums[zeroPositions.Dequeue()] = nums[i];
                nums[i] = 0;
                zeroPositions.Enqueue(i);
            }
        }
    }
    public void Test()
    {
        int[] nums = { 0, 1, 0, 3, 12 };
        MoveZeroes(nums);
    }
}

/*
 283. Move Zeroes
Easy

Given an integer array nums, move all 0"s to the end of it while maintaining the relative order of the non-zero elements.

Note that you must do this in-place without making a copy of the array.

 

Example 1:

Input: nums = [0,1,0,3,12]
Output: [1,3,12,0,0]

Example 2:

Input: nums = [0]
Output: [0]

 

Constraints:

    1 <= nums.length <= 104
    -231 <= nums[i] <= 231 - 1

 
Follow up: Could you minimize the total number of operations done?
 */