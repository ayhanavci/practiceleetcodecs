﻿using System.Text;

namespace PracticeLeetCodeCS.Easy;

internal class MostCommonWord819
{
    public string MostCommonWord(string paragraph, string[] banned)
    {
        paragraph = paragraph.Replace(",", " ");
        //string[] words = paragraph.Split(" ");
        string[] words = System.Text.RegularExpressions.Regex.Split(paragraph, @" +");
        Dictionary<string, int> wordCount = new Dictionary<string, int>();
        string highest = string.Empty;
        int count = 0;
        
        for (int i = 0; i < words.Length; i++)
        {
            StringBuilder word = new StringBuilder();
            for (int j = 0; j < words[i].Length; j++)                            
                if (Char.IsLetter(words[i][j])) word.Append(Char.ToLower(words[i][j]));
           
            bool fBanned = false;
            for (int j = 0; j < banned.Length; ++j)
            {
                if (banned[j].Equals(word.ToString()))
                {
                    fBanned = true;
                    break;
                }
            }
            if (!fBanned)
            {
                if (highest == string.Empty)
                {
                    highest = word.ToString();
                    count = 1;
                }
                wordCount[word.ToString()] = wordCount.ContainsKey(word.ToString()) ? wordCount[word.ToString()] + 1 : 1;

                if (wordCount[word.ToString()] > count)
                {
                    highest = word.ToString();
                    count = wordCount[word.ToString()];
                }
                    
            }
            

        }
        return highest;
    }
    public void Test()
    {
        //string paragraph = "Bob hit a ball, the hit BALL flew far after it was hit.";
        //string[] banned = { "hit" };

        //string paragraph = "a.";
        //string[] banned = { };

        string paragraph = "a, a, a, a, b,b,b,c, c";
        string[] banned = { "a" };

        var retVal = MostCommonWord(paragraph, banned);
    }
}

/*
 819. Most Common Word
Easy
1.4K
2.8K
Companies

Given a string paragraph and a string array of the banned words banned, return the most frequent word that is not banned. It is guaranteed there is at least one word that is not banned, and that the answer is unique.

The words in paragraph are case-insensitive and the answer should be returned in lowercase.

 

Example 1:

Input: paragraph = "Bob hit a ball, the hit BALL flew far after it was hit.", banned = ["hit"]
Output: "ball"
Explanation: 
"hit" occurs 3 times, but it is a banned word.
"ball" occurs twice (and no other word does), so it is the most frequent non-banned word in the paragraph. 
Note that words in the paragraph are not case sensitive,
that punctuation is ignored (even if adjacent to words, such as "ball,"), 
and that "hit" isn"t the answer even though it occurs more because it is banned.

Example 2:

Input: paragraph = "a.", banned = []
Output: "a"

 

Constraints:

    1 <= paragraph.length <= 1000
    paragraph consists of English letters, space " ", or one of the symbols: "!?",;.".
    0 <= banned.length <= 100
    1 <= banned[i].length <= 10
    banned[i] consists of only lowercase English letters.


 */