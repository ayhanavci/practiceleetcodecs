﻿namespace PracticeLeetCodeCS.Easy;

internal class MonotonicArray896
{
    public bool IsMonotonic(int[] nums)
    {        
        int isIncreasing = -1;//-1 unknown, 0 false, 1 true

        for (int i = 1; i < nums.Length; i++)
        {
            if (nums[i] > nums[i-1])
            {
                if (isIncreasing == 0)
                    return false;
                isIncreasing = 1;
            }
            else if (nums[i] < nums[i-1])
            {
                if (isIncreasing == 1)
                    return false;
                isIncreasing = 0;
            }
        }

        return true;
    }
    public void Test()
    {
        int[] nums = { 1, 2, 2, 3 };
        //var retval = IsMonotonic(nums);

        int[] nums2 = { 6, 5, 4, 4 };
        //var retval2 = IsMonotonic(nums2);

        int[] nums3 = { 1, 3, 2 };
        var retval3 = IsMonotonic(nums3);
    }
}

/*
 896. Monotonic Array
Easy
1.9K
60
Companies

An array is monotonic if it is either monotone increasing or monotone decreasing.

An array nums is monotone increasing if for all i <= j, nums[i] <= nums[j]. An array nums is monotone decreasing if for all i <= j, nums[i] >= nums[j].

Given an integer array nums, return true if the given array is monotonic, or false otherwise.

 

Example 1:

Input: nums = [1,2,2,3]
Output: true

Example 2:

Input: nums = [6,5,4,4]
Output: true

Example 3:

Input: nums = [1,3,2]
Output: false

 

Constraints:

    1 <= nums.length <= 105
    -105 <= nums[i] <= 105


 */