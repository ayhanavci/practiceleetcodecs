﻿namespace PracticeLeetCodeCS.Easy;

internal class MinimumDepthofBinaryTree111
{
    //Definition for a binary tree node.
    public class TreeNode
    {
        public int val;
        public TreeNode left;
        public TreeNode right;
        public TreeNode(int val = 0, TreeNode left = null, TreeNode right = null)
        {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }
    public int MinDepth(TreeNode root) 
    {
        if (root == null)
            return 0;

        if (root.left == null && root.right == null)
            return 1;

        int left = 100001;
        int right = 100001;

        if (root.left != null)
            left = MinDepth(root.left);

        if (root.right != null)
            right = MinDepth(root.right);

        return 1 + (Math.Min(left, right));
    }
    public void Test()
    {
        TreeNode right_right_right_right = new TreeNode(6);
        TreeNode right_right_right = new TreeNode(5, null, right_right_right_right);
        TreeNode right_right = new TreeNode(4, null, right_right_right);
        TreeNode right = new TreeNode(3, null, right_right);
        TreeNode root = new TreeNode(2, null, right);

        int depth = MinDepth(root);
    }
}
/*
 111. Minimum Depth of Binary Tree
Easy

Given a binary tree, find its minimum depth.

The minimum depth is the number of nodes along the shortest path from the root node down to the nearest leaf node.

Note: A leaf is a node with no children.

 

Example 1:

Input: root = [3,9,20,null,null,15,7]
Output: 2

Example 2:

Input: root = [2,null,3,null,4,null,5,null,6]
Output: 5

 

Constraints:

    The number of nodes in the tree is in the range [0, 105].
    -1000 <= Node.val <= 1000


 
 */