namespace PracticeLeetCodeCS.Easy;

internal class MostFrequentEvenElement2404
{
    public int MostFrequentEven(int[] nums) 
    {
        Dictionary<int, int> frequencies = new Dictionary<int, int>();        
        for (int i = 0; i < nums.Length; ++i)
        {
            if (nums[i] % 2 == 0)
            {
                if (frequencies.ContainsKey(nums[i])) frequencies[nums[i]]++;
                else frequencies.Add(nums[i], 1);
            }
        }
        
        int minEven = -1;
        int frequency = 0;

        foreach (var pair in frequencies)
        {
            if (pair.Value > frequency)
            {
                minEven = pair.Key;
                frequency = pair.Value;
            }
            else if (pair.Value == frequency && pair.Key < minEven)
            {
                minEven = pair.Key;
            }
        }

        return minEven;
    }
    public void Test()
    {

    }
}
/*
2404. Most Frequent Even Element
Easy
689
25
Companies

Given an integer array nums, return the most frequent even element.

If there is a tie, return the smallest one. If there is no such element, return -1.

 

Example 1:

Input: nums = [0,1,2,2,4,4,1]
Output: 2
Explanation:
The even elements are 0, 2, and 4. Of these, 2 and 4 appear the most.
We return the smallest one, which is 2.

Example 2:

Input: nums = [4,4,4,9,2,4]
Output: 4
Explanation: 4 is the even element appears the most.

Example 3:

Input: nums = [29,47,21,41,13,37,25,7]
Output: -1
Explanation: There is no even element.

 

Constraints:

    1 <= nums.length <= 2000
    0 <= nums[i] <= 105


*/