﻿namespace PracticeLeetCodeCS.Easy;


//TODO: Soru acayip. anlamadim
internal class MaximumRepeatingSubstring1668
{
    public int MaxRepeating(string sequence, string word)
    {
        int count = 0;

        string search = word;
        while (sequence.Contains(search))
        {
            count++;
            search += word;
        }

        return count;
    }
    public void Test()
    {
        /*var retVal1 = MaxRepeating("ababc", "ab");
        var retVal2 = MaxRepeating("ababc", "ba");
        var retVal3 = MaxRepeating("ababc", "ac");
        var retVal4 = MaxRepeating("a", "a");*/
        var retVal5 = MaxRepeating("aaabaaaabaaabaaaabaaaabaaaabaaaaba", "aaaba");
        //var retVal5 = MaxRepeating("aaaba aaaba aaba aaaba aaaba aaaba aaaba", "aaaba");
    }
}
/*
 1668. Maximum Repeating Substring
Easy
538
187
Companies

For a string sequence, a string word is k-repeating if word concatenated k times is a substring of sequence. The word's maximum k-repeating value is the highest value k where word is k-repeating in sequence. If word is not a substring of sequence, word's maximum k-repeating value is 0.

Given strings sequence and word, return the maximum k-repeating value of word in sequence.

 

Example 1:

Input: sequence = "ababc", word = "ab"
Output: 2
Explanation: "abab" is a substring in "ababc".

Example 2:

Input: sequence = "ababc", word = "ba"
Output: 1
Explanation: "ba" is a substring in "ababc". "baba" is not a substring in "ababc".

Example 3:

Input: sequence = "ababc", word = "ac"
Output: 0
Explanation: "ac" is not a substring in "ababc". 

 

Constraints:

    1 <= sequence.length <= 100
    1 <= word.length <= 100
    sequence and word contains only lowercase English letters.


 */