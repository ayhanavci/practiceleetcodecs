﻿namespace PracticeLeetCodeCS.Easy;
internal class MaximumNumberofBallsinaBox1742
{
    public int CountBalls(int lowLimit, int highLimit)
    {
        Dictionary<int, int> boxes = new Dictionary<int, int>();        
        int maxBoxBalls = 1;

        for (int i = lowLimit; i <= highLimit; i++)
        {
            int boxNumber = DigitSum(i);
            int count;
            if (boxes.TryGetValue(boxNumber, out count))
            {
                count = ++boxes[boxNumber];
                if (count > maxBoxBalls)
                    maxBoxBalls = count;
            }                
            else
            {
                boxes.Add(boxNumber, 1);
            }                         
        }

        return maxBoxBalls;
    }
    private int DigitSum(int number)
    {
        int sum = 0;
        while (number > 0)
        {
            sum += number % 10;
            number /= 10;
        }
        return sum;
    }
    public void Test()
    {
        var retVal1 = CountBalls(8, 16);
        var retVal2 = CountBalls(5, 15);
        var retVal3 = CountBalls(19, 28);
    }
}

/*
 1742. Maximum Number of Balls in a Box
Easy
510
119
Companies

You are working in a ball factory where you have n balls numbered from lowLimit up to highLimit inclusive (i.e., n == highLimit - lowLimit + 1), and an infinite number of boxes numbered from 1 to infinity.

Your job at this factory is to put each ball in the box with a number equal to the sum of digits of the ball's number. For example, the ball number 321 will be put in the box number 3 + 2 + 1 = 6 and the ball number 10 will be put in the box number 1 + 0 = 1.

Given two integers lowLimit and highLimit, return the number of balls in the box with the most balls.

 

Example 1:

Input: lowLimit = 1, highLimit = 10
Output: 2
Explanation:
Box Number:  1 2 3 4 5 6 7 8 9 10 11 ...
Ball Count:  2 1 1 1 1 1 1 1 1 0  0  ...
Box 1 has the most number of balls with 2 balls.

Example 2:

Input: lowLimit = 5, highLimit = 15
Output: 2
Explanation:
Box Number:  1 2 3 4 5 6 7 8 9 10 11 ...
Ball Count:  1 1 1 1 2 2 1 1 1 0  0  ...
Boxes 5 and 6 have the most number of balls with 2 balls in each.

Example 3:

Input: lowLimit = 19, highLimit = 28
Output: 2
Explanation:
Box Number:  1 2 3 4 5 6 7 8 9 10 11 12 ...
Ball Count:  0 1 1 1 1 1 1 1 1 2  0  0  ...
Box 10 has the most number of balls with 2 balls.

 

Constraints:

    1 <= lowLimit <= highLimit <= 105


 */