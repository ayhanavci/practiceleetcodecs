﻿namespace PracticeLeetCodeCS.Easy;

internal class MinimumAbsoluteDifference1200
{
    public IList<IList<int>> MinimumAbsDifference(int[] arr)
    {
        IList<IList<int>> retVal = new List<IList<int>>();
        int minDiff = int.MaxValue;

        Array.Sort(arr);
        for (int i = 0; i < arr.Length - 1; i++)
        {
            int diff = Math.Abs(arr[i] - arr[i+1]);
            if (diff < minDiff)
            {
                retVal.Clear();
                List<int> vals = new List<int>();
                vals.Add(arr[i]);
                vals.Add(arr[i+1]);
                retVal.Add(vals);
                minDiff = diff;
            }
            else if (diff == minDiff)
            {
                List<int> vals = new List<int>();
                vals.Add(arr[i]);
                vals.Add(arr[i+1]);
                retVal.Add(vals);
            }
        }
        return retVal;
    }
    public void Test()
    {
        int[] arr1 = { 4, 2, 1, 3 };
        var retVal1 = MinimumAbsDifference(arr1);
        int[] arr2 = { 1, 3, 6, 10, 15 };
        var retVal2 = MinimumAbsDifference(arr2);
        int[] arr3 = { 3, 8, -10, 23, 19, -4, -14, 27 };
        var retVal3 = MinimumAbsDifference(arr3);
    }
}
/*for (int j = i + 1; j < arr.Length; ++j)
           {
               int diff = Math.Abs(arr[i] - arr[j]);
               if (diff < minDiff)
               {
                   retVal.Clear();
                   List<int> vals = new List<int>();
                   vals.Add(arr[i]);
                   vals.Add(arr[j]);
                   retVal.Add(vals);
                   minDiff = diff;
               }
               else if (diff == minDiff)
               {
                   List<int> vals = new List<int>();
                   vals.Add(arr[i]);
                   vals.Add(arr[j]);
                   retVal.Add(vals);
               }                                
           }*/
/*
 1200. Minimum Absolute Difference
Easy
1.9K
62
Companies

Given an array of distinct integers arr, find all pairs of elements with the minimum absolute difference of any two elements.

Return a list of pairs in ascending order(with respect to pairs), each pair [a, b] follows

    a, b are from arr
    a < b
    b - a equals to the minimum absolute difference of any two elements in arr

 

Example 1:

Input: arr = [4,2,1,3]
Output: [[1,2],[2,3],[3,4]]
Explanation: The minimum absolute difference is 1. List all pairs with difference equal to 1 in ascending order.

Example 2:

Input: arr = [1,3,6,10,15]
Output: [[1,3]]

Example 3:

Input: arr = [3,8,-10,23,19,-4,-14,27]
Output: [[-14,-10],[19,23],[23,27]]

 

Constraints:

    2 <= arr.length <= 105
    -106 <= arr[i] <= 106


 */