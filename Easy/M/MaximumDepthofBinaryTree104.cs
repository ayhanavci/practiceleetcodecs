﻿namespace PracticeLeetCodeCS.Easy;

internal class MaximumDepthofBinaryTree104
{
    //Definition for a binary tree node.
    public class TreeNode
    {
        public int val;
        public TreeNode left;
        public TreeNode right;
        public TreeNode(int val = 0, TreeNode left = null, TreeNode right = null)
        {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }
    public int MaxDepth2(TreeNode root)
    {
        List<TreeNode?> nextDepth = new List<TreeNode>();
        List<TreeNode?> children = new List<TreeNode>();

        nextDepth.Add(root);               
        bool moreDepth = false;
        int depth = 0;
        do
        {
            moreDepth = false;
            for (int i = 0; i < nextDepth.Count; ++i)
            {
                if (nextDepth[i] == null)
                {
                    children.Add(null);
                    children.Add(null);
                }
                else
                {
                    moreDepth = true;
                    children.Add(nextDepth[i].left);
                    children.Add(nextDepth[i].right);
                }
            }
            nextDepth.Clear();
            nextDepth.AddRange(children);
            children.Clear();
            if (moreDepth) depth++;
            
        } while (moreDepth);       

        return depth;
    }
    public int MaxDepth(TreeNode root)
    {     
        if (root == null)
            return 0;
        
        int leftDepth = MaxDepth(root.left);
        int rightDepth = MaxDepth(root.right);
        
        return Math.Max(leftDepth + 1, rightDepth + 1);
    }
    public void Test()
    {
        TreeNode leftR = new TreeNode(15);
        TreeNode rightR = new TreeNode(7);

        TreeNode left = new TreeNode(9);
        TreeNode right = new TreeNode(20, leftR, rightR);

        TreeNode root = new TreeNode(3, left, right);

        int depth = MaxDepth(root);
    }
}

/*
 104. Maximum Depth of Binary Tree
Easy

Given the root of a binary tree, return its maximum depth.

A binary tree"s maximum depth is the number of nodes along the longest path from the root node down to the farthest leaf node.

 

Example 1:

Input: root = [3,9,20,null,null,15,7]
Output: 3

Example 2:

Input: root = [1,null,2]
Output: 2

 

Constraints:

    The number of nodes in the tree is in the range [0, 104].
    -100 <= Node.val <= 100


 */