﻿using System.Text;

namespace PracticeLeetCodeCS.Easy;

internal class Maximum69Number1323
{
    public int Maximum69Number(int num)
    {
        StringBuilder number = new StringBuilder(num.ToString());

        for (int i = 0; i < number.Length; ++i)
        {
            if (number[i] == '6')
            {
                number.Replace('6', '9', i, 1);
                break;
            }
                
        }
        return Convert.ToInt32(number.ToString());
    }
    public void Test()
    {
        var retVal1 = Maximum69Number(9669);
        var retVal2 = Maximum69Number(9996);
        var retVal3 = Maximum69Number(9999);
    }
}

/*
 1323. Maximum 69 Number
Easy
2.3K
189
Companies

You are given a positive integer num consisting only of digits 6 and 9.

Return the maximum number you can get by changing at most one digit (6 becomes 9, and 9 becomes 6).

 

Example 1:

Input: num = 9669
Output: 9969
Explanation: 
Changing the first digit results in 6669.
Changing the second digit results in 9969.
Changing the third digit results in 9699.
Changing the fourth digit results in 9666.
The maximum number is 9969.

Example 2:

Input: num = 9996
Output: 9999
Explanation: Changing the last digit 6 to 9 results in the maximum number.

Example 3:

Input: num = 9999
Output: 9999
Explanation: It is better not to apply any change.

 

Constraints:

    1 <= num <= 104
    num consists of only 6 and 9 digits.


 */