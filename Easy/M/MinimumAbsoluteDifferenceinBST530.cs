﻿namespace PracticeLeetCodeCS.Easy;

internal class MinimumAbsoluteDifferenceinBST530
{
    //Definition for a binary tree node.
    public class TreeNode
    {
        public int val;
        public TreeNode left;
        public TreeNode right;
        public TreeNode(int val = 0, TreeNode left = null, TreeNode right = null)
        {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }
    
    int min = int.MaxValue;
    int? prev = null;
    public int GetMinimumDifference2(TreeNode root)
    {
        if (root == null) return int.MaxValue;
        GetMinimumDifference(root.left);

        if (prev == null)
            min = Math.Min(min, prev.Value);
        prev = root.val;

        GetMinimumDifference(root.right);

        return min;
    }
    public int DifferenceWithChildren(TreeNode root, int checkVal)
    {        
        if (root == null) return 100001;
        int diff = Math.Abs(root.val - checkVal);               
        return Math.Min(diff, 
            Math.Min(DifferenceWithChildren(root.left, checkVal), DifferenceWithChildren(root.right, checkVal)));
    }

    public int GetMinimumDifference(TreeNode root)
    {
        if (root == null) return 100001;
        int diff = Math.Min(DifferenceWithChildren(root.left, root.val), DifferenceWithChildren(root.right, root.val));
        return Math.Min(diff, 
            Math.Min(GetMinimumDifference(root.left), GetMinimumDifference(root.right)));
    }

    public void Test()
    {
        TreeNode right_right = new TreeNode(41);
        TreeNode right_left = new TreeNode(12);

        TreeNode left = new TreeNode(20);
        TreeNode right = new TreeNode(48, right_left, right_right);

        TreeNode root = new TreeNode(1, left, right);

        int retVal = GetMinimumDifference(root);
    }
}

/*
 530. Minimum Absolute Difference in BST
Easy

Given the root of a Binary Search Tree (BST), return the minimum absolute difference between the values of any two different nodes in the tree.

 

Example 1:

Input: root = [4,2,6,1,3]
Output: 1

Example 2:

Input: root = [1,0,48,null,null,12,49]
Output: 1

 

Constraints:

    The number of nodes in the tree is in the range [2, 104].
    0 <= Node.val <= 105

 

Note: This question is the same as 783: https://leetcode.com/problems/minimum-distance-between-bst-nodes/

 */