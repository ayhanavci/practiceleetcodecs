﻿using PracticeLeetCodeCS.Medium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticeLeetCodeCS.Easy;

public class MergeTwoSortedLists21
{
    //Definition for singly-linked list.
    public class ListNode
    {
        public int val;
        public ListNode? next;
        public ListNode(int val = 0, ListNode? next = null)
        {
            this.val = val;
            this.next = next;
            
        }
    }
    private ListNode? MergeTwoLists(ListNode? list1, ListNode? list2)
    {
        if (list1 == null && list2 == null)
            return null;
        
        ListNode? headNode = new ListNode();
        ListNode? currentNode = headNode;
        while (true)
        {                                                            
            if (list1 == null && list2 != null)
            {
                currentNode.val = list2.val;               
                list2 = list2.next;
            }
            else if (list2 == null && list1 != null)
            {
                currentNode.val = list1.val;             
                list1 = list1.next;
            }
            else if (list1 != null && list2 != null)
            {
                if (list1.val <= list2.val)
                {
                    currentNode.val = list1.val;                   
                    list1 = list1.next;
                }
                else
                {
                    currentNode.val = list2.val;                   
                    list2 = list2.next;
                }
            }
            if (list1 == null && list2 == null)
            {
                currentNode.next = null;
                return headNode;
            }
            currentNode.next = new ListNode();
            currentNode = currentNode.next;

        }        
    }

    public void Test()
    {
        ListNode list12 = new ListNode(4);
        ListNode list11 = new ListNode(2, list12);
        ListNode list1 = new ListNode(1, list11);


        ListNode list22 = new ListNode(4);
        ListNode list21 = new ListNode(3, list22);
        ListNode list2 = new ListNode(1, list21);

        ListNode? listResult = MergeTwoLists(list1, list2);
    }
}


/*
 21. Merge Two Sorted Lists
Easy

You are given the heads of two sorted linked lists list1 and list2.

Merge the two lists in a one sorted list. The list should be made by splicing together the nodes of the first two lists.

Return the head of the merged linked list.

 

Example 1:

Input: list1 = [1,2,4], list2 = [1,3,4]
Output: [1,1,2,3,4,4]

Example 2:

Input: list1 = [], list2 = []
Output: []

Example 3:

Input: list1 = [], list2 = [0]
Output: [0]

 

Constraints:

    The number of nodes in both lists is in the range [0, 50].
    -100 <= Node.val <= 100
    Both list1 and list2 are sorted in non-decreasing order.


 
 */