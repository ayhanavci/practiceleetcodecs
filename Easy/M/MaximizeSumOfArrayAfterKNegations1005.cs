﻿namespace PracticeLeetCodeCS.Easy;

internal class MaximizeSumOfArrayAfterKNegations1005
{
    public int LargestSumAfterKNegations(int[] nums, int k)
    {
        Array.Sort(nums);        
        for (int i = 0; i < nums.Length; i++)
        {
            if (nums[i] < 0)
            {
                nums[i] *= -1;
                if (--k == 0) break; 
            }
            else break;                 
        }
        Array.Sort(nums);
        while (k > 0)
        {
            nums[0] *= -1;
            k--;
        }

        int sum = 0;
        for (int i = 0; i < nums.Length; i++)        
            sum += nums[i];
        return sum;
    }
    public void Test()
    {
        int[] nums1 = { 4, 2, 3 };
        var retVal1 = LargestSumAfterKNegations(nums1, 1);

        int[] nums2 = { 3, -1, 0, 2 };
        var retVal2 = LargestSumAfterKNegations(nums2, 3);

        int[] nums3 = { 2, -3, -1, 5, -4 };
        var retVal3 = LargestSumAfterKNegations(nums3, 2);

        int[] nums4 = { -8, 3, -5, -3, -5, -2 };
        var retval4 = LargestSumAfterKNegations(nums4, 6);
    }
}

/*
 1005. Maximize Sum Of Array After K Negations
Easy
1.2K
94
Companies

Given an integer array nums and an integer k, modify the array in the following way:

    choose an index i and replace nums[i] with -nums[i].

You should apply this process exactly k times. You may choose the same index i multiple times.

Return the largest possible sum of the array after modifying it in this way.

 

Example 1:

Input: nums = [4,2,3], k = 1
Output: 5
Explanation: Choose index 1 and nums becomes [4,-2,3].

Example 2:

Input: nums = [3,-1,0,2], k = 3
Output: 6
Explanation: Choose indices (1, 2, 2) and nums becomes [3,1,0,2].

Example 3:

Input: nums = [2,-3,-1,5,-4], k = 2
Output: 13
Explanation: Choose indices (1, 4) and nums becomes [2,3,-1,5,4].

 

Constraints:

    1 <= nums.length <= 104
    -100 <= nums[i] <= 100
    1 <= k <= 104


 */