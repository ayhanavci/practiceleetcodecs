﻿namespace PracticeLeetCodeCS.Easy;

internal class MaxConsecutiveOnes485
{
    public int FindMaxConsecutiveOnes(int[] nums)
    {
        int maxConsecutive = 0;
        int currentConsecutive = 0;
        
        for (int i = 0; i < nums.Length; i++)
        {
            if (nums[i] == 1)
            {
                if (++currentConsecutive > maxConsecutive)
                    maxConsecutive = currentConsecutive;
            }
            else            
                currentConsecutive = 0;
            
        }
        return maxConsecutive;
    }
    public void Test()
    {
        int[] nums = { 1, 1, 0, 1, 1, 1 };
        int retVal = FindMaxConsecutiveOnes(nums);
    }
}

/*
 485. Max Consecutive Ones
Easy

Given a binary array nums, return the maximum number of consecutive 1"s in the array.

 

Example 1:

Input: nums = [1,1,0,1,1,1]
Output: 3
Explanation: The first two digits or the last three digits are consecutive 1s. The maximum number of consecutive 1s is 3.

Example 2:

Input: nums = [1,0,1,1,0,1]
Output: 2

 

Constraints:

    1 <= nums.length <= 105
    nums[i] is either 0 or 1.


 */