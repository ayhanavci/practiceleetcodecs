﻿namespace PracticeLeetCodeCS.Easy;

internal class MaximumAverageSubarrayI643
{
    public double FindMaxAverageTimeout(int[] nums, int k)
    {
        double maxAverage = -10001;
        
        for (int i = 0; i <= nums.Length - k; i++) 
        {          
            double sum = 0;
            for (int j = i; j < i + k; ++j)
                sum += nums[j];
            maxAverage = Math.Max(maxAverage, Math.Round(sum / k, 5));
        }

        return maxAverage;
    }
    public double FindMaxAverage(int[] nums, int k)
    {        
        double currentSum = 0;
        for (int i = 0; i < k; ++i)        
            currentSum += nums[i];        

        double maxAverage = Math.Round(currentSum / k, 5);

        for (int i = 1; i <= nums.Length - k; i++)
        {
            currentSum -= nums[i-1];
            currentSum += nums[i+k-1];
            maxAverage = Math.Max(maxAverage, Math.Round(currentSum / k, 5));
        }

        return maxAverage;
    }
    public void Test()
    {
        int[] nums = { 1, 12, -5, -6, 50, 3};
        //int[] nums = { 5 };

        var retVal = FindMaxAverage(nums, 4);
    }
}

/*
 643. Maximum Average Subarray I
Easy
2.1K
173
Companies

You are given an integer array nums consisting of n elements, and an integer k.

Find a contiguous subarray whose length is equal to k that has the maximum average value and return this value. Any answer with a calculation error less than 10-5 will be accepted.

 

Example 1:

Input: nums = [1,12,-5,-6,50,3], k = 4
Output: 12.75000
Explanation: Maximum average is (12 - 5 - 6 + 50) / 4 = 51 / 4 = 12.75

Example 2:

Input: nums = [5], k = 1
Output: 5.00000

 

Constraints:

    n == nums.length
    1 <= k <= n <= 105
    -104 <= nums[i] <= 104


 */