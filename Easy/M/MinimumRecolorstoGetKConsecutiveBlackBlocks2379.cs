namespace PracticeLeetCodeCS.Easy;

internal class MinimumRecolorstoGetKConsecutiveBlackBlocks2379
{
    public int MinimumRecolorsBrute(string blocks, int k) 
    {
        int minRecolor = blocks.Length;
        for (int i = 0; i < blocks.Length - k; ++i)
        {
            int currentRecolor = 0;
            for (int j = i; j < k + i; ++j)            
                if (blocks[j] == 'W') currentRecolor++;
            minRecolor = Math.Min(currentRecolor, minRecolor);
        }
        return minRecolor;
    }

    //https://www.geeksforgeeks.org/window-sliding-technique/
    public int MinimumRecolors(string blocks, int k) //sliding windows
    {
        int minRecolor = int.MaxValue;

        int windowRecolor = 0;
        for (int i = 0; i < k; ++i)        
            if (blocks[i] == 'W') windowRecolor++;

        minRecolor = windowRecolor;  
        for (int i = k; i < blocks.Length; ++i)
        {            
            if (blocks[i-k] == 'W' && blocks[i] == 'B')  windowRecolor--;
            else if (blocks[i-k] == 'B' && blocks[i] == 'W') windowRecolor++;
            minRecolor = Math.Min(minRecolor, windowRecolor);
        }

        return minRecolor;
    }
    public void Test()
    {

    }
}
/*
2379. Minimum Recolors to Get K Consecutive Black Blocks
Easy
549
15
Companies

You are given a 0-indexed string blocks of length n, where blocks[i] is either 'W' or 'B', representing the color of the ith block. The characters 'W' and 'B' denote the colors white and black, respectively.

You are also given an integer k, which is the desired number of consecutive black blocks.

In one operation, you can recolor a white block such that it becomes a black block.

Return the minimum number of operations needed such that there is at least one occurrence of k consecutive black blocks.

 

Example 1:

Input: blocks = "WBBWWBBWBW", k = 7
Output: 3
Explanation:
One way to achieve 7 consecutive black blocks is to recolor the 0th, 3rd, and 4th blocks
so that blocks = "BBBBBBBWBW". 
It can be shown that there is no way to achieve 7 consecutive black blocks in less than 3 operations.
Therefore, we return 3.

Example 2:

Input: blocks = "WBWBBBW", k = 2
Output: 0
Explanation:
No changes need to be made, since 2 consecutive black blocks already exist.
Therefore, we return 0.

 

Constraints:

    n == blocks.length
    1 <= n <= 100
    blocks[i] is either 'W' or 'B'.
    1 <= k <= n


*/