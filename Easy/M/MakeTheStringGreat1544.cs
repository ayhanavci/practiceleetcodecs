﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticeLeetCodeCS.Easy;

internal class MakeTheStringGreat1544
{
    public string MakeGood(string s)
    {
        StringBuilder processedString = new StringBuilder();
        string iteration = s;
        bool fFound = false;
        do
        {
            fFound = false;
            for (int i = 0; i < iteration.Length; ++i)
            {
                if (i == iteration.Length - 1)
                {
                    processedString.Append(iteration[i]);
                    break;
                }

                if (char.ToUpper(iteration[i]) == char.ToUpper(iteration[i + 1]))
                {
                    if (iteration[i] != iteration[i + 1])
                    {
                        i += 1;
                        fFound = true;
                        continue;
                    }

                }
                processedString.Append(iteration[i]);
            }

            iteration = processedString.ToString();
            processedString.Clear();
        } while (fFound);

        return iteration;
    }
    public string MakeGood2(string s)
    {
        StringBuilder processedString = new StringBuilder(s);        
        for (int i = 0; i < processedString.Length - 1;)
        {            
            if (char.ToUpper(processedString[i]) == char.ToUpper(processedString[i + 1]))
            {
                if (processedString[i] != processedString[i + 1])
                {
                    processedString.Remove(i, 2);
                    i = 0;
                    continue;
                }                                  
                    
            }
            i++;            
        }
        return processedString.ToString();
    }
    public void Test()
    {
        Console.WriteLine($"leEeetcode: {MakeGood("leEeetcode")} abBAcC:{MakeGood("abBAcC")}");
    }
}

/*
 1544. Make The String Great
Easy

Given a string s of lower and upper case English letters.

A good string is a string which doesn"t have two adjacent characters s[i] and s[i + 1] where:

    0 <= i <= s.length - 2
    s[i] is a lower-case letter and s[i + 1] is the same letter but in upper-case or vice-versa.

To make the string good, you can choose two adjacent characters that make the string bad and remove them. You can keep doing this until the string becomes good.

Return the string after making it good. The answer is guaranteed to be unique under the given constraints.

Notice that an empty string is also good.

 

Example 1:

Input: s = "leEeetcode"
Output: "leetcode"
Explanation: In the first step, either you choose i = 1 or i = 2, both will result "leEeetcode" to be reduced to "leetcode".

Example 2:

Input: s = "abBAcC"
Output: ""
Explanation: We have many possible scenarios, and all lead to the same answer. For example:
"abBAcC" --> "aAcC" --> "cC" --> ""
"abBAcC" --> "abBA" --> "aA" --> ""

Example 3:

Input: s = "s"
Output: "s"

 

Constraints:

    1 <= s.length <= 100
    s contains only lower and upper case English letters.


 */