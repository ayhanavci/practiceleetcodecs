namespace PracticeLeetCodeCS.Easy;

internal class MinimumDifferenceBetweenHighestandLowestofKScores1984
{
    //TODO: Çözümü aldım. Buna tekrar bak.
    
    public int MinimumDifference(int[] nums, int k) 
    {
        List<int> sortedNums = new List<int>(nums);
        sortedNums.Sort();

        int minimum = int.MaxValue;

        for (int i = k - 1; i < sortedNums.Count; ++i)                     
            minimum = Math.Min(minimum, sortedNums[i] - sortedNums[i-k+1]);  
        
        return minimum;
    }

    public int MinimumDifference2(int[] nums, int k) 
    {
        List<int> sortedNums = new List<int>(nums);
        sortedNums.Sort();

        int minimum = sortedNums[k-1] - sortedNums[0];

        for (int i = k; i < sortedNums.Count; ++i)        
            minimum = Math.Min(minimum, nums[i] - nums[i-k+1]);
        
        return minimum;
    }
    
    public void Test()
    {
        int[] nums1 = {9,4,1,7};
        var retVal1 = MinimumDifference(nums1, 2);
    }
}
/*
We can sort the array and then compare every element at index i with the element at index i+k-1. Take the minimum difference of all such pairs.

Time Complexity - O(NlogN)
Space Complexity - O(1)

class Solution {
    public int minimumDifference(int[] nums, int k) {
        Arrays.sort(nums);
        int res = Integer.MAX_VALUE;
        for(int i=0; i<nums.length; i++){
            if(i+k-1 >= nums.length) break;
            res = Math.min(res, nums[i+k-1] - nums[i]);
        }
        return res;
    }
}
*/
/*
Idea:
Sort the array.
Then, we use a window of size k and calculate the difference between the first and last element in the window.
res will be the minimum of these values.
Time Complexity: O(nlogn)
Space Complexity: O(1)

class Solution {
public:
    int minimumDifference(vector<int>& nums, int k) {
        sort(nums.begin(), nums.end());
        int res = nums[k-1] - nums[0];
        for (int i = k; i < nums.size(); i++) res = min(res, nums[i] - nums[i-k+1]);
        return res;
    }
};
*/
/*
1984. Minimum Difference Between Highest and Lowest of K Scores
Easy
705
126
Companies

You are given a 0-indexed integer array nums, where nums[i] represents the score of the ith student. You are also given an integer k.

Pick the scores of any k students from the array so that the difference between the highest and the lowest of the k scores is minimized.

Return the minimum possible difference.

 

Example 1:

Input: nums = [90], k = 1
Output: 0
Explanation: There is one way to pick score(s) of one student:
- [90]. The difference between the highest and lowest score is 90 - 90 = 0.
The minimum possible difference is 0.

Example 2:

Input: nums = [9,4,1,7], k = 2
Output: 2
Explanation: There are six ways to pick score(s) of two students:
- [9,4,1,7]. The difference between the highest and lowest score is 9 - 4 = 5.
- [9,4,1,7]. The difference between the highest and lowest score is 9 - 1 = 8.
- [9,4,1,7]. The difference between the highest and lowest score is 9 - 7 = 2.
- [9,4,1,7]. The difference between the highest and lowest score is 4 - 1 = 3.
- [9,4,1,7]. The difference between the highest and lowest score is 7 - 4 = 3.
- [9,4,1,7]. The difference between the highest and lowest score is 7 - 1 = 6.
The minimum possible difference is 2.

 

Constraints:

    1 <= k <= nums.length <= 1000
    0 <= nums[i] <= 105


*/