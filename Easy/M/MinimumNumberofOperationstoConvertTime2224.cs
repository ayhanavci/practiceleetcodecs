namespace PracticeLeetCodeCS.Easy;

internal class MinimumNumberofOperationstoConvertTime2224
{
    public int ConvertTime(string current, string correct) 
    {
        int steps = 0;

        int currentHr = ((current[0] - '0') * 10) + (current[1] - '0');
        int correctHr = ((correct[0] - '0') * 10) + (correct[1] - '0');

        int currentMin = ((current[3] - '0') * 10) + (current[4] - '0');
        int correctMin = ((correct[3] - '0') * 10) + (correct[4] - '0');
                
        
        if (currentHr == correctHr)
        {
            if (currentMin == correctMin)
            {
                return 0;
            }
            else if (correctMin > currentMin)
            {
                steps = StepsToReachMinute(currentMin, correctMin);
                return steps;                    
            }
            else //currentmin > correctmin
            {
                steps += 23;
                if (currentHr == 0)
                    currentHr = 23;
                else 
                    currentHr -= 1;
            }
                        
        }

        if (correctHr - currentHr > 1)
        {
            steps += correctHr - currentHr - 1;
            currentHr = correctHr - 1;
        }
        else if (currentHr > correctHr)
        {
            steps += 24 - (currentHr - correctHr) - 1;
        }

        if (correctHr == currentHr + 1)
        {
            if (correctMin == currentMin)
            {
                return steps + 1; //add 1 hour
            }
            else if (correctMin > currentMin)
            {
                if (correctMin == 0) correctMin = 60;
                steps += StepsToReachMinute(currentMin, correctMin);
                return steps + 1; //add 1 hour
            }
            else //currentmin > correctmin
            {
                int minuteToAdd = (60 - currentMin) + correctMin;
                steps += StepsToReachMinute(0, minuteToAdd);
            }
                        
        }
        
        return steps;
    }
    public int StepsToReachMinute(int currentMin, int correctMin)
    {//30+15+5+5+1+1+1+1
        int steps = 0;
        if (correctMin >= currentMin + 30)
        {
            steps += 2;
            currentMin += 30;
        }
        if (correctMin >= currentMin + 15)
        {
            steps += 1;
            currentMin += 15;
        }
        if (correctMin >= currentMin + 10)
        {
            steps += 2;
            currentMin += 10;
        }
        if (correctMin >= currentMin + 5)
        {
            steps += 1;
            currentMin += 5;
        }
        if (correctMin > currentMin)
        {
            steps += correctMin - currentMin;
        }
        return steps;

    }
    public void Test()
    {
        var retVal = ConvertTime("00:00","23:59");
    }
}

/*
2224. Minimum Number of Operations to Convert Time
Easy
374
32
Companies

You are given two strings current and correct representing two 24-hour times.

24-hour times are formatted as "HH:MM", where HH is between 00 and 23, and MM is between 00 and 59. The earliest 24-hour time is 00:00, and the latest is 23:59.

In one operation you can increase the time current by 1, 5, 15, or 60 minutes. You can perform this operation any number of times.

Return the minimum number of operations needed to convert current to correct.

 

Example 1:

Input: current = "02:30", correct = "04:35"
Output: 3
Explanation:
We can convert current to correct in 3 operations as follows:
- Add 60 minutes to current. current becomes "03:30".
- Add 60 minutes to current. current becomes "04:30".
- Add 5 minutes to current. current becomes "04:35".
It can be proven that it is not possible to convert current to correct in fewer than 3 operations.

Example 2:

Input: current = "11:00", correct = "11:01"
Output: 1
Explanation: We only have to add one minute to current, so the minimum number of operations needed is 1.

 

Constraints:

    current and correct are in the format "HH:MM"
    current <= correct


*/