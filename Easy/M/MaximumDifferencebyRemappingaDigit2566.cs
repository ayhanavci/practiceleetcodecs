namespace PracticeLeetCodeCS.Easy;

internal class MaximumDifferencebyRemappingaDigit2566
{
    public int MinMaxDifference(int num) 
    {
        List<int> digits = new List<int>();

        while (num > 0)
        {
            digits.Insert(0, num % 10);
            num /= 10;
        }

        int significantDigit = 9;
        for (int i = 0; i < digits.Count; ++i)
        {
            if (digits[i] != 9) 
            {
                significantDigit = digits[i];
                break;
            }
        }
        
        int min = 0;
        int max = 0;

        int multiplier = 1;
        for (int i = digits.Count - 1; i >= 0; --i)
        {
            if (digits[i] != digits[0])                            
                min += (multiplier * digits[i]);
            if (digits[i] == significantDigit)
                max += (multiplier * 9);
            else 
                max += (multiplier * digits[i]);
            
            multiplier *= 10;
        }
        
        
        return max - min;
    }
    public void Test()
    {

    }
}
/*
2566. Maximum Difference by Remapping a Digit
Easy
183
32
Companies

You are given an integer num. You know that Danny Mittal will sneakily remap one of the 10 possible digits (0 to 9) to another digit.

Return the difference between the maximum and minimum values Danny can make by remapping exactly one digit in num.

Notes:

    When Danny remaps a digit d1 to another digit d2, Danny replaces all occurrences of d1 in num with d2.
    Danny can remap a digit to itself, in which case num does not change.
    Danny can remap different digits for obtaining minimum and maximum values respectively.
    The resulting number after remapping can contain leading zeroes.
    We mentioned "Danny Mittal" to congratulate him on being in the top 10 in Weekly Contest 326.

 

Example 1:

Input: num = 11891
Output: 99009
Explanation: 
To achieve the maximum value, Danny can remap the digit 1 to the digit 9 to yield 99899.
To achieve the minimum value, Danny can remap the digit 1 to the digit 0, yielding 890.
The difference between these two numbers is 99009.

Example 2:

Input: num = 90
Output: 99
Explanation:
The maximum value that can be returned by the function is 99 (if 0 is replaced by 9) and the minimum value that can be returned by the function is 0 (if 9 is replaced by 0).
Thus, we return 99.

 

Constraints:

    1 <= num <= 108


*/