﻿namespace PracticeLeetCodeCS.Easy;

internal class MinimumDistanceBetweenBSTNodes783
{

    //Definition for a binary tree node.
    public class TreeNode
    {
        public int val;
        public TreeNode left;
        public TreeNode right;
        public TreeNode(int val = 0, TreeNode left = null, TreeNode right = null)
        {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }
    private TreeNode prev = null;
    private int minDiff = int.MaxValue;
    public int MinDiffInBST(TreeNode root)
    {        
       if (root == null) return minDiff;
       MinDiffInBST(root.left);
       if (prev != null) minDiff = Math.Min(minDiff, root.val - prev.val);
       prev = root;
       MinDiffInBST(root.right);
       
        return minDiff;
    }
    public void Test()
    {
        TreeNode root = new TreeNode(4, 
            new TreeNode(2, new TreeNode(1), new TreeNode(3)),
            new TreeNode(6));

        TreeNode root2 = new TreeNode(1,
            new TreeNode(0),
            new TreeNode(48, new TreeNode(12), new TreeNode(49)));


        //[27,null,34,null,58,50,null,44] o7 e6
        TreeNode root3 = new TreeNode(27, 
           null,           
           new TreeNode(34, null, 
                new TreeNode(58, 
                    new TreeNode(50, new TreeNode(44), null), 
                    null)));

        var retVal = MinDiffInBST(root3);
    }
}
/*
 

Pre-order: Used to create a copy of a tree. For example, if you want to create a replica of a tree, put the nodes in an array with a pre-order traversal. 
Then perform an Insert operation on a new tree for each value in the array. You will end up with a copy of your original tree.

In-order: : Used to get the values of the nodes in non-decreasing order in a BST.

Post-order: : Used to delete a tree from leaf to root

When to use Pre-Order, In-order or Post-Order?

The traversal strategy the programmer selects depends on the specific needs of the algorithm being designed. The goal is speed, so pick the strategy that brings you the nodes you require the fastest.

    If you know you need to explore the roots before inspecting any leaves, you pick pre-order because you will encounter all the roots before all of the leaves.

    If you know you need to explore all the leaves before any nodes, you select post-order because you don"t waste any time inspecting roots in search for leaves.

    If you know that the tree has an inherent sequence in the nodes, and you want to flatten the tree back into its original sequence, than an in-order traversal should be used. The tree would be flattened in the same way it was created. A pre-order or post-order traversal might not unwind the tree back into the sequence which was used to create it.

 */

/*
 783. Minimum Distance Between BST Nodes
Easy
2K
340
Companies

Given the root of a Binary Search Tree (BST), return the minimum difference between the values of any two different nodes in the tree.

 

Example 1:

Input: root = [4,2,6,1,3]
Output: 1

Example 2:

Input: root = [1,0,48,null,null,12,49]
Output: 1

 

Constraints:

    The number of nodes in the tree is in the range [2, 100].
    0 <= Node.val <= 105

 

Note: This question is the same as 530: https://leetcode.com/problems/minimum-absolute-difference-in-bst/

 */