﻿namespace PracticeLeetCodeCS.Easy;

internal class MiddleoftheLinkedList876
{
    //Definition for singly-linked list.
    public class ListNode
    {
        public int val;
        public ListNode? next;
        public ListNode(int val = 0, ListNode? next = null)
        {
            this.val = val;
            this.next = next;
        }
    }
    public ListNode MiddleNode(ListNode? head)
    {
        List<ListNode> list = new List<ListNode>();
        ListNode? currentNode = head;
        while (currentNode != null)
        {
            list.Add(currentNode);
            currentNode = currentNode.next;
        }
        //int index = list.Count % 2 == 0 ? list.Count / 2 : list.Count / 2;
        return list[list.Count / 2];
    }
    public void Test()
    {
        ListNode next15 = new ListNode(6);
        ListNode next14 = new ListNode(5);
        ListNode next13 = new ListNode(4, next14);
        ListNode next12 = new ListNode(3, next13);
        ListNode next11 = new ListNode(2, next12);
        ListNode head1 = new ListNode(1, next11);

        ListNode result = MiddleNode(head1);
    }
}

/*
 876. Middle of the Linked List
Easy

Given the head of a singly linked list, return the middle node of the linked list.

If there are two middle nodes, return the second middle node.

 

Example 1:

Input: head = [1,2,3,4,5]
Output: [3,4,5]
Explanation: The middle node of the list is node 3.

Example 2:

Input: head = [1,2,3,4,5,6]
Output: [4,5,6]
Explanation: Since the list has two middle nodes with values 3 and 4, we return the second one.

 

Constraints:

    The number of nodes in the list is in the range [1, 100].
    1 <= Node.val <= 100


 */