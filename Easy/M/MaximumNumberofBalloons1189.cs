﻿using System.Text;

namespace PracticeLeetCodeCS.Easy;

internal class MaximumNumberofBalloons1189
{
    public int MaxNumberOfBalloons(string text)
    {
        int bCount = 0;
        int aCount = 0;
        int lCount = 0;
        int oCount = 0;
        int nCount = 0;

        int baloons = 0;

        for (int i = 0; i < text.Length; ++i)
        {
            if (text[i] == 'b') bCount++;
            if (text[i] == 'a') aCount++;
            if (text[i] == 'l') lCount++;
            if (text[i] == 'o') oCount++;
            if (text[i] == 'n') nCount++;     
            if (bCount > 0 && aCount > 0 && lCount > 1 && oCount > 1 && nCount > 0)
            {
                bCount--;
                aCount--;
                lCount -= 2;
                oCount -= 2;
                nCount--;
                baloons++;
            }
        }
        
        return baloons;

    }
    public void Test()
    {
        var retVal1 = MaxNumberOfBalloons("nlaebolko");
        var retVal2 = MaxNumberOfBalloons("loonbalxballpoon");
        var retVal3 = MaxNumberOfBalloons("leetcode");
    }
    /*while (bCount > 0 && aCount > 0 && lCount > 1 && oCount > 1 && nCount > 0) 
      { 
          bCount--;
          aCount--;
          lCount -= 2;
          oCount -= 2;
          nCount--;

          if (bCount >= 0 && aCount >= 0 && lCount >= 0 && oCount >= 0 && nCount >= 0)
              baloons++;
      }*/
}

/*
 * 1189. Maximum Number of Balloons
Easy
1.3K
77
Companies

Given a string text, you want to use the characters of text to form as many instances of the word "balloon" as possible.

You can use each character in text at most once. Return the maximum number of instances that can be formed.

 

Example 1:

Input: text = "nlaebolko"
Output: 1

Example 2:

Input: text = "loonbalxballpoon"
Output: 2

Example 3:

Input: text = "leetcode"
Output: 0

 

Constraints:

    1 <= text.length <= 104
    text consists of lower case English letters only.


 */