﻿using System.Xml.Linq;

namespace PracticeLeetCodeCS.Easy;

internal class BalancedBinaryTree110
{
    //Definition for a binary tree node.
    public class TreeNode
    {
        public int val;
        public TreeNode left;
        public TreeNode right;
        public TreeNode(int val = 0, TreeNode left = null, TreeNode right = null)
        {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }
    /*public bool IsBalanced(TreeNode root)
    {
        if (root == null)
            return true;

        int lh = GetHeight(root.left);
        int rh = GetHeight(root.right);
        if (Math.Abs(lh - rh) > 1)
            return false;

        bool islb = IsBalanced(root.left);
        bool isrb = IsBalanced(root.right);
        if (islb && isrb)
            return true;

        return false;
    }*/
    public bool IsBalanced(TreeNode root)
    {
        if (root == null)
            return true;

        if (Math.Abs(GetHeight(root.left) - GetHeight(root.right)) > 1)
            return false;

        if (IsBalanced(root.left) && IsBalanced(root.right))
            return true;

        return false;
    }
    public int GetHeight(TreeNode node)
    {
        if (node == null)
            return 0;
        return 1 + Math.Max(GetHeight(node.left), GetHeight(node.right));
    }

    public void Test()
    {
        /*TreeNode left_right = new TreeNode(3);

        TreeNode left_left_right = new TreeNode(4);
        TreeNode left_left_left = new TreeNode(4);        
        TreeNode left_left = new TreeNode(3, left_left_left, left_left_right);
                
        TreeNode left = new TreeNode(2, left_left, left_right);
        TreeNode right = new TreeNode(2);

        TreeNode root = new TreeNode(1, left, right);*/

        TreeNode right_right = new TreeNode(7);
        TreeNode right_left = new TreeNode(15);

        TreeNode left = new TreeNode(9);
        TreeNode right = new TreeNode(20, right_left, right_right);

        TreeNode root = new TreeNode(3, left, right);

        bool retVal = IsBalanced(root);
            
    }
}

/*
 110. Balanced Binary Tree
Easy

Given a binary tree, determine if it is height-balanced.

 

Example 1:

Input: root = [3,9,20,null,null,15,7]
Output: true

Example 2:

Input: root = [1,2,2,3,3,null,null,4,4]
Output: false

Example 3:

Input: root = []
Output: true

 

Constraints:

    The number of nodes in the tree is in the range [0, 5000].
    -104 <= Node.val <= 104


 
 */