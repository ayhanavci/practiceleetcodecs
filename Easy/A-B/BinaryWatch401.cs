﻿using System.Text;

namespace PracticeLeetCodeCS.Easy;

internal class BinaryWatch401
{
    public IList<string> ReadBinaryWatch(int turnedOn)
    {
        IList<string> list = new List<string>();
        StringBuilder timeString = new StringBuilder();
        for (int hour = 0; hour < 12; ++hour)
            for (int minute = 0; minute < 60; ++minute)
            {                
                if (BitSize(hour) + BitSize(minute) == turnedOn)
                {
                    timeString.Append(hour + ":");
                    if (minute < 10) 
                        timeString.Append("0");
                    timeString.Append(minute);
                    list.Add(timeString.ToString());
                    timeString.Clear();
                }
            }

        return list;
    }
    int BitSize(int number)
    {
        int bitCount = 0;        
        while (number > 0)
        {
            bitCount += number % 2;
            number = number / 2;
            if (number == 2)
            {
                bitCount++;
                break;
            }

        }
        return bitCount;
    }
    public void Test()
    {
        int test = 9;
        IList<string> retVal = ReadBinaryWatch(9);

    }
}

/*
 401. Binary Watch
Easy

A binary watch has 4 LEDs on the top to represent the hours (0-11), and 6 LEDs on the bottom to represent the minutes (0-59). Each LED represents a zero or one, with the least significant bit on the right.

    For example, the below binary watch reads "4:51".

Given an integer turnedOn which represents the number of LEDs that are currently on (ignoring the PM), return all possible times the watch could represent. You may return the answer in any order.

The hour must not contain a leading zero.

    For example, "01:00" is not valid. It should be "1:00".

The minute must be consist of two digits and may contain a leading zero.

    For example, "10:2" is not valid. It should be "10:02".

 

Example 1:

Input: turnedOn = 1
Output: ["0:01","0:02","0:04","0:08","0:16","0:32","1:00","2:00","4:00","8:00"]

Example 2:

Input: turnedOn = 9
Output: []

 

Constraints:

    0 <= turnedOn <= 10


 */