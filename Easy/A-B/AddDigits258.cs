﻿namespace PracticeLeetCodeCS.Easy;

internal class AddDigits258
{
  
    public int AddDigits(int num)
    {
        int total = num;
        while (num >= 10)
        {
            total = 0;
            while (num > 0) 
            {
                total += num % 10;
                num /= 10;
            }           
            num = total;
        }
        return total;
    }
    public void Test()
    {
        int retVal = AddDigits(38);
    }
}

/*
 258. Add Digits
Easy

Given an integer num, repeatedly add all its digits until the result has only one digit, and return it.

 

Example 1:

Input: num = 38
Output: 2
Explanation: The process is
38 --> 3 + 8 --> 11
11 --> 1 + 1 --> 2 
Since 2 has only one digit, return it.

Example 2:

Input: num = 0
Output: 0

 

Constraints:

    0 <= num <= 231 - 1

 

Follow up: Could you do it without any loop/recursion in O(1) runtime?

 */