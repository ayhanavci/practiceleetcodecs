﻿namespace PracticeLeetCodeCS.Easy;

internal class BinaryNumberwithAlternatingBits693
{
    public bool HasAlternatingBits(int n)
    {        
        bool fPrevious1 = (n & 1) == 1;
        while (n > 0)
        {
            n = n >> 1;
            bool fNext = ((n & 1) == 1);
            if (fPrevious1 == fNext)
                return false;
            fPrevious1 = fNext;
        }
        return true;
    }
    public void Test()
    {
        bool retval = HasAlternatingBits(5);
        retval = HasAlternatingBits(7);
        retval = HasAlternatingBits(11);
    }
}

/*
 693. Binary Number with Alternating Bits
Easy
1.1K
106
Companies

Given a positive integer, check whether it has alternating bits: namely, if two adjacent bits will always have different values.

 

Example 1:

Input: n = 5
Output: true
Explanation: The binary representation of 5 is: 101

Example 2:

Input: n = 7
Output: false
Explanation: The binary representation of 7 is: 111.

Example 3:

Input: n = 11
Output: false
Explanation: The binary representation of 11 is: 1011.

 

Constraints:

    1 <= n <= 231 - 1


 */