﻿using System.Text;

namespace PracticeLeetCodeCS.Easy;

internal class AddStrings415
{
    public string AddStrings(string num1, string num2)
    {        
        int longerCount = num1.Length > num2.Length ? num1.Length : num2.Length;

        StringBuilder total = new StringBuilder();
        int flow = 0;
        for (int i = 0; i < longerCount; i++) 
        {            
            int firstDigit = 0;
            int secondDigit = 0;
            if (i < num1.Length)
                firstDigit = num1[num1.Length - i - 1] - '0';
            if (i < num2.Length)
                secondDigit = num2[num2.Length - i - 1] - '0';
            int sum = firstDigit + secondDigit + flow;
            if (sum > 9)
            {
                flow = 1;
                sum %= 10;
            }
            else
                flow = 0;

            total.Insert(0, sum.ToString());
            
        }
        if (flow == 1)
            total.Insert(0, "1");
        return total.ToString();
    }
    public void Test()
    {
        string retVal = AddStrings("1", "9");
    }
}
/*
 415. Add Strings
Easy

Given two non-negative integers, num1 and num2 represented as string, return the sum of num1 and num2 as a string.

You must solve the problem without using any built-in library for handling large integers (such as BigInteger). You must also not convert the inputs to integers directly.

 

Example 1:

Input: num1 = "11", num2 = "123"
Output: "134"

Example 2:

Input: num1 = "456", num2 = "77"
Output: "533"

Example 3:

Input: num1 = "0", num2 = "0"
Output: "0"

 

Constraints:

    1 <= num1.length, num2.length <= 104
    num1 and num2 consist of only digits.
    num1 and num2 don't have any leading zeros except for the zero itself.


 */