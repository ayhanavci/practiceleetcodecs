﻿namespace PracticeLeetCodeCS.Easy;

internal class AvailableCapturesforRook999
{
    public int NumRookCaptures(char[][] board)
    {
        int rookX = -1;
        int rookY = -1; 
        for (int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 8; ++j)
            {
                if (board[i][j] == 'R') 
                {
                    rookY = i;
                    rookX = j;                    
                    break;
                }
            }
            if (rookX != -1)
                break;
        }

        int captures = 0;
        
        //North
        for (int i = rookY - 1; i >= 0; i--)
        {
            if (board[i][rookX] == 'p')
            {
                captures++;
                break;
            }
            else if (board[i][rookX] == 'B')                            
                break;            
        }

        //South
        for (int i = rookY + 1; i < 8; i++)
        {
            if (board[i][rookX] == 'p')
            {
                captures++;
                break;
            }
            else if (board[i][rookX] == 'B')
                break;
        }

        //West
        for (int i = rookX - 1; i >= 0; i--)
        {
            if (board[rookY][i] == 'p')
            {
                captures++;
                break;
            }
            else if (board[rookY][i] == 'B')
                break;
        }

        //East
        for (int i = rookX + 1; i < 8; i++)
        {
            if (board[rookY][i] == 'p')
            {
                captures++;
                break;
            }
            else if (board[rookY][i] == 'B')
                break;
        }

        return captures;

    }
    public void Test()
    {
        char[][] board = new char[8][];
        board[0] = new char[] { '.', '.', '.', '.', '.', '.', '.', '.' };
        board[1] = new char[] { '.', '.', '.', 'p', '.', '.', '.', '.' };
        board[2] = new char[] { '.', '.', '.', 'R', '.', '.', '.', 'p' };
        board[3] = new char[] { '.', '.', '.', '.', '.', '.', '.', '.' };
        board[4] = new char[] { '.', '.', '.', '.', '.', '.', '.', '.' };        
        board[5] = new char[] { '.', '.', '.', 'p', '.', '.', '.', '.' };
        board[6] = new char[] { '.', '.', '.', '.', '.', '.', '.', '.' };
        board[7] = new char[] { '.', '.', '.', '.', '.', '.', '.', '.' };

        var retVal = NumRookCaptures(board);

        char[][] board1 = new char[8][];
        board1[0] = new char[] { '.', '.', '.', '.', '.', '.', '.', '.' };
        board1[1] = new char[] { '.', 'p', 'p', 'p', 'p', 'p', '.', '.' };
        board1[2] = new char[] { '.', 'p', 'p', 'B', 'p', 'p', '.', '.' };
        board1[3] = new char[] { '.', 'p', 'B', 'R', 'B', 'p', '.', '.' };
        board1[4] = new char[] { '.', 'p', 'p', 'B', 'p', 'p', '.', '.' };
        board1[5] = new char[] { '.', 'p', 'p', 'p', 'p', 'p', '.', '.' };
        board1[6] = new char[] { '.', '.', '.', '.', '.', '.', '.', '.' };
        board1[7] = new char[] { '.', '.', '.', '.', '.', '.', '.', '.' };
        
        var retVal1 = NumRookCaptures(board1);

        char[][] board2 = new char[8][];
        board2[0] = new char[] { '.','.','.','.','.','.','.','.' };
        board2[1] = new char[] { '.','.','.','p','.','.','.','.'};
        board2[2] = new char[] { '.','.','.','p','.','.','.','.'};
        board2[3] = new char[] { 'p','p','.','R','.','p','B','.'};
        board2[4] = new char[] { '.','.','.','.','.','.','.','.'};
        board2[5] = new char[] { '.','.','.','B','.','.','.','.'};
        board2[6] = new char[] { '.','.','.','p','.','.','.','.'};
        board2[7] = new char[] { '.','.','.','.','.','.','.','.'};
        
        var retVal2 = NumRookCaptures(board2);

    }
}

/*
 
 999. Available Captures for Rook
Easy
524
599
Companies

On an 8 x 8 chessboard, there is exactly one white rook 'R' and some number of white bishops 'B', black pawns 'p', and empty squares '.'.

When the rook moves, it chooses one of four cardinal directions (north, east, south, or west), then moves in that direction until it chooses to stop, reaches the edge of the board, captures a black pawn, or is blocked by a white bishop. A rook is considered attacking a pawn if the rook can capture the pawn on the rook's turn. The number of available captures for the white rook is the number of pawns that the rook is attacking.

Return the number of available captures for the white rook.

 

Example 1:

Input: board = [['.','.','.','.','.','.','.','.'],['.','.','.','p','.','.','.','.'],['.','.','.','R','.','.','.','p'],['.','.','.','.','.','.','.','.'],['.','.','.','.','.','.','.','.'],['.','.','.','p','.','.','.','.'],['.','.','.','.','.','.','.','.'],['.','.','.','.','.','.','.','.']]
Output: 3
Explanation: In this example, the rook is attacking all the pawns.

Example 2:

Input: board = [['.','.','.','.','.','.','.','.'],['.','p','p','p','p','p','.','.'],['.','p','p','B','p','p','.','.'],['.','p','B','R','B','p','.','.'],['.','p','p','B','p','p','.','.'],['.','p','p','p','p','p','.','.'],['.','.','.','.','.','.','.','.'],['.','.','.','.','.','.','.','.']]
Output: 0
Explanation: The bishops are blocking the rook from attacking any of the pawns.

Example 3:

Input: board = [['.','.','.','.','.','.','.','.'],['.','.','.','p','.','.','.','.'],['.','.','.','p','.','.','.','.'],['p','p','.','R','.','p','B','.'],['.','.','.','.','.','.','.','.'],['.','.','.','B','.','.','.','.'],['.','.','.','p','.','.','.','.'],['.','.','.','.','.','.','.','.']]
Output: 3
Explanation: The rook is attacking the pawns at positions b5, d6, and f5.

 

Constraints:

    board.length == 8
    board[i].length == 8
    board[i][j] is either 'R', '.', 'B', or 'p'
    There is exactly one cell with board[i][j] == 'R'


 */