﻿namespace PracticeLeetCodeCS.Easy;

internal class BinaryTreePreorderTraversal144
{
    //Definition for a binary tree node.
    public class TreeNode
    {
        public int val;
        public TreeNode left;
        public TreeNode right;
        public TreeNode(int val = 0, TreeNode left = null, TreeNode right = null)
        {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }
    public IList<int> PreorderTraversal(TreeNode root)
    {
        List<int> list = new List<int>();
        if (root == null) return list;

        list.Add(root.val);
        list.AddRange(PreorderTraversal(root.left));
        list.AddRange(PreorderTraversal(root.right));

        return list;
    }
    public void Test()
    {
        TreeNode root = new TreeNode(1);
    }
}

/*
 144. Binary Tree Preorder Traversal
Easy

Given the root of a binary tree, return the preorder traversal of its nodes' values.

 

Example 1:

Input: root = [1,null,2,3]
Output: [1,2,3]

Example 2:

Input: root = []
Output: []

Example 3:

Input: root = [1]
Output: [1]

 

Constraints:

    The number of nodes in the tree is in the range [0, 100].
    -100 <= Node.val <= 100

 

Follow up: Recursive solution is trivial, could you do it iteratively?

 */