﻿namespace PracticeLeetCodeCS.Easy;

internal class AddtoArrayFormofInteger989
{
    public IList<int> AddToArrayForm(int[] num, int k)
    {
        IList<int> retVal = new List<int>();
        int extra = 0;
        int curPos = num.Length - 1;
        while (k > 0)
        {
            int lastDigit = k % 10;
            
            if (curPos >= 0)
            {
                int sum = num[curPos] + lastDigit + extra;                
                extra = sum / 10;
                sum = sum > 9 ? sum % 10 : sum;
                retVal.Insert(0, sum);
                curPos--;
            }
            else
            {
                int sum = lastDigit + extra;
                extra = sum / 10;
                sum = sum > 9 ? sum % 10 : sum;
                retVal.Insert(0, sum);
            }

            k /= 10;
        }
        while (curPos >= 0)
        {
            int sum = num[curPos] + extra;
            extra = sum / 10;
            sum = sum > 9 ? sum % 10 : sum;
            retVal.Insert(0, sum);
            curPos--;
        }
        if (extra > 0)
            retVal.Insert(0, extra);

        return retVal;
    }
    public void Test()
    {

        int[] num1 = { 1, 2, 4, 5 };
        int k1 = 34;
        var retVal = AddToArrayForm(num1, k1);

        int[] num2 = { 2,7,4 };
        int k2 = 181;
        var retVal2 = AddToArrayForm(num2, k2);

        int[] num3 = { 2, 1, 5 };
        int k3 = 8069;
        var retVal3 = AddToArrayForm(num3, k3);
    }
}

/*
 989. Add to Array-Form of Integer
Easy
1.7K
166
Companies

The array-form of an integer num is an array representing its digits in left to right order.

    For example, for num = 1321, the array form is [1,3,2,1].

Given num, the array-form of an integer, and an integer k, return the array-form of the integer num + k.

 

Example 1:

Input: num = [1,2,0,0], k = 34
Output: [1,2,3,4]
Explanation: 1200 + 34 = 1234

Example 2:

Input: num = [2,7,4], k = 181
Output: [4,5,5]
Explanation: 274 + 181 = 455

Example 3:

Input: num = [2,1,5], k = 806
Output: [1,0,2,1]
Explanation: 215 + 806 = 1021

 

Constraints:

    1 <= num.length <= 104
    0 <= num[i] <= 9
    num does not contain any leading zeros except for the zero itself.
    1 <= k <= 104


 */