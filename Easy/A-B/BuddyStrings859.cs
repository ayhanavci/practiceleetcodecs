﻿using System.Text;

namespace PracticeLeetCodeCS.Easy;

internal class BuddyStrings859
{
    public bool BuddyStrings(string s, string goal)
    {
        if (s.Length != goal.Length) return false;
        if (s.Equals(goal))
        {
            HashSet<char> chars = new HashSet<char>();
            for (int i = 0; i < s.Length; i++)            
                chars.Add(s[i]);
            return s.Length > chars.Count;//At least two different characters exists so we can swap.
        }
        //store different char positions
        List<int> diffPos = new List<int>();
        for (int i = 0; i < s.Length; ++i)        
            if (s[i] != goal[i]) diffPos.Add(i);

        
        if (diffPos.Count == 2) //Only 2 chars can be different
            if (s[diffPos[0]] == goal[diffPos[1]]) //Are those chars the same
                if (s[diffPos[1]] == goal[diffPos[0]]) //Are those chars the same
                    return true;
        return false;
    }
    //        return dif.size() == 2 && A.charAt(dif.get(0)) == B.charAt(dif.get(1)) && A.charAt(dif.get(1)) == B.charAt(dif.get(0));
    public bool BuddyStringsTimout(string s, string goal)
    {
        if (s.Length != goal.Length) return false;
        StringBuilder word = new StringBuilder();
        for (int i = 0; i < s.Length; i++)
        {
            
            for (int j = i + 1; j < s.Length; j++)
            {
                word.Append(s);
                word.Replace(s[i], s[j], i, 1);
                word.Replace(s[j], s[i], j, 1);
                if (word.ToString().Equals(goal))
                    return true;
                word.Clear();
            }            
        }
        return false;
    }
    public void Test()
    {
        BuddyStrings("ab", "ba");
    }
}

/*
 859. Buddy Strings
Easy
1.7K
1.1K
Companies

Given two strings s and goal, return true if you can swap two letters in s so the result is equal to goal, otherwise, return false.

Swapping letters is defined as taking two indices i and j (0-indexed) such that i != j and swapping the characters at s[i] and s[j].

    For example, swapping at indices 0 and 2 in "abcd" results in "cbad".

 

Example 1:

Input: s = "ab", goal = "ba"
Output: true
Explanation: You can swap s[0] = 'a' and s[1] = 'b' to get "ba", which is equal to goal.

Example 2:

Input: s = "ab", goal = "ab"
Output: false
Explanation: The only letters you can swap are s[0] = 'a' and s[1] = 'b', which results in "ba" != goal.

Example 3:

Input: s = "aa", goal = "aa"
Output: true
Explanation: You can swap s[0] = 'a' and s[1] = 'a' to get "aa", which is equal to goal.

 

Constraints:

    1 <= s.length, goal.length <= 2 * 104
    s and goal consist of lowercase letters.


 */