﻿namespace PracticeLeetCodeCS.Easy;

internal class BinaryTreeInorderTraversal94
{
    //Definition for a binary tree node.
  public class TreeNode {
      public int val;
      public TreeNode? left;
      public TreeNode? right;
      public TreeNode(int val=0, TreeNode? left=null, TreeNode? right=null) {
          this.val = val;
          this.left = left;
          this.right = right;
      }
  }
 
    public IList<int> InorderTraversal(TreeNode? root)
    {
        List<int> list = new List<int>();        
        if (root == null) return list;
              
        list.AddRange(InorderTraversal(root.left));
        list.Add(root.val);
        list.AddRange(InorderTraversal(root.right));

        return list;
    }
    public void Test()
    {
        TreeNode root = new TreeNode(1)
        {
            right = new TreeNode(2)
        };          
        root.right.left = new TreeNode(3);
        root.right.right = null;
        InorderTraversal(root);
    }
}

/*
 94. Binary Tree Inorder Traversal
Easy

Given the root of a binary tree, return the inorder traversal of its nodes' values.

 

Example 1:

Input: root = [1,null,2,3]
Output: [1,3,2]

Example 2:

Input: root = []
Output: []

Example 3:

Input: root = [1]
Output: [1]

 

Constraints:

    The number of nodes in the tree is in the range [0, 100].
    -100 <= Node.val <= 100

 */