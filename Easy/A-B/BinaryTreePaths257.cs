﻿using System.Text;

namespace PracticeLeetCodeCS.Easy;

internal class BinaryTreePaths257
{
    //Definition for a binary tree node.
    public class TreeNode
    {
        public int val;
        public TreeNode left;
        public TreeNode right;
        public TreeNode(int val = 0, TreeNode left = null, TreeNode right = null)
        {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }
    public IList<string> BinaryTreePaths2(TreeNode root)
    {        
        List<string> paths = new List<string>();
        if (root == null) return paths;

        StringBuilder pathLeft = new StringBuilder();
        pathLeft.Append(root.val.ToString());

        StringBuilder pathRight = new StringBuilder();
        pathRight.Append(root.val.ToString());

        IList<string> leftSubPaths = BinaryTreePaths(root.left);
        foreach (string leftSubPath in leftSubPaths)        
            pathLeft.Append("->" + leftSubPath.ToString());

        paths.Add(pathLeft.ToString());

        IList<string> rightSubPaths = BinaryTreePaths(root.right);
        foreach (string rightSubPath in rightSubPaths)            
            pathRight.Append("->" + rightSubPath.ToString());
        
        paths.Add(pathRight.ToString());
        return paths;
    }
    public IList<string> BinaryTreePaths(TreeNode root)
    {
        List<string> paths = new List<string>();
        if (root == null) return paths;
        
        if (root.left == null && root.right == null) //leaf
        {
            paths.Add(root.val.ToString());
            return paths;
        }
        IList<string> leftSubPaths = BinaryTreePaths(root.left);
        IList<string> rightSubPaths = BinaryTreePaths(root.right);

        for (int i = 0; i < leftSubPaths.Count; ++i)
        {
            string leftPath = root.val.ToString() + "->" + leftSubPaths[i];
            paths.Add(leftPath);
        }
        for (int i = 0; i < rightSubPaths.Count; ++i)
        {
            string rightPath = root.val.ToString() + "->" + rightSubPaths[i];
            paths.Add(rightPath);
        }

        return paths;
    }
    public void Test()
    {
        TreeNode left_left = new TreeNode(6);
        TreeNode left_right = new TreeNode(5);        

        TreeNode right = new TreeNode(3);
        TreeNode left = new TreeNode(2, left_left, left_right);

        TreeNode root = new TreeNode(1, left, right);

        IList<string> paths = BinaryTreePaths(root);
    }
}
/*
 257. Binary Tree Paths
Easy

Given the root of a binary tree, return all root-to-leaf paths in any order.

A leaf is a node with no children.

 

Example 1:

Input: root = [1,2,3,null,5]
Output: ["1->2->5","1->3"]

Example 2:

Input: root = [1]
Output: ["1"]

 

Constraints:

    The number of nodes in the tree is in the range [1, 100].
    -100 <= Node.val <= 100


 */