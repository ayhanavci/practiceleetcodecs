﻿namespace PracticeLeetCodeCS.Easy;

internal class AverageofLevelsinBinaryTree637
{

    //Definition for a binary tree node.
    public class TreeNode
    {
        public int val;
        public TreeNode left;
        public TreeNode right;
        public TreeNode(int val = 0, TreeNode left = null, TreeNode right = null)
        {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }

    public void GetAverage(TreeNode root, int level, List<int> count, List<double> sum)
    {
        if (root == null) return;

        if (count.Count > level)
        {
            count[level]++;
            sum[level] += root.val;
        }
        else
        {
            count.Add(1);
            sum.Add(root.val);
        }
        GetAverage(root.left, level + 1, count, sum);
        GetAverage(root.right, level + 1, count, sum);
    }

    public IList<double> AverageOfLevels(TreeNode root)
    {        
        IList<double> result = new List<double>();
        List<int> count = new List<int>();
        List<double> sum = new List<double>();

        GetAverage(root, 0, count, sum);

        for (int i = 0; i < count.Count; ++i)        
            result.Add(sum[i] / count[i]);
        

        return result;
    }
    public void Test()
    {

    }
}

/*
 637. Average of Levels in Binary Tree
Easy
4.4K
277
Companies
Given the root of a binary tree, return the average value of the nodes on each level in the form of an array. Answers within 10-5 of the actual answer will be accepted.

 

Example 1:

Input: root = [3,9,20,null,null,15,7]
Output: [3.00000,14.50000,11.00000]
Explanation: The average value of nodes on level 0 is 3, on level 1 is 14.5, and on level 2 is 11.
Hence return [3, 14.5, 11].

Example 2:

Input: root = [3,9,20,15,7]
Output: [3.00000,14.50000,11.00000]

 

Constraints:

    The number of nodes in the tree is in the range [1, 104].
    -231 <= Node.val <= 231 - 1


 */