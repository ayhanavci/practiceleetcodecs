﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticeLeetCodeCS.Easy;

public class AddBinary67
{
    public string AddBinary(string a, string b)
    {
        StringBuilder retVal = new StringBuilder();
        bool extra = false;
        
        for (int i = 0; i < a.Length || i < b.Length; i++)
        {
            char left = '0';
            char right = '0';
            
            if (i < a.Length) 
                left = a[a.Length - 1 - i];
            if (i < b.Length)
                right = b[b.Length - 1 - i];
            if (left == '0' && right == '0')
            {                    
                retVal.Insert(0, extra ? '1' : '0');                  
                extra = false;
            }
            else if (left == '1' && right == '1')
            {
                retVal.Insert(0, extra ? '1' : '0');
                extra = true;
            }
            else //if (left == '0' && right == '1' || left == '1' && right == '0')
            {
                retVal.Insert(0, extra ? '0' : '1');                                        
            }
        }
        if (extra) retVal.Insert(0, '1');

        return retVal.ToString();
    }
    public void Test()
    {
        string a = "111010";
        string b = "1011";

        string result = AddBinary(a, b);

    }
}
/*
 67. Add Binary
Easy

Given two binary strings a and b, return their sum as a binary string.

 

Example 1:

Input: a = "11", b = "1"
Output: "100"

Example 2:

Input: a = "1010", b = "1011"
Output: "10101"

 

Constraints:

    1 <= a.length, b.length <= 104
    a and b consist only of '0' or '1' characters.
    Each string does not contain leading zeros except for the zero itself.


 
 */