﻿namespace PracticeLeetCodeCS.Easy;

internal class BinaryPrefixDivisibleBy5_1018
{
    public IList<bool> PrefixesDivBy5(int[] nums)
    {
        IList<bool> result = new List<bool>();

        int number = 0;
        for (int i = 0; i < nums.Length; ++i)
        {
            number <<= 1;
            number += nums[i];
            number %= 5;            
            result.Add(number == 0);
        }

        return result;
    }
    public void Test()
    {
        //int[] nums1 = { 1, 0, 1 };
        //var retVal = PrefixesDivBy5(nums1);

        //int[] nums2 = { 0, 1, 1 };
        //var retval2 = PrefixesDivBy5(nums2);

        //int[] nums3 = { 1, 1, 1 };
        //var retval3 = PrefixesDivBy5(nums3);

        //int[] nums4 = { 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 1, 0, 1, 0, 0, 0, 1 };
        //false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,true,false,false,true,true,true,true,false

        int[] nums4 = { 1, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 1, 0, 0, 1, 0 };
        var retval4 = PrefixesDivBy5(nums4);
        
        //1,1,0,0,0,1,0,0,1
        //1,3,6,12
    }
}

/*
 In Java, an integer int is a 32-bit number, that's why it is in range of [-2^31, 2^31 - 1]. So if we use above way, then it can maximumly represent 32 bits in the array A. If beyond that, then overflow will happen, you may not get correct result.

So we need to use some Math Knowledge(I learnt it from Cryptography Course if my memory services my right):
Consider the formula below which is the key to this problem:

(a * b + c) % d = ((a % d) * (b % d) + c % d) % d

Simply say is that we mod each part in a * b + c, then mod the result.

So in this problem, num = (num << 1) + cur which can be written as num = num * 2 + (0 or 1). From above trick, we get num % 5 = (num % 5) * (2 % 5) + (0 or 1) % 5. Since 2, 0, 1 all smaller than 5, so they mod 5 do not cause any difference, we simplify the formula to => num % 5 = 2 * (num % 5) + (0 or 1).

From above we know that we can update num to num % 5 each time which then avoids overflow for us.
 */

/*
 When a binary number is appended by 0 , the new remainder can be calculated based on current remainder only.
remainder = (remainder * 2) % 5;

And when a binary number is appended by 1.
remainder = (remainder * 2 + 1) % 5;

For example:
If it is appended by 0 it will become 10 (2 in decimal) means twice of the last value.
If it is appended by 1 it will become 11(3 in decimal), twice of last value +1.

public List<Boolean> prefixesDivBy5(int[] A) {
        List<Boolean> list = new ArrayList<>();
        int remainder = 0;
        for(int bit : A) {
            if (bit == 1)
                remainder = (remainder * 2 + 1) % 5;
            if (bit == 0)
                remainder = (remainder * 2) % 5;
            if(remainder%5==0) {
                list.add(true);
            } else {
                list.add(false);
            }
        }
        return list;
    }
 */
/*
 public List<Boolean> prefixesDivBy5(int[] A) {
    List<Boolean> out = new ArrayList<Boolean>();
    int remainder = 0;
    
    for(int a : A) {
        remainder = (remainder * 2 + a) % 5;            
        out.add(remainder == 0);
    }
    
    return out;
}
 */
/*
 1018. Binary Prefix Divisible By 5
Easy
613
158
Companies

You are given a binary array nums (0-indexed).

We define xi as the number whose binary representation is the subarray nums[0..i] (from most-significant-bit to least-significant-bit).

    For example, if nums = [1,0,1], then x0 = 1, x1 = 2, and x2 = 5.

Return an array of booleans answer where answer[i] is true if xi is divisible by 5.

 

Example 1:

Input: nums = [0,1,1]
Output: [true,false,false]
Explanation: The input numbers in binary are 0, 01, 011; which are 0, 1, and 3 in base-10.
Only the first number is divisible by 5, so answer[0] is true.

Example 2:

Input: nums = [1,1,1]
Output: [false,false,false]

 

Constraints:

    1 <= nums.length <= 105
    nums[i] is either 0 or 1.


 */