﻿using System.Text;

namespace PracticeLeetCodeCS.Easy;

internal class Base7_504
{
    public string ConvertToBase7(int num)
    {
        if (num == 0) return "0";
        StringBuilder result = new StringBuilder();        

        int numP = Math.Abs(num);                            
        while (numP > 0)
        {
            result.Insert(0, numP % 7);
            numP /= 7;
        }

        if (num < 0)
            result.Insert(0, "-");
        return result.ToString();
    }
    public void Test()
    {
        int num = -7;
        string base7 = ConvertToBase7(num);
    }
}


/*
 504. Base 7
Easy

Given an integer num, return a string of its base 7 representation.

 

Example 1:

Input: num = 100
Output: "202"

Example 2:

Input: num = -7
Output: "-10"

 

Constraints:

    -107 <= num <= 107


 */