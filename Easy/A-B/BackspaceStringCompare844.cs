﻿using System.Text;

namespace PracticeLeetCodeCS.Easy;

internal class BackspaceStringCompare844
{
    public bool BackspaceCompare(string s, string t)
    {
        StringBuilder finalS = new StringBuilder();
        StringBuilder finalT = new StringBuilder();

        for (int i = 0; i < s.Length; i++)
        {
            if (s[i] == '#')
            {
                if (finalS.Length > 0)
                    finalS.Remove(finalS.Length - 1, 1);                
            }
            else            
                finalS.Append(s[i]);
            
        }

        for (int i = 0; i < t.Length; i++)
        {
            if (t[i] == '#')
            {
                if (finalT.Length > 0)
                    finalT.Remove(finalT.Length - 1, 1);
            }
            else
                finalT.Append(s[i]);

        }
        return finalS.Equals(finalT.ToString());
    }
    public void Test()
    {
        string s = "xywrrmp"; 
        string t = "xywrrmu#p";

        var retVal = BackspaceCompare(s, t);
    }
}


/*
 844. Backspace String Compare
Easy
5.9K
270
Companies

Given two strings s and t, return true if they are equal when both are typed into empty text editors. '#' means a backspace character.

Note that after backspacing an empty text, the text will continue empty.

 

Example 1:

Input: s = "ab#c", t = "ad#c"
Output: true
Explanation: Both s and t become "ac".

Example 2:

Input: s = "ab##", t = "c#d#"
Output: true
Explanation: Both s and t become "".

Example 3:

Input: s = "a#c", t = "b"
Output: false
Explanation: s becomes "c" while t becomes "b".

 

Constraints:

    1 <= s.length, t.length <= 200
    s and t only contain lowercase letters and '#' characters.

 

Follow up: Can you solve it in O(n) time and O(1) space?

 */