﻿namespace PracticeLeetCodeCS.Easy;

//TODO ezberle
internal class CheckIfItIsaStraightLine1232
{
    public bool CheckStraightLine(int[][] coordinates)
    {
        int dY = coordinates[1][1] - coordinates[0][1];
        int dX = coordinates[1][0] - coordinates[0][0];
        for (int i = 2; i < coordinates.Length; i++)
        {
            int[] p = coordinates[i];
            if (dX * (p[1] - coordinates[0][1]) != dY * (p[0] - coordinates[0][0]))
                return false;
        }
        return true;
    }
    public void Test()
    {

    }
}

/*
 (y - y1) / (x - x1) = (y1 - y0) / (x1 - x0)
In order to avoid being divided by 0, use multiplication form:

(x1 - x0) * (y - y1) = (x - x1) * (y1 - y0) =>
dx * (y - y1) = dy * (x - x1), where dx = x1 - x0 and dy = y1 - y0
 */
/*
 1232. Check If It Is a Straight Line
Easy
1.2K
174
Companies

You are given an array coordinates, coordinates[i] = [x, y], where [x, y] represents the coordinate of a point. Check if these points make a straight line in the XY plane.

 

 

Example 1:

Input: coordinates = [[1,2],[2,3],[3,4],[4,5],[5,6],[6,7]]
Output: true

Example 2:

Input: coordinates = [[1,1],[2,2],[3,4],[4,5],[5,6],[7,7]]
Output: false

 

Constraints:

    2 <= coordinates.length <= 1000
    coordinates[i].length == 2
    -10^4 <= coordinates[i][0], coordinates[i][1] <= 10^4
    coordinates contains no duplicate point.

 */