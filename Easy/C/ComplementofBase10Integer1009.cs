﻿using System;

namespace PracticeLeetCodeCS.Easy;

internal class ComplementofBase10Integer1009
{
    public int BitwiseComplement(int n)
    {
        int complement = 0;
        for (int i = 0; n != 0; i++)
        {
            int isOne = n & 1;
            if (isOne == 0)
                complement += (int)Math.Pow(2, i);

            n >>= 1;
        }
        return complement;
    }
    public void Test()
    {
        var retVal1 = BitwiseComplement(5);
        var retVal2 = BitwiseComplement(7);
        var retVal3 = BitwiseComplement(10);
    }
}

/*
 1009. Complement of Base 10 Integer
Easy
1.8K
87
Companies

The complement of an integer is the integer you get when you flip all the 0's to 1's and all the 1's to 0's in its binary representation.

    For example, The integer 5 is "101" in binary and its complement is "010" which is the integer 2.

Given an integer n, return its complement.

 

Example 1:

Input: n = 5
Output: 2
Explanation: 5 is "101" in binary, with complement "010" in binary, which is 2 in base-10.

Example 2:

Input: n = 7
Output: 0
Explanation: 7 is "111" in binary, with complement "000" in binary, which is 0 in base-10.

Example 3:

Input: n = 10
Output: 5
Explanation: 10 is "1010" in binary, with complement "0101" in binary, which is 5 in base-10.

 

Constraints:

    0 <= n < 109

 */