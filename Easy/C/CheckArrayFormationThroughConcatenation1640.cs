﻿namespace PracticeLeetCodeCS.Easy;

internal class CheckArrayFormationThroughConcatenation1640
{
    public bool CanFormArray(int[] arr, int[][] pieces)
    {
        List<List<int>> piecesList = new List<List<int>>();
        for (int i = 0; i < pieces.Length; ++i)        
            piecesList.Add(new List<int>(pieces[i]));        

        for (int i = 0; i < arr.Length;)
        {
            bool fFound = false;
            for (int j = 0; j < piecesList.Count; ++j)
            {
                List<int> piece = piecesList[j];
                fFound = true;
                int checkIndex = i;
                for (int k = 0; k < piece.Count; ++k)
                {
                    if (piece[k] != arr[checkIndex])
                    {
                        fFound = false;
                        break;
                    }
                    checkIndex++;
                }
                if (fFound)
                {
                    i += piece.Count;
                    piecesList.RemoveAt(j);
                    break;
                }
            }
            if (!fFound) return false;
        }

        return piecesList.Count == 0;
    }
    public void Test()
    {
        int[] arr = new int[] { 91, 4, 64, 78 };
        int[][] pieces = new int[3][];
        pieces[0] = new int[] { 78 };
        pieces[1] = new int[] { 4, 64 };
        pieces[2] = new int[] { 91 };

        var retVal = CanFormArray(arr, pieces);
    }
}

/*
 1640. Check Array Formation Through Concatenation
Easy
783
126
Companies

You are given an array of distinct integers arr and an array of integer arrays pieces, where the integers in pieces are distinct. Your goal is to form arr by concatenating the arrays in pieces in any order. However, you are not allowed to reorder the integers in each array pieces[i].

Return true if it is possible to form the array arr from pieces. Otherwise, return false.

 

Example 1:

Input: arr = [15,88], pieces = [[88],[15]]
Output: true
Explanation: Concatenate [15] then [88]

Example 2:

Input: arr = [49,18,16], pieces = [[16,18,49]]
Output: false
Explanation: Even though the numbers match, we cannot reorder pieces[0].

Example 3:

Input: arr = [91,4,64,78], pieces = [[78],[4,64],[91]]
Output: true
Explanation: Concatenate [91] then [4,64] then [78]

 

Constraints:

    1 <= pieces.length <= arr.length <= 100
    sum(pieces[i].length) == arr.length
    1 <= pieces[i].length <= arr.length
    1 <= arr[i], pieces[i][j] <= 100
    The integers in arr are distinct.
    The integers in pieces are distinct (i.e., If we flatten pieces in a 1D array, all the integers in this array are distinct).


 */