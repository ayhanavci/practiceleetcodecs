﻿namespace PracticeLeetCodeCS.Easy;
internal class CheckifArrayIsSortedandRotated1752
{
    public bool Check(int[] nums)
    {
        List<int> numberBoard = new List<int>();
        numberBoard.AddRange(nums);
        numberBoard.AddRange(nums);

        int orderCount = 0;
        for (int i = 0; i < numberBoard.Count - 1; i++)
        {
            if (numberBoard[i + 1] >= numberBoard[i])            
                orderCount++;            
            else
                orderCount = 1;
            if (orderCount == nums.Length)
                return true;
        }

        return false;
    }
    public void Test()
    {
        int[] nums1 = { 3, 4, 5, 1, 2 };
        int[] nums10 = { 3, 4, 5, 1, 2, 3, 4, 5, 1, 2 };
        int[] nums11 = { 5, 1, 2, 3, 4, 5, 1, 2, 3, 4 };
        var retVal = Check(nums1);
    }
}

/*
 1752. Check if Array Is Sorted and Rotated
Easy
1.6K
76
Companies

Given an array nums, return true if the array was originally sorted in non-decreasing order, then rotated some number of positions (including zero). Otherwise, return false.

There may be duplicates in the original array.

Note: An array A rotated by x positions results in an array B of the same length such that A[i] == B[(i+x) % A.length], where % is the modulo operation.

 

Example 1:

Input: nums = [3,4,5,1,2]
Output: true
Explanation: [1,2,3,4,5] is the original sorted array.
You can rotate the array by x = 3 positions to begin on the the element of value 3: [3,4,5,1,2].

Example 2:

Input: nums = [2,1,3,4]
Output: false
Explanation: There is no sorted array once rotated that can make nums.

Example 3:

Input: nums = [1,2,3]
Output: true
Explanation: [1,2,3] is the original sorted array.
You can rotate the array by x = 0 positions (i.e. no rotation) to make nums.

 

Constraints:

    1 <= nums.length <= 100
    1 <= nums[i] <= 100


 */