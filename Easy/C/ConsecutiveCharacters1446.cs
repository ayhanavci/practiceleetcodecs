﻿namespace PracticeLeetCodeCS.Easy;

internal class ConsecutiveCharacters1446
{
    public int MaxPower(string s)
    {
        int maxPower = 1;
        int currentPower = 1;
        char lastChar = s[0];
        
        for (int i = 1; i < s.Length; ++i)
        {
            if (s[i] == lastChar)
            {                
                maxPower = Math.Max(maxPower, ++currentPower);
            }
            else
            {
                currentPower = 1;
                lastChar = s[i];
            }
                
        }

        return maxPower;
    }
    public void Test()
    {
        string s = "abbcccddddeeeeedcba";
        var retVal = MaxPower(s);
    }
}

/*
 1446. Consecutive Characters
Easy
1.5K
29
Companies

The power of the string is the maximum length of a non-empty substring that contains only one unique character.

Given a string s, return the power of s.

 

Example 1:

Input: s = "leetcode"
Output: 2
Explanation: The substring "ee" is of length 2 with the character 'e' only.

Example 2:

Input: s = "abbcccddddeeeeedcba"
Output: 5
Explanation: The substring "eeeee" is of length 5 with the character 'e' only.

 

Constraints:

    1 <= s.length <= 500
    s consists of only lowercase English letters.


 */