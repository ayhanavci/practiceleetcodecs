﻿namespace PracticeLeetCodeCS.Easy;

internal class CountBinarySubstrings696
{
    //TODO

    /*
     First, I count the number of 1 or 0 grouped consecutively.
    For example "0110001111" will be [1, 2, 3, 4].

    Second, for any possible substrings with 1 and 0 grouped consecutively, the number of valid substring will be the minimum number of 0 and 1.
    For example "0001111", will be min(3, 4) = 3, ("01", "0011", "000111")
     */
    public int CountBinarySubstrings(string s)
    {
        int current = 1;
        int previous = 0; ;
        int total = 0;

        for (int i = 1; i < s.Length; i++)
        {
            if (s[i-1] == s[i])
            {
                current++;
            }
            else
            {
                total += Math.Min(current, previous);
                previous = current;
                current = 1;
            }
        }

        return total + Math.Min(current, previous);
    }
    public int CountBinarySubstringsTimeout(string s)
    {
        int count = 0;

        for (int i = 0; i < s.Length; i++)
        {
            int previous = -1;
            int numberof1s = 0;
            int numberof0s = 0;

            for (int j = i; j < s.Length; j++)
            {
                if (previous != -1 && previous != s[j])
                {
                    if ((s[j] == '0' && numberof0s > 0) ||
                        (s[j] == '1' && numberof1s > 0))
                    {
                        if (numberof0s == numberof1s)                        
                            count++;
                        break;
                    } 
                    
                }
                if (s[j] == '0') 
                    numberof0s++;
                else 
                    numberof1s++;
                if (numberof0s == numberof1s)
                {
                    count++;
                    break;
                }
                    
                previous = s[j];
            }
        }

        return count;
    }

    public int CountBinarySubstringsEx(string s)
    {
        int count = 0;        

        for (int i = 0; i < s.Length; i++)
        {
            int numberof1s = 0;
            int numberof0s = 0;
            int previous = -1;

            for (int j = i; j < s.Length; j++)
            {
                if (s[j] == '0')
                {
                    if (previous == -1)
                    {
                        previous = 0;
                        numberof0s++;
                    }
                    else if (previous == 0)
                    {
                        if (++numberof0s == numberof1s)
                        {
                            count++;
                            break;
                        }                        
                    }
                    else if (previous == 1)
                    {
                        previous = 0;
                        if (numberof0s > 0)
                        {
                            if (numberof0s == numberof1s)
                                count++;
                            break;
                        }
                        numberof0s++;
                        if (numberof0s == numberof1s)
                        {
                            count++;
                            break;
                        }                            
                    }
                    
                }
                else
                {
                    if (previous == -1)
                    {
                        previous = 1; 
                        numberof1s++;
                    }
                    else if (previous == 1)
                    {                        
                        if (numberof0s == ++numberof1s)
                        {
                            count++;
                            break;
                        }                                                                  
                    }
                    else if (previous == 0)
                    {
                        previous = 1;                        

                        if (numberof1s > 0)
                        {
                            if (numberof0s == numberof1s)
                                count++;
                            break;
                        }                        
                        numberof1s++;
                        if (numberof0s == numberof1s)
                        {
                            count++;
                            break;
                        }
                    }
                }
            }
        }

        return count;
    }
    public void Test()
    {
        //string s = "00110011";
        string s = "10101";

        var retVal = CountBinarySubstrings(s);
    }
}

/*
 696. Count Binary Substrings
Easy
3.4K
743
Companies

Given a binary string s, return the number of non-empty substrings that have the same number of 0"s and 1"s, and all the 0"s and all the 1"s in these substrings are grouped consecutively.

Substrings that occur multiple times are counted the number of times they occur.

 

Example 1:

Input: s = "00110011"
Output: 6
Explanation: There are 6 substrings that have equal number of consecutive 1"s and 0"s: "0011", "01", "1100", "10", "0011", and "01".
Notice that some of these substrings repeat and are counted the number of times they occur.
Also, "00110011" is not a valid substring because all the 0"s (and 1"s) are not grouped together.

Example 2:

Input: s = "10101"
Output: 4
Explanation: There are 4 substrings: "10", "01", "10", "01" that have equal number of consecutive 1"s and 0"s.

 

Constraints:

    1 <= s.length <= 105
    s[i] is either "0" or "1".


 */