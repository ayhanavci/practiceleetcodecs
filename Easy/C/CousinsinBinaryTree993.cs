﻿namespace PracticeLeetCodeCS.Easy;

internal class CousinsinBinaryTree993
{

    //Definition for a binary tree node.
    public class TreeNode
    {
        public int val;
        public TreeNode left;
        public TreeNode right;
        public TreeNode(int val = 0, TreeNode left = null, TreeNode right = null)
        {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }
    int xDepth = -1;
    int yDepth = -1;
    int xParent = -1;
    int yParent = -1;   

    public void FindDepth(TreeNode root, int x, int y, int curDepth, int parentVal)
    {
        if (root == null) return;
        if (root.val == x)
        {
            xDepth = curDepth;
            xParent = parentVal; 
        }            
        if (root.val == y)
        {
            yDepth = curDepth;
            yParent = parentVal;
        }

        if (xParent != -1 && yParent != -1) return;

        curDepth++;

        FindDepth(root.left, x, y, curDepth, root.val);
        FindDepth(root.right, x, y, curDepth, root.val);
    }

    public bool IsCousins(TreeNode root, int x, int y)
    {        
        FindDepth(root, x, y, 0, root.val);
        return xDepth == yDepth && xParent != yParent;
    }
    public void Test()
    {
        TreeNode root1 = new TreeNode(1, 
            new TreeNode(2, null, new TreeNode(4)),  
            new TreeNode(3, null, new TreeNode(5)));

        //var retVal1 = IsCousins(root, 5, 4);

        TreeNode root2 = new TreeNode(1,
            new TreeNode(2, new TreeNode(4)),
            new TreeNode(3));

        //var retVal2 = IsCousins(root2, 4, 3);

        TreeNode root3 = new TreeNode(1,
            new TreeNode(2, null, new TreeNode(4)),
            new TreeNode(3));

        var retval3 = IsCousins(root3, 2, 3);
    }
}

/*
 993. Cousins in Binary Tree
Easy
3.3K
168
Companies

Given the root of a binary tree with unique values and the values of two different nodes of the tree x and y, return true if the nodes corresponding to the values x and y in the tree are cousins, or false otherwise.

Two nodes of a binary tree are cousins if they have the same depth with different parents.

Note that in a binary tree, the root node is at the depth 0, and children of each depth k node are at the depth k + 1.

 

Example 1:

Input: root = [1,2,3,4], x = 4, y = 3
Output: false

Example 2:

Input: root = [1,2,3,null,4,null,5], x = 5, y = 4
Output: true

Example 3:

Input: root = [1,2,3,null,4], x = 2, y = 3
Output: false

 

Constraints:

    The number of nodes in the tree is in the range [2, 100].
    1 <= Node.val <= 100
    Each node has a unique value.
    x != y
    x and y are exist in the tree.


 */