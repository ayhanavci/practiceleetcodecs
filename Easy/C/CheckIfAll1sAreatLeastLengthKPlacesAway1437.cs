﻿namespace PracticeLeetCodeCS.Easy;

internal class CheckIfAll1sAreatLeastLengthKPlacesAway1437
{
    public bool KLengthApart(int[] nums, int k)
    {
        int previous1Index = -1;
        for (int i = 0; i < nums.Length; i++)
        {
            if (nums[i] == 1)
            {
                if (previous1Index == -1)
                {
                    previous1Index = i;
                    continue;
                }
                if (k >= i - previous1Index)
                    return false;
                previous1Index = i;
            }            

        }
        return true;
    }
    public void Test()
    {
        int[] nums = {1, 0, 0, 0, 1, 0, 0, 1};
        var retVal = KLengthApart(nums, 2);
    }
}

/*
 1437. Check If All 1's Are at Least Length K Places Away
Easy
500
206
Companies

Given an binary array nums and an integer k, return true if all 1's are at least k places away from each other, otherwise return false.

 

Example 1:

Input: nums = [1,0,0,0,1,0,0,1], k = 2
Output: true
Explanation: Each of the 1s are at least 2 places away from each other.

Example 2:

Input: nums = [1,0,0,1,0,1], k = 2
Output: false
Explanation: The second 1 and third 1 are only one apart from each other.

 

Constraints:

    1 <= nums.length <= 105
    0 <= k <= nums.length
    nums[i] is 0 or 1


 */