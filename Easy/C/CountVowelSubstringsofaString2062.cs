namespace PracticeLeetCodeCS.Easy;

internal class CountVowelSubstringsofaString2062
{
    public int CountVowelSubstrings(string word) 
    {
        int count = 0;

        for (int i = 0; i < word.Length; ++i)
        {
            if (word[i] == 'a' ||
                word[i] == 'e' ||
                word[i] == 'i' ||
                word[i] == 'o' ||
                word[i] == 'u')
                count += FindSubStrings(i, word);
        }

        return count;
    }
    private int FindSubStrings(int index, string word)
    {
        bool fFoundA = false;
        bool fFoundE = false;
        bool fFoundI = false;
        bool fFoundO = false;
        bool fFoundU = false;
        int count = 0;
        for (int i = index; i < word.Length; ++i)
        {   
            switch (word[i])
            {
                case 'a': fFoundA = true; break;
                case 'e': fFoundE = true; break;
                case 'i': fFoundI = true; break;
                case 'o': fFoundO = true; break;
                case 'u': fFoundU = true; break;
                default: return count;
            }            
            if (fFoundA && fFoundE && fFoundI && fFoundO && fFoundU)             
                count++;
            
        }
        return count;
    }

    public void Test()
    {
        var retVal = CountVowelSubstrings("unicornarihan");
    }
}
/*
2062. Count Vowel Substrings of a String
Easy
728
176
Companies

A substring is a contiguous (non-empty) sequence of characters within a string.

A vowel substring is a substring that only consists of vowels ('a', 'e', 'i', 'o', and 'u') and has all five vowels present in it.

Given a string word, return the number of vowel substrings in word.

 

Example 1:

Input: word = "aeiouu"
Output: 2
Explanation: The vowel substrings of word are as follows (underlined):
- "aeiouu"
- "aeiouu"

Example 2:

Input: word = "unicornarihan"
Output: 0
Explanation: Not all 5 vowels are present, so there are no vowel substrings.

Example 3:

Input: word = "cuaieuouac"
Output: 7
Explanation: The vowel substrings of word are as follows (underlined):
- "cuaieuouac"
- "cuaieuouac"
- "cuaieuouac"
- "cuaieuouac"
- "cuaieuouac"
- "cuaieuouac"
- "cuaieuouac"

 

Constraints:

    1 <= word.length <= 100
    word consists of lowercase English letters only.


*/