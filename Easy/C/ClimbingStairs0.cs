﻿namespace PracticeLeetCodeCS.Easy;

internal class ClimbingStairs0
{
    public int ClimbStairs(int n)
    {
        if (n <= 1)
            return n;

        int firstFib = 1;
        int secondFib = 2;

        for (int i = 3; i <= n; ++i)
        {
            int nextFib = firstFib + secondFib;
            firstFib = secondFib;
            secondFib = nextFib;
        }
       
        return secondFib;
    }
    public int ClimbStairs2(int n)
    {
        int ways = 0;
        if (n == 0) return 1;
        if (n < 0) return 0;
        ways += ClimbStairs(n - 1);
        ways += ClimbStairs(n - 2);            
        return ways;
    }
    public void Test()
    {
        //1134903170
        Console.WriteLine($"{ClimbStairs2(44)}");
    }
}

/*
1) 1 + 1 + 1 + 1 + 1
2) 1 + 1 + 1 + 2
3) 1 + 1 + 2 + 1
4) 
 */

/*
 0. Climbing Stairs
Easy

You are climbing a staircase. It takes n steps to reach the top.

Each time you can either climb 1 or 2 steps. In how many distinct ways can you climb to the top?

 

Example 1:

Input: n = 2
Output: 2
Explanation: There are two ways to climb to the top.
1. 1 step + 1 step
2. 2 steps

Example 2:

Input: n = 3
Output: 3
Explanation: There are three ways to climb to the top.
1. 1 step + 1 step + 1 step
2. 1 step + 2 steps
3. 2 steps + 1 step

 

Constraints:

    1 <= n <= 45


 
 */