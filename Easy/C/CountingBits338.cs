﻿namespace PracticeLeetCodeCS.Easy;

internal class CountingBits338
{
    public int[] CountBits(int n)
    {
        int[] bits = new int[n+1];
        for (int i = 0; i <= n; i++)
        {
            int bitCount = 0;            
            int number = i;
            while (number > 0)
            {
                bitCount += number % 2;
                number = number / 2;
                if (number == 2)
                {
                    bitCount++;
                    break;
                }
                    
            }
            bits[i] = bitCount;
        }
        return bits;
    }
    public void Test()
    {
        //int[] bits = CountBits(0);
        int[] bits1 = CountBits(1);
        int[] bits2 = CountBits(2);
        int[] bits3 = CountBits(3);
        int[] bits4 = CountBits(4);
        int[] bits5 = CountBits(5);
        int[] bits6 = CountBits(6);
        int[] bits17 = CountBits(17);
    }
}

/*
 338. Counting Bits
Easy

Given an integer n, return an array ans of length n + 1 such that for each i (0 <= i <= n), ans[i] is the number of 1"s in the binary representation of i.

 

Example 1:

Input: n = 2
Output: [0,1,1]
Explanation:
0 --> 0
1 --> 1
2 --> 10

Example 2:

Input: n = 5
Output: [0,1,1,2,1,2]
Explanation:
0 --> 0
1 --> 1
2 --> 10
3 --> 11
4 --> 100
5 --> 101

 

Constraints:

    0 <= n <= 105

 

Follow up:

    It is very easy to come up with a solution with a runtime of O(n log n). Can you do it in linear time O(n) and possibly in a single pass?
    Can you do it without using any built-in function (i.e., like __builtin_popcount in C++)?


 */