﻿namespace PracticeLeetCodeCS.Easy;

internal class ContainsDuplicateII219
{
    public bool ContainsNearbyDuplicate(int[] nums, int k)
    {        
        Dictionary<int, List<int>> occurances = new Dictionary<int, List<int>>();
        for (int i = 0; i < nums.Length; i++)
        {
            List<int> indeces;
            if (occurances.TryGetValue(nums[i], out indeces))
            {                
                foreach (var ind in indeces)
                {
                    if (i - ind <= k)
                        return true;
                }
                indeces.Add(i);
                occurances[nums[i]] = indeces;
            }
            else
            {
                indeces = new List<int>();
                indeces.Add(i);
                occurances[nums[i]] = indeces;
            }            
         
        }
        return false;
    }
    public bool ContainsNearbyDuplicate2(int[] nums, int k)
    {
        for (int i = 0; i < nums.Length; i++)
        {
            for (int j = i + 1; j < nums.Length; j++) 
            {
                if (nums[i] == nums[j] && j - i <= k)
                    return true;
            }
        }
        return false;
    }
    public void Test()
    {
        int[] nums = { 1, 2, 3, 1, 2, 3};
        bool retVal = ContainsNearbyDuplicate(nums, 2);
    }
}
/*
 219. Contains Duplicate II
Easy

Given an integer array nums and an integer k, return true if there are two distinct indices i and j in the array such that nums[i] == nums[j] and abs(i - j) <= k.

 

Example 1:

Input: nums = [1,2,3,1], k = 3
Output: true

Example 2:

Input: nums = [1,0,1,1], k = 1
Output: true

Example 3:

Input: nums = [1,2,3,1,2,3], k = 2
Output: false

 

Constraints:

    1 <= nums.length <= 105
    -109 <= nums[i] <= 109
    0 <= k <= 105


 */