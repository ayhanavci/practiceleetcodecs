﻿using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace PracticeLeetCodeCS.Easy;

internal class ConstructStringfromBinaryTree606
{

    //Definition for a binary tree node.
    public class TreeNode
    {
        public int val;
        public TreeNode left;
        public TreeNode right;
        public TreeNode(int val = 0, TreeNode left = null, TreeNode right = null)
        {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }

    public string Tree2str(TreeNode root)
    {
        if (root == null) return "";
        StringBuilder stringBuilder = new StringBuilder();
        
        stringBuilder.Append(root.val.ToString());

        string leftString = Tree2str(root.left);
        if (leftString.Length != 0)
        {
            stringBuilder.Append("(");
            stringBuilder.Append(leftString);
            stringBuilder.Append(")");
        }
        string rightString = Tree2str(root.right);
        if (rightString.Length != 0)
        {
            if (leftString.Length == 0)            
                stringBuilder.Append("()");
            
            stringBuilder.Append("(");
            stringBuilder.Append(rightString);
            stringBuilder.Append(")");
        }

        return stringBuilder.ToString();
    }
    public void Test()
    {
        /*TreeNode root = new TreeNode(1,
            new TreeNode(2, new TreeNode(4), null),
            new TreeNode(3));*/

        TreeNode root = new TreeNode(1,
            new TreeNode(2, null, new TreeNode(4)),
            new TreeNode(3));

        var retVal = Tree2str(root);
    }
}

/*
 606. Construct String from Binary Tree
Easy
2.4K
2.9K
Companies

Given the root of a binary tree, construct a string consisting of parenthesis and integers from a binary tree with the preorder traversal way, and return it.

Omit all the empty parenthesis pairs that do not affect the one-to-one mapping relationship between the string and the original binary tree.

 

Example 1:

Input: root = [1,2,3,4]
Output: "1(2(4))(3)"
Explanation: Originally, it needs to be "1(2(4)())(3()())", but you need to omit all the unnecessary empty parenthesis pairs. And it will be "1(2(4))(3)"

Example 2:

Input: root = [1,2,3,null,4]
Output: "1(2()(4))(3)"
Explanation: Almost the same as the first example, except we cannot omit the first parenthesis pair to break the one-to-one mapping relationship between the input and the output.

 

Constraints:

    The number of nodes in the tree is in the range [1, 104].
    -1000 <= Node.val <= 1000


 */