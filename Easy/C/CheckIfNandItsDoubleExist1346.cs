﻿namespace PracticeLeetCodeCS.Easy;

internal class CheckIfNandItsDoubleExist1346
{
    //TODO Optimize
    public bool CheckIfExist(int[] arr)
    {
        Array.Sort(arr);
       for (int i = 0; i < arr.Length; ++i)
        {
            for (int j = 0; j < arr.Length; ++j)
                if (i != j && arr[j] == 2 * arr[i])
                    return true;
        }
        /*int h = 0;
        for (int i = 0; i < arr.Length; i++)
        {
            if (arr[i] == -506)
            {
                h = 1;
            }

            if (arr[i] > 0)
            {
                for (int j = i + 1; j < arr.Length; j++)
                {
                    if (arr[j] == 2 * arr[i])
                        return true;
                    if (arr[j] > 2 * arr[i])
                        break;
                }
            }
            else
            {
                for (int j = i - 1; j >= 0; --j)
                {
                    if (arr[j] == 2 * arr[i])
                        return true;
                    if (arr[j] > 2 * arr[i])
                        break;
                }
            }
            
        }*/
        return false;
    }
    public void Test()
    {
        //int[] arr = { -10, 12, -20, -8, 15 };
        //int[] arr = { -766, 259, 203, 601, 896, -226, -844, 168, 126, -542, 159, -833, 950, -454, -253, 824, -395, 155, 94, 894, -766, -63, 836, -433, -780, 611, -907, 695, -395, -975, 256, 373, -971, -813, -154, -765, 691, 812, 617, -919, -616, -510, 608, 201, -138, -669, -764, -77, -658, 394, -506, -675, 52 };
        int[] arr = { -2, 0, 10, -19, 4, 6, -8 };
        var retVal = CheckIfExist(arr);
    }
}

/*
 1346. Check If N and Its Double Exist
Easy
1.4K
159
Companies

Given an array arr of integers, check if there exist two indices i and j such that :

    i != j
    0 <= i, j < arr.length
    arr[i] == 2 * arr[j]

 

Example 1:

Input: arr = [10,2,5,3]
Output: true
Explanation: For i = 0 and j = 2, arr[i] == 10 == 2 * 5 == 2 * arr[j]

Example 2:

Input: arr = [3,1,7,11]
Output: false
Explanation: There is no i and j that satisfy the conditions.

 

Constraints:

    2 <= arr.length <= 500
    -103 <= arr[i] <= 103


 */