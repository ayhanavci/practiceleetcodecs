﻿namespace PracticeLeetCodeCS.Easy;

internal class CounttheNumberofConsistentStrings1684
{
    public int CountConsistentStrings(string allowed, string[] words)
    {
        int count = 0;

        foreach (string word in words)
        {            
            for (int i = 0; i < word.Length; i++)
            {
                if (allowed.IndexOf(word[i]) == -1)
                    break;
                if (i == word.Length - 1)
                    count++;
            }            
        }

        return count;
    }
    public void Test()
    {
        string[] words1 = { "ad", "bd", "aaab", "baa", "badab" };
        var retval1 = CountConsistentStrings("ab", words1);

        string[] words2 = { "a", "b", "c", "ab", "ac", "bc", "abc" };
        var retval2 = CountConsistentStrings("abc", words2);

        string[] words3 = { "cc", "acd", "b", "ba", "bac", "bad", "ac", "d" };
        var retval3 = CountConsistentStrings("cad", words3);
    }
}

/*
 1684. Count the Number of Consistent Strings
Easy
1.3K
55
Companies

You are given a string allowed consisting of distinct characters and an array of strings words. A string is consistent if all characters in the string appear in the string allowed.

Return the number of consistent strings in the array words.

 

Example 1:

Input: allowed = "ab", words = ["ad","bd","aaab","baa","badab"]
Output: 2
Explanation: Strings "aaab" and "baa" are consistent since they only contain characters 'a' and 'b'.

Example 2:

Input: allowed = "abc", words = ["a","b","c","ab","ac","bc","abc"]
Output: 7
Explanation: All strings are consistent.

Example 3:

Input: allowed = "cad", words = ["cc","acd","b","ba","bac","bad","ac","d"]
Output: 4
Explanation: Strings "cc", "acd", "ac", and "d" are consistent.

 

Constraints:

    1 <= words.length <= 104
    1 <= allowed.length <= 26
    1 <= words[i].length <= 10
    The characters in allowed are distinct.
    words[i] and allowed contain only lowercase English letters.


 */