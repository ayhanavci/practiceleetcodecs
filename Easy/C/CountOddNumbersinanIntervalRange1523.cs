﻿namespace PracticeLeetCodeCS.Easy;

internal class CountOddNumbersinanIntervalRange1523
{
    public int CountOdds(int low, int high)
    {        
        if (high % 2 != 0) high++;
        if (low % 2 != 0) low--;
               
        return (high - low) / 2;
    }
    public void Test()
    {
        var retVal = CountOdds(3, 7);
        var retVal2 = CountOdds(8, 10);
        var retVal3 = CountOdds(5, 18);
        var retVal4 = CountOdds(8, 19);
    }
}

/*
 1523. Count Odd Numbers in an Interval Range
Easy
1.3K
91
Companies

Given two non-negative integers low and high. Return the count of odd numbers between low and high (inclusive).

 

Example 1:

Input: low = 3, high = 7
Output: 3
Explanation: The odd numbers between 3 and 7 are [3,5,7].

Example 2:

Input: low = 8, high = 10
Output: 1
Explanation: The odd numbers between 8 and 10 are [9].

 

Constraints:

    0 <= low <= high <= 10^9

 */