namespace PracticeLeetCodeCS.Easy;

internal class CountDaysSpentTogether2409
{
    //TODO: Shit question. copy pasting solution
    static int[] dayInMonth = 
        new int [] { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

    public int CountDaysTogether(
        string arriveAlice, string leaveAlice, string arriveBob, string leaveBob) 
    {
        var start = Math.Max(Days(arriveAlice), Days(arriveBob));
        var end = Math.Min(Days(leaveAlice), Days(leaveBob));

        return Math.Max(0, end-start+1);
    }

    int Days(ReadOnlySpan<char> mmddStr) // "mm-dd" format
    {
        var days = int.Parse(mmddStr.Slice(3, 2));

        var month = int.Parse(mmddStr.Slice(0, 2))-1;

        while(month > 0)
            days += dayInMonth[--month];

        return days;
    }
    public int CountDaysTogetherEx(string arriveAlice, string leaveAlice, string arriveBob, string leaveBob) 
    {
        int[] monthDays = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        
        int arriveDayAlice = (arriveAlice[3] - '0' * 10 + arriveAlice[4] * '0');
        int arriveMonthAlice = (arriveAlice[0] - '0' * 10 + arriveAlice[1] * '0');

        int leaveDayAlice = (leaveAlice[3] - '0' * 10 + leaveAlice[4] * '0');
        int leaveMonthAlice = (leaveAlice[0] - '0' * 10 + leaveAlice[1] * '0');

        int arriveDayBob = (arriveBob[3] - '0' * 10 + arriveBob[4] * '0');
        int arriveMonthBob = (arriveBob[0] - '0' * 10 + arriveBob[1] * '0');

        int leaveDayBob = (leaveBob[3] - '0' * 10 + leaveBob[4] * '0');
        int leaveMonthBob = (leaveBob[0] - '0' * 10 + leaveBob[1] * '0');

        if (arriveMonthAlice > arriveMonthBob)
        {
            if (leaveMonthAlice > leaveMonthBob)
            {
                //ab..aa..lb..la
            }
            else if (leaveMonthAlice == leaveMonthBob)
            {
                //ab..aa..L                

            }
            else
            {
                //ab..aa..la..lb
            }
        }
        else if (arriveMonthAlice == arriveMonthBob)
        {
            if (leaveMonthAlice > leaveMonthBob)
            {
                //A..lb..la
            }
            else if (leaveMonthAlice == leaveMonthBob)
            {
                //A..L
            }
            else
            {
                //A..la..lb
            }
        }
        else //arriveMonthBob > arriveMonthAlice
        {
            if (leaveMonthAlice > leaveMonthBob)
            {
                //aa..ab..lb..la
            }
            else if (leaveMonthAlice == leaveMonthBob)
            {
                //aa..ab..l
            }
            else
            {
                //aa..ab..la..lb
            }
        }
        return 0;
    }
    public void Test()
    {

    }
}
/*
2409. Count Days Spent Together
Easy
205
557
Companies

Alice and Bob are traveling to Rome for separate business meetings.

You are given 4 strings arriveAlice, leaveAlice, arriveBob, and leaveBob. Alice will be in the city from the dates arriveAlice to leaveAlice (inclusive), while Bob will be in the city from the dates arriveBob to leaveBob (inclusive). Each will be a 5-character string in the format "MM-DD", corresponding to the month and day of the date.

Return the total number of days that Alice and Bob are in Rome together.

You can assume that all dates occur in the same calendar year, which is not a leap year. Note that the number of days per month can be represented as: [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31].

 

Example 1:

Input: arriveAlice = "08-15", leaveAlice = "08-18", arriveBob = "08-16", leaveBob = "08-19"
Output: 3
Explanation: Alice will be in Rome from August 15 to August 18. Bob will be in Rome from August 16 to August 19. They are both in Rome together on August 16th, 17th, and 18th, so the answer is 3.

Example 2:

Input: arriveAlice = "10-01", leaveAlice = "10-31", arriveBob = "11-01", leaveBob = "12-31"
Output: 0
Explanation: There is no day when Alice and Bob are in Rome together, so we return 0.

 

Constraints:

    All dates are provided in the format "MM-DD".
    Alice and Bob's arrival dates are earlier than or equal to their leaving dates.
    The given dates are valid dates of a non-leap year.


*/