namespace PracticeLeetCodeCS.Easy;

internal class CountingWordsWithaGivenPrefix2185
{
    public int PrefixCount(string[] words, string pref) 
    {
        int count = 0;

        foreach (var word in words)
        {
            if (word.Length >= pref.Length)
            {
                int i = 0;
                for (; i < pref.Length; ++i) if (word[i] != pref[i]) break;
                if (i == pref.Length) count++;
            }
        }

        return count;        
    }
    public void Test()
    {

    }
}
/*
2185. Counting Words With a Given Prefix
Easy
548
14
Companies

You are given an array of strings words and a string pref.

Return the number of strings in words that contain pref as a prefix.

A prefix of a string s is any leading contiguous substring of s.

 

Example 1:

Input: words = ["pay","attention","practice","attend"], pref = "at"
Output: 2
Explanation: The 2 strings that contain "at" as a prefix are: "attention" and "attend".

Example 2:

Input: words = ["leetcode","win","loops","success"], pref = "code"
Output: 0
Explanation: There are no strings that contain "code" as a prefix.

 

Constraints:

    1 <= words.length <= 100
    1 <= words[i].length, pref.length <= 100
    words[i] and pref consist of lowercase English letters.


*/