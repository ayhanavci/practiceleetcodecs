using System.Linq;
using System.Text;
namespace PracticeLeetCodeCS.Easy;

internal class CheckiftheSentenceIsPangram1832
{
    public bool CheckIfPangram2(string sentence) 
    {
        StringBuilder alphabet = new StringBuilder("abcdefghijklmnopqrstuvwxyz");

        for (int i = 0; i < sentence.Length; ++i) 
        {
            int index = alphabet.ToString().IndexOf(sentence[i]);
            if (index != -1)             
                alphabet.Remove(index, 1);
            if (alphabet.Length == 0)
                return true;                
            
        }
        return false;
    }
    public bool CheckIfPangram(string sentence) 
    {
        HashSet<char> uniqueChars = new HashSet<char>();

        for (int i = 0; i < sentence.Length; ++i) 
        {
            uniqueChars.Add(sentence[i]);
            if (uniqueChars.Count == 26)
                return true;
        }
        return false;
    }
    public void Test()
    {
        var retVal = CheckIfPangram("tewwuuhequhickbgrowneewqfoxjumpsrovtttvdereryheluadwszyhjzcxvsssddog");
    }
}
/*
1832. Check if the Sentence Is Pangram
Easy
2.2K
49
Companies

A pangram is a sentence where every letter of the English alphabet appears at least once.

Given a string sentence containing only lowercase English letters, return true if sentence is a pangram, or false otherwise.

 

Example 1:

Input: sentence = "thequickbrownfoxjumpsoverthelazydog"
Output: true
Explanation: sentence contains at least one of every letter of the English alphabet.

Example 2:

Input: sentence = "leetcode"
Output: false

 

Constraints:

    1 <= sentence.length <= 1000
    sentence consists of lowercase English letters.


*/