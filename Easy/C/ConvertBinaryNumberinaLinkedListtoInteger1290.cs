﻿using System.Text;

namespace PracticeLeetCodeCS.Easy;

internal class ConvertBinaryNumberinaLinkedListtoInteger1290
{

    //Definition for singly-linked list.
    public class ListNode
    {
        public int val;
        public ListNode next;
        public ListNode(int val = 0, ListNode next = null)
        {
            this.val = val;
            this.next = next;
        }
    }
    public int GetDecimalValue(ListNode head)
    {                
        List<int> numbers = new List<int>();
        while (head != null)
        {
            numbers.Insert(0, head.val);
            head = head.next;
        }

        int retVal = 0;
        for (int i = 0; i < numbers.Count; i++)                 
            retVal += ((int)Math.Pow(2, i)) * numbers[i];
        
        return retVal;
    }
    public void Test()
    {
        ListNode head = new ListNode(1);
        head.next = new ListNode(0);
        head.next.next = new ListNode(1);

        var retVal = GetDecimalValue(head);
    }
}

/*
 1290. Convert Binary Number in a Linked List to Integer
Easy
3.4K
145
Companies

Given head which is a reference node to a singly-linked list. The value of each node in the linked list is either 0 or 1. The linked list holds the binary representation of a number.

Return the decimal value of the number in the linked list.

The most significant bit is at the head of the linked list.

 

Example 1:

Input: head = [1,0,1]
Output: 5
Explanation: (101) in base 2 = (5) in base 10

Example 2:

Input: head = [0]
Output: 0

 

Constraints:

    The Linked List is not empty.
    Number of nodes will not exceed 30.
    Each node's value is either 0 or 1.


 */