﻿namespace PracticeLeetCodeCS.Easy;

internal class CountLargestGroup1399
{
    public int CountLargestGroup(int n)
    {
        Dictionary<int, int> group = new Dictionary<int, int>();
        for (int i = 1; i <= n; i++)
        {
            int number = i;
            int sum = 0;
            while (number > 0)
            {
                sum += number % 10;
                number /= 10;
            }
            if (group.ContainsKey(sum)) group[sum]++;
            else group[sum] = 1;
        }

        int largestGroupSize = 0;
        int largestGroupCount = 0;
        for (int i = 0; i < group.Count; i++)
        {
            int groupSize = group.ElementAt(i).Value;
            if (groupSize > largestGroupSize)
            {
                largestGroupSize = groupSize;
                largestGroupCount = 1;
            }           
                
            
            else if (groupSize == largestGroupSize)            
                largestGroupCount++;
            
        }

        return largestGroupCount;
    }
    public void Test()
    {
        var retVal = CountLargestGroup(13);
    }
}

/*
 1399. Count Largest Group
Easy
344
777
Companies

You are given an integer n.

Each number from 1 to n is grouped according to the sum of its digits.

Return the number of groups that have the largest size.

 

Example 1:

Input: n = 13
Output: 4
Explanation: There are 9 groups in total, they are grouped according sum of its digits of numbers from 1 to 13:
[1,10], [2,11], [3,12], [4,13], [5], [6], [7], [8], [9].
There are 4 groups with largest size.

Example 2:

Input: n = 2
Output: 2
Explanation: There are 2 groups [1], [2] of size 1.

 

Constraints:

    1 <= n <= 104


 */