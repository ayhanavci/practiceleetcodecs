namespace PracticeLeetCodeCS.Easy;

internal class CountSquareSumTriples1925
{
    public int CountTriples(int n) 
    {
        int count = 0;
        for (int a = 1; a <= n; ++a)
        { 
            for (int b = 2; b <= n; ++b)
            {
                int squared = (a*a) + (b*b);
                double number = Math.Sqrt(squared);
                if (number > n)
                    break;
                if (number % 1 == 0)
                    count++;
            }
        }
        return count;
    }
    public void Test()
    {
        var retVal1 = CountTriples(5);
        var retVal2 = CountTriples(10);
    }
}

/*
1925. Count Square Sum Triples
Easy
325
28
Companies

A square triple (a,b,c) is a triple where a, b, and c are integers and a2 + b2 = c2.

Given an integer n, return the number of square triples such that 1 <= a, b, c <= n.

 

Example 1:

Input: n = 5
Output: 2
Explanation: The square triples are (3,4,5) and (4,3,5).

Example 2:

Input: n = 10
Output: 4
Explanation: The square triples are (3,4,5), (4,3,5), (6,8,10), and (8,6,10).

 

Constraints:

    1 <= n <= 250


*/