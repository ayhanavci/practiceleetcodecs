﻿namespace PracticeLeetCodeCS.Easy;

internal class ConvertSortedArraytoBinarySearchTree108
{
    //Definition for a binary tree node.
    public class TreeNode
    {
        public int val;
        public TreeNode left;
        public TreeNode right;
        public TreeNode(int val = 0, TreeNode left = null, TreeNode right = null)
        {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }
    public TreeNode SortedArrayToBST(int[] nums)
    {
        if (nums.Length == 0)
            return null;

        int mid = nums.Length / 2;        
        TreeNode node = new TreeNode(nums[mid]);       

        int[] leftArray = new int[mid];
        for (int i = 0; i < mid; i++)
            leftArray[i] = nums[i];

        node.left = SortedArrayToBST(leftArray);        
        
        int[] rightArray = new int[nums.Length - mid - 1];
        int rightLength = nums.Length - mid - 1;
        int rightStartPos = mid + 1;

        Array.Copy(nums, rightStartPos, rightArray, 0, rightLength);
        node.right = SortedArrayToBST(rightArray);

      
        return node;
    }
    public void Test()
    {
        //int[] nums = { -10, -3, 0, 5, 9 };
        int[] nums = { 1, 3 };
        //int[] nums = { 13, 21, 29, 35, 48, 79 };
        TreeNode root = SortedArrayToBST(nums);
    }
}
/*
 108. Convert Sorted Array to Binary Search Tree
Easy

Given an integer array nums where the elements are sorted in ascending order, convert it to a height-balanced binary search tree.

 

Example 1:

Input: nums = [-10,-3,0,5,9]
Output: [0,-3,9,-10,null,5]
Explanation: [0,-10,5,null,-3,null,9] is also accepted:

Example 2:

Input: nums = [1,3]
Output: [3,1]
Explanation: [1,null,3] and [3,1] are both height-balanced BSTs.

 

Constraints:

    1 <= nums.length <= 104
    -104 <= nums[i] <= 104
    nums is sorted in a strictly increasing order.


 */