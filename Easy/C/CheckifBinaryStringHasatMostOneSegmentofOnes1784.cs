﻿namespace PracticeLeetCodeCS.Easy;
internal class CheckifBinaryStringHasatMostOneSegmentofOnes1784
{
    //TODO shit wording
    //Given a binary string s ​​​​​without leading zeros, return true​​​ if s contains ONE GROUP OF of ones.
    public bool CheckOnesSegment(string s)
    {
        return !s.Contains("01");
    }
    public void Test()
    {
        var retVal = CheckOnesSegment("1001");
    }
}
//Given a binary string s ​​​​​without leading zeros, return true​​​ if s contains ONE GROUP OF of ones.
/*
 1784. Check if Binary String Has at Most One Segment of Ones
Easy
257
782
Companies

Given a binary string s ​​​​​without leading zeros, return true​​​ if s contains at most one contiguous segment of ones. Otherwise, return false.

 

Example 1:

Input: s = "1001"
Output: false
Explanation: The ones do not form a contiguous segment.

Example 2:

Input: s = "110"
Output: true

 

Constraints:

    1 <= s.length <= 100
    s[i]​​​​ is either '0' or '1'.
    s[0] is '1'.


 */