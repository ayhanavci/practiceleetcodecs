namespace PracticeLeetCodeCS.Easy;

internal class CellsinaRangeonanExcelSheet2194
{
    public IList<string> CellsInRange(string s) 
    {
        int col1 = s[0] - 'A' + 1;
        int col2 = s[3] - 'A' + 1;    
        int row1 = s[1] - '0';
        int row2 = s[4] - '0';

        int colSize = col2 - col1 + 1;
        int rowSize = row2 - row1 + 1;

        IList<string> retVal = new List<string>();
        for (int i = 0; i < colSize; ++i)
        {
            char colName = Convert.ToChar(s[0] + i);
            for (int j = 0; j < rowSize; ++j)
            {
                char rowName = Convert.ToChar(s[1] + j);
                string cell = String.Format("{0}{1}", colName, rowName);
                retVal.Add(cell);
            }
        }
        return retVal;
    }
    public void Test()
    {
       var retVal = CellsInRange("U7:X9");
    }
}
/*
s =
"U7:X9"
Use Testcase
Output
["U1","U2","U3","V1","V2","V3","W1","W2","W3","X1","X2","X3"]
Expected
["U7","U8","U9","V7","V8","V9","W7","W8","W9","X7","X8","X9"]
*/
/*
2194. Cells in a Range on an Excel Sheet
Easy
463
76
Companies

A cell (r, c) of an excel sheet is represented as a string "<col><row>" where:

    <col> denotes the column number c of the cell. It is represented by alphabetical letters.
        For example, the 1st column is denoted by 'A', the 2nd by 'B', the 3rd by 'C', and so on.
    <row> is the row number r of the cell. The rth row is represented by the integer r.

You are given a string s in the format "<col1><row1>:<col2><row2>", where <col1> represents the column c1, <row1> represents the row r1, <col2> represents the column c2, and <row2> represents the row r2, such that r1 <= r2 and c1 <= c2.

Return the list of cells (x, y) such that r1 <= x <= r2 and c1 <= y <= c2. The cells should be represented as strings in the format mentioned above and be sorted in non-decreasing order first by columns and then by rows.

 

Example 1:

Input: s = "K1:L2"
Output: ["K1","K2","L1","L2"]
Explanation:
The above diagram shows the cells which should be present in the list.
The red arrows denote the order in which the cells should be presented.

Example 2:

Input: s = "A1:F1"
Output: ["A1","B1","C1","D1","E1","F1"]
Explanation:
The above diagram shows the cells which should be present in the list.
The red arrow denotes the order in which the cells should be presented.

 

Constraints:

    s.length == 5
    'A' <= s[0] <= s[3] <= 'Z'
    '1' <= s[1] <= s[4] <= '9'
    s consists of uppercase English letters, digits and ':'.


*/