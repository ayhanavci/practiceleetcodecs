﻿namespace PracticeLeetCodeCS.Easy;

internal class XofaKindinaDeckofCards914
{
    public bool HasGroupsSizeX(int[] deck)
    {
        Dictionary<int, int> partitions = new Dictionary<int, int>();
        
        for (int i = 0; i < deck.Length; i++)
        {
            if (partitions.ContainsKey(deck[i]))
                partitions[deck[i]]++;
            else
                partitions[deck[i]] = 1;
            
        }

        int greatestCommonDivisor = partitions.ElementAt(0).Value;
        for (int i = 1; i < partitions.Count; i++)        
            greatestCommonDivisor = gcd(greatestCommonDivisor, partitions.ElementAt(i).Value);
                        
        return greatestCommonDivisor >= 2;
    }
    public int gcd(int x, int y)
    {
        int remainder;
        while (y != 0)
        {
            remainder = x % y;
            x = y;
            y = remainder;
        }
        return x;
    }
    public void Test()
    {
        //int[] deck = { 1, 2, 3, 4, 4, 3, 2, 1 };
        //var retVal = HasGroupsSizeX(deck);

        int[] deck2 = { 1, 1, 1, 2, 2, 2, 3, 3 };
        var retVal2 = HasGroupsSizeX(deck2);

        int[] deck3 = { 1, 1, 2, 2, 2, 2 };
        var retval3 = HasGroupsSizeX(deck3);

        int[] deck4 = { 1, 1, 1, 1, 2, 2, 2, 2, 2, 2 };
        var retval4 = HasGroupsSizeX(deck3);
        
    }
}

/*
 914. X of a Kind in a Deck of Cards
Easy
1.5K
371
Companies

You are given an integer array deck where deck[i] represents the number written on the ith card.

Partition the cards into one or more groups such that:

    Each group has exactly x cards where x > 1, and
    All the cards in one group have the same integer written on them.

Return true if such partition is possible, or false otherwise.

 

Example 1:

Input: deck = [1,2,3,4,4,3,2,1]
Output: true
Explanation: Possible partition [1,1],[2,2],[3,3],[4,4].

Example 2:

Input: deck = [1,1,1,2,2,2,3,3]
Output: false
Explanation: No possible partition.

 

Constraints:

    1 <= deck.length <= 104
    0 <= deck[i] < 104


 */