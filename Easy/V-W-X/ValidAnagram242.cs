﻿using System.Text;

namespace PracticeLeetCodeCS.Easy;

internal class ValidAnagram242
{
    public bool IsAnagram(string s, string t)
    {
        if (s.Length != t.Length) return false;
        StringBuilder controlString = new StringBuilder(t);

        bool fFound = false;
        for (int i = 0; i < s.Length; i++)
        {
            for (int j = 0; j < controlString.Length; j++)
            {
                if (s[i] == controlString[j])
                {
                    controlString.Remove(j, 1);
                    fFound= true;
                    break;
                }
            }
            if (!fFound)            
                return false;            
        }
        return controlString.Length == 0;

    }
    public void Test()
    {
        bool retVal = IsAnagram("ab", "a");
        retVal = IsAnagram("rat", "car");
    }
}

/*
 242. Valid Anagram
Easy

Given two strings s and t, return true if t is an anagram of s, and false otherwise.

An Anagram is a word or phrase formed by rearranging the letters of a different word or phrase, typically using all the original letters exactly once.

 

Example 1:

Input: s = "anagram", t = "nagaram"
Output: true

Example 2:

Input: s = "rat", t = "car"
Output: false

 

Constraints:

    1 <= s.length, t.length <= 5 * 104
    s and t consist of lowercase English letters.

 

Follow up: What if the inputs contain Unicode characters? How would you adapt your solution to such a case?

 */