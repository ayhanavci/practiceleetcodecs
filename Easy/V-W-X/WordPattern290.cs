﻿namespace PracticeLeetCodeCS.Easy;

internal class WordPattern290
{
    public bool WordPattern(string pattern, string s)
    {
        string [] wordList = s.Split(" ");
        if (wordList.Length != pattern.Length)        
            return false;

        Dictionary<char, string> patternToValuePairs = new Dictionary<char, string> ();        

        for (int i = 0; i < wordList.Length; ++i)
        {            
            string? word;
            if (patternToValuePairs.TryGetValue(pattern[i], out word))
            {
                if (!word.Equals(wordList[i]))
                    return false;
                
            }
            else if (patternToValuePairs.ContainsValue(wordList[i]))
            {
                return false;
            }
            else 
                patternToValuePairs.Add(pattern[i], wordList[i]);
        }

        return true;
    }
    public void Test()
    {
        string pattern1 = "abba";
        string s1 = "dog cat cat dog";
        WordPattern(pattern1, s1);
    }
}
/*
 290. Word Pattern
Easy

Given a pattern and a string s, find if s follows the same pattern.

Here follow means a full match, such that there is a bijection between a letter in pattern and a non-empty word in s.

 

Example 1:

Input: pattern = "abba", s = "dog cat cat dog"
Output: true

Example 2:

Input: pattern = "abba", s = "dog cat cat fish"
Output: false

Example 3:

Input: pattern = "aaaa", s = "dog cat cat dog"
Output: false

 

Constraints:

    1 <= pattern.length <= 300
    pattern contains only lower-case English letters.
    1 <= s.length <= 3000
    s contains only lowercase English letters and spaces " ".
    s does not contain any leading or trailing spaces.
    All the words in s are separated by a single space.


 */