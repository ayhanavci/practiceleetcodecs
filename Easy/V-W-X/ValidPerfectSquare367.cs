﻿namespace PracticeLeetCodeCS.Easy;

internal class ValidPerfectSquare367
{
    public bool IsPerfectSquare(int num)
    {
        if (num == 0 || num == 2) return false;
        if (num == 1) return true;

        long right = num / 2;
        long left = 2;
        long testNumber = (right + left) / 2;
        while (right > left + 1)
        {
            long result = testNumber * testNumber;
            if (num == result)
                return true;
            else if (num > result)            
                left = testNumber;            
            else            
                right = testNumber;
            
            testNumber = (right + left) / 2;
        }
        if (num == right * right)
            return true;
        if (num == left * left)
            return true;
        return false;
    }
  
    public void Test()
    {
        bool retVal = IsPerfectSquare(4);
        retVal = IsPerfectSquare(16);
    }
}

/*
 367. Valid Perfect Square
Easy

Given a positive integer num, write a function which returns True if num is a perfect square else False.

Follow up: Do not use any built-in library function such as sqrt.

 

Example 1:

Input: num = 16
Output: true

Example 2:

Input: num = 14
Output: false

 

Constraints:

    1 <= num <= 2^31 - 1


 */