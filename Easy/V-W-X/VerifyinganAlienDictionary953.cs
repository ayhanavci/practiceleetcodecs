﻿namespace PracticeLeetCodeCS.Easy;

internal class VerifyinganAlienDictionary953
{
    public bool IsAlienSorted(string[] words, string order)
    {
        for (int i = 0; i < words.Length - 1; i++)
        {                        
            int n = words[i].Length > words[i+1].Length ? words[i+1].Length : words[i].Length;

            int j = 0;            
            for (; j < n; ++j)
            {
                if (order.IndexOf(words[i][j]) < order.IndexOf(words[i + 1][j]))
                    break;
                else if (order.IndexOf(words[i][j]) > order.IndexOf(words[i + 1][j]))
                    return false;
             
            }
            if (j == n && words[i + 1].Length < words[i].Length)
                return false;
        }

        return true;
    }
    public void Test()
    {
        string[] words1 = { "hello", "leetcode" };
        string order1 = "hlabcdefgijkmnopqrstuvwxyz";
        var retVal1 = IsAlienSorted(words1, order1);

        string[] words2 = { "word", "world", "row" };
        string order2 = "worldabcefghijkmnpqstuvxyz";
        var retVal2 = IsAlienSorted(words2, order2);

        string[] words3 = { "apple", "app" };
        string order3 = "abcdefghijklmnopqrstuvwxyz";
        var retVal3 = IsAlienSorted(words3, order3);

    }
}

/*
 953. Verifying an Alien Dictionary
Easy
3.3K
1.1K
Companies

In an alien language, surprisingly, they also use English lowercase letters, but possibly in a different order. The order of the alphabet is some permutation of lowercase letters.

Given a sequence of words written in the alien language, and the order of the alphabet, return true if and only if the given words are sorted lexicographically in this alien language.

 

Example 1:

Input: words = ["hello","leetcode"], order = "hlabcdefgijkmnopqrstuvwxyz"
Output: true
Explanation: As "h" comes before "l" in this language, then the sequence is sorted.

Example 2:

Input: words = ["word","world","row"], order = "worldabcefghijkmnpqstuvxyz"
Output: false
Explanation: As "d" comes after "l" in this language, then words[0] > words[1], hence the sequence is unsorted.

Example 3:

Input: words = ["apple","app"], order = "abcdefghijklmnopqrstuvwxyz"
Output: false
Explanation: The first three characters "app" match, and the second string is shorter (in size.) According to lexicographical rules "apple" > "app", because "l" > "∅", where "∅" is defined as the blank character which is less than any other character (More info).

 

Constraints:

    1 <= words.length <= 100
    1 <= words[i].length <= 20
    order.length == 26
    All characters in words[i] and order are English lowercase letters.


 */