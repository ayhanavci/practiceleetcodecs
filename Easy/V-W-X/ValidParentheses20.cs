﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticeLeetCodeCS.Easy;

public class ValidParentheses20
{
    private bool IsValid(string s)
    {
        Stack<char> stack = new Stack<char>();

        for (int i = 0; i < s.Length; i++)
        {
            if (s[i] == '(' || s[i] == '{' || s[i] == '[')
                stack.Push(s[i]);
            else if (stack.Count == 0)
                return false;
            else
            {
                char c = stack.Pop();
                if (c == '(' && s[i] == ')' ||
                    c == '[' && s[i] == ']' ||
                    c == '{' && s[i] == '}')
                    continue;
                else
                    return false;
            }
            
        }

        return stack.Count == 0;
    }

    public void Test()
    {
        //string s = "([)]";
        string s = "]";
        IsValid(s);
    }
}
/*
 20. Valid Parentheses
Easy

Given a string s containing just the characters "(", ")", "{", "}", "[" and "]", determine if the input string is valid.

An input string is valid if:

    Open brackets must be closed by the same type of brackets.
    Open brackets must be closed in the correct order.
    Every close bracket has a corresponding open bracket of the same type.

 

Example 1:

Input: s = "()"
Output: true

Example 2:

Input: s = "()[]{}"
Output: true

Example 3:

Input: s = "(]"
Output: false

 

Constraints:

    1 <= s.length <= 104
    s consists of parentheses only "()[]{}".


 
 */