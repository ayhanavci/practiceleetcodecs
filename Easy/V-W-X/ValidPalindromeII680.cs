﻿using System.Text;

namespace PracticeLeetCodeCS.Easy;

internal class ValidPalindromeII680
{
    public bool IsPalindrome(string s)
    {        
        for (int j = 0; j < s.Length; ++j)
        {
            if (s[j] != s[s.Length - j - 1])
                return false;
        }
        return true;
    }
    public bool ValidPalindrome(string s)
    {        
        for (int i = 0; i < s.Length / 2; i++)
        {
            if (s[i] != s[s.Length - i - 1])
            {
                if (!IsPalindrome(s.Substring(i, s.Length - i * 2 - 1)) && !IsPalindrome(s.Substring(i + 1, s.Length - i * 2 - 1)))
                    return false;
                
                return true;
            }
        }
        return true;
    }
    public void Test()
    {
        //string s = "abckdeedocba";
        //string s = "abca";        
        string s = "cbbcc";
        var retVal = ValidPalindrome(s);

    }
}

/*
 680. Valid Palindrome II
Easy
6.6K
341
Companies

Given a string s, return true if the s can be palindrome after deleting at most one character from it.

 

Example 1:

Input: s = "aba"
Output: true

Example 2:

Input: s = "abca"
Output: true
Explanation: You could delete the character "c".

Example 3:

Input: s = "abc"
Output: false

 

Constraints:

    1 <= s.length <= 105
    s consists of lowercase English letters.


 */