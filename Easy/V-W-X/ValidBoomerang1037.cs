﻿namespace PracticeLeetCodeCS.Easy;

internal class ValidBoomerang1037
{
    public bool IsBoomerang1(int[][] points)
    {
        return (points[0][0] - points[1][0]) * (points[0][1] - points[2][1]) !=
               (points[0][0] - points[2][0]) * (points[0][1] - points[1][1]);
    }
    public bool IsBoomerang(int[][] points)
    {
        return points[0][0] * (points[1][1] - points[2][1]) 
            +  points[1][0] * (points[2][1] - points[0][1]) 
            +  points[2][0] * (points[0][1] - points[1][1]) != 0;
    }
    public void Test()
    {
        int[][] points = new int[3][];
        points[0] = new int[] { 1,1 };
        points[1] = new int[] { 2,3 };
        points[2] = new int[] { 3,2 };
        var retVal = IsBoomerang(points);
    }
}
/*
 The other idea is to calculate the slope of AB and AC.
K_AB = (p[0][0] - p[1][0]) / (p[0][1] - p[1][1])
K_AC = (p[0][0] - p[2][0]) / (p[0][1] - p[2][1])

We check if K_AB != K_AC, instead of calculate a fraction.

return (p[0][0] - p[1][0]) * (p[0][1] - p[2][1]) != (p[0][0] - p[2][0]) * (p[0][1] - p[1][1]);
 */

/*
 Intuition

In other words, we need to return true if the triangle area is not zero.

For the detailed explanation, see the comment by EOAndersson below.
Solution

Calculate the area of the triangle: x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2) and compare it to zero.
bool isBoomerang(vector<vector<int>>& p) {
  return p[0][0] * (p[1][1] - p[2][1]) + p[1][0] * (p[2][1] - p[0][1]) + p[2][0] * (p[0][1] - p[1][1]) != 0;
}
 */
/*
 1037. Valid Boomerang
Easy
298
430
Companies

Given an array points where points[i] = [xi, yi] represents a point on the X-Y plane, return true if these points are a boomerang.

A boomerang is a set of three points that are all distinct and not in a straight line.

 

Example 1:

Input: points = [[1,1],[2,3],[3,2]]
Output: true

Example 2:

Input: points = [[1,1],[2,2],[3,3]]
Output: false

 

Constraints:

    points.length == 3
    points[i].length == 2
    0 <= xi, yi <= 100


 */