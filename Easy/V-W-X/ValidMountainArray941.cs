﻿namespace PracticeLeetCodeCS.Easy;

internal class ValidMountainArray941
{
    public bool ValidMountainArray(int[] arr)
    {
        if (arr.Length == 1) return false;
        if (arr[1] <= arr[0]) return false;

        bool decreasing = false;

        for (int i = 1; i < arr.Length; i++)
        {
            if (decreasing)
            {
                if (arr[i] >= arr[i - 1])
                    return false;                                
            }
            else
            {
                if (arr[i] < arr[i - 1])
                    decreasing = true;
                else if (arr[i] == arr[i - 1])
                    return false;
            }
        }
        return decreasing;
    }
    public void Test()
    {
        int[] arr1 = { 2, 1 };
        var retVal1 = ValidMountainArray(arr1);
        int[] arr2 = { 3,5,5 };
        var retVal2 = ValidMountainArray(arr2);
        int[] arr3 = { 0,3,2,1 };
        var retVal3 = ValidMountainArray(arr3);
        int[] arr4 = { 2,0,2 };
        var retVal4 = ValidMountainArray(arr4);
        int[] arr5 = { 0, 1, 2, 4, 2, 1 };
        var retVal5 = ValidMountainArray(arr5);
        
    }
}

/*
 941. Valid Mountain Array
Easy
2.5K
153
Companies

Given an array of integers arr, return true if and only if it is a valid mountain array.

Recall that arr is a mountain array if and only if:

    arr.length >= 3
    There exists some i with 0 < i < arr.length - 1 such that:
        arr[0] < arr[1] < ... < arr[i - 1] < arr[i] 
        arr[i] > arr[i + 1] > ... > arr[arr.length - 1]

 

Example 1:

Input: arr = [2,1]
Output: false

Example 2:

Input: arr = [3,5,5]
Output: false

Example 3:

Input: arr = [0,3,2,1]
Output: true

 

Constraints:

    1 <= arr.length <= 104
    0 <= arr[i] <= 104


 */