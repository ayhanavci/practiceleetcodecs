﻿using System.Text;

namespace PracticeLeetCodeCS.Easy;

internal class UncommonWordsfromTwoSentences884
{
    public string[] UncommonFromSentences(string s1, string s2)
    {
        string[] words = (s1 + " " + s2).Split(" ");        

        Dictionary<string, int> wordCount = new Dictionary<string, int>();
        for (int i = 0; i < words.Length; i++)
        {
            if (wordCount.ContainsKey(words[i]))
                wordCount[words[i]]++;
            else
                wordCount[words[i]] = 1;
        }

        List<string> result = new List<string>();

        for (int i = 0; i < wordCount.Count; i++)
        {
            if (wordCount.ElementAt(i).Value == 1)
                result.Add(wordCount.ElementAt(i).Key);
        }

        return result.ToArray();
        /*StringBuilder allWords = new StringBuilder();
        allWords.Append(s1);
        allWords.Append(" ");
        allWords.Append(s2);

        string[] wordsList = allWords.ToString().Split(" ");*/

    }
    public void Test()
    {
        string s1 = "this apple is sweet";
        string s2 = "this apple is sour";

        var retVal = UncommonFromSentences(s1, s2);
    }
}


/*
 884. Uncommon Words from Two Sentences
Easy
1.1K
151
Companies

A sentence is a string of single-space separated words where each word consists only of lowercase letters.

A word is uncommon if it appears exactly once in one of the sentences, and does not appear in the other sentence.

Given two sentences s1 and s2, return a list of all the uncommon words. You may return the answer in any order.

 

Example 1:

Input: s1 = "this apple is sweet", s2 = "this apple is sour"
Output: ["sweet","sour"]

Example 2:

Input: s1 = "apple apple", s2 = "banana"
Output: ["banana"]

 

Constraints:

    1 <= s1.length, s2.length <= 200
    s1 and s2 consist of lowercase English letters and spaces.
    s1 and s2 do not have leading or trailing spaces.
    All the words in s1 and s2 are separated by a single space.


 */