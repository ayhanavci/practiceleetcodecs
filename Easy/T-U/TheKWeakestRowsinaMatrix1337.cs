﻿namespace PracticeLeetCodeCS.Easy;

internal class TheKWeakestRowsinaMatrix1337
{
    public int[] KWeakestRows(int[][] mat, int k)
    {
        int[] strengthList = new int[mat.Length];
        List<int> unusedIndeces = new List<int>();

        for (int i = 0; i < mat.Length; i++)
        {
            int strength = 0;
            for (int j = 0; j < mat[i].Length; j++)
                strength += mat[i][j];
            strengthList[i] = strength;
            unusedIndeces.Add(i);
        }
        
        int[] sortedIndices = new int[k];
        
        for (int i = 0; i < k; ++i)
        {
            int smallestIndex = unusedIndeces[0];
            foreach (int j in unusedIndeces)
            {                
                if (strengthList[j] < strengthList[smallestIndex])
                    smallestIndex = j;
            }
            unusedIndeces.Remove(smallestIndex);
            sortedIndices[i] = smallestIndex;            
        }
        
        return sortedIndices;
    }
    public void Test()
    {
        int[][] mat = new int[5][];
        mat[0] = new int[] { 1, 1, 0, 0, 0 };
        mat[1] = new int[] { 1, 1, 1, 1, 0 };
        mat[2] = new int[] { 1, 0, 0, 0, 0 };
        mat[3] = new int[] { 1, 1, 0, 0, 0 };
        mat[4] = new int[] { 1, 1, 1, 1, 1 };

        int[] result = KWeakestRows(mat, 3);
    }
}
/*
 
 1337. The K Weakest Rows in a Matrix
Easy

You are given an m x n binary matrix mat of 1"s (representing soldiers) and 0"s (representing civilians). The soldiers are positioned in front of the civilians. That is, all the 1"s will appear to the left of all the 0"s in each row.

A row i is weaker than a row j if one of the following is true:

    The number of soldiers in row i is less than the number of soldiers in row j.
    Both rows have the same number of soldiers and i < j.

Return the indices of the k weakest rows in the matrix ordered from weakest to strongest.

 

Example 1:

Input: mat = 
[[1,1,0,0,0],
 [1,1,1,1,0],
 [1,0,0,0,0],
 [1,1,0,0,0],
 [1,1,1,1,1]], 
k = 3
Output: [2,0,3]
Explanation: 
The number of soldiers in each row is: 
- Row 0: 2 
- Row 1: 4 
- Row 2: 1 
- Row 3: 2 
- Row 4: 5 
The rows ordered from weakest to strongest are [2,0,3,1,4].

Example 2:

Input: mat = 
[[1,0,0,0],
 [1,1,1,1],
 [1,0,0,0],
 [1,0,0,0]], 
k = 2
Output: [0,2]
Explanation: 
The number of soldiers in each row is: 
- Row 0: 1 
- Row 1: 4 
- Row 2: 1 
- Row 3: 1 
The rows ordered from weakest to strongest are [0,2,3,1].

 

Constraints:

    m == mat.length
    n == mat[i].length
    2 <= n, m <= 100
    1 <= k <= m
    matrix[i][j] is either 0 or 1.


 */