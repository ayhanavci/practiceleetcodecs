﻿namespace PracticeLeetCodeCS.Easy;

internal class UnivaluedBinaryTree965
{

    //Definition for a binary tree node.
    public class TreeNode
    {
        public int val;
        public TreeNode left;
        public TreeNode right;
        public TreeNode(int val = 0, TreeNode left = null, TreeNode right = null)
        {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }
    int value;
    public bool IsUnivalTree(TreeNode root)
    {
        value = root.val;
        return CheckBranches(root);
    }
    public bool CheckBranches(TreeNode root)
    {
        if (root == null) return true;

        if (!CheckBranches(root.left)) return false;
        if (root.val != value) return false;
        if (!CheckBranches(root.right)) return false;

        return true;
    }

    public void Test()
    {

    }
}

/*
 965. Univalued Binary Tree
Easy
1.6K
59
Companies

A binary tree is uni-valued if every node in the tree has the same value.

Given the root of a binary tree, return true if the given tree is uni-valued, or false otherwise.

 

Example 1:

Input: root = [1,1,1,1,1,null,1]
Output: true

Example 2:

Input: root = [2,2,2,5,2]
Output: false

 

Constraints:

    The number of nodes in the tree is in the range [1, 100].
    0 <= Node.val < 100


 */
