﻿using System.Text;

namespace PracticeLeetCodeCS.Easy;

internal class ThousandSeparator1556
{
    public string ThousandSeparator(int n)
    {
        StringBuilder retVal = new StringBuilder();
        string number = n.ToString();

        int count = 0;
        for (int i = number.Length - 1; i >= 0; --i)
        {
            retVal.Insert(0, number[i]);
            if (++count % 3 == 0)            
                retVal.Insert(0, ".");                        
        }
        if (retVal[0] == '.') retVal.Remove(0, 1);
        return retVal.ToString();
    }
    public void Test()
    {
        var retVal1 = ThousandSeparator(1234);
        var retVal2 = ThousandSeparator(987);
        var retVal3 = ThousandSeparator(12345678);
    }
}

/*
 1556. Thousand Separator
Easy
403
22
Companies

Given an integer n, add a dot (".") as the thousands separator and return it in string format.

 

Example 1:

Input: n = 987
Output: "987"

Example 2:

Input: n = 1234
Output: "1.234"

 

Constraints:

    0 <= n <= 231 - 1


 */