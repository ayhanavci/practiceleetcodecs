namespace PracticeLeetCodeCS.Easy;

internal class ThreeDivisors1952
{
    public bool IsThree(int n) 
    {
        int count = 0;
        for (int m = 1; m <= n; ++m)
        {
            if (n % m == 0) {
                if (++count > 3)
                    return false;
            }
        }
        
        return count == 3;
    }
    public void Test()
    {
        var retVal1 = IsThree(4);
    }
}

/*
1952. Three Divisors
Easy
400
20
Companies

Given an integer n, return true if n has exactly three positive divisors. Otherwise, return false.

An integer m is a divisor of n if there exists an integer k such that n = k * m.

 

Example 1:

Input: n = 2
Output: false
Explantion: 2 has only two divisors: 1 and 2.

Example 2:

Input: n = 4
Output: true
Explantion: 4 has three divisors: 1, 2, and 4.

 

Constraints:

    1 <= n <= 104


*/