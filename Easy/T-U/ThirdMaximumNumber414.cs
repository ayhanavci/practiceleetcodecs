﻿namespace PracticeLeetCodeCS.Easy;

internal class ThirdMaximumNumber414
{
    public int ThirdMax2(int[] nums)
    {
        List<int> numsList = new List<int>(nums);
        IEnumerable<int> distinctList = numsList.OrderByDescending(num => num).DistinctBy(num => num);        
        return distinctList.Count() >= 3 ? distinctList.ElementAt(2) : distinctList.ElementAt(0);        
    }
    public int ThirdMax(int[] nums)
    {
        int firstMax = nums[0];
        int? secondMax = null;
        int? thirdMax = null;
        
        for (int i = 1; i < nums.Length; i++)
        {
            if (nums[i] == firstMax || nums[i] == secondMax || nums[i] == thirdMax)            
                continue;
            
            if (nums[i] > firstMax) 
            {
                thirdMax = secondMax;
                secondMax = firstMax;
                firstMax = nums[i];
            }
            else if (secondMax == null || nums[i] > secondMax)
            {
                thirdMax = secondMax;
                secondMax = nums[i];
            }
            else if (thirdMax == null || nums[i] > thirdMax)
            {                 
                thirdMax = nums[i];
            }
        }
        return thirdMax != null ? thirdMax.Value : firstMax;
    }
    public void Test()
    {
        int[] nums = { 2, 2, 3, 1 };
        int retVal = ThirdMax(nums);
    }
}
/*
 414. Third Maximum Number
Easy

Given an integer array nums, return the third distinct maximum number in this array. If the third maximum does not exist, return the maximum number.

 

Example 1:

Input: nums = [3,2,1]
Output: 1
Explanation:
The first distinct maximum is 3.
The second distinct maximum is 2.
The third distinct maximum is 1.

Example 2:

Input: nums = [1,2]
Output: 2
Explanation:
The first distinct maximum is 2.
The second distinct maximum is 1.
The third distinct maximum does not exist, so the maximum (2) is returned instead.

Example 3:

Input: nums = [2,2,3,1]
Output: 1
Explanation:
The first distinct maximum is 3.
The second distinct maximum is 2 (both 2"s are counted together since they have the same value).
The third distinct maximum is 1.

 

Constraints:

    1 <= nums.length <= 104
    -231 <= nums[i] <= 231 - 1

 */