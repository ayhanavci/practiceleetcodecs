﻿namespace PracticeLeetCodeCS.Easy;

internal class TransposeMatrix867
{
    public int[][] Transpose(int[][] matrix)
    {
        int[][] transpose = new int[matrix[0].Length][];

        for (int i = 0; i  < transpose.Length; ++i)        
            transpose[i] = new int[matrix.Length];        

        for (int i = 0; i < matrix.Length; ++i)        
            for (int j = 0; j < matrix[0].Length; ++j)            
                transpose[j][i] = matrix[i][j];            
        return transpose;
    }
    public void Test()
    {
        int[][] matrix = new int[3][];
        matrix[0] = new int[] { 1, 2, 3 };
        matrix[1] = new int[] { 4, 5, 6 };
        matrix[2] = new int[] { 7, 8, 9 };

        var retVal = Transpose(matrix);

        int[][] matrix2 = new int[2][];
        matrix2[0] = new int[] { 1, 2, 3 };
        matrix2[1] = new int[] { 4, 5, 6 };

        var retVal2 = Transpose(matrix2);
    }
}


/*
 867. Transpose Matrix
Easy
2.5K
416
Companies

Given a 2D integer array matrix, return the transpose of matrix.

The transpose of a matrix is the matrix flipped over its main diagonal, switching the matrix"s row and column indices.

 

Example 1:

Input: matrix = [[1,2,3],[4,5,6],[7,8,9]]
Output: [[1,4,7],[2,5,8],[3,6,9]]

Example 2:

Input: matrix = [[1,2,3],[4,5,6]]
Output: [[1,4],[2,5],[3,6]]

 

Constraints:

    m == matrix.length
    n == matrix[i].length
    1 <= m, n <= 1000
    1 <= m * n <= 105
    -109 <= matrix[i][j] <= 109


 */