﻿namespace PracticeLeetCodeCS.Easy;

internal class UniqueNumberofOccurrences1207
{
    public bool UniqueOccurrences(int[] arr)
    {
        Dictionary<int, int> occurances = new Dictionary<int, int>();
        for (int i = 0; i < arr.Length; i++)
        {
            if (occurances.ContainsKey(arr[i])) occurances[arr[i]]++;
            else occurances[arr[i]] = 1;
        }

        HashSet<int> result = new HashSet<int>();

        for (int i = 0; i < occurances.Count; i++)
        {
            int occurance = occurances.ElementAt(i).Value;
            if (result.Contains(occurance))
                return false;
            result.Add(occurance);
        }
        return true;
    }
    public void Test()
    {
        int[] arr1 = { 1, 2 };
        int[] arr2 = { -3, 0, 1, -3, 1, 1, 1, -3, 10, 0 };

        var retval = UniqueOccurrences(arr1);
    }
}

/*
 1207. Unique Number of Occurrences
Easy
3.1K
68
Companies

Given an array of integers arr, return true if the number of occurrences of each value in the array is unique or false otherwise.

 

Example 1:

Input: arr = [1,2,2,1,1,3]
Output: true
Explanation: The value 1 has 3 occurrences, 2 has 2 and 3 has 1. No two values have the same number of occurrences.

Example 2:

Input: arr = [1,2]
Output: false

Example 3:

Input: arr = [-3,0,1,-3,1,1,1,-3,10,0]
Output: true

 

Constraints:

    1 <= arr.length <= 1000
    -1000 <= arr[i] <= 1000


 */

