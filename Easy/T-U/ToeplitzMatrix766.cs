﻿namespace PracticeLeetCodeCS.Easy;

internal class ToeplitzMatrix766
{
    public bool IsToeplitzMatrix(int[][] matrix)
    {
        for (int i = 0; i < matrix.Length; i++)
        {
            int item = matrix[i][0];
            for (int j = i+1, k = 1; j < matrix.Length && k < matrix[0].Length; j++, ++k)
            {
                if (matrix[j][k] != item)
                    return false;
            }                        
        }

        for (int i = 1; i < matrix[0].Length; ++i)
        {
            int item = matrix[0][i];
            for (int j = 1, k = i + 1; j < matrix.Length && k < matrix[0].Length; ++j, ++k)
            {
                if (matrix[j][k] != item)
                    return false;
            }
        }

        return true;
    }
    public void Test()
    {
        int[][] matrix = new int[3][];
        matrix[0] = new int[]{ 1, 2, 3, 4 };
        matrix[1] = new int[]{ 5, 1, 2, 3 };
        matrix[2] = new int[]{ 9, 5, 1, 2 };
        var retval = IsToeplitzMatrix(matrix);
    }
}

/*
 766. Toeplitz Matrix
Easy
3.1K
151
Companies

Given an m x n matrix, return true if the matrix is Toeplitz. Otherwise, return false.

A matrix is Toeplitz if every diagonal from top-left to bottom-right has the same elements.

 

Example 1:

Input: matrix = [[1,2,3,4],[5,1,2,3],[9,5,1,2]]
Output: true
Explanation:
In the above grid, the diagonals are:
"[9]", "[5, 5]", "[1, 1, 1]", "[2, 2, 2]", "[3, 3]", "[4]".
In each diagonal all elements are the same, so the answer is True.

Example 2:

Input: matrix = [[1,2],[2,2]]
Output: false
Explanation:
The diagonal "[1, 2]" has different elements.

 

Constraints:

    m == matrix.length
    n == matrix[i].length
    1 <= m, n <= 20
    0 <= matrix[i][j] <= 99

 

Follow up:

    What if the matrix is stored on disk, and the memory is limited such that you can only load at most one row of the matrix into the memory at once?
    What if the matrix is so large that you can only load up a partial row into the memory at once?


 */