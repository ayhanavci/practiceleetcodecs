﻿using System.Text;

namespace PracticeLeetCodeCS.Easy;

internal class ToLowerCase709
{
    public string ToLowerCase(string s)
    {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < s.Length; ++i)
        {
            if (s[i] >= 'A' && s[i] <= 'Z')
                stringBuilder.Append((char)(s[i] + 32));
            else
                stringBuilder.Append(s[i]);
        }            
            
        return stringBuilder.ToString();
    }
    public void Test()
    {
        Console.WriteLine(ToLowerCase("Hello"));
        Console.WriteLine(ToLowerCase("here"));
        Console.WriteLine(ToLowerCase("LOVELY"));
        Console.WriteLine(ToLowerCase("LOVeLY"));
        /*int a = "a"; //97
        Console.WriteLine(a);
        a = "A"; //65
        //32
        Console.WriteLine(a);

        char ch = (char)("z" - 32);
        Console.WriteLine(ch);*/
    }
}

/*
 709. To Lower Case
Easy
1.4K
2.5K
Companies

Given a string s, return the string after replacing every uppercase letter with the same lowercase letter.

 

Example 1:

Input: s = "Hello"
Output: "hello"

Example 2:

Input: s = "here"
Output: "here"

Example 3:

Input: s = "LOVELY"
Output: "lovely"

 

Constraints:

    1 <= s.length <= 100
    s consists of printable ASCII characters.


 */