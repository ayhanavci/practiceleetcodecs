﻿namespace PracticeLeetCodeCS.Easy;

internal class TwoSumIVInputisaBST653
{

    //Definition for a binary tree node.
    public class TreeNode
    {
        public int val;
        public TreeNode left;
        public TreeNode right;
        public TreeNode(int val = 0, TreeNode left = null, TreeNode right = null)
        {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }

    public bool CheckAll(TreeNode root, List<int> numbers, int k)
    {
        if (root == null) return false;        
        
        for (int i = 0; i < numbers.Count; ++i)
        {
            if (numbers[i] + root.val == k) 
                return true;
        }

        numbers.Add(root.val);

        if (CheckAll(root.left, numbers, k)) return true;
        if (CheckAll(root.right, numbers, k)) return true;

        return false;
    }

    
    public bool FindTarget(TreeNode root, int k)
    {
        if (root == null) return false;
        
        List<int> numbers = new List<int>();

        return CheckAll(root, numbers, k);
    }
    public void Test()
    {
        TreeNode root = new TreeNode(2, 
            new TreeNode(0, new TreeNode(-4), new TreeNode(1)), 
            new TreeNode(3));

        var retVal = FindTarget(root, -1);
    }
}

/*
 653. Two Sum IV - Input is a BST
Easy
5.4K
235
Companies

Given the root of a binary search tree and an integer k, return true if there exist two elements in the BST such that their sum is equal to k, or false otherwise.

 

Example 1:

Input: root = [5,3,6,2,4,null,7], k = 9
Output: true

Example 2:

Input: root = [5,3,6,2,4,null,7], k = 28
Output: false

 

Constraints:

    The number of nodes in the tree is in the range [1, 104].
    -104 <= Node.val <= 104
    root is guaranteed to be a valid binary search tree.
    -105 <= k <= 105


 */