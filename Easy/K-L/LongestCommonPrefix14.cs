﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticeLeetCodeCS.Easy;

public class LongestCommonPrefix14
{
    private string LongestCommonPrefix(string[] strs)
    {
        StringBuilder commonPrefix = new StringBuilder();
        
        for (int i = 0; ; ++i)
        {            
            foreach (string str in strs)
            {                                
                if (str.Length < i + 1)
                    return commonPrefix.ToString();

                if (str[i] != strs[0][i])                
                    return commonPrefix.ToString();                                                        
            }            
            commonPrefix.Append(strs[0][i]);
        }        
    }
    public void Test()
    {
        string[] strs = { "flower", "flow", "flight" };
        Console.WriteLine($"LongestCommonPrefixP. Number:{strs} value is {LongestCommonPrefix(strs)}");
        
        string[] strs2 = { "dog","racecar","car" };
        Console.WriteLine($"LongestCommonPrefixP. Number:{strs2} value is {LongestCommonPrefix(strs2)}");

        string[] strs3 = { "flower", "flow", "floght" };
        Console.WriteLine($"LongestCommonPrefixP. Number:{strs3} value is {LongestCommonPrefix(strs3)}");
    }

}
/*
 14. Longest Common Prefix
Easy

Write a function to find the longest common prefix string amongst an array of strings.

If there is no common prefix, return an empty string "".

 

Example 1:

Input: strs = ["flower","flow","flight"]
Output: "fl"

Example 2:

Input: strs = ["dog","racecar","car"]
Output: ""
Explanation: There is no common prefix among the input strings.

 

Constraints:

    1 <= strs.length <= 200
    0 <= strs[i].length <= 200
    strs[i] consists of only lowercase English letters.


 */