namespace PracticeLeetCodeCS.Easy;

internal class LongerContiguousSegmentsofOnesthanZeros1869
{
    //TODO: 19%. speed it up.
    public bool CheckZeroOnes(string s) 
    {
        int current1Length = 0;
        int max1Length = 0;
        int current0Length = 0;
        int max0Length = 0;

        for (int i = 0; i < s.Length; ++i)
        {
            if (s[i] == '1') 
            {
                if (current0Length > 0) 
                {
                    max0Length = Math.Max(current0Length, max0Length);                                    
                    current0Length = 0;
                }                    
                current1Length++;                
            }
            else
            {
                if (current1Length > 0) 
                {
                    max1Length = Math.Max(current1Length, max1Length);                                    
                    current1Length = 0;
                }                    
                current0Length++;     
            }
        }

        max0Length = Math.Max(current0Length, max0Length);                                    
        max1Length = Math.Max(current1Length, max1Length);   
        return max1Length > max0Length;

    }
    public void Test()
    {
        var retVal1 = CheckZeroOnes("110100010");
        var retVal2 = CheckZeroOnes("1101");
        var retVal3 = CheckZeroOnes("111000");        
    }
}
/*
1869. Longer Contiguous Segments of Ones than Zeros
Easy
421
10
Companies

Given a binary string s, return true if the longest contiguous segment of 1's is strictly longer than the longest contiguous segment of 0's in s, or return false otherwise.

    For example, in s = "110100010" the longest continuous segment of 1s has length 2, and the longest continuous segment of 0s has length 3.

Note that if there are no 0's, then the longest continuous segment of 0's is considered to have a length 0. The same applies if there is no 1's.

 

Example 1:

Input: s = "1101"
Output: true
Explanation:
The longest contiguous segment of 1s has length 2: "1101"
The longest contiguous segment of 0s has length 1: "1101"
The segment of 1s is longer, so return true.

Example 2:

Input: s = "111000"
Output: false
Explanation:
The longest contiguous segment of 1s has length 3: "111000"
The longest contiguous segment of 0s has length 3: "111000"
The segment of 1s is not longer, so return false.

Example 3:

Input: s = "110100010"
Output: false
Explanation:
The longest contiguous segment of 1s has length 2: "110100010"
The longest contiguous segment of 0s has length 3: "110100010"
The segment of 1s is not longer, so return false.

 

Constraints:

    1 <= s.length <= 100
    s[i] is either '0' or '1'.


*/