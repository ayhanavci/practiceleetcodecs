﻿namespace PracticeLeetCodeCS.Easy;

internal class KthLargestElementinaStream703
{
    public class KthLargest 
    {
        PriorityQueue<int, int> heap;
        int k;
        public KthLargest(int k, int[] nums)
        {
            this.k = k;
            heap = new PriorityQueue<int, int>();
            for (int i = 0; i < nums.Length; i++)            
                heap.Enqueue(nums[i], nums[i]);
            
            while (heap.Count > k) 
                heap.Dequeue();
        }

        public int Add(int val)
        {
            heap.Enqueue(val, val);
            while (heap.Count > k)
                heap.Dequeue();
            return heap.Peek();
        }
    }

    /*public class KthLargest ////TIMEOUT
    {
        List<int> _items;
        int _k;
        public KthLargest(int k, int[] nums)
        {
            _items = new List<int>(nums);
            _items.Sort((a, b) => b.CompareTo(a));
            _k = k;
        }

        public int Add(int val)
        {
            _items.Add(val);
            _items.Sort((a, b) => b.CompareTo(a));
            return _items[_k - 1];
        }
    }*/

    /**
     * Your KthLargest object will be instantiated and called as such:
     * KthLargest obj = new KthLargest(k, nums);
     * int param_1 = obj.Add(val);
     */
    public void Test()
    {
        int[] nums = { 4, 5, 8, 2 };
        KthLargest kthLargest = new KthLargest(3, nums);
        int retval = kthLargest.Add(3);   // return 4
        retval = kthLargest.Add(5);   // return 5
        retval = kthLargest.Add(10);  // return 5
        retval = kthLargest.Add(9);   // return 8
        retval = kthLargest.Add(4);   // return 8

        /*int[] nums = { 0 };
        KthLargest kthLargest = new KthLargest(2, nums);
        int retval = kthLargest.Add(-1);   */
    }
}

/*
 703. Kth Largest Element in a Stream
Easy
3.7K
2.2K
Companies

Design a class to find the kth largest element in a stream. Note that it is the kth largest element in the sorted order, not the kth distinct element.

Implement KthLargest class:

    KthLargest(int k, int[] nums) Initializes the object with the integer k and the stream of integers nums.
    int add(int val) Appends the integer val to the stream and returns the element representing the kth largest element in the stream.

 

Example 1:

Input
["KthLargest", "add", "add", "add", "add", "add"]
[[3, [4, 5, 8, 2]], [3], [5], [10], [9], [4]]
Output
[null, 4, 5, 5, 8, 8]

Explanation
KthLargest kthLargest = new KthLargest(3, [4, 5, 8, 2]);
kthLargest.add(3);   // return 4
kthLargest.add(5);   // return 5
kthLargest.add(10);  // return 5
kthLargest.add(9);   // return 8
kthLargest.add(4);   // return 8

 

Constraints:

    1 <= k <= 104
    0 <= nums.length <= 104
    -104 <= nums[i] <= 104
    -104 <= val <= 104
    At most 104 calls will be made to add.
    It is guaranteed that there will be at least k elements in the array when you search for the kth element.


 */