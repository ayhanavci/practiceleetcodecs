namespace PracticeLeetCodeCS.Easy;

internal class LongestSubsequenceWithLimitedSum2389
{
    public int[] AnswerQueries(int[] nums, int[] queries) 
    {
        int[] retVal = new int[queries.Length];
        Array.Sort(nums);

        for (int i = 0; i < queries.Length; ++i)
        {            
            int total = 0;            

            for (int j = 0; j < nums.Length; ++j)
            {
                total += nums[j];                    
                if (total > queries[i])                                            
                    break;           
                retVal[i]++;                                     
            }
            
        }

        return retVal;
    }
    public int[] AnswerQueriesEX(int[] nums, int[] queries) //Ardışıla göre window sliding ile yapıldı. Ama ardışıl şart değilmiş.
    {
        int[] retVal = new int[queries.Length];

        for (int i = 0; i < queries.Length; ++i)
        {
            int maxLength = 0;
            int total = 0;
            int curLength = 0;

            for (int j = 0; j < nums.Length; ++j)
            {
                total += nums[j];
                curLength++;
                if (total > queries[i])
                {
                    curLength--;
                    total -= nums[j - curLength];
                }
                maxLength = Math.Max(maxLength, curLength);
            }

            retVal[i] = maxLength;
        }

        return retVal;
    }
    public void Test()
    {
        int[] nums = {4,5,2,1};
        int[] queries = {3,10,21};
        var retVal = AnswerQueries(nums, queries);

        int[] nums2 = {736411,184882,914641,37925,214915};
        int[] queries2 = {331244,273144,118983,118252,305688,718089,665450};
        var retVal2 = AnswerQueries(nums2, queries2);
        //[2,2,1,1,2,2,2]
        //[2,2,1,1,2,3,3]
    }
}
/*
2389. Longest Subsequence With Limited Sum
Easy
1.6K
140
Companies

You are given an integer array nums of length n, and an integer array queries of length m.

Return an array answer of length m where answer[i] is the maximum size of a subsequence that you can take from nums such that the sum of its elements is less than or equal to queries[i].

A subsequence is an array that can be derived from another array by deleting some or no elements without changing the order of the remaining elements.

 

Example 1:

Input: nums = [4,5,2,1], queries = [3,10,21]
Output: [2,3,4]
Explanation: We answer the queries as follows:
- The subsequence [2,1] has a sum less than or equal to 3. It can be proven that 2 is the maximum size of such a subsequence, so answer[0] = 2.
- The subsequence [4,5,1] has a sum less than or equal to 10. It can be proven that 3 is the maximum size of such a subsequence, so answer[1] = 3.
- The subsequence [4,5,2,1] has a sum less than or equal to 21. It can be proven that 4 is the maximum size of such a subsequence, so answer[2] = 4.

Example 2:

Input: nums = [2,3,4,5], queries = [1]
Output: [0]
Explanation: The empty subsequence is the only subsequence that has a sum less than or equal to 1, so answer[0] = 0.

 

Constraints:

    n == nums.length
    m == queries.length
    1 <= n, m <= 1000
    1 <= nums[i], queries[i] <= 106


*/