﻿using System.Text;

namespace PracticeLeetCodeCS.Easy;
internal class LatestTimebyReplacingHiddenDigits1736
{
    public string MaximumTime(string time)
    {        
        StringBuilder retVal = new StringBuilder();

        if (time[0] == '?' && time[1] == '?')
        {
            retVal.Append("23");
        }
        else if (time[0] == '?' && time[1] != '?')
        {
            if (time[1] > '3') retVal.Append('1');                            
            else retVal.Append('2');            

            retVal.Append(time[1]);
        }
        else if (time[0] != '?' && time[1] == '?')
        {
            retVal.Append(time[0]);

            if (time[0] == '2') retVal.Append('3');
            else retVal.Append('9');
        }
        else
        {
            retVal.Append(time[0]);
            retVal.Append(time[1]);
        }
        retVal.Append(':');

        if (time[3] == '?' && time[4] == '?')
        {
            retVal.Append("59");
        }
        else if (time[3] == '?' && time[4] != '?')
        {
            retVal.Append('5');
            retVal.Append(time[4]);
        }
        else if (time[3] != '?' && time[4] == '?')
        {
            retVal.Append(time[3]);
            retVal.Append('9');
        }
        else
        {
            retVal.Append(time[3]);
            retVal.Append(time[4]);
        }

        return retVal.ToString();
    }
    public void Test()
    {
        var retVal1 = MaximumTime("2?:?0");
        var retVal2 = MaximumTime("0?:3?");
        var retVal3 = MaximumTime("1?:22");
        var retVal4 = MaximumTime("?4:??");
    }
}

/*
 1736. Latest Time by Replacing Hidden Digits
Easy
290
150
Companies

You are given a string time in the form of  hh:mm, where some of the digits in the string are hidden (represented by ?).

The valid times are those inclusively between 00:00 and 23:59.

Return the latest valid time you can get from time by replacing the hidden digits.

 

Example 1:

Input: time = "2?:?0"
Output: "23:50"
Explanation: The latest hour beginning with the digit '2' is 23 and the latest minute ending with the digit '0' is 50.

Example 2:

Input: time = "0?:3?"
Output: "09:39"

Example 3:

Input: time = "1?:22"
Output: "19:22"

 

Constraints:

    time is in the format hh:mm.
    It is guaranteed that you can produce a valid time from the given string.


 */