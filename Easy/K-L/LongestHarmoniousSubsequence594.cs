﻿namespace PracticeLeetCodeCS.Easy;

internal class LongestHarmoniousSubsequence594
{
   
    public int FindLHS(int[] nums)
    {
        int longest = 0;

        for (int i = 0; i < nums.Length; i++)
        {
            int subSeqUpper = 1;
            int subSeqLower = 1;
            bool diffUpper = false;
            bool diffLower = false;           

            for (int j = i + 1; j < nums.Length; j++)
            {
                if (nums[j] == nums[i])
                {
                    subSeqUpper++;
                    subSeqLower++;
                }
                else if (nums[j] == nums[i] + 1)
                {
                    subSeqUpper++;
                    diffUpper = true;
                }
                else if (nums[j] == nums[i] - 1)
                {
                    subSeqLower++;
                    diffLower = true;
                }
            }
            if (diffUpper)
                longest = Math.Max(longest, subSeqUpper);
            if (diffLower)
                longest = Math.Max(longest, subSeqLower);          
        }
        return longest;
    }
    public int FindLHS2(int[] nums)
    {
        int longest = 0;

        List<int> subSeqUpper = new List<int>();
        List<int> subSeqLower = new List<int>();

        for (int i = 0; i < nums.Length; i++)
        {
            subSeqUpper.Clear();
            subSeqLower.Clear();
            bool diffUpper = false;
            bool diffLower = false;
            subSeqUpper.Add(nums[i]);
            subSeqLower.Add(nums[i]);

            for (int j = i + 1; j < nums.Length; j++)
            {
                if (nums[j] == nums[i])
                {
                    subSeqUpper.Add(nums[j]);
                    subSeqLower.Add(nums[j]);
                }                    
                else if (nums[j] == nums[i] + 1)
                {
                    subSeqUpper.Add(nums[j]);
                    diffUpper = true;
                }                    
                else if (nums[j] == nums[i] - 1)
                {
                    subSeqLower.Add(nums[j]);
                    diffLower = true;
                }                    
            }
            if (diffUpper) 
                longest = Math.Max(longest, subSeqUpper.Count);
            if (diffLower)
                longest = Math.Max(longest, subSeqLower.Count);
        }
        return longest;
    }
    public void Test()
    {
        //int[] nums = { 1, 3, 2, 2, 5, 2, 3, 7 };
        //int[] nums = { 1, 2, 3, 4 };
        //int[] nums = { 1, 1, 1, 1 };
        int[] nums = { 1, 2, 2, 1 };

        var retVal = FindLHS(nums);
    }
}

/*
 594. Longest Harmonious Subsequence
Easy
1.8K
168
Companies

We define a harmonious array as an array where the difference between its maximum value and its minimum value is exactly 1.

Given an integer array nums, return the length of its longest harmonious subsequence among all its possible subsequences.

A subsequence of array is a sequence that can be derived from the array by deleting some or no elements without changing the order of the remaining elements.

 

Example 1:

Input: nums = [1,3,2,2,5,2,3,7]
Output: 5
Explanation: The longest harmonious subsequence is [3,2,2,2,3].

Example 2:

Input: nums = [1,2,3,4]
Output: 2

Example 3:

Input: nums = [1,1,1,1]
Output: 0

 

Constraints:

    1 <= nums.length <= 2 * 104
    -109 <= nums[i] <= 109

 */