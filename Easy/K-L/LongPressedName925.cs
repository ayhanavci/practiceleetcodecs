﻿namespace PracticeLeetCodeCS.Easy;

internal class LongPressedName925
{
    public bool IsLongPressedName(string name, string typed)
    {
        if (name.Length > typed.Length) return false;

        int lastPos = 0;
        for (int i = 0; i < name.Length; ++i)
        {
            if (lastPos == typed.Length) return false;
            for (int j = lastPos; j < typed.Length; ++j)
            {
                if (name[i] == typed[j])
                {
                    lastPos = j+1;
                    break;
                }
                    
                if (i == 0) return false;
                if (name[i - 1] != typed[j]) return false;

            }
        }
        if (typed.Length > lastPos)
        {
            for (int i = lastPos; i < typed.Length; ++i) 
            {
                if (typed[i] != name[name.Length - 1])
                    return false;
            }
        }

        return true;
    }
    public void Test()
    {
        //var retVal1 = IsLongPressedName("alex", "aaleex");
        //var retVal2 = IsLongPressedName("saeed", "ssaaedd");
        //var retVal3 = IsLongPressedName("alex", "aaleexa");
        //var retVal4 = IsLongPressedName("pyplrz", "ppyypllr");
        var retVal4 = IsLongPressedName("alex", "alexxr");
    }
}


/*
 925. Long Pressed Name
Easy
2K
286
Companies

Your friend is typing his name into a keyboard. Sometimes, when typing a character c, the key might get long pressed, and the character will be typed 1 or more times.

You examine the typed characters of the keyboard. Return True if it is possible that it was your friends name, with some characters (possibly none) being long pressed.

 

Example 1:

Input: name = "alex", typed = "aaleex"
Output: true
Explanation: "a" and "e" in "alex" were long pressed.

Example 2:

Input: name = "saeed", typed = "ssaaedd"
Output: false
Explanation: "e" must have been pressed twice, but it was not in the typed output.

 

Constraints:

    1 <= name.length, typed.length <= 1000
    name and typed consist of only lowercase English letters.


 */