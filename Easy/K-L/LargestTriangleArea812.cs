﻿namespace PracticeLeetCodeCS.Easy;

internal class LargestTriangleArea812
{
    public double LargestTriangleArea(int[][] points)
    {
        double largest = 0;
        for (int i = 0; i < points.Length; i++)
        {
            for (int j = 0; j < points.Length; j++) 
            {
                for (int k = 0; k < points.Length; k++)
                {
                    largest = Math.Max(largest,
                        0.5 * Math.Abs(
                                points[i][0] * points[j][1] +
                                points[j][0] * points[k][1] +
                                points[k][0] * points[i][1] -
                                points[j][0] * points[i][1] -
                                points[k][0] * points[j][1] -
                                points[i][0] * points[k][1]));
                }
            }
        }
        return largest;
    }
    /*public double areaCal(int[] pt1, int[] pt2, int[] pt3)
    {
        return Math.abs(pt1[0] * (pt2[1] - pt3[1]) + pt2[0] * (pt3[1] - pt1[1]) + pt3[0] * (pt1[1] - pt2[1])) / 2.0;
    }*/
    public void Test()
    {

    }
}
//https://www.mathopenref.com/coordtrianglearea.html
/* https://leetcode.com/problems/largest-triangle-area/solutions/122711/c-java-python-solution-with-explanation-and-prove/?orderBy=most_votes
 * 
 * public double largestTriangleArea(int[][] p) {
        double res = 0;
        for (int[] i: p)
            for (int[] j: p)
                for (int[] k: p)
            res = Math.max(res, 0.5 * Math.abs(
                    i[0] * j[1] + 
                    j[0] * k[1] + 
                    k[0] * i[1] - 
                    j[0] * i[1] - 
                    k[0] * j[1] - 
                    i[0] * k[1]));
        return res;
    }
 812. Largest Triangle Area
Easy
443
1.5K
Companies

Given an array of points on the X-Y plane points where points[i] = [xi, yi], return the area of the largest triangle that can be formed by any three different points. Answers within 10-5 of the actual answer will be accepted.

 

Example 1:

Input: points = [[0,0],[0,1],[1,0],[0,2],[2,0]]
Output: 2.00000
Explanation: The five points are shown in the above figure. The red triangle is the largest.

Example 2:

Input: points = [[1,0],[0,0],[0,1]]
Output: 0.50000

 

Constraints:

    3 <= points.length <= 50
    -50 <= xi, yi <= 50
    All the given points are unique.


 */