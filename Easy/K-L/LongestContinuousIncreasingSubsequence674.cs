﻿namespace PracticeLeetCodeCS.Easy;

internal class LongestContinuousIncreasingSubsequence674
{
    public int FindLengthOfLCIS(int[] nums)
    {
        int longest = 1;

        int currentLength;
        for (int i = 0; i < nums.Length; i += currentLength)
        {
            currentLength = 1;
            for (int j = i + 1; j < nums.Length; ++j)
            {
                if (nums[j] > nums[j - 1])                
                    currentLength++;                
                else
                    break;
            }            
            longest = Math.Max(longest, currentLength);
        }

        return longest;
    }
    public void Test()
    {
        //int[] nums = { 1, 3, 5, 4, 7 };
        int[] nums = { 2, 1, 3 };

        var retVal = FindLengthOfLCIS(nums);
    }
}

/*
 674. Longest Continuous Increasing Subsequence
Easy
2K
168
Companies

Given an unsorted array of integers nums, return the length of the longest continuous increasing subsequence (i.e. subarray). The subsequence must be strictly increasing.

A continuous increasing subsequence is defined by two indices l and r (l < r) such that it is [nums[l], nums[l + 1], ..., nums[r - 1], nums[r]] and for each l <= i < r, nums[i] < nums[i + 1].

 

Example 1:

Input: nums = [1,3,5,4,7]
Output: 3
Explanation: The longest continuous increasing subsequence is [1,3,5] with length 3.
Even though [1,3,5,7] is an increasing subsequence, it is not continuous as elements 5 and 7 are separated by element
4.

Example 2:

Input: nums = [2,2,2,2,2]
Output: 1
Explanation: The longest continuous increasing subsequence is [2] with length 1. Note that it must be strictly
increasing.

 

Constraints:

    1 <= nums.length <= 104
    -109 <= nums[i] <= 109


 */