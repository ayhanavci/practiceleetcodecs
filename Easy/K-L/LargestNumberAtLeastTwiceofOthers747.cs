﻿namespace PracticeLeetCodeCS.Easy;

internal class LargestNumberAtLeastTwiceofOthers747
{
    public int DominantIndex(int[] nums)
    {        
        int secondMaxIndex = -1;
        int maxIndex = 0;

        for (int i = 0; i < nums.Length; i++)
        {           
            if (nums[i] > nums[maxIndex])
            {
                secondMaxIndex = maxIndex;                
                maxIndex = i;
            }
            if (maxIndex != i)
            {
                if (secondMaxIndex == -1)
                    secondMaxIndex = i;
                else if (nums[i] > nums[secondMaxIndex])
                    secondMaxIndex = i;
            }
            
            
        }        
        return nums[maxIndex] >= nums[secondMaxIndex] * 2 ? maxIndex : -1;
    }
    public void Test()
    {
        //int[] nums = { 3, 6, 1, 0, 4, 12, 2 };
        int[] nums = { 1, 0 };
        //int[] nums = { 3, 0, 0, 2 };
        var retVal = DominantIndex(nums);
    }
}

/*
 747. Largest Number At Least Twice of Others
Easy
890
820
Companies

You are given an integer array nums where the largest integer is unique.

Determine whether the largest element in the array is at least twice as much as every other number in the array. If it is, return the index of the largest element, or return -1 otherwise.

 

Example 1:

Input: nums = [3,6,1,0]
Output: 1
Explanation: 6 is the largest integer.
For every other number in the array x, 6 is at least twice as big as x.
The index of value 6 is 1, so we return 1.

Example 2:

Input: nums = [1,2,3,4]
Output: -1
Explanation: 4 is less than twice the value of 3, so we return -1.

 

Constraints:

    2 <= nums.length <= 50
    0 <= nums[i] <= 100
    The largest element in nums is unique.


 */