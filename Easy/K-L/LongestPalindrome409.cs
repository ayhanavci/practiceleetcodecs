﻿namespace PracticeLeetCodeCS.Easy;

internal class LongestPalindrome409
{
    public int LongestPalindrome(string s)
    {
        Dictionary<char, int> charFrequency = new Dictionary<char, int>();

        int count = 0;
        for (int i = 0; i < s.Length; ++i)
        {
            if (charFrequency.ContainsKey(s[i]))
            {
                if (++charFrequency[s[i]] == 2)
                {
                    count++;
                    charFrequency.Remove(s[i]);                    
                }
            }                
            else            
                charFrequency[s[i]] = 1;
            
                
        }
        return count * 2 + (charFrequency.Count > 0 ? 1 : 0);

    }
    public void Test()
    {
        string s = "abccccdd";
        //string s = "aaabbbcccc";
        int retVal = LongestPalindrome(s);
    }
}
/*
 409. Longest Palindrome
Easy

Given a string s which consists of lowercase or uppercase letters, return the length of the longest palindrome that can be built with those letters.

Letters are case sensitive, for example, "Aa" is not considered a palindrome here.

 

Example 1:

Input: s = "abccccdd"
Output: 7
Explanation: One longest palindrome that can be built is "dccaccd", whose length is 7.

Example 2:

Input: s = "a"
Output: 1
Explanation: The longest palindrome that can be built is "a", whose length is 1.

 

Constraints:

    1 <= s.length <= 2000
    s consists of lowercase and/or uppercase English letters only.


 */