﻿namespace PracticeLeetCodeCS.Easy;

internal class LargestPerimeterTriangle976
{
    public int LargestPerimeter(int[] nums)
    {
        Array.Sort(nums);
        for (int i = nums.Length - 1; i > 1; i--)
        {
            if (nums[i] < nums[i - 1] + nums[i - 2])
                return nums[i] + nums[i - 1] + nums[i - 2];
        }
        return 0;
    }
    public void Test()
    {
        int[] nums = { 1, 2, 1, 10 };
        var retVal = LargestPerimeter(nums);

        int[] nums2 = { 2, 1, 2 };
        var retVal2 = LargestPerimeter(nums2);      
    }
}
//For a >= b >= c, a,b,c can form a triangle if a < b + c.
/*
 976. Largest Perimeter Triangle
Easy
2.5K
355
Companies

Given an integer array nums, return the largest perimeter of a triangle with a non-zero area, formed from three of these lengths. If it is impossible to form any triangle of a non-zero area, return 0.

 

Example 1:

Input: nums = [2,1,2]
Output: 5
Explanation: You can form a triangle with three side lengths: 1, 2, and 2.

Example 2:

Input: nums = [1,2,1,10]
Output: 0
Explanation: 
You cannot use the side lengths 1, 1, and 2 to form a triangle.
You cannot use the side lengths 1, 1, and 10 to form a triangle.
You cannot use the side lengths 1, 2, and 10 to form a triangle.
As we cannot use any three side lengths to form a triangle of non-zero area, we return 0.

 

Constraints:

    3 <= nums.length <= 104
    1 <= nums[i] <= 106


 */