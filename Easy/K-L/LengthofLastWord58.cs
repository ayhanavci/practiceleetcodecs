﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticeLeetCodeCS.Easy;

public class LengthofLastWord58
{
    public int LengthOfLastWord(string s)
    {
        int length = 0;
        int lastLength = 0;
        for (int i = 0; i < s.Length; ++i)
        {
            if (s[i] != ' ')
                length++;
            else if (length > 0)
            {
                lastLength = length;
                length = 0;
            }                                    
        }
        if (length == 0) length = lastLength;
        return length;
    }
    public void Test()
    {
        //string s = "luffy is still joyboy";
        string s = "   fly me   to   the moon  ";
        //string s = "Hello World";
        int length = LengthOfLastWord(s);
    }
}

/*
 58. Length of Last Word
Easy

Given a string s consisting of words and spaces, return the length of the last word in the string.

A word is a maximal substring consisting of non-space characters only.

 

Example 1:

Input: s = "Hello World"
Output: 5
Explanation: The last word is "World" with length 5.

Example 2:

Input: s = "   fly me   to   the moon  "
Output: 4
Explanation: The last word is "moon" with length 4.

Example 3:

Input: s = "luffy is still joyboy"
Output: 6
Explanation: The last word is "joyboy" with length 6.

 

Constraints:

    1 <= s.length <= 104
    s consists of only English letters and spaces " ".
    There will be at least one word in s.


 */