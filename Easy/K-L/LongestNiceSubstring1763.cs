﻿namespace PracticeLeetCodeCS.Easy;
internal class LongestNiceSubstring1763
{
    //TODO looked at the solution
    public string LongestNiceSubstring(string s)
    {
        HashSet<char> chars = new HashSet<char>();
        for (int i = 0; i < s.Length; i++) chars.Add(s[i]);

        for (int i = 0; i < s.Length; ++i)
        {
            if (chars.Contains(Char.ToLower(s[i])) && chars.Contains(Char.ToUpper(s[i]))) continue;
            string firstHalf = LongestNiceSubstring(s.Substring(0, i));
            string secondHalf = LongestNiceSubstring(s.Substring(i + 1));
            return firstHalf.Length >= secondHalf.Length ? firstHalf : secondHalf;
        }
        return s;
    }
    public void Test()
    {
        var retVal = LongestNiceSubstring("YazaAay");
    }
}

/*
 1763. Longest Nice Substring
Easy
984
693
Companies

A string s is nice if, for every letter of the alphabet that s contains, it appears both in uppercase and lowercase. For example, "abABB" is nice because 'A' and 'a' appear, and 'B' and 'b' appear. However, "abA" is not because 'b' appears, but 'B' does not.

Given a string s, return the longest substring of s that is nice. If there are multiple, return the substring of the earliest occurrence. If there are none, return an empty string.

 

Example 1:

Input: s = "YazaAay"
Output: "aAa"
Explanation: "aAa" is a nice string because 'A/a' is the only letter of the alphabet in s, and both 'A' and 'a' appear.
"aAa" is the longest nice substring.

Example 2:

Input: s = "Bb"
Output: "Bb"
Explanation: "Bb" is a nice string because both 'B' and 'b' appear. The whole string is a substring.

Example 3:

Input: s = "c"
Output: ""
Explanation: There are no nice substrings.

 

Constraints:

    1 <= s.length <= 100
    s consists of uppercase and lowercase English letters.


 */