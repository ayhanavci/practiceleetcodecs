namespace PracticeLeetCodeCS.Easy;

internal class LargestLocalValuesinaMatrix2373
{
    public int[][] LargestLocal(int[][] grid) 
    {
        int[][] maxLocal = new int[grid.Length - 2][];
        for (int i = 0; i < grid.Length - 2; ++i)        
            maxLocal[i] = new int[grid.Length-2];
        
        for (int i = 0; i < grid.Length - 2; ++i)        
            for (int j = 0; j < grid.Length - 2; ++j)            
                maxLocal[i][j] = LargestInCoords(grid, i, j);                    
        
        return maxLocal;
    }
    public int LargestInCoords(int[][] grid, int originX, int originY)
    {
        int retVal = int.MinValue;
        for (int i = originX; i < originX + 3; ++i)
            for (int j = originY; j < originY + 3; ++j)            
                retVal = Math.Max(grid[i][j], retVal);
            
        return retVal;
    }
    public void Test()
    {
        int[][] grid = new int[4][];
        grid[0] = new int[] {9,9,8,1};
        grid[1] = new int[] {5,6,2,6};
        grid[2] = new int[] {8,2,6,4};
        grid[3] = new int[] {6,2,2,2};

        var retVal = LargestLocal(grid);
    }
}
/*
2373. Largest Local Values in a Matrix
Easy
555
58
Companies

You are given an n x n integer matrix grid.

Generate an integer matrix maxLocal of size (n - 2) x (n - 2) such that:

    maxLocal[i][j] is equal to the largest value of the 3 x 3 matrix in grid centered around row i + 1 and column j + 1.

In other words, we want to find the largest value in every contiguous 3 x 3 matrix in grid.

Return the generated matrix.

 

Example 1:

Input: grid = [[9,9,8,1],[5,6,2,6],[8,2,6,4],[6,2,2,2]]
Output: [[9,9],[8,6]]
Explanation: The diagram above shows the original matrix and the generated matrix.
Notice that each value in the generated matrix corresponds to the largest value of a contiguous 3 x 3 matrix in grid.

Example 2:

Input: grid = [[1,1,1,1,1],[1,1,1,1,1],[1,1,2,1,1],[1,1,1,1,1],[1,1,1,1,1]]
Output: [[2,2,2],[2,2,2],[2,2,2]]
Explanation: Notice that the 2 is contained within every contiguous 3 x 3 matrix in grid.

 

Constraints:

    n == grid.length == grid[i].length
    3 <= n <= 100
    1 <= grid[i][j] <= 100


*/