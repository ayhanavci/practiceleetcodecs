﻿namespace PracticeLeetCodeCS.Easy;

internal class KeyboardRow500
{
    public string[] FindWords(string[] words)
    {
        List<string> result = new List<string>();
        string top = "qwertyuiop"; //0
        string middle = "asdfghjkl"; //1
        string bottom = "zxcvbnm"; //2

        int position = -1;
        for (int i = 0; i < words.Length; i++)
        {
            string word = words[i].ToLower();
            if (top.Contains(word[0]))
                position = 0;
            else if (middle.Contains(word[0]))
                position = 1;
            else if (bottom.Contains(word[0]))
                position = 2;
            int j = 1;
            for (; j < word.Length; j++)
            {
                if (top.Contains(word[j]))
                {
                    if (position != 0)
                        break;
                }                    
                else if (middle.Contains(word[j]))
                {
                    if (position != 1)
                        break;
                }                    
                else if (bottom.Contains(word[j]))
                {
                    if (position != 2)
                        break;
                }                                    
            }
            if (j == word.Length)
                result.Add(words[i]);
        }
        return result.ToArray();

    }
    public void Test()
    {
        //string[] words = { "Hello", "Alaska", "Dad", "Peace" };
        //string[] words = { "Omk" };
        string[] words = { "adsdf", "sfd" };
        string[] retVal = FindWords(words);
    }
}

/*
 500. Keyboard Row
Easy

Given an array of strings words, return the words that can be typed using letters of the alphabet on only one row of American keyboard like the image below.

In the American keyboard:

    the first row consists of the characters "qwertyuiop",
    the second row consists of the characters "asdfghjkl", and
    the third row consists of the characters "zxcvbnm".

 

Example 1:

Input: words = ["Hello","Alaska","Dad","Peace"]
Output: ["Alaska","Dad"]

Example 2:

Input: words = ["omk"]
Output: []

Example 3:

Input: words = ["adsdf","sfd"]
Output: ["adsdf","sfd"]

 

Constraints:

    1 <= words.length <= 20
    1 <= words[i].length <= 100
    words[i] consists of English letters (both lowercase and uppercase). 


 */