﻿namespace PracticeLeetCodeCS.Easy;

internal class LeafSimilarTrees872
{

    //Definition for a binary tree node.
    public class TreeNode
    {
        public int val;
        public TreeNode left;
        public TreeNode right;
        public TreeNode(int val = 0, TreeNode left = null, TreeNode right = null)
        {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }

    public List<int> GetLeafs(TreeNode root)
    {
        List<int> result = new List<int>();
        if (root == null) return result;

        if (root.left == null && root.right == null)
        {
            result.Add(root.val);
            return result;
        }

        result.AddRange(GetLeafs(root.left));
        result.AddRange(GetLeafs(root.right));

        return result;
    }
    public bool LeafSimilar(TreeNode root1, TreeNode root2)
    {
        List<int> leafs1 = GetLeafs(root1);
        List<int> leafs2 = GetLeafs(root2);

        if (leafs1.Count != leafs2.Count) return false;
        
        for (int i = 0; i < leafs1.Count; ++i)        
            if (leafs1[i] != leafs2[i]) return false;
        
        return true;
    }
    public void Test()
    {
        TreeNode root1 = new TreeNode(3,
            
            new TreeNode(5, 
                new TreeNode(6), 
                new TreeNode(2, new TreeNode(7), new TreeNode(4))),

            new TreeNode(1, new TreeNode(9), new TreeNode(8)));


        TreeNode root2 = new TreeNode(3,
            new TreeNode(5, new TreeNode(6), new TreeNode(7)),
            new TreeNode(1, new TreeNode(4),
                new TreeNode(2, new TreeNode(9), new TreeNode(8))));

        var retVal = LeafSimilar(root1, root2);

    }
}

/*
 872. Leaf-Similar Trees
Easy
2.8K
66
Companies

Consider all the leaves of a binary tree, from left to right order, the values of those leaves form a leaf value sequence.

For example, in the given tree above, the leaf value sequence is (6, 7, 4, 9, 8).

Two binary trees are considered leaf-similar if their leaf value sequence is the same.

Return true if and only if the two given trees with head nodes root1 and root2 are leaf-similar.

 

Example 1:

Input: root1 = [3,5,1,6,2,9,8,null,null,7,4], root2 = [3,5,1,6,7,4,2,null,null,null,null,null,null,9,8]
Output: true

Example 2:

Input: root1 = [1,2,3], root2 = [1,3,2]
Output: false

 

Constraints:

    The number of nodes in each tree will be in the range [1, 200].
    Both of the given trees will have values in the range [0, 200].


 */