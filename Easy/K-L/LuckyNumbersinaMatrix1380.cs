﻿namespace PracticeLeetCodeCS.Easy;

internal class LuckyNumbersinaMatrix1380
{
    public IList<int> LuckyNumbers(int[][] matrix)
    {
        IList<int> result = new List<int>();
                
        for (int i = 0; i < matrix.Length; i++)
        {
            int minRowIndex = 0;
            for (int j = 0; j < matrix[0].Length; ++j)
            {
                if (matrix[i][j] < matrix[i][minRowIndex])
                    minRowIndex = j;
                    
            }
            int maxColIndex = 0;
            for (int j = 0; j < matrix.Length; ++j)
            {
                if (matrix[j][minRowIndex] > matrix[maxColIndex][minRowIndex])
                    maxColIndex = j;
            }
            if (maxColIndex == i)
                result.Add(matrix[i][minRowIndex]);
        }
        return result;
    }
    public void Test()
    {
        //[ 1, 10,  4,  2],
        //[ 9,  3,  8,  7],
        //[15, 16, 17, 12]
        int[][] matrix = new int[3][];
        matrix[0] = new int[] { 1, 10, 4, 2 };
        matrix[1] = new int[] { 9, 3, 8, 7 };
        matrix[2] = new int[] { 15, 16, 17, 12 };
        var retVal = LuckyNumbers(matrix);

        int[][] matrix2 = new int[3][];
        matrix2[0] = new int[] { 3, 7, 8 };
        matrix2[1] = new int[] { 9, 11, 13 };
        matrix2[2] = new int[] { 15, 16, 17 };
        var retVal2 = LuckyNumbers(matrix2);
    }
}

/*
 1380. Lucky Numbers in a Matrix
Easy
1.3K
73
Companies

Given an m x n matrix of distinct numbers, return all lucky numbers in the matrix in any order.

A lucky number is an element of the matrix such that it is the minimum element in its row and maximum in its column.

 

Example 1:

Input: matrix = [[3,7,8],[9,11,13],[15,16,17]]
Output: [15]
Explanation: 15 is the only lucky number since it is the minimum in its row and the maximum in its column.

Example 2:

Input: matrix = [[1,10,4,2],[9,3,8,7],[15,16,17,12]]
Output: [12]
Explanation: 12 is the only lucky number since it is the minimum in its row and the maximum in its column.

Example 3:

Input: matrix = [[7,8],[1,2]]
Output: [7]
Explanation: 7 is the only lucky number since it is the minimum in its row and the maximum in its column.

 

Constraints:

    m == mat.length
    n == mat[i].length
    1 <= n, m <= 50
    1 <= matrix[i][j] <= 105.
    All elements in the matrix are distinct.


 */