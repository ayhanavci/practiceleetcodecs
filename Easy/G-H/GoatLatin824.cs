﻿using System.Text;

namespace PracticeLeetCodeCS.Easy;

internal class GoatLatin824
{
    public string ToGoatLatin(string sentence)
    {
        string[] words = sentence.Split(' ');
        char[] wovels = { 'a', 'e', 'i', 'o', 'u' };
        StringBuilder retVal = new StringBuilder();
        for (int i = 0; i < words.Length; i++)
        {
            StringBuilder newWord = new StringBuilder(words[i]);
            if (newWord[0] != 'a' &&
                newWord[0] != 'e' &&
                newWord[0] != 'i' &&
                newWord[0] != 'o' &&
                newWord[0] != 'u' &&
                newWord[0] != 'A' &&
                newWord[0] != 'E' &&
                newWord[0] != 'I' &&
                newWord[0] != 'O' &&
                newWord[0] != 'U')
            {
                newWord.Append(newWord[0]);
                newWord.Remove(0, 1);
            }
                
            newWord.Append("ma");
            for (int j = 0; j < i + 1; ++j)
                newWord.Append('a');

            retVal.Append(newWord);            
            retVal.Append(' ');
        }
        retVal.Remove(retVal.Length - 1, 1);
        return retVal.ToString();
    }
    public void Test()
    {
        //string sentence = "The quick brown fox jumped over the lazy dog";
        //var result = "heTmaa uickqmaaa rownbmaaaa oxfmaaaaa umpedjmaaaaaa overmaaaaaaa hetmaaaaaaaa azylmaaaaaaaaa ogdmaaaaaaaaaa";        

        string sentence = "Each word consists of lowercase and uppercase letters only";
        var result = "Eachmaa ordwmaaa onsistscmaaaa ofmaaaaa owercaselmaaaaaa andmaaaaaaa uppercasemaaaaaaaa etterslmaaaaaaaaa onlymaaaaaaaaaa";
        var retVal = ToGoatLatin(sentence);
        
    }
}


/*
 824. Goat Latin
Easy
764
1.2K
Companies

You are given a string sentence that consist of words separated by spaces. Each word consists of lowercase and uppercase letters only.

We would like to convert the sentence to "Goat Latin" (a made-up language similar to Pig Latin.) The rules of Goat Latin are as follows:

    If a word begins with a vowel ("a", "e", "i", "o", or "u"), append "ma" to the end of the word.
        For example, the word "apple" becomes "applema".
    If a word begins with a consonant (i.e., not a vowel), remove the first letter and append it to the end, then add "ma".
        For example, the word "goat" becomes "oatgma".
    Add one letter "a" to the end of each word per its word index in the sentence, starting with 1.
        For example, the first word gets "a" added to the end, the second word gets "aa" added to the end, and so on.

Return the final sentence representing the conversion from sentence to Goat Latin.

 

Example 1:

Input: sentence = "I speak Goat Latin"
Output: "Imaa peaksmaaa oatGmaaaa atinLmaaaaa"

Example 2:

Input: sentence = "The quick brown fox jumped over the lazy dog"
Output: "heTmaa uickqmaaa rownbmaaaa oxfmaaaaa umpedjmaaaaaa overmaaaaaaa hetmaaaaaaaa azylmaaaaaaaaa ogdmaaaaaaaaaa"

 

Constraints:

    1 <= sentence.length <= 150
    sentence consists of English letters and spaces.
    sentence has no leading or trailing spaces.
    All the words in sentence are separated by a single space.


 */