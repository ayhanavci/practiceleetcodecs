﻿namespace PracticeLeetCodeCS.Easy;

internal class GreatestCommonDivisorofStrings1071
{
    public string GcdOfStrings(string str1, string str2)
    {
        if (str1 + str2 != str2 + str1) return "";
        if (str1 == str2) return str1;

        if (str1.Length > str2.Length)
            return GcdOfStrings(str1.Substring(str2.Length), str2);
        else
            return GcdOfStrings(str2.Substring(str1.Length), str1);
    }
    public void Test()
    {
        string str1 = "ABCABC";
        string str2 = "ABC";
        var retVal = GcdOfStrings(str1, str2);
    }
}
/*
 This solution is based on the Euclidean algorithm

First, let's talk about how we obtain the greatest common divisor (GCD) from numbers. Let's say you have two numbers 252 and 105, and you want to find the GCD of both numbers (which is 21).

Via the Euclidean algorithm, we know that the GCD of 252 and 105 is the same as the GCD of 252 - 105 (which is 147) and 105. In other words: 252, 105, and 147 are all multiples of 21. 
This can be observed from the fact that 252 = 21 * 12 and 105 = 21 * 5.

In simpler terms, if we subtract an arbitrary multiple of 21 from either 252 or 105, we know the resulting number will still be a multiple of 21.

Lets look at some examples.
105 - (21 * 3) = 42, which is still a multiple of 21.
105 - (21 * 1) = 84, which is still a multiple of 21.
252 - (21 * 5) = 126, which is still a multiple of 21.
252 - (21 * 8) = 84, which is still a multiple of 21.

Therefore in plain English, we can obtain the GCD of two numbers by recursively calculating the GCD of two variables:

    The result of the larger number subtracted by the smaller number, and
    The smaller number

When the two variables are equal, we have obtained the GCD.

Confused? Let's look at an example. Using the above logic, we can calculate the GCD of 252 and 105 as follows:

GCD(252, 105) is equivalent to:

    GCD of 252 - 105 (= 147) and 105
    GCD of 147 - 105 (= 42) and 105
    GCD of 105 - 42 (= 63) and 42
    GCD of 63 - 42 (= 21) and 42
    GCD of 42 - 21 (= 21) and 21
    GCD of 21 and 21, which is final answer: 21

Applying this same logic for strings, we get the following code:

var gcdOfStrings = function(str1, str2) {
    if (str1 + str2 != str2 + str1){ // Check whether or not a GCD is possible, first
        return "";
    } else if (str1 == str2){
        return str1;
    } else if (str1.length > str2.length){
        return gcdOfStrings(str1.slice(str2.length), str2);
    } else {
        return gcdOfStrings(str2.slice(str1.length), str1);
    }
};
 */
/*
 1071. Greatest Common Divisor of Strings
Easy
1.5K
309
Companies

For two strings s and t, we say "t divides s" if and only if s = t + ... + t (i.e., t is concatenated with itself one or more times).

Given two strings str1 and str2, return the largest string x such that x divides both str1 and str2.

 

Example 1:

Input: str1 = "ABCABC", str2 = "ABC"
Output: "ABC"

Example 2:

Input: str1 = "ABABAB", str2 = "ABAB"
Output: "AB"

Example 3:

Input: str1 = "LEET", str2 = "CODE"
Output: ""

 

Constraints:

    1 <= str1.length, str2.length <= 1000
    str1 and str2 consist of English uppercase letters.


 */