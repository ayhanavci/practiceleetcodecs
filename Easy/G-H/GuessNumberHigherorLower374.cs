﻿namespace PracticeLeetCodeCS.Easy;

/** 
 * Forward declaration of guess API.
 * @param  num   your guess
 * @return 	     -1 if num is higher than the picked number
 *			      1 if num is lower than the picked number
 *               otherwise return 0
 * int guess(int num);
 */

internal class GuessNumberHigherorLower374
{
    int guess(int num)
    {
        int pick = 1;
        if (num > pick) return -1;
        if (num < pick) return 1;
        return 0;
    }
    public int GuessNumber(int n)
    {
        long left = 1;
        long right = n;
        long pick = (left + right) / 2;
        while (right > left + 1)
        {
            int result = guess((int)pick);
            if (result == -1)            
                right = pick;            
            else if (result == 1)            
                left = pick;            
            else
                return (int)pick;
            pick = (left + right) / 2;
        }
        pick = guess((int)left);
        return pick == 0 ? (int)left : (int)right;
        
    }
    public void Test()
    {
        int pick = GuessNumber(2);
    }
}

/*
 374. Guess Number Higher or Lower
Easy

We are playing the Guess Game. The game is as follows:

I pick a number from 1 to n. You have to guess which number I picked.

Every time you guess wrong, I will tell you whether the number I picked is higher or lower than your guess.

You call a pre-defined API int guess(int num), which returns three possible results:

    -1: Your guess is higher than the number I picked (i.e. num > pick).
    1: Your guess is lower than the number I picked (i.e. num < pick).
    0: your guess is equal to the number I picked (i.e. num == pick).

Return the number that I picked.

 

Example 1:

Input: n = 10, pick = 6
Output: 6

Example 2:

Input: n = 1, pick = 1
Output: 1

Example 3:

Input: n = 2, pick = 1
Output: 1

 

Constraints:

    1 <= n <= 231 - 1
    1 <= pick <= n


 */