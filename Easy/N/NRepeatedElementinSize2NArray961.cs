﻿namespace PracticeLeetCodeCS.Easy;

internal class NRepeatedElementinSize2NArray961
{
    public int RepeatedNTimes(int[] nums)
    {                        
        Dictionary<int, int> occurances = new Dictionary<int, int>();
        for (int i = 0; i < nums.Length; i++)        
            if (occurances.ContainsKey(nums[i]))
                occurances[nums[i]]++;
            else
                occurances[nums[i]] = 1;

        for (int i = 0; i < occurances.Count; ++i)
            if (occurances.ElementAt(i).Value == nums.Length / 2)
                return occurances.ElementAt(i).Key;

        return -1;
    }
    public void Test()
    {
        //if (nums.Count(x => x == nums[i]) == nums.Length / 2)
        int[] nums = { 1, 2, 3, 3 };
        var retVal = RepeatedNTimes(nums);

        int[] nums2 = { 2, 1, 2, 5, 3, 2 };
        var retVal2 = RepeatedNTimes(nums2);

        int[] nums3 = { 5, 1, 5, 2, 5, 3, 5, 4 };
        var retVal3 = RepeatedNTimes(nums3);
    }
}

/*
 961. N-Repeated Element in Size 2N Array
Easy
1.1K
311
Companies

You are given an integer array nums with the following properties:

    nums.length == 2 * n.
    nums contains n + 1 unique elements.
    Exactly one element of nums is repeated n times.

Return the element that is repeated n times.

 

Example 1:

Input: nums = [1,2,3,3]
Output: 3

Example 2:

Input: nums = [2,1,2,5,3,2]
Output: 2

Example 3:

Input: nums = [5,1,5,2,5,3,5,4]
Output: 5

 

Constraints:

    2 <= n <= 5000
    nums.length == 2 * n
    0 <= nums[i] <= 104
    nums contains n + 1 unique elements and one of them is repeated exactly n times.


 */