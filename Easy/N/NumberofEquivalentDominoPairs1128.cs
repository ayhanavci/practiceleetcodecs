﻿namespace PracticeLeetCodeCS.Easy;

internal class NumberofEquivalentDominoPairs1128
{
    public int NumEquivDominoPairs(int[][] dominoes)
    {
        Dictionary<int, int> map = new Dictionary<int, int>();

        for (int i = 0; i < dominoes.Length; i++)
        {
            int number;
            if (dominoes[i][0] > dominoes[i][1])
            {
                number = dominoes[i][1] * 10;
                number += dominoes[i][0];
            }
            else
            {
                number = dominoes[i][0] * 10;
                number += dominoes[i][1];
            }
            if (map.ContainsKey(number)) map[number]++;
            else map[number] = 1;
        }

        int retVal = 0;
        for (int i = 0; i < map.Count; i++)
        {
            int val = map.ElementAt(i).Value;
            retVal += val * (val-1) / 2;
        }
        return retVal;
    }
    public void Test()
    {
        int[][] dominoes1 = new int[6][];
        dominoes1[0] = new int[] { 1,2 };
        dominoes1[1] = new int[] { 1,2 };
        dominoes1[2] = new int[] { 1,1 };
        dominoes1[3] = new int[] { 1,2 };
        dominoes1[4] = new int[] { 2,2 };
        dominoes1[5] = new int[] { 2,1 };


        int[][] dominoes2 = new int[4][];
        dominoes2[0] = new int[] { 1, 2 };
        dominoes2[1] = new int[] { 2, 1 };
        dominoes2[2] = new int[] { 3, 4 };
        dominoes2[3] = new int[] { 5, 6 };

        var retval = NumEquivDominoPairs(dominoes2);
    }
}

/*
 1128. Number of Equivalent Domino Pairs
Easy
562
283
Companies

Given a list of dominoes, dominoes[i] = [a, b] is equivalent to dominoes[j] = [c, d] if and only if either (a == c and b == d), or (a == d and b == c) - that is, one domino can be rotated to be equal to another domino.

Return the number of pairs (i, j) for which 0 <= i < j < dominoes.length, and dominoes[i] is equivalent to dominoes[j].

 

Example 1:

Input: dominoes = [[1,2],[2,1],[3,4],[5,6]]
Output: 1

Example 2:

Input: dominoes = [[1,2],[1,2],[1,1],[1,2],[2,2]]
Output: 3

 

Constraints:

    1 <= dominoes.length <= 4 * 104
    dominoes[i].length == 2
    1 <= dominoes[i][j] <= 9


 */
