﻿namespace PracticeLeetCodeCS.Easy;

internal class NumberofRecentCalls933
{
    public class RecentCounter
    {
        List<int> requests = new List<int>();
        public RecentCounter()
        {
            requests.Clear();
        }

        public int Ping(int t)
        {            
            int count = 1;            
            for (int i = 0; i < requests.Count; i++)
            {
                if (t - 3000 <= requests[i])
                    count++;
            }
            /*foreach (int req in requests)
            {
                if (t - 3000 <= req)
                    count++;
                else
                    requests.Remove(req);
            }*/
            requests.Add(t);
            return count;
        }
    }
    public void Test()
    {
        RecentCounter obj = new RecentCounter();
        int param_1 = obj.Ping(1);
        int param_2 = obj.Ping(100);
        int param_3 = obj.Ping(3001);
        int param_4 = obj.Ping(3002);
    }
    /**
 * Your RecentCounter object will be instantiated and called as such:
 * RecentCounter obj = new RecentCounter();
 * int param_1 = obj.Ping(t);
 */
}

/*
 933. Number of Recent Calls
Easy
881
2.6K
Companies

You have a RecentCounter class which counts the number of recent requests within a certain time frame.

Implement the RecentCounter class:

    RecentCounter() Initializes the counter with zero recent requests.
    int ping(int t) Adds a new request at time t, where t represents some time in milliseconds, and returns the number of requests that has happened in the past 3000 milliseconds (including the new request). Specifically, return the number of requests that have happened in the inclusive range [t - 3000, t].

It is guaranteed that every call to ping uses a strictly larger value of t than the previous call.

 

Example 1:

Input
["RecentCounter", "ping", "ping", "ping", "ping"]
[[], [1], [100], [3001], [3002]]
Output
[null, 1, 2, 3, 3]

Explanation
RecentCounter recentCounter = new RecentCounter();
recentCounter.ping(1);     // requests = [1], range is [-2999,1], return 1
recentCounter.ping(100);   // requests = [1, 100], range is [-2900,100], return 2
recentCounter.ping(3001);  // requests = [1, 100, 3001], range is [1,3001], return 3
recentCounter.ping(3002);  // requests = [1, 100, 3001, 3002], range is [2,3002], return 3

 

Constraints:

    1 <= t <= 109
    Each test case will call ping with strictly increasing values of t.
    At most 104 calls will be made to ping.


 */