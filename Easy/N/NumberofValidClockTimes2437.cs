namespace PracticeLeetCodeCS.Easy;

internal class NumberofValidClockTimes2437
{
    public int CountTime(string time) 
    {
        int count = 1;
        int hour1 = time[0] == '?' ? -1 : time[0] - '0';
        int hour2 = time[1] == '?' ? -1 : time[1] - '0';
        int minute1 = time[3] == '?' ? -1 : time[3] - '0';;
        int minute2 = time[4] == '?' ? -1 : time[4] - '0';;

        if (hour1 == -1)
        {            
            if (hour2 == -1 || hour2 < 4) count = 3;
            else count = 2;
        }
        if (hour2 == -1)
        {            
            if (hour1 == -1) count = 24;            
            else if (hour1 == 0 || hour1 == 1) count = 10;            
            else count = 3; //hour1 == 2                            
        }
        if (minute1 == -1)
        {
            count *= 6;
        }
        if (minute2 == -1)
        {
            count *= 10;
        }
        

        return count;
    }
    public void Test()
    {
        var retVal1 = CountTime("?5:00");
        var retVal2 = CountTime("0?:0?");
        var retVal3 = CountTime("??:??");
        var retVal4 = CountTime("2?:??"); //o 180 e 240
    }
}
/*
2437. Number of Valid Clock Times
Easy
194
182
Companies

You are given a string of length 5 called time, representing the current time on a digital clock in the format "hh:mm". The earliest possible time is "00:00" and the latest possible time is "23:59".

In the string time, the digits represented by the ? symbol are unknown, and must be replaced with a digit from 0 to 9.

Return an integer answer, the number of valid clock times that can be created by replacing every ? with a digit from 0 to 9.

 

Example 1:

Input: time = "?5:00"
Output: 2
Explanation: We can replace the ? with either a 0 or 1, producing "05:00" or "15:00". Note that we cannot replace it with a 2, since the time "25:00" is invalid. In total, we have two choices.

Example 2:

Input: time = "0?:0?"
Output: 100
Explanation: Each ? can be replaced by any digit from 0 to 9, so we have 100 total choices.

Example 3:

Input: time = "??:??"
Output: 1440
Explanation: There are 24 possible choices for the hours, and 60 possible choices for the minutes. In total, we have 24 * 60 = 1440 choices.

 

Constraints:

    time is a valid string of length 5 in the format "hh:mm".
    "00" <= hh <= "23"
    "00" <= mm <= "59"
    Some of the digits might be replaced with '?' and need to be replaced with digits from 0 to 9.


*/