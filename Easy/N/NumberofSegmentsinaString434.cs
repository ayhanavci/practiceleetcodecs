﻿namespace PracticeLeetCodeCS.Easy;

internal class NumberofSegmentsinaString434
{
    public int CountSegments(string s)
    {
        s = s.Trim();
        if (s.Length == 0) return 0;
        int wordCount = 0;
        
        for (int i = 0; i < s.Length; ++i)
        {
            if (s[i] == ' ')
                wordCount++;
            while (s[i] == ' ' && i < s.Length)
                ++i;
        }

        return wordCount + 1;
    }
    public void Test()
    {
        //string s = ", , , ,        a, eaefa";
        string s = "Hello, my name is John";
        int retVal = CountSegments(s);
    }
}
/*
 434. Number of Segments in a String
Easy

Given a string s, return the number of segments in the string.

A segment is defined to be a contiguous sequence of non-space characters.

 

Example 1:

Input: s = "Hello, my name is John"
Output: 5
Explanation: The five segments are ["Hello,", "my", "name", "is", "John"]

Example 2:

Input: s = "Hello"
Output: 1

 

Constraints:

    0 <= s.length <= 300
    s consists of lowercase and uppercase English letters, digits, or one of the following characters "!@#$%^&*()_+-=",.:".
    The only space character in s is " ".


 */