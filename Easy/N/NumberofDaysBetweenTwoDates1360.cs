﻿namespace PracticeLeetCodeCS.Easy;

//TODO çözümleri ezberle
internal class NumberofDaysBetweenTwoDates1360
{
    public int DaysBetweenDates(string date1, string date2)
    {
        /*DateTime datetime1 = DateTime.ParseExact(date1, "yyyy-MM-dd", null);
        DateTime datetime2 = DateTime.ParseExact(date2, "yyyy-MM-dd", null);

        return Math.Abs((int)(datetime1 - datetime2).Days);*/
        return Math.Abs((DateTime.ParseExact(date1, "yyyy-MM-dd", null) - DateTime.ParseExact(date2, "yyyy-MM-dd", null)).Days);

    }
    public void Test()
    {
        string date1 = "2020-01-15";
        string date2 = "2019-12-31";
        var retVal = DaysBetweenDates(date2, date1);
    }
}
/* Solution 2 with No Time-API:
   static int monthDays[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    public int daysBetweenDates(String date1, String date2) {
        int[] d1 = stringDateConvertor(date1);
        int[] d2 = stringDateConvertor(date2);
        
        int date1Days = d1[0] * 365 + d1[2]; 
  
        // Add number of days for the given month  
        for (int i = 0; i < d1[1] - 1; i++)  
        { 
            date1Days += monthDays[i]; 
        } 
  
        // Since each leap year has 366 days, 
        // Add an extra day for every leap year we meet. 
        date1Days += countLeapYear(d1[0], d1[1]); 
  
        int date2Days = d2[0] * 365 + d2[2]; 
        for (int i = 0; i < d2[1] - 1; i++) 
        { 
            date2Days += monthDays[i]; 
        } 
        date2Days += countLeapYear(d2[0], d2[1]); 
  
        // Return number of days between Two dates. 
        return Math.abs(date1Days - date2Days); 
    }
    
    public int[] stringDateConvertor(String date) {
        int[] dateTrans = new int[3];
        
        String[] d = date.split("-");
        dateTrans[0] = Integer.valueOf(d[0]);
        dateTrans[1] = Integer.valueOf(d[1]);
        dateTrans[2] = Integer.valueOf(d[2]);
        
        return dateTrans;
    }
    
    public int countLeapYear(int year, int month) {
        // If the current year not reach to Feb., then we do not need to consider it
        // for the count of leap years.
        if (month <= 2)  
        { 
            year--; 
        } 
  
        // A leap year is a multiple of 4, multiple of 400 BUT not a multiple of 100. 
        return year / 4 - year / 100 + year / 400; 
    }
 */
/*Solution 2.5: Write a Function to Count the day from year of 1971 
 public int daysBetweenDates(String date1, String date2) {    
			return Math.abs(countSince1971(date1) - countSince1971(date2));
    }

    public int countSince1971(String date) {
        int[] monthDays = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        String[] data = date.split("-");
        
        int year = Integer.parseInt(data[0]);
        int month = Integer.parseInt(data[1]);
        int day = Integer.parseInt(data[2]);
        
        for (int i = 1971; i < year; i++) {
            day += isALeapYear(i) ? 366 : 365;
        }
        for (int i = 1; i < month; i++) {
            if (isALeapYear(year) && i == 2) {
                day += 1;
            } 
            day += monthDays[i];
        }
        return day;
    }

    public boolean isALeapYear(int year) {
        return (year % 4 == 0 && year % 100 != 0) || year % 400 == 0;
    }
 */
/*
 1360. Number of Days Between Two Dates
Easy
262
1K
Companies

Write a program to count the number of days between two dates.

The two dates are given as strings, their format is YYYY-MM-DD as shown in the examples.

 

Example 1:

Input: date1 = "2019-06-29", date2 = "2019-06-30"
Output: 1

Example 2:

Input: date1 = "2020-01-15", date2 = "2019-12-31"
Output: 15

 

Constraints:

    The given dates are valid dates between the years 1971 and 2100.


 */