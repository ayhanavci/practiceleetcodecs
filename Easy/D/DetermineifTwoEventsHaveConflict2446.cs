namespace PracticeLeetCodeCS.Easy;

internal class DetermineifTwoEventsHaveConflict2446
{
    public bool HaveConflict(string[] event1, string[] event2) 
    {
        /*int event1StartHour = (event1[0][0] - '0') * 10 + (event1[0][1] - '0');
        int event1StartMinute = (event1[0][3] - '0') * 10 + (event1[0][4] - '0');
        int event1EndHour = (event1[1][0] - '0') * 10 + (event1[1][1] - '0');
        int event1EndMinute = (event1[1][3] - '0') * 10 + (event1[1][4] - '0');


        int event2StartHour = (event2[0][0] - '0') * 10 + (event2[0][1] - '0');
        int event2StartMinute = (event2[0][3] - '0') * 10 + (event2[0][4] - '0');
        int event2EndHour = (event2[1][0] - '0') * 10 + (event2[1][1] - '0');
        int event2EndMinute = (event2[1][3] - '0') * 10 + (event2[1][4] - '0');*/

        //e1...e1...e2...e2
        //e2...e2...e1...e1

        TimeOnly e1Start = TimeOnly.Parse(event1[0]);
        TimeOnly e1End = TimeOnly.Parse(event1[1]);
        TimeOnly e2Start = TimeOnly.Parse(event2[0]);
        TimeOnly e2End = TimeOnly.Parse(event2[1]);
        
        if (e2Start > e1End || e1Start > e2End)        
            return false;        
        return true;
    }
    public void Test()
    {
        string[] event1 = new string[2];
        string[] event2 = new string[2];

        event1[0] = "10:15";
        event1[1] = "11:00";
        event2[0] = "14:00";
        event2[1] = "15:00";


        var retVal1 = HaveConflict(event1, event2);
    }
}
/*
2446. Determine if Two Events Have Conflict
Easy
324
42
Companies

You are given two arrays of strings that represent two inclusive events that happened on the same day, event1 and event2, where:

    event1 = [startTime1, endTime1] and
    event2 = [startTime2, endTime2].

Event times are valid 24 hours format in the form of HH:MM.

A conflict happens when two events have some non-empty intersection (i.e., some moment is common to both events).

Return true if there is a conflict between two events. Otherwise, return false.

 

Example 1:

Input: event1 = ["01:15","02:00"], event2 = ["02:00","03:00"]
Output: true
Explanation: The two events intersect at time 2:00.

Example 2:

Input: event1 = ["01:00","02:00"], event2 = ["01:20","03:00"]
Output: true
Explanation: The two events intersect starting from 01:20 to 02:00.

Example 3:

Input: event1 = ["10:00","11:00"], event2 = ["14:00","15:00"]
Output: false
Explanation: The two events do not intersect.

 

Constraints:

    evnet1.length == event2.length == 2.
    event1[i].length == event2[i].length == 5
    startTime1 <= endTime1
    startTime2 <= endTime2
    All the event times follow the HH:MM format.


*/