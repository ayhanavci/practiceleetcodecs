using System.Text;
namespace PracticeLeetCodeCS.Easy;

internal class DeleteCharacterstoMakeFancyString1957
{
    //TODO: Slow 6.90%
    public string MakeFancyString(string s) 
    {  
        StringBuilder fancy = new StringBuilder(s);
        
        for (int i = 0; i < fancy.Length - 2; ++i)
        {
            if (fancy[i] == fancy[i+1] && fancy[i] == fancy[i+2])            
                fancy.Remove(i--, 1);                
            
        }
        return fancy.ToString();
    }
    public void Test()
    {
        var retVal1 = MakeFancyString("leeetcode");
        var retVal2 = MakeFancyString("aaabaaaa");
    }
}

/*
1957. Delete Characters to Make Fancy String
Easy
336
15
Companies

A fancy string is a string where no three consecutive characters are equal.

Given a string s, delete the minimum possible number of characters from s to make it fancy.

Return the final string after the deletion. It can be shown that the answer will always be unique.

 

Example 1:

Input: s = "leeetcode"
Output: "leetcode"
Explanation:
Remove an 'e' from the first group of 'e's to create "leetcode".
No three consecutive characters are equal, so return "leetcode".

Example 2:

Input: s = "aaabaaaa"
Output: "aabaa"
Explanation:
Remove an 'a' from the first group of 'a's to create "aabaaaa".
Remove two 'a's from the second group of 'a's to create "aabaa".
No three consecutive characters are equal, so return "aabaa".

Example 3:

Input: s = "aab"
Output: "aab"
Explanation: No three consecutive characters are equal, so return "aab".

 

Constraints:

    1 <= s.length <= 105
    s consists only of lowercase English letters.


*/