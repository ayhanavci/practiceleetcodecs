﻿namespace PracticeLeetCodeCS.Easy;
internal class DetermineColorofaChessboardSquare1812
{
    public bool SquareIsWhite(string coordinates)
    {
        int row = coordinates[0] - 'a';
        int col = coordinates[1] - '1';

        int startColor = row % 2; //0 black, 1 white
        
        int squareColor;
        if (startColor == 0) squareColor = col % 2;        
        else squareColor = (col % 2 == 0 ? 1 : 0);
        
        return squareColor == 1;
    }
    public void Test()
    {
        var retVal1 = SquareIsWhite("h1");
        var retVal2 = SquareIsWhite("h2");
        var retVal3 = SquareIsWhite("h3");
        var retVal4 = SquareIsWhite("h4");
        var retVal5 = SquareIsWhite("h5");
        var retVal6 = SquareIsWhite("h6");
        var retVal7 = SquareIsWhite("h7");
        var retVal8 = SquareIsWhite("h8");
    }
}

/*
 1812. Determine Color of a Chessboard Square
Easy
626
15
Companies

You are given coordinates, a string that represents the coordinates of a square of the chessboard. Below is a chessboard for your reference.

Return true if the square is white, and false if the square is black.

The coordinate will always represent a valid chessboard square. The coordinate will always have the letter first, and the number second.

 

Example 1:

Input: coordinates = "a1"
Output: false
Explanation: From the chessboard above, the square with coordinates "a1" is black, so return false.

Example 2:

Input: coordinates = "h3"
Output: true
Explanation: From the chessboard above, the square with coordinates "h3" is white, so return true.

Example 3:

Input: coordinates = "c7"
Output: false

 

Constraints:

    coordinates.length == 2
    'a' <= coordinates[0] <= 'h'
    '1' <= coordinates[1] <= '8'


 */