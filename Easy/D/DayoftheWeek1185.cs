﻿namespace PracticeLeetCodeCS.Easy;

//TODO düzelt
internal class DayoftheWeek1185
{
    string[] dayNames = { "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" };
    int[] daysInMonth = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
    
    public int DaysSinceStart(int year, int month, int day)
    {
        int numDays = 0;
        for (int i = 1971; i < year; ++i)
        {
            numDays += 365;
            numDays += HasLeapDay(i) ? 1 : 0;
        }
            

        for (int i = 1; i < month; ++i)
        {
            numDays += daysInMonth[i - 1];
            if (i == 2) numDays += HasLeapDay(i) ? 1 : 0;
        }
            

        numDays += day;
        
        return numDays;
    }
    public bool HasLeapDay(int year)
    {
        if (year % 400 == 0 || (year % 4 == 0 && year % 100 != 100))
            return true;
        return false;

    }
    public string DayOfTheWeek(int day, int month, int year)
    {
        int knownStart = DaysSinceStart(2023, 1, 9);//this day is monday
        int days = DaysSinceStart(year, month, day);            
        return dayNames[((days - knownStart) % 7 + 7) % 7];
    }

    /* dayNames[((days - knownStart) % 7 + 7) % 7]; can be written as;
    
     int i=(cnt-curr)%7;
    if(i<0){
    i=i+7;
    }
    return days[i];
     */
    public string DayOfTheWeek1(int day, int month, int year)
    {
        string[] daysInWeek = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" };
        int[] daysByMonthMod7 = { 0, 3, 2, 5, 0, 3, 5, 1, 4, 6, 2, 4 };
        if (month < 3) year -= 1;
        return daysInWeek[(year + (year / 4 - year / 100 + year / 400) + daysByMonthMod7[month - 1] + day) % 7]; // (year*365)%7 = year. Add the leap year days. Add extra month days. Add day.

        //int[] daysInMonth = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };        
    }
   
    bool isleapyear(int year)
    {
        return ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0));
    }
    public void Test()
    {
        var retVal1 = DayOfTheWeek(31, 8, 2019);
        var retVal2 = DayOfTheWeek(18, 7, 1999);
        var retVal3 = DayOfTheWeek(15, 8, 1993);
    }
}

/*
 class Solution {
		int[] months = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
		String[] days = {"Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday", "Monday"};
		public String dayOfTheWeek(int day, int month, int year) {
				int curr = daysFromStart(31, 12, 2019);
				int cnt = daysFromStart(day, month, year);
				return days[((cnt - curr) % 7 + 7) % 7];
		}

		private int daysFromStart(int day, int month, int year) {
				int sum = 0;
				for (int i = 1971; i < year; i++) {
						sum += 365;
						if (isLeapYear(i)) {
								sum++;
						}
				}
				for (int i = 1; i < month; i++) {
						sum += months[i - 1];
						if (i == 2 && isLeapYear(year)) {
								sum++;
						}
				}
				sum += day - 1;
				return sum;
		}

		private boolean isLeapYear(int year) {
				return (year % 4 == 0 && year % 100 != 0) || year % 400 == 0;
		}
 */
/*
 Python3 Solution With No Knowledge (Without knowing formulas or week day of 1/1/1971 beforehand)
magks
287
Sep 15, 2019

In an interview situation where perhaps it is only you and a whiteboard, it is helpful to be able to find the solution without
needing extra information than what you walked in with. This does assume knowledge of the number of days each month has.

Other solutions require knowing which day of the week fell on 1971/1/1 or one of the closed formulas like the Zeller or Sakamoto formulas.

This solution uses the current date (literally the day you sit down to solve the problem) to find the correct offset to a list of strings representings the days of the week, i.e. [ "Sunday", "Monday", ... ].

        def unknownStartDay(day, month, year):
		    def hasLeapDay(year):
			    return 1 if year % 4 == 0 and year % 100 != 0 or year % 400 == 0 else 0
		    
			dayNames = ["Saturday", "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday"]
            daysInMonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
            # days since 31, 12, 1970
            def daysSinceStart(day, month, year):
                numDays = 0
                for y in range(year - 1, 1970, -1):
                    numDays += 365 + hasLeapDay(y)
                numDays += sum(daysInMonth[:month-1])
                numDays += day 
                if month > 2:    
                    numDays += hasLeapDay(year)
                return numDays
			
            knownStart = daysSinceStart(14,9,2019)
            d = daysSinceStart(day, month, year) 
            return dayNames[ (d - knownStart) % 7]

In general, you can start the list of days with a known date. For example, if today when writing the solution is Day = 14, Month = 9, and Year = 2019 and you know the day name is "Saturday" then +1 day must be "Sunday" and likewise -1 day must be "Friday".

So start your list of day names with the known date of "today", e.g. "Saturday". Then, find the number of days since the start of the valid range for both your known date and the input date. Note, the code considers 1, 1, 1971 to be 1 day after the start and so on.

The difference will be the +1, -1, .. etc. offset that you are looking for, just use this difference mod 7 to correctly index the list of days.

This is not the most efficient but allows for a correctly working algorithm on a whiteboard without having extra knowledge (except the number of days in each month and the names of the days).
 */

/*
 Time: O(1)
Space: O(1)
Beats 100% runtime & 100% memory. Memory usage slightly less than Zeller Formula. Also more intuitive and easier to remember.

Java:

    public String dayOfTheWeek(int day, int month, int year) {
        String[] daysInWeek = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};
        int[] DaysByMonthMod7 = {0,3,2,5,0,3,5,1,4,6,2,4}; // Jan: 0, Feb: 31%7=3, Mar: 58%7=2, Apr: 89%7=5, etc
        if(month < 3) year -= 1; // 3
        return daysInWeek[(year + (year/4 - year/100 + year/400) + DaysByMonthMod7[month-1] + day) % 7]; // (year*365)%7 = year. Add the leap year days. Add extra month days. Add day.
    }

C++:

    string dayOfTheWeek(int day, int month, int year) {
        string daysInWeek [7] = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"}; // 1
        int DaysByMonthMod7[12] = {0,3,2,5,0,3,5,1,4,6,2,4}; // 2
        if(month < 3) year -= 1; // 3
        return daysInWeek[(year + (year/4 - year/100 + year/400) + DaysByMonthMod7[month-1] + day) % 7]; // 4
    }

Python3

    def dayOfTheWeek(self, day: int, month: int, year: int) -> str:
        daysInWeek = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"] #1
        DaysByMonthMod7 = [0,3,2,5,0,3,5,1,4,6,2,4]; # 2
        if(month < 3) : year = year - 1 # 3
        return daysInWeek[(year + (year//4 - year//100 + year//400) + DaysByMonthMod7[month-1] + day) % 7]; # 4

    1/1/1 is Monday, according to the Gregarian calendar. Use this to put Monday at index 1 for easier modulus manipulation during the return.

    If date is after February, reduce by 1 to account for leap day that'll be added later.

    DaysByMonthMod7 = {0, 31%7 = 3, 3+(28%7)-1 = 2, 2+(31%7) = 5, 5+(30%7) = 0, 0+(31%7) = 3, 3+(30%7) = 5, 5+(31%7) = 1, 1+(31%7) = 4, 4+(30%7) = 6, 6+(31%7) = 2, 2+(30%7) = 4};

    If date is before February, reduce year by 1 to cancel out the leap day we're adding in step 4.

    Add year because non-leap years have 365 days, and 365 % 7 = 1.
    Add (year/4 - year/100 + year/400) because these 3 integer divisions gives the floor, yielding the correct leap day. So if the most recent past February has a leap day, this yields 1. If not, this yields 0.

    Some examples and what they return:

    1 AD, Jan 1st: days = (0 + (0-0+0) + 0 + 1) % 7 = 1 => Monday
    3 AD, Jan 1st: days = (2 + (0-0+0) + 0 + 1) % 7 = 3 => Wednesday
    5 AD, Jan 1st: days = (4 + (1-0+0) + 0 + 1) % 7 = 6 => Saturday
    2003 Mar 7: days = (2003 + (500-20+5) + 2 + 7) % 7 = 5 => Friday
    2016 Feb 29: days = (2015 + (503-20+5) + 3 + 29) % 7 = 1 => Monday
    2019 Sep 14: days = (2019 + (504-20+5) + 4 + 14) % 7 = 6 => Saturday

 */
/*
 1185. Day of the Week
Easy
297
2.1K
Companies

Given a date, return the corresponding day of the week for that date.

The input is given as three integers representing the day, month and year respectively.

Return the answer as one of the following values {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"}.

 

Example 1:

Input: day = 31, month = 8, year = 2019
Output: "Saturday"

Example 2:

Input: day = 18, month = 7, year = 1999
Output: "Sunday"

Example 3:

Input: day = 15, month = 8, year = 1993
Output: "Sunday"

 

Constraints:

    The given dates are valid dates between the years 1971 and 2100.


 */