﻿using System.Security.Cryptography;

namespace PracticeLeetCodeCS.Easy;

internal class DesignHashSet705
{
    public class MyHashSet
    {
        double A = 0.6112312;
        int keyCount = 10001;
        List<int>[] nums;
        public MyHashSet()
        {
            nums = new List<int>[keyCount];
        }
        public int Hash(int key)
        {
            return (int)Math.Floor(((key * A) - Math.Truncate(key * A)) * keyCount);
        }
        public void Add(int key)
        {
            int hashIndex = Hash(key);
            var items = nums[hashIndex];
            if (items == null) items = new List<int>();
            if (!items.Contains(key))
            {
                items.Add(key);
                nums[hashIndex] = items;
            }
        }

        public void Remove(int key)
        {
            var items = nums[Hash(key)];
            if (items == null) return;
            items.Remove(key);
        }

        public bool Contains(int key)
        {
            var items = nums[Hash(key)];
            if (items == null) return false;
            return items.Contains(key);
        }
    }

    /**
     * Your MyHashSet object will be instantiated and called as such:
     * MyHashSet obj = new MyHashSet();
     * obj.Add(key);
     * obj.Remove(key);
     * bool param_3 = obj.Contains(key);
     */
    public void Test()
    {
        MyHashSet myHashSet = new MyHashSet();
        myHashSet.Add(1);      // set = [1]
        myHashSet.Add(2);      // set = [1, 2]
        var retVal = myHashSet.Contains(1); // return True
        retVal = myHashSet.Contains(3); // return False, (not found)
        myHashSet.Add(2);      // set = [1, 2]
        retVal = myHashSet.Contains(2); // return True
        myHashSet.Remove(2);   // set = [1]
        retVal = myHashSet.Contains(2); // return False, (already removed)
    }
}
/*
https://www.cs.miami.edu/home/odelia/teaching/csc317.sp15/syllabus/Algorithms5cSlides.pdf
https://www.cs.miami.edu/home/odelia/teaching/csc317.sp15/syllabus/Algorithms5bSlides.pdf
 Multiplication method
(1) Choose constant
(2) Multiply key k by A
(3) Extract fractional part of k A (this gives
us a number between 0 and 1)
(4) Multiply fractional part by m and take
floor of the multiplication (this transforms
a number between 0 and 1, to a discrete
number between 0 and m-1 that we can
map to slot in the hash table)
 public int Hash(int key)
        {
            double step1 = key * A;
            double step2 = step1 - Math.Truncate(step1);
            double step3 = step2 * keyCount;
            double step4 = Math.Floor(step3);
            return (int)step4;
        }
 
 */
/* Knuth"s Method
 std::uint32_t knuth(int x, int p) {
    assert(p >= 0 && p <= 32);

    const std::uint32_t knuth = 2654435769;
    const std::uint32_t y = x;
    return (y * knuth) >> (32 - p);
}

public long hash(int key) {
    long l = 2654435769L;
    return (key * l >> 32) % N ;
}
 */

/*
 705. Design HashSet
Easy
2.5K
233
Companies

Design a HashSet without using any built-in hash table libraries.

Implement MyHashSet class:

    void add(key) Inserts the value key into the HashSet.
    bool contains(key) Returns whether the value key exists in the HashSet or not.
    void remove(key) Removes the value key in the HashSet. If key does not exist in the HashSet, do nothing.

 

Example 1:

Input
["MyHashSet", "add", "add", "contains", "contains", "add", "contains", "remove", "contains"]
[[], [1], [2], [1], [3], [2], [2], [2], [2]]
Output
[null, null, null, true, false, null, true, null, false]

Explanation
MyHashSet myHashSet = new MyHashSet();
myHashSet.add(1);      // set = [1]
myHashSet.add(2);      // set = [1, 2]
myHashSet.contains(1); // return True
myHashSet.contains(3); // return False, (not found)
myHashSet.add(2);      // set = [1, 2]
myHashSet.contains(2); // return True
myHashSet.remove(2);   // set = [1]
myHashSet.contains(2); // return False, (already removed)

 

Constraints:

    0 <= key <= 106
    At most 104 calls will be made to add, remove, and contains.


 */