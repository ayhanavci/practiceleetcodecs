﻿using System.Text;

namespace PracticeLeetCodeCS.Easy;

internal class DefanginganIPAddress1108
{
    public string DefangIPaddr(string address)
    {
        StringBuilder retVal = new StringBuilder();
        
        for (int i = 0; i < address.Length; i++)
        {
            if (address[i] == '.')            
                retVal.Append("[.]");
            else
                retVal.Append(address[i]);            
        }
        return retVal.ToString();
    }
    public void Test()
    {
        string address = "255.100.50.0";
        var retVal = DefangIPaddr(address);
    }
}

/*
 1108. Defanging an IP Address
Easy
1.5K
1.6K
Companies

Given a valid (IPv4) IP address, return a defanged version of that IP address.

A defanged IP address replaces every period "." with "[.]".

 

Example 1:

Input: address = "1.1.1.1"
Output: "1[.]1[.]1[.]1"

Example 2:

Input: address = "255.100.50.0"
Output: "255[.]100[.]50[.]0"

 

Constraints:

    The given address is a valid IPv4 address.

 */