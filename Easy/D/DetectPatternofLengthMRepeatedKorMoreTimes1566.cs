﻿namespace PracticeLeetCodeCS.Easy;

internal class DetectPatternofLengthMRepeatedKorMoreTimes1566
{
    public bool ContainsPattern(int[] arr, int m, int k)
    {
        int[] control = new int[m];        
        
        for (int i = 0; i < arr.Length; i++)
        {
            if ((m * k) + i > arr.Length)
                return false;
            int j = 0;
            for (j = 0; j < m; ++j)
            {
                if (i + j >= arr.Length) return false;
                control[j] = arr[i+j];                
            }
            int cursorPos = i + j;
            bool fStop = false;
            int repetation = 1;
            for (; repetation < k; ++repetation)
            {
                cursorPos = (repetation * m) + i;
                for (int t = 0; t <m; ++t)
                {
                    if (control[t] != arr[cursorPos + t])
                    {          
                        fStop = true;
                        break;
                    }
                }
                if (fStop) break;
            }
            if (repetation == k)
                return true;
        }

        return false;
    }
    public void Test()
    {
        int[] arr1 = new int[] { 1, 2, 4, 4, 4 };
        var retVal1 = ContainsPattern(arr1, 1, 3);

        int[] arr2 = new int[] { 7, 1, 2, 3, 1, 2, 1, 1, 1, 1, 3 };
        var retVal2 = ContainsPattern(arr2, 3, 2);

        int[] arr3 = new int[] { 1, 2, 1, 2, 1, 3 };
        var retVal3 = ContainsPattern(arr3, 2, 3);

    }
}

/*
 1566. Detect Pattern of Length M Repeated K or More Times
Easy
564
110
Companies

Given an array of positive integers arr, find a pattern of length m that is repeated k or more times.

A pattern is a subarray (consecutive sub-sequence) that consists of one or more values, repeated multiple times consecutively without overlapping. A pattern is defined by its length and the number of repetitions.

Return true if there exists a pattern of length m that is repeated k or more times, otherwise return false.

 

Example 1:

Input: arr = [1,2,4,4,4,4], m = 1, k = 3
Output: true
Explanation: The pattern (4) of length 1 is repeated 4 consecutive times. Notice that pattern can be repeated k or more times but not less.

Example 2:

Input: arr = [1,2,1,2,1,1,1,3], m = 2, k = 2
Output: true
Explanation: The pattern (1,2) of length 2 is repeated 2 consecutive times. Another valid pattern (2,1) is also repeated 2 times.

Example 3:

Input: arr = [1,2,1,2,1,3], m = 2, k = 3
Output: false
Explanation: The pattern (1,2) is of length 2 but is repeated only 2 times. There is no pattern of length 2 that is repeated 3 or more times.

 

Constraints:

    2 <= arr.length <= 100
    1 <= arr[i] <= 100
    1 <= m <= 100
    2 <= k <= 100


 */