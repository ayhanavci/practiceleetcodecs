﻿namespace PracticeLeetCodeCS.Easy;

internal class DiameterofBinaryTree543
{
    //Definition for a binary tree node.
    public class TreeNode
    {
        public int val;
        public TreeNode left;
        public TreeNode right;
        public TreeNode(int val = 0, TreeNode left = null, TreeNode right = null)
        {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }
    int diameter = 0;
    public int DiameterOfBinaryTree(TreeNode root)
    {
        GetDepth(root);
        return diameter;        

    }
    public int GetDepth(TreeNode root)
    {
        if (root == null) return 0;

        int leftDepth = GetDepth(root.left);
        int rightDepth = GetDepth(root.right);

        diameter = Math.Max(diameter, leftDepth + rightDepth);

        return Math.Max(leftDepth, rightDepth) + 1;
    }
    public void Test()
    {
        /*TreeNode right_right_right_right = new TreeNode(10);

        TreeNode right_right_left = new TreeNode(7, null, null);
        TreeNode right_right_right = new TreeNode(8, null, right_right_right_right);

        TreeNode right_left_left_right_left = new TreeNode(11, null, null);

        TreeNode right_left_left_right = new TreeNode(9, right_left_left_right_left, null);
        TreeNode right_left_left = new TreeNode(6, null, right_left_left_right);

        TreeNode right_left = new TreeNode(4, right_left_left, null);
        TreeNode right_right = new TreeNode(5, right_right_left, right_right_right);
        
        TreeNode left = new TreeNode(2);
        TreeNode right = new TreeNode(3, right_left, right_right);
        TreeNode root = new TreeNode(1, left, right);*/

        TreeNode root = new TreeNode(1, 
            new TreeNode(2, null, null),
            new TreeNode(3,
                new TreeNode(4, 
                    new TreeNode(6, null, 
                        new TreeNode(9, 
                            new TreeNode(11, 
                                new TreeNode(12), null), 
                            null)), 
                        null),
                new TreeNode(5, 
                    new TreeNode(7), 
                    new TreeNode(8, null, 
                        new TreeNode(10))))
        );

        int retVal = DiameterOfBinaryTree(root);
    }
}

/*
 543. Diameter of Binary Tree
Easy

Given the root of a binary tree, return the length of the diameter of the tree.

The diameter of a binary tree is the length of the longest path between any two nodes in a tree. This path may or may not pass through the root.

The length of a path between two nodes is represented by the number of edges between them.

 

Example 1:

Input: root = [1,2,3,4,5]
Output: 3
Explanation: 3 is the length of the path [4,2,1,3] or [5,2,1,3].

Example 2:

Input: root = [1,2]
Output: 1

 

Constraints:

    The number of nodes in the tree is in the range [1, 104].
    -100 <= Node.val <= 100


 */