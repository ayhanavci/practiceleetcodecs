﻿namespace PracticeLeetCodeCS.Easy;

internal class DIStringMatch942
{
    public int[] DiStringMatch(string s)
    {
        int[] retVal = new int[s.Length + 1];                
        int high = s.Length;
        int low = 0;
        for (int i = 0; i < s.Length; ++i)        
            retVal[i] = s[i] == 'I' ? low++ : high--;        

        retVal[s.Length] = low;
        return retVal;
    }
    public void Test()
    {
        var retVal1 = DiStringMatch("IDID");
        var retVal2 = DiStringMatch("III");
        var retVal3 = DiStringMatch("DDI");
    }
}

/*
 942. DI String Match
Easy
2K
811
Companies

A permutation perm of n + 1 integers of all the integers in the range [0, n] can be represented as a string s of length n where:

    s[i] == "I" if perm[i] < perm[i + 1], and
    s[i] == "D" if perm[i] > perm[i + 1].

Given a string s, reconstruct the permutation perm and return it. If there are multiple valid permutations perm, return any of them.

 

Example 1:

Input: s = "IDID"
Output: [0,4,1,3,2]

Example 2:

Input: s = "III"
Output: [0,1,2,3]

Example 3:

Input: s = "DDI"
Output: [3,2,0,1]

 

Constraints:

    1 <= s.length <= 105
    s[i] is either "I" or "D".


 */