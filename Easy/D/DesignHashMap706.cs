﻿namespace PracticeLeetCodeCS.Easy;

internal class DesignHashMap706
{
    public class MyHashMap
    {
        double A = 0.6112312;
        int keyCount = 10001;
        Dictionary<int, int>[] nums;
        public MyHashMap()
        {
            nums = new Dictionary<int, int>[keyCount];
        }
        public int Hash(int key)
        {
            return (int)Math.Floor(((key * A) - Math.Truncate(key * A)) * keyCount);
        }
        public void Put(int key, int value)
        {
            int hashIndex = Hash(key);
            var map = nums[hashIndex];
            if (map == null) map = new Dictionary<int, int>();
            map[key] = value;
            nums[hashIndex] = map;
        }

        public int Get(int key)
        {
            var map = nums[Hash(key)];
            if (map == null || !map.ContainsKey(key)) return -1;            
            return map[key];            
        }

        public void Remove(int key)
        {
            var map = nums[Hash(key)];
            if (map == null || !map.ContainsKey(key)) return;
            map.Remove(key);
        }
    }

    /**
     * Your MyHashMap object will be instantiated and called as such:
     * MyHashMap obj = new MyHashMap();
     * obj.Put(key,value);
     * int param_2 = obj.Get(key);
     * obj.Remove(key);
     */
    public void Test()
    {
        MyHashMap myHashMap = new MyHashMap();
        myHashMap.Put(1, 1); // The map is now [[1,1]]
        myHashMap.Put(2, 2); // The map is now [[1,1], [2,2]]
        var retVal = myHashMap.Get(1);    // return 1, The map is now [[1,1], [2,2]]
        retVal = myHashMap.Get(3);    // return -1 (i.e., not found), The map is now [[1,1], [2,2]]
        myHashMap.Put(2, 1); // The map is now [[1,1], [2,1]] (i.e., update the existing value)
        retVal = myHashMap.Get(2);    // return 1, The map is now [[1,1], [2,1]]
        myHashMap.Remove(2); // remove the mapping for 2, The map is now [[1,1]]
        retVal = myHashMap.Get(2);    // return -1 (i.e., not found), The map is now [[1,1]]
    }
}

/*
 706. Design HashMap
Easy
3.8K
345
Companies

Design a HashMap without using any built-in hash table libraries.

Implement the MyHashMap class:

    MyHashMap() initializes the object with an empty map.
    void put(int key, int value) inserts a (key, value) pair into the HashMap. If the key already exists in the map, update the corresponding value.
    int get(int key) returns the value to which the specified key is mapped, or -1 if this map contains no mapping for the key.
    void remove(key) removes the key and its corresponding value if the map contains the mapping for the key.

 

Example 1:

Input
["MyHashMap", "put", "put", "get", "get", "put", "get", "remove", "get"]
[[], [1, 1], [2, 2], [1], [3], [2, 1], [2], [2], [2]]
Output
[null, null, null, 1, -1, null, 1, null, -1]

Explanation
MyHashMap myHashMap = new MyHashMap();
myHashMap.put(1, 1); // The map is now [[1,1]]
myHashMap.put(2, 2); // The map is now [[1,1], [2,2]]
myHashMap.get(1);    // return 1, The map is now [[1,1], [2,2]]
myHashMap.get(3);    // return -1 (i.e., not found), The map is now [[1,1], [2,2]]
myHashMap.put(2, 1); // The map is now [[1,1], [2,1]] (i.e., update the existing value)
myHashMap.get(2);    // return 1, The map is now [[1,1], [2,1]]
myHashMap.remove(2); // remove the mapping for 2, The map is now [[1,1]]
myHashMap.get(2);    // return -1 (i.e., not found), The map is now [[1,1]]

 

Constraints:

    0 <= key, value <= 106
    At most 104 calls will be made to put, get, and remove.


 */