using System.Text;
namespace PracticeLeetCodeCS.Easy;

internal class DecodetheMessage2325
{
    public string DecodeMessage(string key, string message) 
    {
        List<char> table = new List<char>();

        for (int i = 0; i < key.Length; ++i)
        {
            if (key[i] != ' ' && table.IndexOf(key[i]) == -1)
                table.Add(key[i]);
        }
        StringBuilder decodedMsg = new StringBuilder();

        for (int i = 0; i < message.Length; ++i)
        {   
            if (message[i] == ' ') decodedMsg.Append(' ');
            else decodedMsg.Append((char)('a' + table.IndexOf(message[i])));
        }
        return decodedMsg.ToString();
    }
    public void Test()
    {

    }
}
/*
2325. Decode the Message
Easy
693
73
Companies

You are given the strings key and message, which represent a cipher key and a secret message, respectively. The steps to decode message are as follows:

    Use the first appearance of all 26 lowercase English letters in key as the order of the substitution table.
    Align the substitution table with the regular English alphabet.
    Each letter in message is then substituted using the table.
    Spaces ' ' are transformed to themselves.

    For example, given key = "happy boy" (actual key would have at least one instance of each letter in the alphabet), we have the partial substitution table of ('h' -> 'a', 'a' -> 'b', 'p' -> 'c', 'y' -> 'd', 'b' -> 'e', 'o' -> 'f').

Return the decoded message.

 

Example 1:

Input: key = "the quick brown fox jumps over the lazy dog", message = "vkbs bs t suepuv"
Output: "this is a secret"
Explanation: The diagram above shows the substitution table.
It is obtained by taking the first appearance of each letter in "the quick brown fox jumps over the lazy dog".

Example 2:

Input: key = "eljuxhpwnyrdgtqkviszcfmabo", message = "zwx hnfx lqantp mnoeius ycgk vcnjrdb"
Output: "the five boxing wizards jump quickly"
Explanation: The diagram above shows the substitution table.
It is obtained by taking the first appearance of each letter in "eljuxhpwnyrdgtqkviszcfmabo".

 

Constraints:

    26 <= key.length <= 2000
    key consists of lowercase English letters and ' '.
    key contains every letter in the English alphabet ('a' to 'z') at least once.
    1 <= message.length <= 2000
    message consists of lowercase English letters and ' '.


*/