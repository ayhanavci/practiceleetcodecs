﻿using System.Text;

namespace PracticeLeetCodeCS.Easy;

internal class DecryptStringfromAlphabettoIntegerMapping1309
{
    public string FreqAlphabets(string s)
    {
        StringBuilder retVal = new StringBuilder();
        
        for (int i = 0; i < s.Length; i++)
        {            
            if (s[i] > '2' && s[i] <= '9')
            {
                retVal.Append((char)(s[i] - '0' + 'a' - 1));
            }
            else if (i + 2 < s.Length && s[i + 2] == '#')
            {                
                int num = ((s[i] - '0') * 10);
                num += (s[i + 1] - '0');
                char ch = (char)(num + 'a' - 1);
                retVal.Append(ch);
                i += 2;
            }
            else
            {
                retVal.Append((char)(s[i] - '0' + 'a' - 1));
            }
        }

        return retVal.ToString();
    }
    public void Test()
    {
        //char ch = (char) ('4' - '1');
        //ch += 'a';
        string s = "10#11#12";
        var retVal = FreqAlphabets(s);
    }
}

/*
 1309. Decrypt String from Alphabet to Integer Mapping
Easy
1.3K
86
Companies

You are given a string s formed by digits and '#'. We want to map s to English lowercase characters as follows:

    Characters ('a' to 'i') are represented by ('1' to '9') respectively.
    Characters ('j' to 'z') are represented by ('10#' to '26#') respectively.

Return the string formed after mapping.

The test cases are generated so that a unique mapping will always exist.

 

Example 1:

Input: s = "10#11#12"
Output: "jkab"
Explanation: "j" -> "10#" , "k" -> "11#" , "a" -> "1" , "b" -> "2".

Example 2:

Input: s = "1326#"
Output: "acz"

 

Constraints:

    1 <= s.length <= 1000
    s consists of digits and the '#' letter.
    s will be a valid string such that mapping is always possible.


 */