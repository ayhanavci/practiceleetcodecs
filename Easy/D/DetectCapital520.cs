﻿namespace PracticeLeetCodeCS.Easy;

internal class DetectCapital520
{
    public bool DetectCapitalUse(string word)
    {
        bool isUpper = true;        
        bool isLower = true;

        for (int i = 0; i < word.Length; i++)
        {
            if (word[i] >= 'a')
                isUpper = false;
            
            if (i != 0 && word[i] < 'a')
                isLower = false;

            if (!isLower && !isUpper)
                return false;
        }
        return isUpper || isLower;
    }
    public void Test()
    {
        int ch1 = 'a';
        int ch2 = 'A';
        var retVal = DetectCapitalUse("USA");
        retVal = DetectCapitalUse("flag");
        retVal = DetectCapitalUse("Flag");
        retVal = DetectCapitalUse("FlaG");
    }
}

/*
 520. Detect Capital
Easy

We define the usage of capitals in a word to be right when one of the following cases holds:

    All letters in this word are capitals, like "USA".
    All letters in this word are not capitals, like "leetcode".
    Only the first letter in this word is capital, like "Google".

Given a string word, return true if the usage of capitals in it is right.

 

Example 1:

Input: word = "USA"
Output: true

Example 2:

Input: word = "FlaG"
Output: false

 

Constraints:

    1 <= word.length <= 100
    word consists of lowercase and uppercase English letters.


 */