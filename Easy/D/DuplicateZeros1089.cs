﻿namespace PracticeLeetCodeCS.Easy;

internal class DuplicateZeros1089
{
    public void DuplicateZeros(int[] arr)
    {
        List<int> tmp = new List<int>();
        for (int i = 0; i < arr.Length; i++)
        {
            if (arr[i] == 0)
            {
                tmp.Add(0);
                tmp.Add(0);
            }
            else 
                tmp.Add(arr[i]);
        }
        for (int i = 0; i < arr.Length; i++)        
            arr[i] = tmp[i];
        
    }
    public void Test()
    {
        int[] arr1 = { 1, 0, 2, 3, 0, 4, 5, 0 };
        DuplicateZeros(arr1);   
        
        int[] arr2 = { 1, 2, 3 };
        DuplicateZeros(arr2);
    }
}

/*
 1089. Duplicate Zeros
Easy
2.1K
637
Companies

Given a fixed-length integer array arr, duplicate each occurrence of zero, shifting the remaining elements to the right.

Note that elements beyond the length of the original array are not written. Do the above modifications to the input array in place and do not return anything.

 

Example 1:

Input: arr = [1,0,2,3,0,4,5,0]
Output: [1,0,0,2,3,0,0,4]
Explanation: After calling your function, the input array is modified to: [1,0,0,2,3,0,0,4]

Example 2:

Input: arr = [1,2,3]
Output: [1,2,3]
Explanation: After calling your function, the input array is modified to: [1,2,3]

 

Constraints:

    1 <= arr.length <= 104
    0 <= arr[i] <= 9


 */