namespace PracticeLeetCodeCS.Easy;

internal class DetermineWhetherMatrixCanBeObtainedByRotation1886
{
    public bool FindRotation(int[][] mat, int[][] target) 
    {
        //Check if they are the same
        if (AreEqual(mat, target)) return true;
        
        int rotationCount = 3; //90, 180, 270

        while (rotationCount-- > 0) 
        {
            //Transpose of matrix.
            for (int i = 0; i < mat.Length; ++i) 
            {
                for (int j = i; j < mat.Length; ++j)
                {
                    int tmp = mat[i][j];
                    mat[i][j] = mat[j][i];
                    mat[j][i] = tmp;
                }
            }

            //Reverse each row
            for (int i = 0; i < mat.Length; ++i) 
            {
                for (int j = 0; j < mat.Length / 2; ++j)
                {
                     int tmp = mat[i][j];
                     mat[i][j] = mat[i][mat.Length - 1 - j];
                     mat[i][mat.Length - 1 - j] = tmp;
                }
            }
            //Check if they are the same
             if (AreEqual(mat, target)) return true;
        }
        return false;
    }
    private bool AreEqual(int[][] mat, int[][] target)
    {
        for (int i = 0; i < mat.Length; ++i) 
            {
                for (int j = 0; j < mat.Length; ++j)
                {
                    if (mat[i][j] != target[i][j])
                        return false;                
                }
            }
        return true;
    }
    public void Test()
    {

    }
}
/*
We need to check for the 0, 90, 180, 270 degree's only.

0 degree - If given matrix is same as target
90 degree - rotate matrix
180 degree - rotate previous 90 degree more 90 degree to get 180 degree rotation.
270 degree - rotate previous 180 degree more 90 degree to get 270 degree rotation.

How rotation Works?

let the matrix be-
0 1 1
0 0 1
1 0 0

first take the transpose(rows become columns and vice versa) ie. -
0 0 1
1 0 0
1 1 0

now reverse each row ie-
1 0 0
0 0 1
0 1 1

Now you can see that, this resulted matix is the 90 degree rotation of the original matrix. We need to do this 2 more time to check for 180, 270 degree's.
*/
/*
1886. Determine Whether Matrix Can Be Obtained By Rotation
Easy
1.1K
92
Companies

Given two n x n binary matrices mat and target, return true if it is possible to make mat equal to target by rotating mat in 90-degree increments, or false otherwise.

 

Example 1:

Input: mat = [[0,1],[1,0]], target = [[1,0],[0,1]]
Output: true
Explanation: We can rotate mat 90 degrees clockwise to make mat equal target.

Example 2:

Input: mat = [[0,1],[1,1]], target = [[1,0],[0,1]]
Output: false
Explanation: It is impossible to make mat equal to target by rotating mat.

Example 3:

Input: mat = [[0,0,0],[0,1,0],[1,1,1]], target = [[1,1,1],[0,1,0],[0,0,0]]
Output: true
Explanation: We can rotate mat 90 degrees clockwise two times to make mat equal target.

 

Constraints:

    n == mat.length == target.length
    n == mat[i].length == target[i].length
    1 <= n <= 10
    mat[i][j] and target[i][j] are either 0 or 1.


*/