﻿namespace PracticeLeetCodeCS.Easy;

internal class DegreeofanArray697
{
    public int FindShortestSubArray(int[] nums)
    {
        Dictionary<int, int> firstIndex = new Dictionary<int, int>();
        Dictionary<int, int> lastIndex = new Dictionary<int, int>();
        Dictionary<int, int> count = new Dictionary<int, int>();

        for (int i = 0; i < nums.Length; i++) 
        {
            int number = nums[i];
            if (!firstIndex.ContainsKey(number))            
                firstIndex[number] = i;
            lastIndex[number] = i;
            if (!count.ContainsKey(number))
                count[number] = 1;
            else
                count[number]++;
        }

        int min = nums.Length;
        int degree = count.Values.Max();
        for (int i = 0; i < count.Count; ++i)
        {
            var number = count.ElementAt(i).Key;
            var numberCount = count.ElementAt(i).Value;

            if (numberCount == degree)
            {
                min = Math.Min(min, lastIndex[number] - firstIndex[number] + 1);
            }
        }
        return min;
    }
    public record info
    {        
        public int count;
        public int firstIndex;
        public int lastIndex;
    }
    public int FindShortestSubArrayTimeout(int[] nums)
    {                
        Dictionary<int, info> allItems = new Dictionary<int, info>();
        info currentItem;

        for (int i = 0; i < nums.Length; i++)
        {
            
            if (allItems.ContainsKey(nums[i]))
            {
                currentItem = allItems[nums[i]];
                currentItem.lastIndex = i;
                currentItem.count++;
            }
            else
            {
                currentItem = new info();
                currentItem.count = 1;
                currentItem.firstIndex = i;
                currentItem.lastIndex = i;
                allItems[nums[i]] = currentItem;
            }           
        }

        Dictionary<int, info> degrees = new Dictionary<int, info>();
      

        for (int i = 0; i < allItems.Count; i++)
        {
            currentItem = allItems.ElementAt(i).Value;
            int currentNumber = allItems.ElementAt(i).Key;
            if (degrees.Count == 0)
            {
                degrees[currentNumber] = currentItem;
            }
            else
            {
                for (int j = 0; j < degrees.Count; ++j)
                {
                    var degree = degrees.ElementAt(j).Value;                    
                    if (currentItem.count > degree.count)
                    {
                        degrees.Clear();
                        degrees[currentNumber] = currentItem;
                        break;
                    }
                    else if (currentItem.count == degree.count)
                    {
                        degrees[currentNumber] = currentItem;
                    }
                }
            }
            
        }             

        
        int min = 50001;
        for (int j = 0; j < degrees.Count; ++j)
        {
            var degree = degrees.ElementAt(j).Value;
            min = Math.Min(min, degree.lastIndex - degree.firstIndex + 1);
        }
        return min;
    }
    public void Test()
    {
        int[] nums = { 1, 2, 2, 3, 1, 4, 2 };
        //int[] nums = { 1, 2, 2, 3, 1 };
        var retVal = FindShortestSubArray(nums);
    }
}


/*
 697. Degree of an Array
Easy
2.5K
1.5K
Companies

Given a non-empty array of non-negative integers nums, the degree of this array is defined as the maximum frequency of any one of its elements.

Your task is to find the smallest possible length of a (contiguous) subarray of nums, that has the same degree as nums.

 

Example 1:

Input: nums = [1,2,2,3,1]
Output: 2
Explanation: 
The input array has a degree of 2 because both elements 1 and 2 appear twice.
Of the subarrays that have the same degree:
[1, 2, 2, 3, 1], [1, 2, 2, 3], [2, 2, 3, 1], [1, 2, 2], [2, 2, 3], [2, 2]
The shortest length is 2. So return 2.

Example 2:

Input: nums = [1,2,2,3,1,4,2]
Output: 6
Explanation: 
The degree is 3 because the element 2 is repeated 3 times.
So [2,2,3,1,4,2] is the shortest subarray, therefore returning 6.

 

Constraints:

    nums.length will be between 1 and 50,000.
    nums[i] will be an integer between 0 and 49,999.


 */