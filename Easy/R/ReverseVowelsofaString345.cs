﻿using System.Text;

namespace PracticeLeetCodeCS.Easy;

internal class ReverseVowelsofaString345
{
    public string ReverseVowels(string s)
    {
        StringBuilder reversedString = new StringBuilder(s);
        int lastBackIndex = s.Length;
        for (int i = 0; i < s.Length; i++)
        {
            if (s[i] == 'a' || s[i] == 'e' || s[i] == 'i' || s[i] == 'o' || s[i] == 'u' ||
                s[i] == 'A' || s[i] == 'E' || s[i] == 'I' || s[i] == 'O' || s[i] == 'U')
            {
                for (int j = lastBackIndex - 1; j > i; j--)
                {
                    if (s[j] == 'a' || s[j] == 'e' || s[j] == 'i' || s[j] == 'o' || s[j] == 'u' ||
                        s[j] == 'A' || s[j] == 'E' || s[j] == 'I' || s[j] == 'O' || s[j] == 'U')
                    {
                        char tmp = s[j];
                        reversedString.Replace(s[j], s[i], j, 1);
                        reversedString.Replace(s[i], tmp, i, 1);
                        lastBackIndex = j;
                        break;
                    }
                    
                }
            }            
        }
        return reversedString.ToString();
    }
    public void Test()
    {
        //string s = 'okkkkkkkkkkake';
        string s = "aA";
        //string s = 'leetcode';
        var retVal = ReverseVowels(s);
    }
}

/*
 345. Reverse Vowels of a String
Easy

Given a string s, reverse only all the vowels in the string and return it.

The vowels are 'a', 'e', 'i', 'o', and 'u', and they can appear in both lower and upper cases, more than once.

 

Example 1:

Input: s = 'hello'
Output: 'holle'

Example 2:

Input: s = 'leetcode'
Output: 'leotcede'

 

Constraints:

    1 <= s.length <= 3 * 105
    s consist of printable ASCII characters.


 */