﻿using System.Text;

namespace PracticeLeetCodeCS.Easy;

internal class RemoveAllAdjacentDuplicatesInString1047
{
    public string RemoveDuplicates(string s)
    {
        StringBuilder word = new StringBuilder(s);
        

        for (int i = 0; i < word.Length - 1; )
        {
            if (word[i] == word[i + 1])
            {
                word.Remove(i, 1);
                word.Remove(i, 1);
                if (i > 0) i--;                    
            }
            else i++;
        }

        return word.ToString();
    }
    public void Test()
    {
        string s1 = "abbaca";
        var retVal = RemoveDuplicates(s1);

        string s2 = "azxxzy";
        var retval2 = RemoveDuplicates(s2);

        string s3 = "aaaaaaaaa";
        var retval3 = RemoveDuplicates(s3);

    }
}

/*
 1047. Remove All Adjacent Duplicates In String
Easy
5.3K
206
Companies

You are given a string s consisting of lowercase English letters. A duplicate removal consists of choosing two adjacent and equal letters and removing them.

We repeatedly make duplicate removals on s until we no longer can.

Return the final string after all such duplicate removals have been made. It can be proven that the answer is unique.

 

Example 1:

Input: s = "abbaca"
Output: "ca"
Explanation: 
For example, in "abbaca" we could remove "bb" since the letters are adjacent and equal, and this is the only possible move.  The result of this move is that the string is "aaca", of which only "aa" is possible, so the final string is "ca".

Example 2:

Input: s = "azxxzy"
Output: "ay"

 

Constraints:

    1 <= s.length <= 105
    s consists of lowercase English letters.


 */