﻿namespace PracticeLeetCodeCS.Easy;

internal class ReverseLinkedList206
{
    //Definition for singly-linked list.
    public class ListNode
    {
        public int val;
        public ListNode next;
        public ListNode(int val = 0, ListNode next = null)
        {
            this.val = val;
            this.next = next;
        }
    }
    public ListNode ReverseList(ListNode head)
    {
        ListNode newHead = head;
        ListNode previousNode = null;
        ListNode currentNode = head;

        while (currentNode != null) 
        {
            if (currentNode.next == null)
                newHead = currentNode;
            ListNode tempNext = currentNode.next;
            currentNode.next = previousNode;
            previousNode = currentNode;            
            currentNode = tempNext;
        }
     
        return newHead;
    }
    public ListNode ReverseList2(ListNode head)
    {
        Stack<ListNode> stack = new Stack<ListNode>();
        ListNode currentNode = head;
        while (currentNode != null)
        {
            stack.Push(currentNode);
            currentNode = currentNode.next;
        }
        
        currentNode = stack.Pop();
        head = currentNode;

        while (stack.Count > 0)
        {
            currentNode.next = stack.Pop();
            currentNode = currentNode.next;
        }
        return head;
    }
    
    public void Test()
    {
        ListNode firstNumber1 = new ListNode(5);
        ListNode firstNumber2 = new ListNode(4, firstNumber1);
        ListNode firstNumber3 = new ListNode(3, firstNumber2);
        ListNode firstNumber4 = new ListNode(2, firstNumber3);
        ListNode firstNumber5 = new ListNode(1, firstNumber4);

        ListNode head = ReverseList(firstNumber5);

    }
}
/*
 206. Reverse Linked List
Easy

Given the head of a singly linked list, reverse the list, and return the reversed list.

 

Example 1:

Input: head = [1,2,3,4,5]
Output: [5,4,3,2,1]

Example 2:

Input: head = [1,2]
Output: [2,1]

Example 3:

Input: head = []
Output: []

 

Constraints:

    The number of nodes in the list is the range [0, 5000].
    -5000 <= Node.val <= 5000

 */