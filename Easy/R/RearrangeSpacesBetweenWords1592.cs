﻿using System.Text;

namespace PracticeLeetCodeCS.Easy;

internal class RearrangeSpacesBetweenWords1592
{
    public string ReorderSpaces(string text)
    {
        StringBuilder retVal = new StringBuilder();

        int spaceCount = text.Count(x => x == ' ');
        string[] words = text.Split(' ', StringSplitOptions.RemoveEmptyEntries);
        int spaceBetweenWords = 0;
        if (words.Length > 1)
            spaceBetweenWords =  spaceCount / (words.Length - 1);

        int extraSpace = spaceCount; 
        if (words.Length > 1)
            extraSpace = spaceCount % (words.Length - 1);

        for (int i = 0; i < words.Length - 1; ++i)
        {
            retVal.Append(words[i]);
            retVal.Append(' ', spaceBetweenWords);
        }
        retVal.Append(words[words.Length - 1]);
        if (extraSpace > 0) 
            retVal.Append(' ', extraSpace);

        return retVal.ToString();
    }
    public void Test()
    {
        string text1 = "  this   is  a sentence ";
        var retVal1 = ReorderSpaces(text1);

        string text2 = " practice   makes   perfect";
        var retVal2 = ReorderSpaces(text2);

        string text3 = "   a ";
        var retVal3 = ReorderSpaces(text3);
    }
}

/*
 1592. Rearrange Spaces Between Words
Easy
354
292
Companies

You are given a string text of words that are placed among some number of spaces. Each word consists of one or more lowercase English letters and are separated by at least one space. It's guaranteed that text contains at least one word.

Rearrange the spaces so that there is an equal number of spaces between every pair of adjacent words and that number is maximized. If you cannot redistribute all the spaces equally, place the extra spaces at the end, meaning the returned string should be the same length as text.

Return the string after rearranging the spaces.

 

Example 1:

Input: text = "  this   is  a sentence "
Output: "this   is   a   sentence"
Explanation: There are a total of 9 spaces and 4 words. We can evenly divide the 9 spaces between the words: 9 / (4-1) = 3 spaces.

Example 2:

Input: text = " practice   makes   perfect"
Output: "practice   makes   perfect "
Explanation: There are a total of 7 spaces and 3 words. 7 / (3-1) = 3 spaces plus 1 extra space. We place this extra space at the end of the string.

 

Constraints:

    1 <= text.length <= 100
    text consists of lowercase English letters and ' '.
    text contains at least one word.


 */