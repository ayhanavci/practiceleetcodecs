﻿using System.Text;

namespace PracticeLeetCodeCS.Easy;

internal class RotateString796
{
    public bool RotateString(string s, string goal)
    {
        StringBuilder sBuilder = new StringBuilder(s);

        if (sBuilder.ToString().Equals(goal))
            return true;

        for (int i = 0; i < s.Length - 1; i++)
        {           
            char ch = sBuilder[0];
            sBuilder.Remove(0, 1);
            sBuilder.Append(ch);
            if (sBuilder.ToString().Equals(goal))
                return true;
        }
        return false;
    }
    public void Test()
    {
        string s = "abcde";
        string goal = "cdeab";

        var retVal = RotateString(s, goal);
    }
}

/*
 796. Rotate String
Easy
2.4K
101
Companies

Given two strings s and goal, return true if and only if s can become goal after some number of shifts on s.

A shift on s consists of moving the leftmost character of s to the rightmost position.

    For example, if s = "abcde", then it will be "bcdea" after one shift.

 

Example 1:

Input: s = "abcde", goal = "cdeab"
Output: true

Example 2:

Input: s = "abcde", goal = "abced"
Output: false

 

Constraints:

    1 <= s.length, goal.length <= 100
    s and goal consist of lowercase English letters.


 */