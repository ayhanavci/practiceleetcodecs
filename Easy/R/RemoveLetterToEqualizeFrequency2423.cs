namespace PracticeLeetCodeCS.Easy;

//TODO: Çözülemedi
internal class RemoveLetterToEqualizeFrequency2423
{
    public bool EqualFrequency(string word) 
    {
        Dictionary<char, int> frequencies = new Dictionary<char, int>();
        
        foreach(char ch in word) 
        {
            if (frequencies.ContainsKey(ch)) frequencies[ch]++;
            else frequencies.Add(ch, 1);            
        }                   
        
        Dictionary<int, int> freqCounts = new Dictionary<int, int>();
        
        foreach (var pair in frequencies)
        {
            if (freqCounts.ContainsKey(pair.Value)) freqCounts[pair.Value]++;
            else freqCounts.Add(pair.Value, 1);
        }
        
        if (freqCounts.Count > 2) 
            return false;
        if (freqCounts.Count == 1)
        {
            if (freqCounts.ElementAt(0).Key == 1)            
                return true;
            if (frequencies.Count == 1)
                return true;
            return false;
        }
        if ((freqCounts.ElementAt(0).Key == 1 && freqCounts.ElementAt(0).Value == 1) || 
            (freqCounts.ElementAt(1).Key == 1 && freqCounts.ElementAt(1).Value == 1)) 
            return true;
        
        if (Math.Abs(freqCounts.ElementAt(0).Key - freqCounts.ElementAt(1).Key) != 1) 
            return false;

        if (freqCounts.ElementAt(0).Value > 1 &&  freqCounts.ElementAt(1).Value > 1) 
            return false;
        return true;       
    }
    public void Test()
    {
        //bac true olmalı
        //abbcc true olmalı
        //aca true
        //ddaccb false
        //zz true
        //cccd true
        //aaaabbbbccc false
        var retVal1 = EqualFrequency("bac");
        var retVal2 = EqualFrequency("abbcc");
        var retVal3 = EqualFrequency("aca");
        var retVal4 = EqualFrequency("ddaccb");
        var retVal5 = EqualFrequency("zz");
        var retVal6 = EqualFrequency("cccd");
        var retval7 = EqualFrequency("aaaabbbbccc");
    }
}
/*
2423. Remove Letter To Equalize Frequency
Easy
438
876
Companies

You are given a 0-indexed string word, consisting of lowercase English letters. You need to select one index and remove the letter at that index from word so that the frequency of every letter present in word is equal.

Return true if it is possible to remove one letter so that the frequency of all letters in word are equal, and false otherwise.

Note:

    The frequency of a letter x is the number of times it occurs in the string.
    You must remove exactly one letter and cannot chose to do nothing.

 

Example 1:

Input: word = "abcc"
Output: true
Explanation: Select index 3 and delete it: word becomes "abc" and each character has a frequency of 1.

Example 2:

Input: word = "aazz"
Output: false
Explanation: We must delete a character, so either the frequency of "a" is 1 and the frequency of "z" is 2, or vice versa. It is impossible to make all present letters have equal frequency.

 

Constraints:

    2 <= word.length <= 100
    word consists of lowercase English letters only.


*/