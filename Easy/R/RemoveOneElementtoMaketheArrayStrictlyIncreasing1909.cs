namespace PracticeLeetCodeCS.Easy;

internal class RemoveOneElementtoMaketheArrayStrictlyIncreasing1909
{
    //TODO: Uğraştırdı
    public bool CanBeIncreasing(int[] nums) 
    {
        bool removed = false;
        int previousValid = nums[0];

        for (int i = 1; i < nums.Length; ++i)
        {       
            if (nums[i] > previousValid) 
            {              
                if (i+1 == nums.Length)
                    break;  
                if (nums[i] < nums[i+1]) 
                {                                        
                    previousValid = nums[i];
                    continue;
                }                
                else if (removed)                
                    return false;                             
                else 
                {                    
                    if (nums[i+1] <= previousValid) 
                    {                                       
                        previousValid = nums[i];                        
                        i++;                        
                    }                                                            
                    removed = true;
                }
                    
            }    
            else 
            {
                if (removed)                
                    return false; 
                if (i+1 == nums.Length)
                    break;                    
                previousValid = nums[i];                                                        
                removed = true;
            } 
            
            
        }
        
        return true;
    }

    /*
    
    if (previous >= nums[i]) 
            {
                     
                if (removed)
                    return false;                                    
                removed = true;                
                if (i+1 == nums.Length)                 
                    return true;                
                else 
                {                                                                            
                    if (nums[i] < nums[i+1])
                    {
                        previous = nums[i];
                    }                    
                    else if (previous < nums[i+1])
                    {
                        //do nothing. previous=previous
                    }
                    else if (nums[i] >= nums[i+1] && previous >= nums[i+1]) 
                    {
                        return false;                        
                    }
                }                
            }             
            else
                previous = nums[i];            
    */
    public bool CanBeIncreasingEx(int[] nums) 
    {
        bool removed = false;
        int previous = nums[1];
        int preprevious = nums[0];

        for (int i = 2; i < nums.Length; ++i)
        {
            if (previous >= nums[i]) 
            {
                if (removed)
                    return false;                
                removed = true;
                //
                if (preprevious >= nums[i]) //1-3-6-2-7-8-9
                {

                }
                else                        //1-2-5-3-4-6-7
                {

                }
                //
            } 

            previous = nums[i];
            preprevious = nums[i-1];
        }
        
        return true;
    }
    public void Test()
    {
        int[] nums = {1,2,10,5,7};
        var retVal = CanBeIncreasing(nums);

        int[] nums2 = {2,3,1,2};
        var retVal2 = CanBeIncreasing(nums2);

        int[] nums3 = {1,1,1};
        var retVal3 = CanBeIncreasing(nums3);

        int[] nums4 = {1,2,5,3,4,6,7,8};
        var retVal4 = CanBeIncreasing(nums4);

        int[] nums5 = {1,3,6,2,7,8};
        var retVal5 = CanBeIncreasing(nums5);

        int[] nums6 = {1,3,6,2,7,6, 8};
        var retVal6 = CanBeIncreasing(nums6);
    }
}
/*

When we find a drop, we check if the current number nums[i] is greater than the number before the previous one nums[i - 2].

    If so, the number nums[i - 1] needs to be removed.
    Otherwise, the current number needs to be removed (nums[i]).
        For simplicity, I just assign the previous value to the current number (nums[i] = nums[i - 1]).

And, of course, we return false if we find a second drop.

C++

bool canBeIncreasing(vector<int>& nums) {
    int cnt = 0;
    for (int i = 1; i < nums.size() && cnt < 2; ++i) {
        if (nums[i - 1] >= nums[i]) {
            ++cnt;
            if (i > 1 && nums[i - 2] >= nums[i])
                nums[i] = nums[i - 1];
        }
    }
    return cnt < 2;
}
*/
/*
1909. Remove One Element to Make the Array Strictly Increasing
Easy
948
280
Companies

Given a 0-indexed integer array nums, return true if it can be made strictly increasing after removing exactly one element, or false otherwise. If the array is already strictly increasing, return true.

The array nums is strictly increasing if nums[i - 1] < nums[i] for each index (1 <= i < nums.length).

 

Example 1:

Input: nums = [1,2,10,5,7]
Output: true
Explanation: By removing 10 at index 2 from nums, it becomes [1,2,5,7].
[1,2,5,7] is strictly increasing, so return true.

Example 2:

Input: nums = [2,3,1,2]
Output: false
Explanation:
[3,1,2] is the result of removing the element at index 0.
[2,1,2] is the result of removing the element at index 1.
[2,3,2] is the result of removing the element at index 2.
[2,3,1] is the result of removing the element at index 3.
No resulting array is strictly increasing, so return false.

Example 3:

Input: nums = [1,1,1]
Output: false
Explanation: The result of removing any element is [1,1].
[1,1] is not strictly increasing, so return false.

 

Constraints:

    2 <= nums.length <= 1000
    1 <= nums[i] <= 1000


*/