﻿namespace PracticeLeetCodeCS.Easy;

internal class RemoveLinkedListElements203
{
    //Definition for singly-linked list.
    public class ListNode
    {
        public int val;
        public ListNode next;
        public ListNode(int val = 0, ListNode next = null)
        {
            this.val = val;
            this.next = next;
        }
    }
    public ListNode RemoveElements(ListNode head, int val)
    {
        ListNode newHead = head;
        ListNode curNode = head;
        ListNode previousNode = null;
        
        while (curNode != null)
        {
            if (curNode.val == val)
            {
                if (previousNode == null)                
                    newHead = curNode.next;                
                else                
                    previousNode.next = curNode.next;                
                curNode = curNode.next;
            }
            else
            {
                previousNode = curNode;
                curNode = curNode.next;
            }
            
        }
        return newHead;
    }
    public void Test()
    {
        ListNode firstNumber1 = new ListNode(6);
        ListNode firstNumber2 = new ListNode(5, firstNumber1);
        ListNode firstNumber3 = new ListNode(4, firstNumber2);
        ListNode firstNumber4 = new ListNode(3, firstNumber3);
        ListNode firstNumber5 = new ListNode(6, firstNumber4);
        ListNode firstNumber6 = new ListNode(2, firstNumber5);
        ListNode firstNumber7 = new ListNode(1, firstNumber6);

        ListNode result = RemoveElements(firstNumber7, 6);
    }

}
/*
 203. Remove Linked List Elements
Easy

Given the head of a linked list and an integer val, remove all the nodes of the linked list that has Node.val == val, and return the new head.

 

Example 1:

Input: head = [1,2,6,3,4,5,6], val = 6
Output: [1,2,3,4,5]

Example 2:

Input: head = [], val = 1
Output: []

Example 3:

Input: head = [7,7,7,7], val = 7
Output: []

 

Constraints:

    The number of nodes in the list is in the range [0, 104].
    1 <= Node.val <= 50
    0 <= val <= 50


 */