﻿namespace PracticeLeetCodeCS.Easy;

internal class RelativeSortArray1122
{
    public int[] RelativeSortArray(int[] arr1, int[] arr2)
    {
        int[] retVal = new int[arr1.Length];
        int insertIndex = 0;        
        List<int> arr1List = new List<int>(arr1);

        for (int i = 0; i < arr2.Length; i++)
        {
            for (int j = 0; j < arr1List.Count; ++j)
            {
                if (arr2[i] == arr1List[j])
                {
                    retVal[insertIndex++] = arr1List[j];
                    arr1List.RemoveAt(j);
                    j--;
                }
            }
        }
        arr1List.Sort();

        int end = arr1.Length - 1;
        for (int i = arr1List.Count - 1; i >= 0; --i)
            retVal[end--] = arr1List[i];

        return retVal.ToArray();
    }

    public void Test()
    {
        int[] arr1 = { 2, 3, 1, 3, 2, 4, 6, 7, 9, 2, 19 };
        int[] arr2 = { 2, 1, 4, 3, 9, 6 };
        var retVal = RelativeSortArray(arr1, arr2);
    }
}

/*
 1122. Relative Sort Array
Easy
2.1K
119
Companies

Given two arrays arr1 and arr2, the elements of arr2 are distinct, and all elements in arr2 are also in arr1.

Sort the elements of arr1 such that the relative ordering of items in arr1 are the same as in arr2. Elements that do not appear in arr2 should be placed at the end of arr1 in ascending order.

 

Example 1:

Input: arr1 = [2,3,1,3,2,4,6,7,9,2,19], arr2 = [2,1,4,3,9,6]
Output: [2,2,2,1,4,3,3,9,6,7,19]

Example 2:

Input: arr1 = [28,6,22,8,44,17], arr2 = [22,28,8,6]
Output: [22,28,8,6,17,44]

 

Constraints:

    1 <= arr1.length, arr2.length <= 1000
    0 <= arr1[i], arr2[i] <= 1000
    All the elements of arr2 are distinct.
    Each arr2[i] is in arr1.


 */