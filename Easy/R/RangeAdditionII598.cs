﻿namespace PracticeLeetCodeCS.Easy;

internal class RangeAdditionII598
{
    //ERROR
    //OUT OF MEMORY
    public int MaxCount(int m, int n, int[][] ops)
    {       
        //Init matrix
        int[][] mat = new int[m][];
        for (int i = 0; i < m; i++) mat[i] = new int[n];
        
        //For each operation in ops
        for (int i = 0; i < ops.Length; ++i)                    
            //Do increment on all numbers in x,y 
            for (int j = 0; j < ops[i][0]; ++j)            
                for (int k = 0; k < ops[i][1]; ++k)                
                    mat[j][k]++;
        
        //Count the repetition of top left (0,0) number in the matrix
        int count = 0;
        for (int i = 0; i < m; ++i)        
            for (int j = 0; j < n; ++j)            
                if (mat[0][0] == mat[i][j])
                    count++;                    

        return count;
    }
    public void Test()
    {
        int[][] ops = new int[2][];
        ops[0] = new int[] { 2, 2 };
        ops[1] = new int[] { 3, 3 };

        /*int[][] ops = new int[2][];
        ops[0] = new int[] { 2, 2 };
        ops[1] = new int[] { 3, 3 };*/


        var retVal = MaxCount(3, 3, ops);
    }
}

/*
 598. Range Addition II
Easy
764
869
Companies

You are given an m x n matrix M initialized with all 0"s and an array of operations ops, where ops[i] = [ai, bi] means M[x][y] should be incremented by one for all 0 <= x < ai and 0 <= y < bi.

Count and return the number of maximum integers in the matrix after performing all the operations.

 

Example 1:

Input: m = 3, n = 3, ops = [[2,2],[3,3]]
Output: 4
Explanation: The maximum integer in M is 2, and there are four of it in M. So return 4.

Example 2:

Input: m = 3, n = 3, ops = [[2,2],[3,3],[3,3],[3,3],[2,2],[3,3],[3,3],[3,3],[2,2],[3,3],[3,3],[3,3]]
Output: 4

Example 3:

Input: m = 3, n = 3, ops = []
Output: 9

 

Constraints:

    1 <= m, n <= 4 * 104
    0 <= ops.length <= 104
    ops[i].length == 2
    1 <= ai <= m
    1 <= bi <= n


 */