﻿namespace PracticeLeetCodeCS.Easy;

internal class ReshapetheMatrix566
{
    public int[][] MatrixReshape(int[][] mat, int r, int c)
    {
        if (mat.Length * mat[0].Length != r * c)
            return mat;

        int[][] matrix = new int[r][];

        for (int i = 0; i < r; ++i)
        {
            matrix[i] = new int[c];
            for (int j = 0; j < c; ++j)
            {
                int pos = i * c + j;
                int oldRow = pos / mat[0].Length;
                int oldCol = pos % mat[0].Length;

                matrix[i][j] = mat[oldRow][oldCol];
            }
        }

        return matrix;
    }
    public void Test()
    {
        int[][] mat = new int[3][];
        mat[0] = new int[] { 1, 2, 3, 4 };
        mat[1] = new int[] { 5, 6, 7, 8 };
        mat[2] = new int[] { 9, 10, 11, 12 };        

        var retVal = MatrixReshape(mat, 2, 6);
    }
}

/*
 566. Reshape the Matrix
Easy
2.9K
324
Companies

In MATLAB, there is a handy function called reshape which can reshape an m x n matrix into a new one with a different size r x c keeping its original data.

You are given an m x n matrix mat and two integers r and c representing the number of rows and the number of columns of the wanted reshaped matrix.

The reshaped matrix should be filled with all the elements of the original matrix in the same row-traversing order as they were.

If the reshape operation with given parameters is possible and legal, output the new reshaped matrix; Otherwise, output the original matrix.

 

Example 1:

Input: mat = [[1,2],[3,4]], r = 1, c = 4
Output: [[1,2,3,4]]

Example 2:

Input: mat = [[1,2],[3,4]], r = 2, c = 4
Output: [[1,2],[3,4]]

 */