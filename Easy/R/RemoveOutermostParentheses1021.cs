﻿using System.Text;

namespace PracticeLeetCodeCS.Easy;

internal class RemoveOutermostParentheses1021
{
    public string RemoveOuterParentheses(string s)
    {
        StringBuilder retVal = new StringBuilder();
        int openCount = 0;

        StringBuilder part = new StringBuilder();
        for (int i = 0; i < s.Length; i++)
        {
            openCount += s[i] == '(' ? 1 : -1;
            part.Append(s[i]);
            if (openCount == 0)
            {
                part.Remove(0, 1);
                part.Remove(part.Length - 1, 1);
                retVal.Append(part.ToString());
                part.Clear();
            }
        }
        return retVal.ToString();
    }
    public void Test()
    {
        string s1 = "(()())(())";
        var retVal1 = RemoveOuterParentheses(s1);

        string s2 = "(()())(())(()(()))";
        var retVal2 = RemoveOuterParentheses(s2);

        string s3 = "()()";
        var retVal3 = RemoveOuterParentheses(s3);
    }
}

/*
 1021. Remove Outermost Parentheses
Easy
1.8K
1.3K
Companies

A valid parentheses string is either empty "", "(" + A + ")", or A + B, where A and B are valid parentheses strings, and + represents string concatenation.

    For example, "", "()", "(())()", and "(()(()))" are all valid parentheses strings.

A valid parentheses string s is primitive if it is nonempty, and there does not exist a way to split it into s = A + B, with A and B nonempty valid parentheses strings.

Given a valid parentheses string s, consider its primitive decomposition: s = P1 + P2 + ... + Pk, where Pi are primitive valid parentheses strings.

Return s after removing the outermost parentheses of every primitive string in the primitive decomposition of s.

 

Example 1:

Input: s = "(()())(())"
Output: "()()()"
Explanation: 
The input string is "(()())(())", with primitive decomposition "(()())" + "(())".
After removing outer parentheses of each part, this is "()()" + "()" = "()()()".

Example 2:

Input: s = "(()())(())(()(()))"
Output: "()()()()(())"
Explanation: 
The input string is "(()())(())(()(()))", with primitive decomposition "(()())" + "(())" + "(()(()))".
After removing outer parentheses of each part, this is "()()" + "()" + "()(())" = "()()()()(())".

Example 3:

Input: s = "()()"
Output: ""
Explanation: 
The input string is "()()", with primitive decomposition "()" + "()".
After removing outer parentheses of each part, this is "" + "" = "".

 

Constraints:

    1 <= s.length <= 105
    s[i] is either '(' or ')'.
    s is a valid parentheses string.


 */