﻿using System.Text;

namespace PracticeLeetCodeCS.Easy;

internal class RansomNote383
{
    public bool CanConstruct(string ransomNote, string magazine)
    {                
        for (int i = 0; i < ransomNote.Length; i++)
        {
            int index = magazine.LastIndexOf(ransomNote[i]);
            if (index == -1) return false;
            magazine = magazine.Remove(index, 1);          
        }
        return true;
    }
    public bool CanConstruct2(string ransomNote, string magazine)
    {       
        for (int i = 0; i < ransomNote.Length; i++)
        {
            bool fFound = false;
            for (int j = 0; j < magazine.Length; j++)
            {
                if (magazine[j] == ransomNote[i])
                {
                    magazine = magazine.Remove(j, 1);
                    fFound = true;
                    break;
                }
            }
            if (!fFound)
                return false;            
        }
        return true;
    }
    public void Test()
    {
        string ransomNote = "aa";
        string magazine = "ab";
        bool fResult = CanConstruct(ransomNote, magazine);
    }
}

/*
 383. Ransom Note
Easy

Given two strings ransomNote and magazine, return true if ransomNote can be constructed by using the letters from magazine and false otherwise.

Each letter in magazine can only be used once in ransomNote.

 

Example 1:

Input: ransomNote = "a", magazine = "b"
Output: false

Example 2:

Input: ransomNote = "aa", magazine = "ab"
Output: false

Example 3:

Input: ransomNote = "aa", magazine = "aab"
Output: true

 

Constraints:

    1 <= ransomNote.length, magazine.length <= 105
    ransomNote and magazine consist of lowercase English letters.


 */