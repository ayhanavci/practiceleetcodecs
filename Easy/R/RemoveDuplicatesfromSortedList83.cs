﻿namespace PracticeLeetCodeCS.Easy;

internal class RemoveDuplicatesfromSortedList83
{
    //Definition for singly-linked list.
    public class ListNode {
        public int val;
        public ListNode? next;
        public ListNode(int val = 0, ListNode? next = null)
        {
            this.val = val;
            this.next = next;
        }
     }
    public ListNode? DeleteDuplicates(ListNode? head)
    {
        if (head == null) return null;
        ListNode current = head;
        while (current.next != null)
        {
            if (current.val == current.next.val)            
                current.next = current.next.next;            
            else            
                current = current.next;
            
        }
        return head;
    }
    public void Test()
    {
        ListNode next14 = new ListNode(3);
        ListNode next13 = new ListNode(3, next14);
        ListNode next12 = new ListNode(2, next13);
        ListNode next11 = new ListNode(1, next12);
        ListNode head1 = new ListNode(1, next11);                

        ListNode? result = DeleteDuplicates(head1);
    }
}

/*
 83. Remove Duplicates from Sorted List
Easy

Given the head of a sorted linked list, delete all duplicates such that each element appears only once. Return the linked list sorted as well.

 

Example 1:

Input: head = [1,1,2]
Output: [1,2]

Example 2:

Input: head = [1,1,2,3,3]
Output: [1,2,3]

 

Constraints:

    The number of nodes in the list is in the range [0, 300].
    -100 <= Node.val <= 100
    The list is guaranteed to be sorted in ascending order.


 */