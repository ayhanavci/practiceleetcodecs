﻿namespace PracticeLeetCodeCS.Easy;

internal class RectangleOverlap836
{
    //TODO üzerinden geç
    public bool IsRectangleOverlap(int[] rec1, int[] rec2)
    {
        return 
            rec1[0] < rec2[2] && rec2[0] < rec1[2] && 
            rec1[1] < rec2[3] && rec2[1] < rec1[3];
    }
    public bool IsRectangleOverlap2(int[] rec1, int[] rec2)
    {
        // check if either rectangle is actually a line
        if (rec1[0] == rec1[2] || rec1[1] == rec1[3] ||
            rec2[0] == rec2[2] || rec2[1] == rec2[3])
        {
            // the line cannot have positive overlap
            return false;
        }

        return !(rec1[2] <= rec2[0] ||   // left
                 rec1[3] <= rec2[1] ||   // bottom
                 rec1[0] >= rec2[2] ||   // right
                 rec1[1] >= rec2[3]);    // top
    }
    public bool IsRectangleOverlapWRONG(int[] rec1, int[] rec2)
    {
        if (rec2[0] > rec1[0] && rec2[0] < rec1[2])
        {
            if (rec2[1] >= rec1[1] && rec2[1] < rec1[3])
                return true;
        }



        if (rec1[0] > rec2[0] && rec1[0] < rec2[2])
        {
            if (rec1[1] >= rec2[1] && rec1[1] < rec2[3])
                return true;

        }
        return false;
    }
    public void Test()
    {
        //int[] rec1 = { 0, 0, 2, 2 };
        //int[] rec2 = { 1, 1, 3, 3 };

        //int[] rec1 = { 7, 8, 13, 15 };
        //int[] rec2 = { 10, 8, 12, 20 };

        //int[] rec1 = { 0, 0, 1, 1 };
        //int[] rec2 = { 1, 0, 2, 1};

        int[] rec1 = { 2, 17, 6, 20 };
        int[] rec2 = { 3, 8, 6, 20 };

        /*
           rec1[0] < rec2[2] && rec2[0] < rec1[2] && 
            rec1[1] < rec2[3] && rec2[1] < rec1[3];
         */

        var retVal = IsRectangleOverlap(rec1, rec2);
    }
}

/*
 836. Rectangle Overlap
Easy
1.7K
420
Companies

An axis-aligned rectangle is represented as a list [x1, y1, x2, y2], where (x1, y1) is the coordinate of its bottom-left corner, and (x2, y2) is the coordinate of its top-right corner. 
Its top and bottom edges are parallel to the X-axis, and its left and right edges are parallel to the Y-axis.

Two rectangles overlap if the area of their intersection is positive. To be clear, two rectangles that only touch at the corner or edges do not overlap.

Given two axis-aligned rectangles rec1 and rec2, return true if they overlap, otherwise return false.

 

Example 1:

Input: rec1 = [0,0,2,2], rec2 = [1,1,3,3]
Output: true

Example 2:

Input: rec1 = [0,0,1,1], rec2 = [1,0,2,1]
Output: false

Example 3:

Input: rec1 = [0,0,1,1], rec2 = [2,2,3,3]
Output: false

 

Constraints:

    rec1.length == 4
    rec2.length == 4
    -109 <= rec1[i], rec2[i] <= 109
    rec1 and rec2 represent a valid rectangle with a non-zero area.


 */