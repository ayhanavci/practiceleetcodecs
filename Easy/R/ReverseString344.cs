﻿namespace PracticeLeetCodeCS.Easy;

internal class ReverseString344
{
    public void ReverseString(char[] s)
    {
        for (int i = 0; i < s.Length / 2; i++)
        {
            char tmp = s[s.Length - i - 1];
            s[s.Length - i - 1] = s[i];
            s[i] = tmp;
        }

    }
    public void Test()
    {
        char[] s = { 'H', 'a', 'n', 'n', 'a', 'h' };
        char[] s2 = { 'h', 'e', 'l', 'l', 'o' };
        ReverseString(s2);

    }
}

/*
 344. Reverse String
Easy

Write a function that reverses a string. The input string is given as an array of characters s.

You must do this by modifying the input array in-place with O(1) extra memory.

 

Example 1:

Input: s = ["h","e","l","l","o"]
Output: ["o","l","l","e","h"]

Example 2:

Input: s = ["H","a","n","n","a","h"]
Output: ["h","a","n","n","a","H"]

 

Constraints:

    1 <= s.length <= 105
    s[i] is a printable ascii character.


 */