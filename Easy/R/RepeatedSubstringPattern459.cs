﻿namespace PracticeLeetCodeCS.Easy;

internal class RepeatedSubstringPattern459
{
    public bool RepeatedSubstringPattern(string s)
    {
        if (s == null) return false;
        if (s.Length == 0) return false;
        
        string combined = s.Substring(1) + s.Substring(0, s.Length - 1);
        return combined.Contains(s);
    }
    public void Test()
    {
        string s = "abcdabcd";
        string combined = s.Substring(1) + s.Substring(0, s.Length - 1);
        bool retVal = RepeatedSubstringPattern(s);
    }
}


/*
 459. Repeated Substring Pattern
Easy

Given a string s, check if it can be constructed by taking a substring of it and appending multiple copies of the substring together.

 

Example 1:

Input: s = "abab"
Output: true
Explanation: It is the substring "ab" twice.

Example 2:

Input: s = "aba"
Output: false

Example 3:

Input: s = "abcabcabcabc"
Output: true
Explanation: It is the substring "abc" four times or the substring "abcabc" twice.

 

Constraints:

    1 <= s.length <= 104
    s consists of lowercase English letters.


 */