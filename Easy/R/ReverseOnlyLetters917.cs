﻿using System.Text;

namespace PracticeLeetCodeCS.Easy;

internal class ReverseOnlyLetters917
{
    public string ReverseOnlyLetters(string s)
    {
        Dictionary<int, char> nonAlpha = new Dictionary<int, char>();

        StringBuilder retVal = new StringBuilder();           
        
        for (int i = 0; i < s.Length; ++i)
        {
            if (char.IsLetter(s[i]))
                retVal.Append(s[i]);
            else
                nonAlpha.Add(i, s[i]);

        }
        for (int i = 0; i < retVal.Length / 2; ++i)
        {
            char ch = retVal[i];
            retVal[i] = retVal[retVal.Length - 1 - i];
            retVal[retVal.Length - 1 - i] = ch;
        }
        
        for (int i = 0; i < nonAlpha.Count; ++i)        
            retVal.Insert(nonAlpha.ElementAt(i).Key, nonAlpha.ElementAt(i).Value);
        

        return retVal.ToString();
    }
    public string ReverseOnlyLetters2(string s)
    {
        StringBuilder retVal =  new StringBuilder();
        Stack<char> chars = new Stack<char>();

        for (int i = 0; i < s.Length; ++i)
        {
            if (char.IsLetter(s[i]))
                chars.Push(s[i]);
        }

        for (int i = 0; i < s.Length; ++i)
        {
            if (char.IsLetter(s[i]))
                retVal.Append(chars.Pop());
            else
                retVal.Append(s[i]);
        }

        return retVal.ToString();
    }
    public void Test()
    {
        string s = "Test1ng-Leet=code-Q!";
        var retVal = ReverseOnlyLetters2(s);
    }
}

/*
 917. Reverse Only Letters
Easy
1.7K
58
Companies

Given a string s, reverse the string according to the following rules:

    All the characters that are not English letters remain in the same position.
    All the English letters (lowercase or uppercase) should be reversed.

Return s after reversing it.

 

Example 1:

Input: s = "ab-cd"
Output: "dc-ba"

Example 2:

Input: s = "a-bC-dEf-ghIj"
Output: "j-Ih-gfE-dCba"

Example 3:

Input: s = "Test1ng-Leet=code-Q!"
Output: "Qedo1ct-eeLg=ntse-T!"

 

Constraints:

    1 <= s.length <= 100
    s consists of characters with ASCII values in the range [33, 122].
    s does not contain "\"" or "\\".


 */