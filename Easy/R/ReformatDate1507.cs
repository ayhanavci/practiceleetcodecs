﻿using System.Text;

namespace PracticeLeetCodeCS.Easy;

internal class ReformatDate1507
{
    public string ReformatDate(string date)
    {
        string[] months = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
        string[] parts = date.Split(' ');

        StringBuilder retVal = new StringBuilder();

        retVal.Append(parts[2]);
        retVal.Append("-");

        for (int i = 0; i < months.Length; i++)
        {
            if (parts[1].Equals(months[i]))
            {
                retVal.Append((i + 1).ToString("00"));
                retVal.Append("-");
                break;
            }
        }

        int day = 0;

        if (Char.IsNumber(parts[0][1]))
        {
            day = (parts[0][0] - '0') * 10;
            day += (parts[0][1] - '0');
        }
        else
        {
            day = (parts[0][0] - '0');
        }
        retVal.Append(day.ToString("00"));        
        
        return retVal.ToString();
    }
    public void Test()
    {
        var retVal = ReformatDate("20th Oct 2052");
        var retVal2 = ReformatDate("6th Jun 1933");
    }
}


/*
 1507. Reformat Date
Easy
330
382
Companies

Given a date string in the form Day Month Year, where:

    Day is in the set {"1st", "2nd", "3rd", "4th", ..., "30th", "31st"}.
    Month is in the set {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"}.
    Year is in the range [1900, 2100].

Convert the date string to the format YYYY-MM-DD, where:

    YYYY denotes the 4 digit year.
    MM denotes the 2 digit month.
    DD denotes the 2 digit day.

 

Example 1:

Input: date = "20th Oct 2052"
Output: "2052-10-20"

Example 2:

Input: date = "6th Jun 1933"
Output: "1933-06-06"

Example 3:

Input: date = "26th May 1960"
Output: "1960-05-26"

 

Constraints:

    The given dates are guaranteed to be valid, so no error handling is necessary.


 */