﻿using System.Text;

namespace PracticeLeetCodeCS.Easy;

internal class ReverseWordsinaStringIII557
{
    public string ReverseWords(string s)
    {
        string [] words = s.Split(" ");
        StringBuilder reverseWords = new StringBuilder();
        
        for (int i = 0; i < words.Length; i++)
        {
            StringBuilder word = new StringBuilder(words[i]);            
            for (int j = 0; j < word.Length / 2; ++j)
            {
                char tmp = word[j];
                word[j] = word[word.Length - j - 1];
                word[word.Length - j - 1] = tmp;
            }
            reverseWords.Append(word);
            reverseWords.Append(" ");
        }
        reverseWords.Remove(reverseWords.Length - 1, 1);
        return reverseWords.ToString();
    }
    public void Test()
    {
        string s = "Let's take LeetCode contest";
        var retVal = ReverseWords(s);
    }
}

/*
 557. Reverse Words in a String III
Easy
4.4K
215
Companies

Given a string s, reverse the order of characters in each word within a sentence while still preserving whitespace and initial word order.

 

Example 1:

Input: s = "Let"s take LeetCode contest"
Output: "s"teL ekat edoCteeL tsetnoc"

Example 2:

Input: s = "God Ding"
Output: "doG gniD"

 

Constraints:

    1 <= s.length <= 5 * 104
    s contains printable ASCII characters.
    s does not contain any leading or trailing spaces.
    There is at least one word in s.
    All the words in s are separated by a single space.


 */