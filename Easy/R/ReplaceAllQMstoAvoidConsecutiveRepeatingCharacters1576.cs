﻿using System.Text;

namespace PracticeLeetCodeCS.Easy;

internal class ReplaceAllQMstoAvoidConsecutiveRepeatingCharacters1576
{
    public string ModifyString(string s)
    {
        StringBuilder str = new StringBuilder(s);

        for (int i = 0; i < str.Length; i++)
        {
            if (str[i] == '?')
            {
                char ch = 'a';
                if (i == 0) //basta
                {
                    if (i + 1 < str.Length && ch == str[i+1])                    
                        ch = 'b';                    
                }
                else 
                {
                    if (i + 1 < str.Length) //ortada
                    {
                        while (ch == str[i - 1] || ch == str[i + 1])
                            ch = (char)(ch + 1);
                    }
                    else if (ch == str[i - 1])//sonda                    
                        ch = 'b';                    
                }
                str.Replace('?', ch, i, 1);
            }
        }
        return str.ToString();
    }
    public void Test()
    {
        var retVal1 = ModifyString("?as");
        var retVal2 = ModifyString("uba?b");
    }
}
/*
 1576. Replace All ?'s to Avoid Consecutive Repeating Characters
Easy
476
156
Companies

Given a string s containing only lowercase English letters and the '?' character, convert all the '?' characters into lowercase letters such that the final string does not contain any consecutive repeating characters. You cannot modify the non '?' characters.

It is guaranteed that there are no consecutive repeating characters in the given string except for '?'.

Return the final string after all the conversions (possibly zero) have been made. If there is more than one solution, return any of them. It can be shown that an answer is always possible with the given constraints.

 

Example 1:

Input: s = "?zs"
Output: "azs"
Explanation: There are 25 solutions for this problem. From "azs" to "yzs", all are valid. Only "z" is an invalid modification as the string will consist of consecutive repeating characters in "zzs".

Example 2:

Input: s = "ubv?w"
Output: "ubvaw"
Explanation: There are 24 solutions for this problem. Only "v" and "w" are invalid modifications as the strings will consist of consecutive repeating characters in "ubvvw" and "ubvww".

 

Constraints:

    1 <= s.length <= 100
    s consist of lowercase English letters and '?'.


 */