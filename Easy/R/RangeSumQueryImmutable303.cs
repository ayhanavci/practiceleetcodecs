﻿namespace PracticeLeetCodeCS.Easy;

internal class RangeSumQueryImmutable303
{
    //Advanced
    public class NumArray
    {
        int[] sums;
        public NumArray(int[] nums)
        {
            sums = new int[nums.Length];
            sums[0] = nums[0];
            for (int i = 1; i < nums.Length; ++i)
            {
                sums[i] = nums[i];
                sums[i] += sums[i - 1];
            }
        }

        public int SumRange(int left, int right)
        {
            if (left == 0) return sums[right];
            return sums[right] - sums[left - 1];
        }
    }
    //Easy
    public class NumArray2
    {
        int[] _nums;
        public NumArray2(int[] nums) 
        {
            this._nums = nums;
        }

        public int SumRange(int left, int right)
        {
            int total = 0;
            for (int i = left; i <= right; i++) 
            { 
                total += _nums[i];
            }
            return total;
        }
    }
    
    /**
     * Your NumArray object will be instantiated and called as such:
     * NumArray obj = new NumArray(nums);
     * int param_1 = obj.SumRange(left,right);
     */
    public void Test()
    {
        int[] nums = { -2, 0, 3, -5, 2, -1 };
        int left = 0;
        int right = 5;  
        NumArray2 obj = new NumArray2(nums);
        int param_1 = obj.SumRange(left, right);
    }
}

/*
 303. Range Sum Query - Immutable
Easy

Given an integer array nums, handle multiple queries of the following type:

    Calculate the sum of the elements of nums between indices left and right inclusive where left <= right.

Implement the NumArray class:

    NumArray(int[] nums) Initializes the object with the integer array nums.
    int sumRange(int left, int right) Returns the sum of the elements of nums between indices left and right inclusive (i.e. nums[left] + nums[left + 1] + ... + nums[right]).

 

Example 1:

Input
["NumArray", "sumRange", "sumRange", "sumRange"]
[[[-2, 0, 3, -5, 2, -1]], [0, 2], [2, 5], [0, 5]]
Output
[null, 1, -1, -3]

Explanation
NumArray numArray = new NumArray([-2, 0, 3, -5, 2, -1]);
numArray.sumRange(0, 2); // return (-2) + 0 + 3 = 1
numArray.sumRange(2, 5); // return 3 + (-5) + 2 + (-1) = -1
numArray.sumRange(0, 5); // return (-2) + 0 + 3 + (-5) + 2 + (-1) = -3

 

Constraints:

    1 <= nums.length <= 104
    -105 <= nums[i] <= 105
    0 <= left <= right < nums.length
    At most 104 calls will be made to sumRange.


 */