﻿
namespace PracticeLeetCodeCS.Easy;

public class SearchInsertPosition35
{
    public int SearchInsert11(int[] nums, int target)
    {
        int left = 0;
        int right = nums.Length - 1;

        while (right >= left)
        {
            int mid = (left + right) / 2;
            if (nums[mid] == target)
                return mid;

            else if (nums[mid] < target)
                left = mid + 1;
            else
                right = mid - 1;
        }
        
        return right + 1;

    }
    public int SearchInsert3(int[] nums, int target)
    {
        for (int i = 0; i < nums.Length; i++)
        {
            if (nums[i] == target)
                return i;
            else if (nums[i] > target)
                return i;
        }
                        
        return nums.Length;

    }
    public int SearchInsert(int[] nums, int target)
    {
        int searchIndex = nums.Length / 2;
        int left = 0;
        int right = nums.Length - 1;
                
        while (right >= left)
        {
            if (target == nums[searchIndex])            
                return searchIndex;                

            if (target > nums[searchIndex])            
                left = searchIndex;                            
            else            
                right = searchIndex;                
            
            searchIndex = (left + right) / 2;    
            if (right - left <= 1)
            {
                if (nums[right] == target || (target > nums[left] && target < nums[right]))
                    return right;                                                
                if (target > nums[right])
                    return right + 1;
                if (nums[left] >= target)
                    return left;                
            }
        }
        return right + 1;
    }
    public void Test()
    {
        //int[] nums = { 1, 3, 5, 6, 9, 15, 17, 22, 25 };
        //int target = 18;
        //int[] nums = { 1, 3, 5, 6 };
        //int target = 0;
        int[] nums = { 1};
        int target = 0;

        int output = SearchInsert(nums, target);
    }
}

/*
 35. Search Insert Position
Easy

Given a sorted array of distinct integers and a target value, return the index if the target is found. If not, return the index where it would be if it were inserted in order.

You must write an algorithm with O(log n) runtime complexity.

 

Example 1:

Input: nums = [1,3,5,6], target = 5
Output: 2

Example 2:

Input: nums = [1,3,5,6], target = 2
Output: 1

Example 3:

Input: nums = [1,3,5,6], target = 7
Output: 4

 

Constraints:

    1 <= nums.length <= 104
    -104 <= nums[i] <= 104
    nums contains distinct values sorted in ascending order.
    -104 <= target <= 104


 */