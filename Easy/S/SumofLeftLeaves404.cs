﻿namespace PracticeLeetCodeCS.Easy;

internal class SumofLeftLeaves404
{
    // Definition for a binary tree node.
    public class TreeNode
    {
        public int val;
        public TreeNode left;
        public TreeNode right;
        public TreeNode(int val = 0, TreeNode left = null, TreeNode right = null)
        {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }
    public int SumOfLeftLeaves(TreeNode root)
    {
        if (root == null) return 0;
        if (root.left == null && root.right == null)
            return 0;        
        int sum = 0;
        if (root.left != null)
        {
            if (root.left.left == null && root.left.right == null)
                sum += root.left.val;
        }

        sum += SumOfLeftLeaves(root.left);
        sum += SumOfLeftLeaves(root.right);

        return sum;

    }
    public void Test()
    {

        /*TreeNode right_right = new TreeNode(7);
        TreeNode right_left = new TreeNode(15);

        TreeNode left = new TreeNode(9);
        TreeNode right = new TreeNode(20, right_left, right_right);

        TreeNode root = new TreeNode(3, left, right);*/
        TreeNode root = new TreeNode(1, new TreeNode(2));

        int sum = SumOfLeftLeaves(root);
    }
}
/*
 404. Sum of Left Leaves
Easy

Given the root of a binary tree, return the sum of all left leaves.

A leaf is a node with no children. A left leaf is a leaf that is the left child of another node.

 

Example 1:

Input: root = [3,9,20,null,null,15,7]
Output: 24
Explanation: There are two left leaves in the binary tree, with values 9 and 15 respectively.

Example 2:

Input: root = [1]
Output: 0

 

Constraints:

    The number of nodes in the tree is in the range [1, 1000].
    -1000 <= Node.val <= 1000


 */