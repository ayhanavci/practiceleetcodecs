﻿using System.Collections;

namespace PracticeLeetCodeCS.Easy;
internal class SecondLargestDigitinaString1796
{
    public int SecondHighest(string s)
    {
        List<int> numbers = new List<int>();

        for (int i = 0; i < s.Length; ++i)        
            if (Char.IsDigit(s[i])) numbers.Add(s[i] - '0');
        numbers.Sort();
        numbers.Reverse();

        for (int i = 0; i < numbers.Count; ++i)        
            if (numbers[i] < numbers[0]) return numbers[i];        

        return -1;      
    }
    public void Test()
    {
        var retVal = SecondHighest("ck077");
    }
}

/*
 1796. Second Largest Digit in a String
Easy
401
102
Companies

Given an alphanumeric string s, return the second largest numerical digit that appears in s, or -1 if it does not exist.

An alphanumeric string is a string consisting of lowercase English letters and digits.

 

Example 1:

Input: s = "dfa12321afd"
Output: 2
Explanation: The digits that appear in s are [1, 2, 3]. The second largest digit is 2.

Example 2:

Input: s = "abc1111"
Output: -1
Explanation: The digits that appear in s are [1]. There is no second largest digit. 

 

Constraints:

    1 <= s.length <= 500
    s consists of only lowercase English letters and/or digits.


 */