﻿namespace PracticeLeetCodeCS.Easy;

internal class SingleNumber136
{
    public int SingleNumber2(int[] nums)
    {
        int constraint = 10001;
        bool fMatchFound = false;
        for (int i = 0; i < nums.Length; i++)
        {
            if (nums[i] == constraint)
                continue;
            fMatchFound = false;
            int j = i + 1;
            for (; j < nums.Length; j++)
            {
                if (nums[j] == constraint)
                    continue;
                if (nums[i] == nums[j])
                {
                    nums[i] = constraint;
                    nums[j] = constraint;
                    fMatchFound = true;
                    break;
                }
            }
            if (!fMatchFound)
                return nums[i];
        }
       
        return 0;
    }
    public int SingleNumber(int[] nums)
    {
        List<int> numsList = new List<int>(nums);

        bool fContinueSearch = true;

        while (numsList.Count > 1 && fContinueSearch)
        {
            fContinueSearch = false;
            for (int i = 1; i < numsList.Count; ++i)
            {
                if (numsList[0] == numsList[i])
                {
                    numsList.RemoveAt(i);
                    numsList.RemoveAt(0);                    
                    fContinueSearch = true;
                    break;
                }

            }
         
        }

        return numsList[0];   
    }
    public void Test()
    {
        int[] nums = { 1,1,2,3,4,5,4,5,3,-1,2 };
        //int[] nums = { 2, 2, 1 };
        int result = SingleNumber(nums);
    }
}

/*
 136. Single Number
Easy

Given a non-empty array of integers nums, every element appears twice except for one. Find that single one.

You must implement a solution with a linear runtime complexity and use only constant extra space.

 

Example 1:

Input: nums = [2,2,1]
Output: 1

Example 2:

Input: nums = [4,1,2,1,2]
Output: 4

Example 3:

Input: nums = [1]
Output: 1

 

Constraints:

    1 <= nums.length <= 3 * 104
    -3 * 104 <= nums[i] <= 3 * 104
    Each element in the array appears twice except for one element which appears only once.


 */