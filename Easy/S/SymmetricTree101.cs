﻿namespace PracticeLeetCodeCS.Easy;

internal class SymmetricTree101
{
    //Definition for a binary tree node.
    public class TreeNode
    {
        public int val;
        public TreeNode? left;
        public TreeNode? right;
        public TreeNode(int val = 0, TreeNode? left = null, TreeNode? right = null)
        {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }
    public bool IsSymmetric(TreeNode? root)
    {        
        List<TreeNode?> nextDepth = new List<TreeNode?>();
        List<TreeNode?> children = new List<TreeNode?>();
        
        nextDepth.Add(root);
        
        while (true)
        {
            for (int i = 0; i < nextDepth.Count; i++)
            {
                if (nextDepth[i] == null)
                {
                    children.Add(null);
                    children.Add(null);
                }
                else
                {
                    children.Add(nextDepth[i].left);
                    children.Add(nextDepth[i].right);
                }
                
            }          

            bool fAllChildrenAreNull = true;
            for (int index = 0; index < children.Count / 2; index++)
            {
                if (children[index] == null && children[children.Count - 1 - index] != null)
                    return false;
                if (children[index] != null && children[children.Count - 1 - index] == null)
                    return false;
                if (children[index] == null && children[children.Count - 1 - index] == null)
                    continue;
                fAllChildrenAreNull = false;
                if (children[index].val != children[children.Count - 1 - index].val)
                    return false;
            }
            if (fAllChildrenAreNull) return true;
            nextDepth.Clear();
            nextDepth.AddRange(children);
            children.Clear();
        }                            
        
    }
    public void Test()
    {
        /*TreeNode depth21Left = new TreeNode(3, new TreeNode(7), new TreeNode(5));
        TreeNode depth21Right = new TreeNode(4, new TreeNode(9), new TreeNode(6));

        TreeNode depth22Left = new TreeNode(4, new TreeNode(6), new TreeNode(9));
        TreeNode depth22Right = new TreeNode(3, new TreeNode(5), new TreeNode(7));

        TreeNode depth1Left = new TreeNode(2, depth21Left, depth21Right);
        TreeNode depth1Right = new TreeNode(2, depth22Left, depth22Right);

        TreeNode root = new TreeNode(1, depth1Left, depth1Right);*/


        TreeNode? depth21Left = null;
        TreeNode depth21Right = new TreeNode(3);

        TreeNode depth22Left = new TreeNode(3);
        TreeNode? depth22Right = null;

        TreeNode depth1Left = new TreeNode(2, depth21Left, depth21Right);
        TreeNode depth1Right = new TreeNode(2, depth22Left, depth22Right);

        TreeNode root = new TreeNode(1, depth1Left, depth1Right);
        bool result = IsSymmetric(root);
    }
}
/*
 101. Symmetric Tree
Easy

Given the root of a binary tree, check whether it is a mirror of itself (i.e., symmetric around its center).

 

Example 1:

Input: root = [1,2,2,3,4,4,3]
Output: true

Example 2:

Input: root = [1,2,2,null,3,null,3]
Output: false

 

Constraints:

    The number of nodes in the tree is in the range [1, 1000].
    -100 <= Node.val <= 100

 
Follow up: Could you solve it both recursively and iteratively?
 */