﻿namespace PracticeLeetCodeCS.Easy;

internal class SumofAllOddLengthSubarrays1588
{
    public int SumOddLengthSubarrays(int[] arr)
    {
        int sum = 0;
        for (int odd = 1; odd <= arr.Length; odd+=2)
        {
            for (int i = 0; i < arr.Length; ++i)
            {
                if (i + odd <= arr.Length)
                {
                    for (int j = 0; j < odd; ++j)
                        sum += arr[j + i];
                }
                else
                    break;
            }
        }
        return sum;
    }
    public void Test()
    {
        int[] arr1 = { 1, 4, 2, 5, 3 };
        var retVal = SumOddLengthSubarrays(arr1);

        int[] arr2 = { 1, 2 };
        var retVal2 = SumOddLengthSubarrays(arr2);

        int[] arr3 = { 10, 11, 12 };
        var retVal3 = SumOddLengthSubarrays(arr3);
    }
}

/*
 588. Sum of All Odd Length Subarrays
Easy
3K
223
Companies

Given an array of positive integers arr, return the sum of all possible odd-length subarrays of arr.

A subarray is a contiguous subsequence of the array.

 

Example 1:

Input: arr = [1,4,2,5,3]
Output: 58
Explanation: The odd-length subarrays of arr and their sums are:
[1] = 1
[4] = 4
[2] = 2
[5] = 5
[3] = 3
[1,4,2] = 7
[4,2,5] = 11
[2,5,3] = 10
[1,4,2,5,3] = 15
If we add all these together we get 1 + 4 + 2 + 5 + 3 + 7 + 11 + 10 + 15 = 58

Example 2:

Input: arr = [1,2]
Output: 3
Explanation: There are only 2 subarrays of odd length, [1] and [2]. Their sum is 3.

Example 3:

Input: arr = [10,11,12]
Output: 66

 

Constraints:

    1 <= arr.length <= 100
    1 <= arr[i] <= 1000

 

Follow up:

Could you solve this problem in O(n) time complexity?

 */