﻿namespace PracticeLeetCodeCS.Easy;

internal class Shift2DGrid1260
{
    public IList<IList<int>> ShiftGrid(int[][] grid, int k)
    {        
        IList<IList<int>> retVal = new List<IList<int>>();
        k = k % (grid.Length * grid[0].Length);

        for (int i = 0; i < grid.Length; ++i)
        {
            List<int> newRow = new List<int>();
            retVal.Insert(i, newRow);
            for (int j = 0; j < grid[0].Length; ++j)            
                newRow.Insert(j, 0);
            
        }
            

        for (int i = 0; i < grid.Length; i++)
        {
            for (int j = 0; j < grid[0].Length; ++j)
            {
                //int rowDiff = (k + j) / grid[0].Length;
                int distFromZero = i * grid[0].Length + j;
                int shiftTo = (distFromZero + k) % (grid.Length * grid[0].Length);
                int newRow = shiftTo / grid[0].Length;
                int newCol = shiftTo % grid[0].Length;
                retVal[newRow][newCol] = grid[i][j];                                
            }
        }
        return retVal;
    }
    public void Test()
    {
        int[][] grid = new int[3][];
        grid[0] = new int[] { 1,2,3};
        grid[1] = new int[] { 4,5,6};
        grid[2] = new int[] { 7,8,9};

        var retVal = ShiftGrid(grid, 1);

    }
}


/*
 260. Shift 2D Grid
Easy
1.5K
319
Companies

Given a 2D grid of size m x n and an integer k. You need to shift the grid k times.

In one shift operation:

    Element at grid[i][j] moves to grid[i][j + 1].
    Element at grid[i][n - 1] moves to grid[i + 1][0].
    Element at grid[m - 1][n - 1] moves to grid[0][0].

Return the 2D grid after applying shift operation k times.

 

Example 1:

Input: grid = [[1,2,3],[4,5,6],[7,8,9]], k = 1
Output: [[9,1,2],[3,4,5],[6,7,8]]

Example 2:

Input: grid = [[3,8,1,9],[19,7,2,5],[4,6,11,10],[12,0,21,13]], k = 4
Output: [[12,0,21,13],[3,8,1,9],[19,7,2,5],[4,6,11,10]]

Example 3:

Input: grid = [[1,2,3],[4,5,6],[7,8,9]], k = 9
Output: [[1,2,3],[4,5,6],[7,8,9]]

 

Constraints:

    m == grid.length
    n == grid[i].length
    1 <= m <= 50
    1 <= n <= 50
    -1000 <= grid[i][j] <= 1000
    0 <= k <= 100


 */
