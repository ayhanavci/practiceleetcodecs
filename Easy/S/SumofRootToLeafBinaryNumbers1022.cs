﻿using System.Text;

namespace PracticeLeetCodeCS.Easy;

internal class SumofRootToLeafBinaryNumbers1022
{

    //Definition for a binary tree node.
    public class TreeNode
    {
        public int val;
        public TreeNode left;
        public TreeNode right;
        public TreeNode(int val = 0, TreeNode left = null, TreeNode right = null)
        {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }
    List<string> numbers = new List<string>();

    public void CreateBinaries(TreeNode root, string number)
    {
        if (root == null) return;
        number += root.val.ToString();
        if (root.left == null && root.right == null)
        {
            numbers.Add(number.ToString());
            return;
        }
        CreateBinaries(root.left, number);
        CreateBinaries(root.right, number);
    }

    public int SumRootToLeaf(TreeNode root)
    {
        CreateBinaries(root, "");

        int sum = 0;
        for (int i = 0; i < numbers.Count; ++i)
        {
            for (int j = 0; j < numbers[i].Length; ++j)    
                if (numbers[i][j] == '1')
                    sum += (int) Math.Pow(2, numbers[i].Length - j - 1);
            
        }
        return sum;
    }
    public void Test()
    {
        TreeNode root = new TreeNode(1,
            new TreeNode(0, new TreeNode(0), new TreeNode(1)),
            new TreeNode(1, new TreeNode(0), new TreeNode(1)));

        var retVal = SumRootToLeaf(root);
    }
}

/*
 1022. Sum of Root To Leaf Binary Numbers
Easy
2.9K
172
Companies

You are given the root of a binary tree where each node has a value 0 or 1. Each root-to-leaf path represents a binary number starting with the most significant bit.

    For example, if the path is 0 -> 1 -> 1 -> 0 -> 1, then this could represent 01101 in binary, which is 13.

For all leaves in the tree, consider the numbers represented by the path from the root to that leaf. Return the sum of these numbers.

The test cases are generated so that the answer fits in a 32-bits integer.

 

Example 1:

Input: root = [1,0,1,0,1,0,1]
Output: 22
Explanation: (100) + (101) + (110) + (111) = 4 + 5 + 6 + 7 = 22

Example 2:

Input: root = [0]
Output: 0

 

Constraints:

    The number of nodes in the tree is in the range [1, 1000].
    Node.val is 0 or 1.


 */