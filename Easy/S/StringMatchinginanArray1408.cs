﻿namespace PracticeLeetCodeCS.Easy;

internal class StringMatchinginanArray1408
{
    public IList<string> StringMatching(string[] words)
    {
        IList<string> result = new List<string>();

        for (int i = 0; i < words.Length; ++i)
        {
            for (int j = 0; j < words.Length; ++j)
            {                
                if (i == j || words[i].Length > words[j].Length)
                    continue;
                if (words[j].IndexOf(words[i]) != -1)
                {
                    result.Add(words[i]);
                    break;
                }
                    
            }
        }

        return result;
    }
    public void Test()
    {

    }
}

/*
 1408. String Matching in an Array
Easy
689
79
Companies

Given an array of string words, return all strings in words that is a substring of another word. You can return the answer in any order.

A substring is a contiguous sequence of characters within a string

 

Example 1:

Input: words = ["mass","as","hero","superhero"]
Output: ["as","hero"]
Explanation: "as" is substring of "mass" and "hero" is substring of "superhero".
["hero","as"] is also a valid answer.

Example 2:

Input: words = ["leetcode","et","code"]
Output: ["et","code"]
Explanation: "et", "code" are substring of "leetcode".

Example 3:

Input: words = ["blue","green","bu"]
Output: []
Explanation: No string of words is substring of another string.

 

Constraints:

    1 <= words.length <= 100
    1 <= words[i].length <= 30
    words[i] contains only lowercase English letters.
    All the strings of words are unique.


 */