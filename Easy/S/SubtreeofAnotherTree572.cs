﻿namespace PracticeLeetCodeCS.Easy;

internal class SubtreeofAnotherTree572
{
   
    //Definition for a binary tree node.
    public class TreeNode {
        public int val;
        public TreeNode left;
        public TreeNode right;
        public TreeNode(int val=0, TreeNode left=null, TreeNode right=null) {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }
 
    public bool IsSameTree(TreeNode root, TreeNode subRoot)
    {
        if (root == null && subRoot == null) return true;
        if (root == null || subRoot == null) return false;

        if (root.val != subRoot.val) return false;
        if (!IsSameTree(root.left, subRoot.left)) return false;
        if (!IsSameTree(root.right, subRoot.right)) return false;

        return true;
    }
    public bool IsSubtree(TreeNode root, TreeNode subRoot)
    {
        if (root == null) return false;

        if (IsSameTree(root, subRoot))
            return true;

        if (IsSubtree(root.left, subRoot))
            return true;

        if (IsSubtree(root.right, subRoot))
            return true;

        return false;
    }
    public void Test()
    {
        /*TreeNode root = new TreeNode(3, 
            new TreeNode(4,
                new TreeNode(1),
                new TreeNode(2)),
            new TreeNode(5));*/

        TreeNode root = new TreeNode(3,
            new TreeNode(4,
                new TreeNode(1),
                new TreeNode(2, new TreeNode(0), null)),
            new TreeNode(5));

        TreeNode subRoot = new TreeNode(4, new TreeNode(1), new TreeNode(2));


        var retVal = IsSubtree(root, subRoot);
    }
}
/*
 572. Subtree of Another Tree
Easy
6.7K
376
Companies

Given the roots of two binary trees root and subRoot, return true if there is a subtree of root with the same structure and node values of subRoot and false otherwise.

A subtree of a binary tree tree is a tree that consists of a node in tree and all of this node"s descendants. The tree tree could also be considered as a subtree of itself.

 

Example 1:

Input: root = [3,4,5,1,2], subRoot = [4,1,2]
Output: true

Example 2:

Input: root = [3,4,5,1,2,null,null,null,null,0], subRoot = [4,1,2]
Output: false

 

Constraints:

    The number of nodes in the root tree is in the range [1, 2000].
    The number of nodes in the subRoot tree is in the range [1, 1000].
    -104 <= root.val <= 104
    -104 <= subRoot.val <= 104


 */