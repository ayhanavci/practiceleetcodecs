﻿namespace PracticeLeetCodeCS.Easy;

internal class Sqrtx69
{
    public int MySqrt(int x)
    {
        if (x == 1 ) return 1;

        long right = x / 2;
        long left = 1;
        long searchIndex = (right + left) / 2;

        while (right > left + 1)
        {
            double result = (searchIndex * searchIndex);
            if (result == x)
                return (int)searchIndex;
            if (result > x)            
                right = searchIndex;                                       
            else            
                left = searchIndex;                
            
            searchIndex = (right + left) / 2;
        }        

        return right * right > x ? (int)left : (int)right;
    }
    public int MySqrtEx(int x)
    {                
        for (int root = 1;; ++root)
        {
            int test = root * root;            
            if (test > x) return root - 1;
            if (test == x) return root;
        }        
    }
    public void Test()
    {
        //MySqrt(2147395599);
        Console.WriteLine($"4:{MySqrt(4)} 8:{MySqrt(8)} 7:{MySqrt(7)} 16:{MySqrt(16)} 9:{MySqrt(9)} 26:{MySqrt(26)} 47:{MySqrt(47)} 100:{MySqrt(100)} 78956:{MySqrt(78956)}");
    }
}

/*
 69. Sqrt(x)
Easy

Given a non-negative integer x, return the square root of x rounded down to the nearest integer. The returned integer should be non-negative as well.

You must not use any built-in exponent function or operator.

    For example, do not use pow(x, 0.5) in c++ or x ** 0.5 in python.

 

Example 1:

Input: x = 4
Output: 2
Explanation: The square root of 4 is 2, so we return 2.

Example 2:

Input: x = 8
Output: 2
Explanation: The square root of 8 is 2.82842..., and since we round it down to the nearest integer, 2 is returned.

 

Constraints:

    0 <= x <= 231 - 1


 */