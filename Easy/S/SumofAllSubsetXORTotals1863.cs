namespace PracticeLeetCodeCS.Easy;

//TODO: çözümleri anlamadim
internal class SumofAllSubsetXORTotals1863
{
    public int SubsetXORSum(int[] nums) 
    {
        return helper(nums, 0, 0);
    }
     private int helper(int[] nums, int index, int currentXor) 
     {
        // return currentXor when all elements in nums are already considered
        if (index == nums.Length) return currentXor;
        
        // calculate sum of xor with current element
        int withElement = helper(nums, index + 1, currentXor ^ nums[index]);
        
        // calculate sum of xor without current element
        int withoutElement = helper(nums, index + 1, currentXor);
        
        // return sum of xors from recursion
        return withElement + withoutElement;
    }
    public void Test()
    {

    }
}
//https://leetcode.com/problems/sum-of-all-subset-xor-totals/solutions/1242073/java-backtracking-approach-with-explanation/?orderBy=most_votes
/*
General Approach

    Firstly, we need to generate all the possible subsets from the input array.
    Then, for all calculated subsets, calculate the XOR of all elements in each subset.
    Return the sum of all calculated XORs.

Detailed Approach

    Generate all possible subsets:
    We can recursively generate all possible subsets given an array. For each element in the array, you will have two choices: Include the current element in the array in the generated subset, or withhold the current element from the generated subset.
    Visualization:
    image

    If we are going the step by step approach, we would store all the subsets in a list and calculate their XORs. However, this is not necessary as we can calculate the ongoing XOR total while generating the subsets. The trick is to be able to remember the state of the current xor that is calculated for each recursive step.

    As we are not explicitly storing the subsets in a list, we just need to return the sum of the calculated xor when the current element is considered + the calculated xor when the current element is not considered.

*/
/*
863. Sum of All Subset XOR Totals
Easy
1.2K
112
Companies

The XOR total of an array is defined as the bitwise XOR of all its elements, or 0 if the array is empty.

    For example, the XOR total of the array [2,5,6] is 2 XOR 5 XOR 6 = 1.

Given an array nums, return the sum of all XOR totals for every subset of nums. 

Note: Subsets with the same elements should be counted multiple times.

An array a is a subset of an array b if a can be obtained from b by deleting some (possibly zero) elements of b.

 

Example 1:

Input: nums = [1,3]
Output: 6
Explanation: The 4 subsets of [1,3] are:
- The empty subset has an XOR total of 0.
- [1] has an XOR total of 1.
- [3] has an XOR total of 3.
- [1,3] has an XOR total of 1 XOR 3 = 2.
0 + 1 + 3 + 2 = 6

Example 2:

Input: nums = [5,1,6]
Output: 28
Explanation: The 8 subsets of [5,1,6] are:
- The empty subset has an XOR total of 0.
- [5] has an XOR total of 5.
- [1] has an XOR total of 1.
- [6] has an XOR total of 6.
- [5,1] has an XOR total of 5 XOR 1 = 4.
- [5,6] has an XOR total of 5 XOR 6 = 3.
- [1,6] has an XOR total of 1 XOR 6 = 7.
- [5,1,6] has an XOR total of 5 XOR 1 XOR 6 = 2.
0 + 5 + 1 + 6 + 4 + 3 + 7 + 2 = 28

Example 3:

Input: nums = [3,4,5,6,7,8]
Output: 480
Explanation: The sum of all XOR totals for every subset is 480.

 

Constraints:

    1 <= nums.length <= 12
    1 <= nums[i] <= 20


*/