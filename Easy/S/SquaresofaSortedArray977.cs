﻿namespace PracticeLeetCodeCS.Easy;

internal class SquaresofaSortedArray977
{
    public int[] SortedSquares(int[] nums)
    {
        for (int i = 0; i < nums.Length; i++)        
            nums[i] *= nums[i];
        Array.Sort(nums);
        return nums;
    }
    public int[] SortedSquaresLATER(int[] nums)
    {
        Stack<int> minusNumbers = new Stack<int>();
        int[] result = new int[nums.Length];

        int readPos = 0;
        int insertPos = 0;
        for (; readPos < nums.Length; readPos++)
        {
            if (nums[readPos] > 0)
                break;
            minusNumbers.Push(nums[readPos] * nums[readPos]);
        }

        for (; readPos < nums.Length; readPos++)
        {
            int curNumber = nums[readPos] * nums[readPos];
            if (minusNumbers.Count > 0)
            {
                int minusNumber = minusNumbers.Peek();
                ////////
            }
        }

        while (minusNumbers.Count > 0)        
            result[insertPos] = minusNumbers.Pop();

        return result;
    }
    public void Test()
    {

    }
}

/*
 977. Squares of a Sorted Array
Easy
7.1K
178
Companies

Given an integer array nums sorted in non-decreasing order, return an array of the squares of each number sorted in non-decreasing order.

 

Example 1:

Input: nums = [-4,-1,0,3,10]
Output: [0,1,9,16,100]
Explanation: After squaring, the array becomes [16,1,0,9,100].
After sorting, it becomes [0,1,9,16,100].

Example 2:

Input: nums = [-7,-3,2,3,11]
Output: [4,9,9,49,121]

 

Constraints:

    1 <= nums.length <= 104
    -104 <= nums[i] <= 104
    nums is sorted in non-decreasing order.

 
Follow up: Squaring each element and sorting the new array is very trivial, could you find an O(n) solution using a different approach?
 */