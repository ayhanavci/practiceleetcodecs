﻿namespace PracticeLeetCodeCS.Easy;

internal class SortArrayByParity905
{
    public int[] SortArrayByParity(int[] nums)
    {        
        Queue<int> oddIndeces = new Queue<int>();

        for (int i = 0; i < nums.Length; i++)
        {
            if (nums[i] % 2 != 0)
                oddIndeces.Enqueue(i);
            else if (oddIndeces.Count > 0)
            {
                int oddIndex = oddIndeces.Dequeue();                
                int temp = nums[oddIndex];
                nums[oddIndex] = nums[i];
                nums[i] = temp;
                oddIndeces.Enqueue(i);
            }
        }
        return nums;
    }
    public void Test()
    {
        int[] nums = { 1, 0, 2};
        var retVal = SortArrayByParity(nums);
    }
}

/*
 905. Sort Array By Parity
Easy
4.1K
132
Companies

Given an integer array nums, move all the even integers at the beginning of the array followed by all the odd integers.

Return any array that satisfies this condition.

 

Example 1:

Input: nums = [3,1,2,4]
Output: [2,4,3,1]
Explanation: The outputs [4,2,3,1], [2,4,1,3], and [4,2,1,3] would also be accepted.

Example 2:

Input: nums = [0]
Output: [0]

 

Constraints:

    1 <= nums.length <= 5000
    0 <= nums[i] <= 5000


 */