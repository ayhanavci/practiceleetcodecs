﻿namespace PracticeLeetCodeCS.Easy;

internal class SetMismatch645
{
    public int[] FindErrorNums(int[] nums)
    {
        int[] retVal = new int[2];
        retVal[0] = -1;
        
        HashSet<long> numberSet = new HashSet<long>();
        long total = 0;
        for (int i = 0; i < nums.Length; ++i)
        {            
            if (retVal[0] == -1)
            {
                if (numberSet.Contains(nums[i]))
                    retVal[0] = nums[i];
            }            
            numberSet.Add(nums[i]);
            total += i + 1;
        }            

        long sum = numberSet.Sum();
        retVal[1] = (int)(total - sum);

       
        return retVal;

    }
    public void Test()
    {
        //int[] nums = { 1, 2, 2, 4 };
        //int[] nums = { 3, 2, 2 };
        int[] nums = { 2, 2 };
        var retval = FindErrorNums(nums);
    }
}


/*
 645. Set Mismatch
Easy
3.4K
785
Companies

You have a set of integers s, which originally contains all the numbers from 1 to n. Unfortunately, due to some error, one of the numbers in s got duplicated to another number in the set, which results in repetition of one number and loss of another number.

You are given an integer array nums representing the data status of this set after the error.

Find the number that occurs twice and the number that is missing and return them in the form of an array.

 

Example 1:

Input: nums = [1,2,2,4]
Output: [2,3]

Example 2:

Input: nums = [1,1]
Output: [1,2]

 

Constraints:

    2 <= nums.length <= 104
    1 <= nums[i] <= 104


 */