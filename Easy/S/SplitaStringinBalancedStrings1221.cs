﻿namespace PracticeLeetCodeCS.Easy;

internal class SplitaStringinBalancedStrings1221
{
    public int BalancedStringSplit(string s)
    {
        int rCount = 0;
        int lCount = 0;
        int numberOfSubstrings = 0;
        for (int i = 0; i < s.Length; i++)
        {
            if (s[i] == 'L') lCount++;
            else rCount++;
            if (lCount > 0 && rCount > 0 && lCount == rCount)
            {
                numberOfSubstrings++;
                lCount = 0;
                rCount = 0;
            }
        }
        return numberOfSubstrings;
    }
    public void Test()
    {
        //string s = "RLRRLLRLRL";
        string s = "RLRRRLLRLL";
        var retVal = BalancedStringSplit(s);
    }
}

/*
 1221. Split a String in Balanced Strings
Easy
2.2K
827
Companies

Balanced strings are those that have an equal quantity of 'L' and 'R' characters.

Given a balanced string s, split it into some number of substrings such that:

    Each substring is balanced.

Return the maximum number of balanced strings you can obtain.

 

Example 1:

Input: s = "RLRRLLRLRL"
Output: 4
Explanation: s can be split into "RL", "RRLL", "RL", "RL", each substring contains same number of 'L' and 'R'.

Example 2:

Input: s = "RLRRRLLRLL"
Output: 2
Explanation: s can be split into "RL", "RRRLLRLL", each substring contains same number of 'L' and 'R'.
Note that s cannot be split into "RL", "RR", "RL", "LR", "LL", because the 2nd and 5th substrings are not balanced.

Example 3:

Input: s = "LLLLRRRR"
Output: 1
Explanation: s can be split into "LLLLRRRR".

 

Constraints:

    2 <= s.length <= 1000
    s[i] is either 'L' or 'R'.
    s is a balanced string.


 */