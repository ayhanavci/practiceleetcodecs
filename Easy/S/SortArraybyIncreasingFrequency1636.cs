﻿namespace PracticeLeetCodeCS.Easy;

internal class SortArraybyIncreasingFrequency1636
{
    public int[] FrequencySort(int[] nums)
    {
        List<int> retVal = new List<int>();
        Dictionary<int, int> frequencies = new Dictionary<int, int>();

        for (int i = 0; i < nums.Length; i++)
        {            
            if (frequencies.ContainsKey(nums[i]))            
                frequencies[nums[i]]++;            
            else            
                frequencies.Add(nums[i], 1);            
        }

        List<int> lowest = new List<int>();
        while (frequencies.Count > 0)
        {            
            int lowestFrequency = int.MaxValue;            

            for (int i = 0; i < frequencies.Count; i++)
            {
                var item = frequencies.ElementAt(i);
                int number = item.Key;
                int frequency = item.Value;
                if (frequency < lowestFrequency)
                {
                    lowest.Clear();
                    lowestFrequency = frequency;
                    lowest.Add(number);
                }
                else if (frequency == lowestFrequency)
                {
                    lowest.Add(number);
                }
            }
            lowest.Sort();
            lowest.Reverse();
            for (int i = 0; i < lowest.Count; i++)
            {
                for (int j = 0; j < lowestFrequency; j++)                
                    retVal.Add(lowest[i]);
                frequencies.Remove(lowest[i]);
                
            }

            lowest.Clear();
        }

        return retVal.ToArray();

    }
    public void Test()
    {
        int[] nums = { 1, 1, 2, 2, 2, 3 };
        var retVal = FrequencySort(nums);

        int[] nums2 = { 2, 3, 1, 3, 2 };
        var retVal2 = FrequencySort(nums2);

        int[] nums3 = { -1, 1, -6, 4, 5, -6, 1, 4, 1 };
        var retVal3 = FrequencySort(nums3);
    }
}

/*
 1636. Sort Array by Increasing Frequency
Easy
2.2K
86
Companies

Given an array of integers nums, sort the array in increasing order based on the frequency of the values. If multiple values have the same frequency, sort them in decreasing order.

Return the sorted array.

 

Example 1:

Input: nums = [1,1,2,2,2,3]
Output: [3,1,1,2,2,2]
Explanation: '3' has a frequency of 1, '1' has a frequency of 2, and '2' has a frequency of 3.

Example 2:

Input: nums = [2,3,1,3,2]
Output: [1,3,3,2,2]
Explanation: '2' and '3' both have a frequency of 2, so they are sorted in decreasing order.

Example 3:

Input: nums = [-1,1,-6,4,5,-6,1,4,1]
Output: [5,-1,4,4,-6,-6,1,1,1]

 

Constraints:

    1 <= nums.length <= 100
    -100 <= nums[i] <= 100


 */