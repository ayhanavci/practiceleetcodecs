﻿namespace PracticeLeetCodeCS.Easy;

internal class SurfaceAreaof3DShapes892
{
    public int SurfaceArea(int[][] grid)
    {
        int area = 0;

        for (int i = 0; i < grid.Length; i++)
        {
            for (int j = 0; j < grid[0].Length; j++)
            {
                if (grid[i][j] > 0) area += grid[i][j] * 4 + 2;
                if (i > 0) area -= Math.Min(grid[i][j], grid[i - 1][j]) * 2;
                if (j > 0) area -= Math.Min(grid[i][j], grid[i][j - 1]) * 2;
                //area += grid[i][j] * 6 - 2 * (grid[i][j] - 1);
                //if (i < grid.Length - 1) area -= 2 * Math.Min(grid[i][j], grid[i + 1][j]);
                //if (j < grid.Length - 1) area -= 2 * Math.Min(grid[i][j], grid[i][j + 1]);
            }
        }
        return area;
    }
    public void Test()
    {

    }
}
/*
 Surface area of 1 cube is equal to 6 according to description (1 * 1 * 1) * 6, cube have 6 sides.
grid[i][j] is a count of cubes placed on top of grid cell. For instance, if we have a grid[i][j] = 2, it means we placed two cubes one on other and they have a common area is equal to 2, 
because we have two connected sides. If the grid[i][j] = 3, then common area is equal to 4.
It turns out when grid[i][j] is equal to:
2 cubes - 2
3 cubes - 4
4 cubes - 6
5 cubes - 8 => 2 * (grid[i][j] - 1)
We also have to substract common area on y and x axis. We only compute up to n - 1 for both axis by calculating minimum of two connected Vs and multiply by 2.
Red squares is a common areas which we have to subtract from resulting total surface area.
The following depicted picture shows example: [[2, 3, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]]. The answer is: 5 * 6 - 2 - 4 = 20.
 */
/*
 892. Surface Area of 3D Shapes
Easy
471
664
Companies

You are given an n x n grid where you have placed some 1 x 1 x 1 cubes. Each value v = grid[i][j] represents a tower of v cubes placed on top of cell (i, j).

After placing these cubes, you have decided to glue any directly adjacent cubes to each other, forming several irregular 3D shapes.

Return the total surface area of the resulting shapes.

Note: The bottom face of each shape counts toward its surface area.

 

Example 1:

Input: grid = [[1,2],[3,4]]
Output: 34

Example 2:

Input: grid = [[1,1,1],[1,0,1],[1,1,1]]
Output: 32

Example 3:

Input: grid = [[2,2,2],[2,1,2],[2,2,2]]
Output: 46

 

Constraints:

    n == grid.length == grid[i].length
    1 <= n <= 50
    0 <= grid[i][j] <= 50


 */