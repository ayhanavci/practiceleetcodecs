﻿namespace PracticeLeetCodeCS.Easy;

internal class SortIntegersbyTheNumberof1Bits1356
{
    public int[] SortByBits(int[] arr)
    {        
        var list = new List<Tuple<int, int>>();

        for (int i = 0; i < arr.Length; ++i)
        {
            int number = arr[i];
            int numberOfBits = 0;
            while (number > 0)
            {
                numberOfBits += number & 1;
                number >>= 1;
            }
            list.Add(Tuple.Create(numberOfBits, arr[i]));            
        }
        list.Sort((x, y) =>
        {
            if (x.Item1 != y.Item1)            
                return x.Item1.CompareTo(y.Item1);            
            else             
                return x.Item2.CompareTo(y.Item2);                        
        });

        for (int i = 0; i < list.Count; ++i)        
            arr[i] = list[i].Item2;
        
        return arr;
    }
    public void Test()
    {
        int[] arr = new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8 };
        var retVal = SortByBits(arr);
    }
}

/*
 1356. Sort Integers by The Number of 1 Bits
Easy
1.4K
64
Companies

You are given an integer array arr. Sort the integers in the array in ascending order by the number of 1's in their binary representation and in case of two or more integers have the same number of 1's you have to sort them in ascending order.

Return the array after sorting it.

 

Example 1:

Input: arr = [0,1,2,3,4,5,6,7,8]
Output: [0,1,2,4,8,3,5,6,7]
Explantion: [0] is the only integer with 0 bits.
[1,2,4,8] all have 1 bit.
[3,5,6] have 2 bits.
[7] has 3 bits.
The sorted array by bits is [0,1,2,4,8,3,5,6,7]

Example 2:

Input: arr = [1024,512,256,128,64,32,16,8,4,2,1]
Output: [1,2,4,8,16,32,64,128,256,512,1024]
Explantion: All integers have 1 bit in the binary representation, you should just sort them in ascending order.

 

Constraints:

    1 <= arr.length <= 500
    0 <= arr[i] <= 104


 */