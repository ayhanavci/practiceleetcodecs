namespace PracticeLeetCodeCS.Easy;
//TODO slow 7.14%
internal class StrongPasswordCheckerII2299
{
    public bool StrongPasswordCheckerII(string password) 
    {
        if (password.Length < 8)
            return false;
        
        string special = "!@#$%^&*()-+";

        bool hasUpper = false;
        bool hasLower = false;
        bool hasDigit = false;        
        bool hasSpecial = false;

        for (int i = 0; i < password.Length; ++i)
        {
            if (password[i] >= 'a' && password[i] <= 'z')
                hasLower = true;
            if (password[i] >= 'A' && password[i] <= 'Z')
                hasUpper = true;
            if (password[i] >= '0' && password[i] <= '9')
                hasDigit = true;    
            if (special.IndexOf(password[i]) != -1)
                hasSpecial = true;
            if (i < password.Length - 1)
            {
                if (password[i] == password[i+1])
                    return false;
            }
        }
        return hasUpper && hasLower && hasDigit && hasSpecial;
    }
    public void Test()
    {

    }
}

/*
2299. Strong Password Checker II
Easy
275
35
Companies

A password is said to be strong if it satisfies all the following criteria:

    It has at least 8 characters.
    It contains at least one lowercase letter.
    It contains at least one uppercase letter.
    It contains at least one digit.
    It contains at least one special character. The special characters are the characters in the following string: "!@#$%^&*()-+".
    It does not contain 2 of the same character in adjacent positions (i.e., "aab" violates this condition, but "aba" does not).

Given a string password, return true if it is a strong password. Otherwise, return false.

 

Example 1:

Input: password = "IloveLe3tcode!"
Output: true
Explanation: The password meets all the requirements. Therefore, we return true.

Example 2:

Input: password = "Me+You--IsMyDream"
Output: false
Explanation: The password does not contain a digit and also contains 2 of the same character in adjacent positions. Therefore, we return false.

Example 3:

Input: password = "1aB!"
Output: false
Explanation: The password does not meet the length requirement. Therefore, we return false.

 

Constraints:

    1 <= password.length <= 100
    password consists of letters, digits, and special characters: "!@#$%^&*()-+".


*/