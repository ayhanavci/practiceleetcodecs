﻿namespace PracticeLeetCodeCS.Easy;

internal class SecondMinimumNodeInaBinaryTree671
{

    //Definition for a binary tree node.
    public class TreeNode
    {
        public int val;
        public TreeNode left;
        public TreeNode right;
        public TreeNode(int val = 0, TreeNode left = null, TreeNode right = null)
        {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }

    public int secondMin = -1;
    public int min = -1;

    public int FindSecondMinimumValue(TreeNode root)
    {
        if (root == null) return secondMin;                
        if (min == -1) min = root.val;

        if (root.val > min)
        {
            if (secondMin == -1) secondMin = root.val;
            secondMin = Math.Min(secondMin, root.val);
        }
        FindSecondMinimumValue(root.left);
        FindSecondMinimumValue(root.right);

        return secondMin;
    }
    public void Test()
    {
        TreeNode root = new TreeNode(2,
           new TreeNode(2),
           new TreeNode(5, new TreeNode(5), new TreeNode(7)));

        var retVal = FindSecondMinimumValue(root);
    }
}

/*
 671. Second Minimum Node In a Binary Tree
Easy
1.5K
1.7K
Companies

Given a non-empty special binary tree consisting of nodes with the non-negative value, where each node in this tree has exactly two or zero sub-node. If the node has two sub-nodes, then this node"s value is the smaller value among its two sub-nodes. More formally, the property root.val = min(root.left.val, root.right.val) always holds.

Given such a binary tree, you need to output the second minimum value in the set made of all the nodes" value in the whole tree.

If no such second minimum value exists, output -1 instead.

 

 

Example 1:

Input: root = [2,2,5,null,null,5,7]
Output: 5
Explanation: The smallest value is 2, the second smallest value is 5.

Example 2:

Input: root = [2,2,2]
Output: -1
Explanation: The smallest value is 2, but there isn"t any second smallest value.

 

Constraints:

    The number of nodes in the tree is in the range [1, 25].
    1 <= Node.val <= 231 - 1
    root.val == min(root.left.val, root.right.val) for each internal node of the tree.


 */