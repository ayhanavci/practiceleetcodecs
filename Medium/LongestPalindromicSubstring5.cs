using System.Text;

namespace PracticeLeetCodeCS.Medium;

public class LongestPalindromicSubstring5
{
    public string LongestPalindrome(string s) 
    {
        string currentLongest = "";
        currentLongest += s[0];        
        
        for (int i = 0; i < s.Length; ++i)
        {            
            for (int j = i + currentLongest.Length; j < s.Length; ++j)
            {
                string testSub = s.Substring(i, j - i + 1);
                if (IsPalindrome(testSub) && testSub.Length > currentLongest.Length)                
                    currentLongest = testSub;
                
            }
        }
        
        return currentLongest.ToString();
    }
    public bool IsPalindrome(string s)
    {
        for (int i = 0; i < s.Length / 2; ++i)
        {
            if (s[i] != s[s.Length - i - 1])
                return false;
        }
        return true;
    }
    public void Test()
    {
       
        var result0 = LongestPalindrome("babad");
        var result1 = LongestPalindrome("cbbd");
        var result2 = LongestPalindrome("a");
        var result3 = LongestPalindrome("bb");
    }
}

/*
5. Longest Palindromic Substring
Medium
28.3K
1.7K
Companies

Given a string s, return the longest
palindromic
substring
in s.

 

Example 1:

Input: s = "babad"
Output: "bab"
Explanation: "aba" is also a valid answer.

Example 2:

Input: s = "cbbd"
Output: "bb"

 

Constraints:

    1 <= s.length <= 1000
    s consist of only digits and English letters.



*/