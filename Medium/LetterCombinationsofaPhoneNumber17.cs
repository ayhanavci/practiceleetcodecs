using System.Text;

namespace PracticeLeetCodeCS.Medium;

public class LetterCombinationsofaPhoneNumber17
{
    public IList<string> LetterCombinations(string digits) 
    {
        IList<string> combinations = new List<string>();        
        if (digits.Length == 0) return combinations;

        Dictionary<char, string> keys = new Dictionary<char, string>
        {
            { '2', "abc" },
            { '3', "def" },
            { '4', "ghi" },
            { '5', "jkl" },
            { '6', "mno" },
            { '7', "pqrs" },
            { '8', "tuv" },
            { '9', "wxyz" },
        };

        int size = 1;
        for (int i = 0; i < digits.Length; ++i)         
            size *= keys[digits[i]].Length;      
        
        for (int i = 0; i < size; ++i)
            combinations.Add("");        

        if (digits.Length == 1)
        {
            string chars = keys[digits[0]];
            for (int i = 0; i < chars.Length; ++i)            
                combinations[i] += chars[i];            
        }
        else if (digits.Length == 2)
        {
            int currentIndex = 0;
            string firstDigitChars = keys[digits[0]];
            for (int i = 0; i < firstDigitChars.Length; ++i) 
            {
                string secondDigitChars = keys[digits[1]];
                
                for (int j = 0; j < secondDigitChars.Length; ++j)
                {
                    combinations[currentIndex] += firstDigitChars[i]; 
                    combinations[currentIndex++] += secondDigitChars[j];
                }

            }                
        }
        else if (digits.Length == 3)
        {
            string firstDigitChars = keys[digits[0]];
            int currentIndex = 0;
            for (int i = 0; i < firstDigitChars.Length; ++i) 
            {
                string secondDigitChars = keys[digits[1]];                
                for (int j = 0; j < secondDigitChars.Length; ++j)
                {                    
                    string thirdDigitChars = keys[digits[2]];                    
                    for (int k = 0; k < thirdDigitChars.Length; ++k)
                    {
                        combinations[currentIndex] += firstDigitChars[i]; 
                        combinations[currentIndex] += secondDigitChars[j];
                        combinations[currentIndex++] += thirdDigitChars[k];
                    }
                }

            }                
        }
        else if (digits.Length == 4)
        {
            string firstDigitChars = keys[digits[0]];
            int currentIndex = 0;
            for (int i = 0; i < firstDigitChars.Length; ++i) 
            {
                string secondDigitChars = keys[digits[1]];                
                for (int j = 0; j < secondDigitChars.Length; ++j)
                {                    
                    string thirdDigitChars = keys[digits[2]];                    
                    for (int k = 0; k < thirdDigitChars.Length; ++k)
                    {
                        string fourthDigitChars = keys[digits[3]];
                        for (int l = 0; l < fourthDigitChars.Length; ++l)
                        {
                            combinations[currentIndex] += firstDigitChars[i]; 
                            combinations[currentIndex] += secondDigitChars[j];
                            combinations[currentIndex] += thirdDigitChars[k];
                            combinations[currentIndex++] += fourthDigitChars[l];
                        }
                    }
                }

            }                
        }

        return combinations;
    }
    public void Test()
    {
        var result1 = LetterCombinations("23");
        var result2 = LetterCombinations("");
        var result3 = LetterCombinations("7");
        var result4 = LetterCombinations("293");
        var result5 = LetterCombinations("234");
        var result6 = LetterCombinations("2344");
    }
}
/*
adg
adh
adi
aeg
aeh
aei
afg
afh
afi
bdg
bdh
bdi
beg
beh
bei
bfg
bfh
bfi
cdg
cdh
cdi
ceg
ceh
cei
cfg
cfh
cfi
*/
/*
17. Letter Combinations of a Phone Number
Medium
17.7K
932
Companies

Given a string containing digits from 2-9 inclusive, return all possible letter combinations that the number could represent. Return the answer in any order.

A mapping of digits to letters (just like on the telephone buttons) is given below. Note that 1 does not map to any letters.

 

Example 1:

Input: digits = "23"
Output: ["ad","ae","af","bd","be","bf","cd","ce","cf"]

Example 2:

Input: digits = ""
Output: []

Example 3:

Input: digits = "2"
Output: ["a","b","c"]

 

Constraints:

    0 <= digits.length <= 4
    digits[i] is a digit in the range ['2', '9'].


*/