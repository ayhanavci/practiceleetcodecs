﻿using System.Text;

namespace PracticeLeetCodeCS.Medium;

public class StringtoIntegeratoi8
{
    public int MyAtoi(string s) 
    {
        bool positive = true;
        Int32 number = 0;
        
        
        //Step 1
        s = s.TrimStart();
        //
        if (s == string.Empty) return 0;

        //Step 2
        if (s[0] == '-')
            positive = false;
        
        if (s[0] == '-' || s[0] == '+')
            s = s.Remove(0, 1);
        //        
        if (s == string.Empty) return 0;
        s = s.TrimStart('0');
        if (s == string.Empty) return 0;

        Stack<int> digits = new Stack<int>();
        for (int i = 0; i < s.Length; ++i)
        {
            if (s[i] < '0' || s[i] > '9')
                break;
            
            digits.Push(s[i] - '0');

        }

        int pow = 1;
        while (digits.Count > 0)
        {                        
            int nextDigit = digits.Pop();
            if (pow == 1000000000)
            {
                if (nextDigit > 2)
                    return positive ? Int32.MaxValue : Int32.MinValue;
                                
            }
            else if (pow > 1000000000)
            {
                return positive ? Int32.MaxValue : Int32.MinValue;        
            }
            Int32 limitRoom = Int32.MaxValue - number;                
            Int32 addition = nextDigit * pow;     

            if (positive)
            {                    
                if (addition >= limitRoom) 
                    return Int32.MaxValue;
            }
            else
            {                                                   
                if (addition == limitRoom) 
                    return Int32.MinValue + 1;
                else if (addition > limitRoom)
                    return Int32.MinValue;
            }   
            number += addition;
            
            pow *= 10;
        }

        return positive ? number : -number;

    }
    public void Test()
    {       
        var result00 = MyAtoi("");
        var result01 = MyAtoi("0000000000012345678");
        var result02 = MyAtoi("-2147483647");
        var result0 = MyAtoi("42");
        var result1 = MyAtoi("   -42");
        var result2 = MyAtoi("4193 with words");
        var result3 = MyAtoi("1534236469");
        var result4 = MyAtoi("2147483646");        
        var result5 = MyAtoi("2147483647");        
        var result6 = MyAtoi("2147483648");        
        var result7 = MyAtoi("-2147483648");
        var result8 = MyAtoi("-2147483649");
        var result9 = MyAtoi("-3147483648");
        //2147483647
        //2147483648
    }
}

/*
8. String to Integer (atoi)
Medium
4.1K
12.7K
Companies

Implement the myAtoi(string s) function, which converts a string to a 32-bit signed integer (similar to C/C++'s atoi function).

The algorithm for myAtoi(string s) is as follows:

    Read in and ignore any leading whitespace.
    Check if the next character (if not already at the end of the string) is '-' or '+'. Read this character in if it is either. This determines if the final result is negative or positive respectively. Assume the result is positive if neither is present.
    Read in next the characters until the next non-digit character or the end of the input is reached. The rest of the string is ignored.
    Convert these digits into an integer (i.e. "123" -> 123, "0032" -> 32). If no digits were read, then the integer is 0. Change the sign as necessary (from step 2).
    If the integer is out of the 32-bit signed integer range [-231, 231 - 1], then clamp the integer so that it remains in the range. Specifically, integers less than -231 should be clamped to -231, and integers greater than 231 - 1 should be clamped to 231 - 1.
    Return the integer as the final result.

Note:

    Only the space character ' ' is considered a whitespace character.
    Do not ignore any characters other than the leading whitespace or the rest of the string after the digits.

 

Example 1:

Input: s = "42"
Output: 42
Explanation: The underlined characters are what is read in, the caret is the current reader position.
Step 1: "42" (no characters read because there is no leading whitespace)
         ^
Step 2: "42" (no characters read because there is neither a '-' nor '+')
         ^
Step 3: "42" ("42" is read in)
           ^
The parsed integer is 42.
Since 42 is in the range [-231, 231 - 1], the final result is 42.

Example 2:

Input: s = "   -42"
Output: -42
Explanation:
Step 1: "   -42" (leading whitespace is read and ignored)
            ^
Step 2: "   -42" ('-' is read, so the result should be negative)
             ^
Step 3: "   -42" ("42" is read in)
               ^
The parsed integer is -42.
Since -42 is in the range [-231, 231 - 1], the final result is -42.

Example 3:

Input: s = "4193 with words"
Output: 4193
Explanation:
Step 1: "4193 with words" (no characters read because there is no leading whitespace)
         ^
Step 2: "4193 with words" (no characters read because there is neither a '-' nor '+')
         ^
Step 3: "4193 with words" ("4193" is read in; reading stops because the next character is a non-digit)
             ^
The parsed integer is 4193.
Since 4193 is in the range [-231, 231 - 1], the final result is 4193.

 

Constraints:

    0 <= s.length <= 200
    s consists of English letters (lower-case and upper-case), digits (0-9), ' ', '+', '-', and '.'.


 */