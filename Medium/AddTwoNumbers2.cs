﻿namespace PracticeLeetCodeCS.Medium;

public class AddTwoNumbers2
{
    //Definition for singly-linked list.
    private class ListNode
    {
        public int val;
        public ListNode? next;
        public ListNode(int val = 0, ListNode? next = null)
        {
            this.val = val;
            this.next = next;
        }
    }
    private ListNode? AddTwoNumbersOld(ListNode l1, ListNode l2)
    {
        long firstNumber = 0;
        ListNode currentItem = l1;
        long digit = 1;
        while (true) {
            firstNumber += currentItem.val * digit;
            if (currentItem.next == null)
                break;
            digit *= 10;
            currentItem = currentItem.next;
        }

        long secondNumber = 0;
        currentItem = l2;
        digit = 1;
        while (true)
        {
            secondNumber += currentItem.val * digit;
            if (currentItem.next == null)
                break;
            digit *= 10;
            currentItem = currentItem.next;
        }

        //Console.WriteLine($"{firstNumber} + {secondNumber} = {firstNumber + secondNumber}");

        string sum = (firstNumber + secondNumber).ToString();

        ListNode? nextNode = null;
        for(int i = 0; i < sum.Length; i++)
        {
            ListNode newNode = new ListNode(sum[i] - '0', nextNode);
            nextNode = newNode;
        }

        return nextNode;
    }

    private ListNode AddTwoNumbers(ListNode l1, ListNode l2)
    {
        ListNode startNode = new ListNode(0);
        ListNode nextNode = startNode;

        ListNode? leftNode = l1;
        ListNode? rightNode = l2;
        bool extra = false;
        while (true)
        {            
            int number1 = leftNode == null ? 0 : leftNode.val;
            int number2 = rightNode == null ? 0 : rightNode.val;

            nextNode.val = number1 + number2;
            
            if (extra)
            {
                extra = false;
                nextNode.val += 1;                
            }
            
            if (nextNode.val >= 10)
            {
                extra = true;
                nextNode.val = nextNode.val % 10;
            }
 
            leftNode = leftNode == null ? null : leftNode.next;
            rightNode = rightNode == null ? null : rightNode.next;

            if (leftNode == null && rightNode == null)
            {
                nextNode.next = extra ? new ListNode(1, null) : null;                
                break;
            }

            nextNode.next = new ListNode();
            nextNode = nextNode.next;

        }

        return startNode;
    }

    public void Test()
    {
        ListNode firstNumber1 = new ListNode(3);        
        ListNode firstNumber2 = new ListNode(4, firstNumber1);
        ListNode firstNumber3 = new ListNode(2, firstNumber2);

        ListNode secondNumber1 = new ListNode(4);
        ListNode secondNumber2 = new ListNode(6, secondNumber1);
        ListNode secondNumber3 = new ListNode(5, secondNumber2);
        ListNode result = AddTwoNumbers(firstNumber3, secondNumber3);

        ListNode nextVal = result;
        while(true)
        {
            Console.Write($"{nextVal.val}, ");
            if (nextVal.next == null)
                break;
            nextVal = nextVal.next;
        }
        Console.WriteLine("");

        firstNumber1 = new ListNode(9);
        firstNumber2 = new ListNode(9, firstNumber1);
        firstNumber3 = new ListNode(9, firstNumber2);
        ListNode firstNumber4 = new ListNode(9, firstNumber3);
        ListNode firstNumber5 = new ListNode(9, firstNumber4);
        ListNode firstNumber6 = new ListNode(9, firstNumber5);
        ListNode firstNumber7 = new ListNode(9, firstNumber6);

        secondNumber1 = new ListNode(9);
        secondNumber2 = new ListNode(9, secondNumber1);
        secondNumber3 = new ListNode(9, secondNumber2);
        ListNode secondNumber4 = new ListNode(9, secondNumber3);
        result = AddTwoNumbers(firstNumber7, secondNumber4);

        nextVal = result;
        while (true)
        {
            Console.Write($"{nextVal.val}, ");
            if (nextVal.next == null)
                break;
            nextVal = nextVal.next;
        }
        Console.WriteLine("");

        firstNumber1 = new ListNode(9);
       
        secondNumber1 = new ListNode(9);
        secondNumber2 = new ListNode(9, secondNumber1);
        secondNumber3 = new ListNode(9, secondNumber2);
        secondNumber4 = new ListNode(9, secondNumber3);
        ListNode secondNumber5 = new ListNode(9, secondNumber4);
        ListNode secondNumber6 = new ListNode(9, secondNumber5);
        ListNode secondNumber7 = new ListNode(9, secondNumber6);
        ListNode secondNumber8 = new ListNode(9, secondNumber7);
        ListNode secondNumber9 = new ListNode(9, secondNumber8);
        ListNode secondNumber10 = new ListNode(1, secondNumber9);
        result = AddTwoNumbers(firstNumber1, secondNumber10);

        nextVal = result;
        while (true)
        {
            Console.Write($"{nextVal.val}, ");
            if (nextVal.next == null)
                break;
            nextVal = nextVal.next;
        }
        Console.WriteLine("");

        //Console.WriteLine($"342 + 465 = {AddTwoNumbers(firstNumber3, secondNumber3)}");

    }
}

/*
 2. Add Two Numbers
Medium

You are given two non-empty linked lists representing two non-negative integers. The digits are stored in reverse order, and each of their nodes contains a single digit. Add the two numbers and return the sum as a linked list.

You may assume the two numbers do not contain any leading zero, except the number 0 itself.

 

Example 1:

Input: l1 = [2,4,3], l2 = [5,6,4]
Output: [7,0,8]
Explanation: 342 + 465 = 807.

Example 2:

Input: l1 = [0], l2 = [0]
Output: [0]

Example 3:

Input: l1 = [9,9,9,9,9,9,9], l2 = [9,9,9,9]
Output: [8,9,9,9,0,0,0,1]

 

Constraints:

    The number of nodes in each linked list is in the range [1, 100].
    0 <= Node.val <= 9
    It is guaranteed that the list represents a number that does not have leading zeros.


 */