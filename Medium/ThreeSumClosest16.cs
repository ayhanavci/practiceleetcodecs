﻿using System.Text;

namespace PracticeLeetCodeCS.Medium;

public class ThreeSumClosest16
{
    public int ThreeSumClosest(int[] nums, int target) 
    {
        int closestSum = int.MaxValue;        
        Array.Sort(nums);

        for (int i = 0; i < nums.Length - 2; ++i)
        {
            int left = i + 1;
            int right = nums.Length - 1;

            while (right > left)
            {
                int sum = nums[i] + nums[right] + nums[left];
                if (Math.Abs(target - sum) < Math.Abs(target - closestSum)) 
                    closestSum = sum;

                if(sum > target) right--;
                else left++;
            }
           
        }
        return closestSum;
    }
    public int ThreeSumClosest_SLOW(int[] nums, int target) 
    {
        int minDistance = int.MaxValue;
        int retVal = int.MaxValue;
        Array.Sort(nums);

        for (int i = 0; i < nums.Length - 2; ++i)
        {
            for (int j = i + 1; j < nums.Length - 1; ++j)
            {
                for (int k = j + 1; k < nums.Length; ++k)
                {
                    int sum = nums[i] + nums[j] + nums[k];
                    int distance = Math.Abs(sum - target);
                    if (distance < minDistance)
                    {
                        minDistance = distance;
                        retVal = sum;
                    }                
                }
            }
           
        }
        return retVal;
    }
    /*int left = i + 1;
            int right = nums.Length - 1;

            while (right > left)
            {
                int sum = nums[i] + nums[right] + nums[left];
                int distance = Math.Abs(sum - target);
                if (distance < minDistance)
                {
                    minDistance = distance;
                    retVal = sum;
                }                
            }*/
    public void Test()
    {
        int[] nums1 = {-1,2,1,-4};
        var result1 = ThreeSumClosest(nums1, 1);

        int[] nums2 = {0,0,0};
        var result2 = ThreeSumClosest(nums2, 1);

     
    }
}

/*
16. 3Sum Closest
Medium
10.1K
536
Companies

Given an integer array nums of length n and an integer target, find three integers in nums such that the sum is closest to target.

Return the sum of the three integers.

You may assume that each input would have exactly one solution.

 

Example 1:

Input: nums = [-1,2,1,-4], target = 1
Output: 2
Explanation: The sum that is closest to the target is 2. (-1 + 2 + 1 = 2).

Example 2:

Input: nums = [0,0,0], target = 1
Output: 0
Explanation: The sum that is closest to the target is 0. (0 + 0 + 0 = 0).

 

Constraints:

    3 <= nums.length <= 500
    -1000 <= nums[i] <= 1000
    -104 <= target <= 104


 */