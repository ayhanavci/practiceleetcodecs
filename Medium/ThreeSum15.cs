﻿using System.Text;

namespace PracticeLeetCodeCS.Medium;

public class ThreeSum15
{
    public IList<IList<int>> ThreeSum(int[] nums) 
    {
        IList<IList<int>> retVal = new List<IList<int>>();
        Array.Sort(nums);    

        for (int i = 0; i < nums.Length; ++i)
        {
            int j = i + 1;
            int k = nums.Length - 1;

            while (j < k)
            {
                int sum = nums[i] + nums[j] + nums[k];
                if (sum == 0)
                {
                    List<int> triplets = new List<int>()
                    {
                        nums[i],
                        nums[j++],
                        nums[k--]
                    };

                    bool add = true;
                    for (int n = 0; n < retVal.Count; ++n)
                    {
                        IList<int> check = retVal[n];
                        if (check[0] == triplets[0] && check[1] == triplets[1])
                        {
                            add = false;
                            break;
                        }
                    }
                    if (add)                    
                        retVal.Add(triplets);                                            
                }
                else if (sum < 0)                
                    j++;                
                else                
                    k--;
                
            }
        }
      
        
        return retVal;
    }
    public IList<IList<int>> ThreeSum_TIMEOUT(int[] nums) 
    {
        IList<IList<int>> retVal = new List<IList<int>>();
        Array.Sort(nums);

        for (int i = 0; i < nums.Length - 2; ++i)
        {
            if (nums[i] >= 0) break;
            for (int j = i + 1; j < nums.Length - 1; ++j) 
            {
                if (nums[i] + nums[j] >= 0) break;
                for (int k = j + 1; k < nums.Length; ++k)
                {
                    int sum = nums[i] + nums[j] + nums[k];
                    if (sum > 0) break;
                    if (sum == 0)
                    {
                        List<int> triplets = new List<int>
                        {
                            nums[i],
                            nums[j],
                            nums[k]
                        };
                        triplets.Sort();
                        
                        bool add = true;
                        for (int n = 0; n < retVal.Count; ++n)
                        {
                            IList<int> check = retVal[n];
                            if (check[0] == triplets[0] && check[1] == triplets[1])
                            {
                                add = false;
                                break;
                            }
                        }
                        if (add) 
                        {
                            retVal.Add(triplets);
                            break;
                        }
                            
                    }
                }
            }
        }
            
                
        
        return retVal;
    }
    public void Test()
    {
        int[] nums1 = {-1,0,1,2,-1,-4};
        var result1 = ThreeSum(nums1);

        int[] nums2 = {0,1,1};
        var result2 = ThreeSum(nums2);

        int[] nums3 = {0, 0, 0};
        var result3 = ThreeSum(nums3);
    }
}

/*
15. 3Sum
Medium
29.5K
2.7K
Companies

Given an integer array nums, return all the triplets [nums[i], nums[j], nums[k]] such that i != j, i != k, and j != k, and nums[i] + nums[j] + nums[k] == 0.

Notice that the solution set must not contain duplicate triplets.

 

Example 1:

Input: nums = [-1,0,1,2,-1,-4]
Output: [[-1,-1,2],[-1,0,1]]
Explanation: 
nums[0] + nums[1] + nums[2] = (-1) + 0 + 1 = 0.
nums[1] + nums[2] + nums[4] = 0 + 1 + (-1) = 0.
nums[0] + nums[3] + nums[4] = (-1) + 2 + (-1) = 0.
The distinct triplets are [-1,0,1] and [-1,-1,2].
Notice that the order of the output and the order of the triplets does not matter.

Example 2:

Input: nums = [0,1,1]
Output: []
Explanation: The only possible triplet does not sum up to 0.

Example 3:

Input: nums = [0,0,0]
Output: [[0,0,0]]
Explanation: The only possible triplet sums up to 0.

 

Constraints:

    3 <= nums.length <= 3000
    -105 <= nums[i] <= 105

 */