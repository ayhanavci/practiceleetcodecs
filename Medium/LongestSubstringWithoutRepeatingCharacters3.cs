using System.Text;

namespace PracticeLeetCodeCS.Medium;

public class LongestSubstringWithoutRepeatingCharacters3
{
    public int lengthOfLongestSubstring(string s) 
    {
        int longest = 0;

        StringBuilder items = new StringBuilder();

        for (int i = 0; i < s.Length; ++i)
        {                       
            int index = items.ToString().IndexOf(s[i]);
            if (index != -1)            
                items.Remove(0, index + 1);
            
            items.Append(s[i]);
            longest = Math.Max(items.Length, longest);            
        }

        return longest;
    }
    public void Test()
    {
        var result0 = lengthOfLongestSubstring(" ");
        var result1 = lengthOfLongestSubstring("abcabcbb");
        var result2 = lengthOfLongestSubstring("bbbbb");
        var result3 = lengthOfLongestSubstring("pwwkew");
        var result4 = lengthOfLongestSubstring("aab");
        var result5 = lengthOfLongestSubstring("dvdf");

    }
}

/*
3. Longest Substring Without Repeating Characters
Medium
38.3K
1.8K
Companies

Given a string s, find the length of the longest
substring
without repeating characters.

 

Example 1:

Input: s = "abcabcbb"
Output: 3
Explanation: The answer is "abc", with the length of 3.

Example 2:

Input: s = "bbbbb"
Output: 1
Explanation: The answer is "b", with the length of 1.

Example 3:

Input: s = "pwwkew"
Output: 3
Explanation: The answer is "wke", with the length of 3.
Notice that the answer must be a substring, "pwke" is a subsequence and not a substring.

 

Constraints:

    0 <= s.length <= 5 * 104
    s consists of English letters, digits, symbols and spaces.


*/