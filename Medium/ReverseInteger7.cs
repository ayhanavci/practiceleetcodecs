﻿using System.Text;

namespace PracticeLeetCodeCS.Medium;

public class ReverseInteger7
{
    public int Reverse(int x) 
    {
        Int32 reversed = 0;
        if (x == Int32.MinValue) return 0;
        Int32 number = Math.Abs(x);
        
        List<int> digits = new List<int>();
        while (number > 0)
        {
            int remainder = number % 10;
            digits.Add(remainder);
            number /= 10;
        }
        
        for (int i = 0; i < digits.Count; ++i)
        {            
            Int32 pow = 1;
            for (int j = 0; j < digits.Count - i - 1; ++j)
                pow *= 10;
            
            if (pow == 1000000000 && digits[0] > 2) return 0;

            Int32 limitRoom = Int32.MaxValue - reversed;
            Int32 addition = pow * digits[i];

            if (addition > limitRoom)  return 0;
            
            reversed += addition;            
            if (reversed < 0) return 0;
        }

        return reversed * (x < 0 ? -1 : 1);
        
    }
    public void Test()
    {       
        var result0 = Reverse(123);
        var result1 = Reverse(-123);
        var result2 = Reverse(120);
        var result3 = Reverse(1534236469);
        var result4 = Reverse(-2147483648);
        var result5 = Reverse(-2147483647);
    }
}

/*
7. Reverse Integer
Medium
12.3K
13.2K
Companies

Given a signed 32-bit integer x, return x with its digits reversed. If reversing x causes the value to go outside the signed 32-bit integer range [-231, 231 - 1], then return 0.

Assume the environment does not allow you to store 64-bit integers (signed or unsigned).

 

Example 1:

Input: x = 123
Output: 321

Example 2:

Input: x = -123
Output: -321

Example 3:

Input: x = 120
Output: 21

 

Constraints:

    -231 <= x <= 231 - 1
 */