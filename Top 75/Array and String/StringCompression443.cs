namespace Top75;

internal class StringCompression443
{

    public int Compress(char[] chars)
    {
        int position = 0;
        int count = 1;
        char lastChar = chars[0];
        for (int i = 1; i < chars.Length; ++i)
        {
            if (chars[i] == lastChar)
            {
                count++;                                
            }
            else if (count == 1)
            {
                chars[position++] = lastChar;
                lastChar = chars[i];
            }
            else
            {
                chars[position++] = lastChar;
                lastChar = chars[i];
                string countStr = count.ToString();            
                foreach (char ch in countStr)            
                    chars[position++] = ch;            
                count = 1;
            }
        }
        if (count == 1)
        {
            chars[position++] = lastChar;
        }
        else
        {
            chars[position++] = lastChar;
            string countStr = count.ToString();            
            foreach (char ch in countStr)            
                chars[position++] = ch;            
        }
        return position;
    }
    /*
     if (count >= 1000)
                {
                    chars[position++] = Convert.ToChar('0' + count / 1000);
                    count %= 1000;
                }
                if (count >= 100)
                {
                    chars[position++] = Convert.ToChar('0' + count / 100);
                    count %= 100;
                }
                if (count >= 10)
                {
                    chars[position++] = Convert.ToChar('0' + count / 10);
                    count %= 10;                    
                    chars[position++] = Convert.ToChar('0' + count);
                }
                else if (count > 1)
                {
                    chars[position++] = Convert.ToChar('0' + count);
                }
                else if (i == chars.Length - 1)
                {
                    position++;
                }
    */
    /*
      if (count >= 1000)
            {
                chars[position++] = Convert.ToChar('0' + count / 1000);
                count %= 1000;
            }
            if (count >= 100)
            {
                chars[position++] = Convert.ToChar('0' + count / 100);
                count %= 100;
            }
            if (count >= 10)
            {
                chars[position++] = Convert.ToChar('0' + count / 10);
                count %= 10;                    
                chars[position++] = Convert.ToChar('0' + count);
            }
            else if (count > 1)
            {
                chars[position++] = Convert.ToChar('0' + count);
            }            
    */
    public int Compress_(char[] chars)
    {
        int position = 1;
        char lastChar = chars[0];
        int count = 1;
        for (int i = 1; i < chars.Length; ++i)
        {
            bool takeRecord = false;
            char curChar = chars[i];
            if (curChar == lastChar)
                count++;
            else if (count == 1)
                position++;
            else
                takeRecord = true;
            if (position >= chars.Length)
                return position;
            if (i == chars.Length - 1)
            {
                takeRecord = true;
            }
            if (takeRecord)
            {
                if (count == 1)
                {
                    chars[position++] = curChar;
                }
                else
                {
                    int ones = count % 10;
                    count /= 10;
                    int tens = count % 10;
                    count /= 10;
                    int hundreds = count % 10;
                    count /= 10;
                    int thousands = count;

                    chars[position - 1] = lastChar;
                    if (thousands > 0)
                        chars[position++] = Convert.ToChar('0' + thousands);
                    if (hundreds > 0)
                        chars[position++] = Convert.ToChar('0' + hundreds);
                    if (tens > 0)
                        chars[position++] = Convert.ToChar('0' + tens);
                    if (ones > 1)
                        chars[position++] = Convert.ToChar('0' + ones);

                    /*if (i < chars.Length - 1)
                        position += 1;*/
                    count = 1;
                    if (i == chars.Length - 1)
                    {
                        chars[position++] = curChar;
                    }
                }

            }
            lastChar = curChar;
        }


        return position;
    }
    public void Test()
    {
        Test1();
        Test2();
        Test3();
        Test4();
        Test5();
        Test6();
        Test7();
        Test8();
        Test9();        
        Test10();
    }
    public void Test9()
    {
        char[] chars = new char[] { 'a','a','a','a','a','a','b','b','b','b','b','b','b','b','b','b','b','b','b','b','b','b','b','b','b','b','b','c','c','c','c','c','c','c','c','c','c','c','c','c','c' };
        var result = Compress(chars);
        //O:["a","6","b","2","c","1","4"]        
        //E:["a","6","b","2","1","c","1","4"]
    }
    public void Test10()
    {
        char[] chars = new char[] { 'a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a' };
        var result = Compress(chars);    
        //["a","1"]
        //["a","1","0","0"]    
    }
    public void Test1()
    {
        char[] chars = new char[] { 'a', 'a', 'b', 'b', 'c', 'c', 'c' };
        var result = Compress(chars);
    }
    public void Test5()
    {
        char[] chars = new char[] { 'a', 'a', 'a', 'a', 'a', 'b' };
        var result = Compress(chars);
    }
    public void Test3()
    {
        char[] chars = new char[] { 'a' };
        var result = Compress(chars);
    }
    public void Test6()
    {
        char[] chars = new char[] { 'a', 'a' };
        var result = Compress(chars);
    }
    public void Test7()
    {
        char[] chars = new char[] { 'a', 'b', 'c' };
        var result = Compress(chars);
    }
    public void Test8()
    {
        char[] chars = new char[] { 'a', 'a', 'a', 'a', 'b', 'a' };
        var result = Compress(chars);
    }

    public void Test4()
    {
        char[] chars = new char[] { 'a', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b' };
        var result = Compress(chars);
    }
    public void Test2()
    {
        char[] chars = new char[] { 'a', 'a', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'c', 'c', 'c' };
        var result = Compress(chars);
    }
}
/*
443. String Compression
Medium
Topics
Companies
Hint

Given an array of characters chars, compress it using the following algorithm:

Begin with an empty string s. For each group of consecutive repeating characters in chars:

    If the group's length is 1, append the character to s.
    Otherwise, append the character followed by the group's length.

The compressed string s should not be returned separately, but instead, be stored in the input character array chars. Note that group lengths that are 10 or longer will be split into multiple characters in chars.

After you are done modifying the input array, return the new length of the array.

You must write an algorithm that uses only constant extra space.

 

Example 1:

Input: chars = ["a","a","b","b","c","c","c"]
Output: Return 6, and the first 6 characters of the input array should be: ["a","2","b","2","c","3"]
Explanation: The groups are "aa", "bb", and "ccc". This compresses to "a2b2c3".

Example 2:

Input: chars = ["a"]
Output: Return 1, and the first character of the input array should be: ["a"]
Explanation: The only group is "a", which remains uncompressed since it's a single character.

Example 3:

Input: chars = ["a","b","b","b","b","b","b","b","b","b","b","b","b"]
Output: Return 4, and the first 4 characters of the input array should be: ["a","b","1","2"].
Explanation: The groups are "a" and "bbbbbbbbbbbb". This compresses to "ab12".

 

Constraints:

    1 <= chars.length <= 2000
    chars[i] is a lowercase English letter, uppercase English letter, digit, or symbol.


*/

/*
if (count >= 10)
        {
            int ones = count % 10;
            count /= 10;
            int tens = count % 10;
            count /= 10;
            int hundreds = count % 10;
            count /= 10;
            int thousands = count;

            chars[position - 1] = lastChar;
            if (thousands > 0)
                chars[position++] = Convert.ToChar('0' + thousands);
            if (hundreds > 0)
                chars[position++] = Convert.ToChar('0' + hundreds);
            chars[position++] = Convert.ToChar('0' + tens);
            chars[position++] = Convert.ToChar('0' + ones);
        }
        else if (count > 1)
        {
            chars[position - 1] = lastChar;
            chars[position] = Convert.ToChar('0' + count);
            position += 1;
        }
        else 
        {
            chars[position - 1] = lastChar;
            position += 1;
        }
*/