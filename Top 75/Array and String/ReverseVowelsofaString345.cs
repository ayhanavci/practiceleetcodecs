using System.Text;

namespace Top75;

internal class ReverseVowelsofaString345
{
    public string ReverseVowels2(string s) 
    {
        Stack<int> positions = new Stack<int>();
        Queue<char> vowels = new Queue<char>();
        StringBuilder str = new StringBuilder(s);

        for (int i = 0; i < str.Length; ++i)
        {
             if (str[i] == 'a' || str[i] == 'e' || str[i] == 'i' || str[i] == 'o' || str[i] == 'u' ||
                str[i] == 'A' || str[i] == 'E' || str[i] == 'I' || str[i] == 'O' || str[i] == 'U')
                {
                    positions.Push(i);
                    vowels.Enqueue(str[i]);
                }
        }

        while (vowels.Count > 0)        
            str[positions.Pop()] = vowels.Dequeue();
        
        return str.ToString();
    }
    public string ReverseVowels(string s) 
    {        
        int rightPos = s.Length - 1;
        StringBuilder str = new StringBuilder(s);

        for (int i = 0; i < s.Length; ++i)
        {
            if (str[i] == 'a' || str[i] == 'e' || str[i] == 'i' || str[i] == 'o' || str[i] == 'u' ||
                str[i] == 'A' || str[i] == 'E' || str[i] == 'I' || str[i] == 'O' || str[i] == 'U')
            {
                while (rightPos > i)
                {
                    if (str[rightPos] == 'a' || str[rightPos] == 'e' || str[rightPos] == 'i' || str[rightPos] == 'o' || str[rightPos] == 'u' ||
                        str[rightPos] == 'A' || str[rightPos] == 'E' || str[rightPos] == 'I' || str[rightPos] == 'O' || str[rightPos] == 'U')
                    {
                        char tmp = str[i];
                        str[i] = str[rightPos];
                        str[rightPos] = tmp;       
                        rightPos--;
                        break;               
                    }
                    rightPos--;
                }
                if (rightPos <= i)
                    return str.ToString();
            }
        }
        return str.ToString();
    }
    public void Test()
    {
        var result = ReverseVowels2("hello");
        var result1 = ReverseVowels2("leetcode");
        var result2 = ReverseVowels2("aA");
        var result3 = ReverseVowels2("race car");
    }
}

/*
345. Reverse Vowels of a String
Solved
Easy
Topics
Companies

Given a string s, reverse only all the vowels in the string and return it.

The vowels are 'a', 'e', 'i', 'o', and 'u', and they can appear in both lower and upper cases, more than once.

 

Example 1:

Input: s = "hello"
Output: "holle"

Example 2:

Input: s = "leetcode"
Output: "leotcede"

 

Constraints:

    1 <= s.length <= 3 * 105
    s consist of printable ASCII characters.


*/