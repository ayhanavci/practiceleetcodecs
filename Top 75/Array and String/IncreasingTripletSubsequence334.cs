namespace Top75;

internal class IncreasingTripletSubsequence334
{
    public bool IncreasingTriplet(int[] nums) 
    {
        int first = int.MaxValue;
        int second = int.MaxValue;        

        foreach (var num in nums)
        {
            if (num > second)
                return true;
            
            if (num <= first)
                first = num;
            else if (num <= second)
                second = num;                            
        }
        return false;
    }
    public bool IncreasingTriplet3(int[] nums) 
    {
        int n1 = int.MaxValue;
        int n2 = int.MaxValue;        

        //Greedy
        foreach (var num in nums)
        {
            if (num > n2)
                return true;
            
            if (num > n1)
                n2 = Math.Min(num, n2);
            
            n1 = Math.Min(n1, n1);
                
        }
        return false;
    }

    
    public bool IncreasingTriplet_2(int[] nums) 
    {
        int length = nums.Length;
        if (length < 3) return false;

        int[] leftMin = new int[length];
        int[] rightMax = new int[length];

        leftMin[0] = nums[0];
        for (int i = 1; i < length; ++i)        
            leftMin[i] = Math.Min(leftMin[i-1], nums[i]);

        rightMax[length - 1] = nums[length - 1];
        for (int i = length - 2; i >= 0; --i)
            rightMax[i] = Math.Max(rightMax[i+1], nums[i]);
        
        for (int i = 0; i < length; ++i)
            if (rightMax[i] > nums[i] && nums[i] > leftMin[i])
                return true;
        
        return false;
        
    }
    public bool IncreasingTriplet_BRUTE(int[] nums) 
    {
        Dictionary<int, int> indexValues = new Dictionary<int, int>();
        for (int i = 0; i < nums.Length - 2; ++i)
        {   
            for (int j = i + 1; j < nums.Length - 1; ++j)
            {
                for (int k = j + 1; k < nums.Length; ++k)
                {
                    if (nums[k] > nums[j] && nums[j] > nums[i])
                        return true;
                }
            }
            
        }
        return false;
    }
    public void Test()
    {
        var result1 = IncreasingTriplet(new int[] {1,2,3,4,5});//t
        var result2 = IncreasingTriplet(new int[] {5,4,3,2,1});//f
        var result3 = IncreasingTriplet(new int[] {2,1,5,0,4,6});//t
        var result4 = IncreasingTriplet(new int[] {20,100,10,12,5,13});
        //var result5 = IncreasingTriplet(new int[] {});
    }
}
/*
334. Increasing Triplet Subsequence
Medium
Topics
Companies

Given an integer array nums, return true if there exists a triple of indices (i, j, k) such that i < j < k and nums[i] < nums[j] < nums[k]. If no such indices exists, return false.

 

Example 1:

Input: nums = [1,2,3,4,5]
Output: true
Explanation: Any triplet where i < j < k is valid.

Example 2:

Input: nums = [5,4,3,2,1]
Output: false
Explanation: No triplet exists.

Example 3:

Input: nums = [2,1,5,0,4,6]
Output: true
Explanation: The triplet (3, 4, 5) is valid because nums[3] == 0 < nums[4] == 4 < nums[5] == 6.

 

Constraints:

    1 <= nums.length <= 5 * 105
    -231 <= nums[i] <= 231 - 1

 
Follow up: Could you implement a solution that runs in O(n) time complexity and O(1) space complexity?
*/