namespace Top75;

internal class DailyTemperatures739
{
    public int[] DailyTemperatures(int[] temperatures) 
    {
        Stack<Tuple<int, int>> stack = new Stack<Tuple<int, int>>();        
        stack.Push(new Tuple<int, int>(temperatures[0], 0));
        int [] ret = new int[temperatures.Length];
        
        for (int i = 1; i < temperatures.Length; ++i)
        {
            var top = stack.Peek();

            while (stack.Count > 0 && temperatures[i] > top.Item1)
            {
                top = stack.Pop();
                ret[top.Item2] = i - top.Item2;
                if (stack.Count > 0)
                    top = stack.Peek();
            }
            stack.Push(new Tuple<int, int>(temperatures[i], i));
        }

        return ret;
    }
    public void Test()
    {
        var result1 = DailyTemperatures(new int[] {73,74,75,71,69,72,76,73});//[1,1,4,2,1,1,0,0]
        var result2 = DailyTemperatures(new int[] {30,40,50,60});//[1,1,1,0]
        var result3 = DailyTemperatures(new int[] {30,60,90});//[1,1,0]
    }
}
/*
739. Daily Temperatures
Medium
Topics
Companies
Hint

Given an array of integers temperatures represents the daily temperatures, return an array answer such that answer[i] is the number of days you have to wait after the ith day to get a warmer temperature. If there is no future day for which this is possible, keep answer[i] == 0 instead.

 

Example 1:

Input: temperatures = [73,74,75,71,69,72,76,73]
Output: [1,1,4,2,1,1,0,0]

Example 2:

Input: temperatures = [30,40,50,60]
Output: [1,1,1,0]

Example 3:

Input: temperatures = [30,60,90]
Output: [1,1,0]

 

Constraints:

    1 <= temperatures.length <= 105
    30 <= temperatures[i] <= 100


*/