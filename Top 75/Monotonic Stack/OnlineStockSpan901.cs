namespace Top75;

internal class OnlineStockSpan901
{
    //fastish? 33%
    public class StockSpanner
    {
        Stack<Tuple<int, int>> stack;
        public StockSpanner()
        {
            stack = new Stack<Tuple<int, int>>();
        }   

        public int Next(int price)
        {
            int span = 1;

            while (stack.Count > 0 && stack.Peek().Item1 <= price)            
                span += stack.Pop().Item2;
            
            stack.Push(new Tuple<int, int>(price, span));
            return span;
        }
    }
    //Slow 7.5%
    public class StockSpanner2
    {
        Dictionary<int, int> history;
        public StockSpanner2()
        {
            history = new Dictionary<int, int>();
        }

        public int Next(int price)
        {
            int span = 1;
            for (int i = history.Count - 1; i >= 0; --i)
            {
                if (history[i] <= price)
                    span++;
                else                    
                    break;
            }
            history.Add(history.Count, price);
            return span;
        }
    }
    public void Test()
    {
        Test1();
        Test2();
    }
    public void Test1()
    {
        StockSpanner stockSpanner = new StockSpanner();
        int ret = stockSpanner.Next(100); // return 1
        ret = stockSpanner.Next(80);  // return 1
        ret = stockSpanner.Next(60);  // return 1
        ret = stockSpanner.Next(70);  // return 2
        ret = stockSpanner.Next(60);  // return 1
        ret = stockSpanner.Next(75);  // return 4, because the last 4 prices (including today's price of 75) were less than or equal to today's price.
        ret = stockSpanner.Next(85);  // return 6
    }
    public void Test2()
    {
        StockSpanner stockSpanner = new StockSpanner();
        int ret = stockSpanner.Next(31); // return 1
        ret = stockSpanner.Next(41);  // return 2
        ret = stockSpanner.Next(48);  // return 3
        ret = stockSpanner.Next(59);  // return 4
        ret = stockSpanner.Next(79);  // return 5        
    }
}

/*
901. Online Stock Span
Medium
Topics
Companies

Design an algorithm that collects daily price quotes for some stock and returns the span of that stock's price for the current day.

The span of the stock's price in one day is the maximum number of consecutive days (starting from that day and going backward) for which the stock price was less than or equal to the price of that day.

    For example, if the prices of the stock in the last four days is [7,2,1,2] and the price of the stock today is 2, then the span of today is 4 because starting from today, the price of the stock was less than or equal 2 for 4 consecutive days.
    Also, if the prices of the stock in the last four days is [7,34,1,2] and the price of the stock today is 8, then the span of today is 3 because starting from today, the price of the stock was less than or equal 8 for 3 consecutive days.

Implement the StockSpanner class:

    StockSpanner() Initializes the object of the class.
    int next(int price) Returns the span of the stock's price given that today's price is price.

 

Example 1:

Input
["StockSpanner", "next", "next", "next", "next", "next", "next", "next"]
[[], [100], [80], [60], [70], [60], [75], [85]]
Output
[null, 1, 1, 1, 2, 1, 4, 6]

Explanation
StockSpanner stockSpanner = new StockSpanner();
stockSpanner.next(100); // return 1
stockSpanner.next(80);  // return 1
stockSpanner.next(60);  // return 1
stockSpanner.next(70);  // return 2
stockSpanner.next(60);  // return 1
stockSpanner.next(75);  // return 4, because the last 4 prices (including today's price of 75) were less than or equal to today's price.
stockSpanner.next(85);  // return 6

 

Constraints:

    1 <= price <= 105
    At most 104 calls will be made to next.


*/