namespace Top75;

internal class KokoEatingBananas875
{
    public int MinEatingSpeed(int[] piles, int h) 
    {        
        Array.Sort(piles);
        
        int kMin = 1;
        int kMax = piles[piles.Length - 1];
        if (h == piles.Length) return kMax;

        //Binary search
        int elapsed;
        while (kMax > kMin + 1)
        {
            int mid = (kMin + kMax) / 2;
            elapsed = 0;
            for (int i = piles.Length - 1; i >= 0; --i)
            {
                elapsed += piles[i] / mid;
                elapsed += piles[i] % mid == 0 ? 0 : 1;

                if (elapsed > h) //This mid is too low. No need to continue.
                    break;
            }
            if (elapsed > h)
                kMin = mid + 1;
            else
                kMax = mid - 1;
            
        }
        elapsed = 0;
        
        for (int i = piles.Length - 1; i >= 0; --i)
        {
            elapsed += piles[i] / kMin;
            elapsed += piles[i] % kMin == 0 ? 0 : 1;            
        }
        if (elapsed <= h)            
            return kMin;
        return kMax;
    }
    public void Test()
    {
        Test4();
        Test1();
        Test2();
        Test3();
    }

    public void Test4()
    {
        int[] piles = {312884470};
        var result = MinEatingSpeed(piles, 968709470);//1
    }
    public void Test1()
    {
        int[] piles = {3,6,7,11};
        var result = MinEatingSpeed(piles, 8);//4
    }
    public void Test2()
    {
        int[] piles = {30,11,23,4,20};
        var result = MinEatingSpeed(piles, 5);//30
    }
    public void Test3()
    {
        int[] piles = {30,11,23,4,20};
        var result = MinEatingSpeed(piles, 6);//23
    }
}

/*
875. Koko Eating Bananas
Medium
Topics
Companies

Koko loves to eat bananas. There are n piles of bananas, the ith pile has piles[i] bananas. The guards have gone and will come back in h hours.

Koko can decide her bananas-per-hour eating speed of k. Each hour, she chooses some pile of bananas and eats k bananas from that pile. If the pile has less than k bananas, she eats all of them instead and will not eat any more bananas during this hour.

Koko likes to eat slowly but still wants to finish eating all the bananas before the guards return.

Return the minimum integer k such that she can eat all the bananas within h hours.

 

Example 1:

Input: piles = [3,6,7,11], h = 8
Output: 4

Example 2:

Input: piles = [30,11,23,4,20], h = 5
Output: 30

Example 3:

Input: piles = [30,11,23,4,20], h = 6
Output: 23

 

Constraints:

    1 <= piles.length <= 104
    piles.length <= h <= 109
    1 <= piles[i] <= 109


*/