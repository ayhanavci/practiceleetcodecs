namespace Top75;

internal class FindPeakElement162
{
    public int FindPeakElement(int[] nums)
    {
        int left = 0;
        int right = nums.Length - 1;
        int mid = 0;
        while (right >= left)
        {
            mid = left + ((right - left) / 2);

            if (mid > 0 && nums[mid] < nums[mid - 1])
                right = mid - 1;
            else if (mid < nums.Length - 1 && nums[mid] < nums[mid + 1])
                left = mid + 1;
            else
                break;
        }
        return mid;
    }
    public int FindPeakElement2(int[] nums)
    {        
        if (nums.Length == 1) return 0;
        if (nums[0] > nums[1]) return 0;
        if (nums[nums.Length - 1] > nums[nums.Length - 2]) return nums.Length - 1;
        int left = 0;
        int right = nums.Length - 1;

        while (right > left + 1)
        {
            int mid = (right + left) / 2;

            if (nums[mid] > nums[mid + 1])
            {
                if (nums[mid] > nums[mid - 1])
                    return mid;
                else
                    right = mid;
            }
            else
            {
                left = mid;
            }
        }
        return -1;
    }
    public void Test()
    {
        Test1();
        Test2();
        Test3();
        Test4();
        Test5();
    }
    public void Test1()
    {
        int[] nums = new int[] { 4, 5, 6, 7, 0, 1, 2 };
        var result = FindPeakElement(nums);//
    }
    public void Test2()
    {
        int[] nums = new int[] { 3, 4, 3, 2, 1 };
        var result = FindPeakElement(nums);//
    }
    public void Test3()
    {
        int[] nums = new int[] { 1 };
        var result = FindPeakElement(nums);//
    }
    public void Test4()
    {
        int[] nums = new int[] { 1, 2 };
        var result = FindPeakElement(nums);//
    }
    public void Test5()
    {
        int[] nums = new int[] { 1, 2, 1, 2, 1 };
        var result = FindPeakElement(nums);//
    }
}
/*
If nums[mid] < nums[mid+1], then there has to be a peak element in the right side of mid.
Either the numbers increase until we reach the end and then the last element will be a peak, or at some point a number gets smaller and right there we will have a peak.
Same for the left side of mid.???
[1,2,3,4,5,5,5]-> mid is 5, no peak at right side
*/
/*
162. Find Peak Element
Medium
Topics
Companies

A peak element is an element that is strictly greater than its neighbors.

Given a 0-indexed integer array nums, find a peak element, and return its index. If the array contains multiple peaks, return the index to any of the peaks.

You may imagine that nums[-1] = nums[n] = -∞. In other words, an element is always considered to be strictly greater than a neighbor that is outside the array.

You must write an algorithm that runs in O(log n) time.

 

Example 1:

Input: nums = [1,2,3,1]
Output: 2
Explanation: 3 is a peak element and your function should return the index number 2.

Example 2:

Input: nums = [1,2,1,3,5,6,4]
Output: 5
Explanation: Your function can return either index number 1 where the peak element is 2, or index number 5 where the peak element is 6.

 

Constraints:

    1 <= nums.length <= 1000
    -231 <= nums[i] <= 231 - 1
    nums[i] != nums[i + 1] for all valid i.


*/