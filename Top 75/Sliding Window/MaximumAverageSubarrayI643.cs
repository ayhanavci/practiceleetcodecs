namespace Top75;

internal class MaximumAverageSubarrayI643
{
    public double FindMaxAverage(int[] nums, int k) 
    {        
        double sum = 0;        
        
        for (int i = 0; i < k; ++i)        
            sum += nums[i];
        
        double maxAverage = sum / k;

        int left = 0;        
        int right = k;

        while (right < nums.Length)
        {
            sum -= nums[left++];
            sum += nums[right++];

            maxAverage = Math.Max(maxAverage, sum / k);
        }
        
        return maxAverage;
    }
    public double FindMaxAverage_OLD(int[] nums, int k) 
    {
        double maxAverage = -10001;
        
        for (int i = 0; i <= nums.Length - k; i++) 
        {          
            double sum = 0;
            for (int j = i; j < i + k; ++j)
                sum += nums[j];
            maxAverage = Math.Max(maxAverage, Math.Round(sum / k, 5));
        }

        return maxAverage;
    }
    public void Test()
    {
        var result3 = FindMaxAverage(new int[]{0,1,1,3,3}, 4);//e 2
        var result1 = FindMaxAverage(new int[]{1,12,-5,-6,50,3}, 4);//12.75
        var result2 = FindMaxAverage(new int[]{5}, 1);//5
    }
}
/*
643. Maximum Average Subarray I
Solved
Easy
Topics
Companies

You are given an integer array nums consisting of n elements, and an integer k.

Find a contiguous subarray whose length is equal to k that has the maximum average value and return this value. Any answer with a calculation error less than 10-5 will be accepted.

 

Example 1:

Input: nums = [1,12,-5,-6,50,3], k = 4
Output: 12.75000
Explanation: Maximum average is (12 - 5 - 6 + 50) / 4 = 51 / 4 = 12.75

Example 2:

Input: nums = [5], k = 1
Output: 5.00000

 

Constraints:

    n == nums.length
    1 <= k <= n <= 105
    -104 <= nums[i] <= 104


*/