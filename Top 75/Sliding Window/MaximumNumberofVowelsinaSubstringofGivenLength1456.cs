namespace Top75;

internal class MaximumNumberofVowelsinaSubstringofGivenLength1456
{
    public int MaxVowels(string s, int k) 
    {
        int ret = 0;
        int count = 0;        
        int end = k - 1;
        
        for (int i = 0; i < k; ++i)
        {
            if (s[i] == 'a' || s[i] == 'e' || s[i] == 'i' || s[i] == 'o' || s[i] == 'u')
                count++;
        }

        if (k == s.Length)
            return count;
        
        ret = count;
        for (int start = 1; start <= s.Length - k; ++start)
        {
            end += 1;
            if (s[start-1] == 'a' || s[start-1] == 'e' || s[start-1] == 'i' || s[start-1] == 'o' || s[start-1] == 'u')
                if (count > 0) count -= 1;
                                                                             
            if (s[end] == 'a' || s[end] == 'e' || s[end] == 'i' || s[end] == 'o' || s[end] == 'u')
                count += 1;
            
            ret = Math.Max(ret, count);
        }

        return ret;
    }
    public bool IsVowel(char ch)
    {
        return ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u';
    }
    //return s[i] == 'a' || s[i] == 'e' || s[i] == 'i' || s[i] == 'o' || s[i] == 'u'
    public void Test()
    {
        var result5 = MaxVowels("ibpbhixfiouhdljnjfflpapptrxgcomvnb", 33);//o6 e7
        var result4 = MaxVowels("weallloveyou", 7);//o3 e4
        var result1 = MaxVowels("abciiidef", 3);
        var result2 = MaxVowels("aeiou", 2);
        var result3 = MaxVowels("leetcode", 3);
    }
}

/*
1456. Maximum Number of Vowels in a Substring of Given Length
Medium
Topics
Companies
Hint

Given a string s and an integer k, return the maximum number of vowel letters in any substring of s with length k.

Vowel letters in English are 'a', 'e', 'i', 'o', and 'u'.

 

Example 1:

Input: s = "abciiidef", k = 3
Output: 3
Explanation: The substring "iii" contains 3 vowel letters.

Example 2:

Input: s = "aeiou", k = 2
Output: 2
Explanation: Any substring of length 2 contains 2 vowels.

Example 3:

Input: s = "leetcode", k = 3
Output: 2
Explanation: "lee", "eet" and "ode" contain 2 vowels.

 

Constraints:

    1 <= s.length <= 105
    s consists of lowercase English letters.
    1 <= k <= s.length


*/