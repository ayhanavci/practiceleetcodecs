namespace Top75;

internal class LongestSubarrayof1sAfterDeletingOneElement1493
{
    public int LongestSubarray(int[] nums) 
    {
        if (nums.Length == 1)        
            return 0;

        int longest = 0;
        int left = 0;
        int right = 1;
        int zeros = 0;

        if (nums[left] == 0)
            zeros = 1;
        while (right < nums.Length)
        {
            if (nums[right] == 0)
                zeros++;
            
            if (zeros > 1)
            {
                while (right >= left)
                {
                    if (nums[left++] == 0) 
                    {
                        zeros--;
                        break;
                    }                        
                }
            }

            longest = Math.Max(longest, right - left);
            right++;
        }
        
        return longest;
    }
    public void Test()
    {
        var result1 = LongestSubarray(new int[] {1,1,0,1});//3
        var result2 = LongestSubarray(new int[] {0,1,1,1,0,1,1,0,1});//5
        var result3 = LongestSubarray(new int[] {1,1,1});//2 must delete one element
    }
}
/*
1493. Longest Subarray of 1's After Deleting One Element
Medium
Topics
Companies
Hint

Given a binary array nums, you should delete one element from it.

Return the size of the longest non-empty subarray containing only 1's in the resulting array. Return 0 if there is no such subarray.

 

Example 1:

Input: nums = [1,1,0,1]
Output: 3
Explanation: After deleting the number in position 2, [1,1,1] contains 3 numbers with value of 1's.

Example 2:

Input: nums = [0,1,1,1,0,1,1,0,1]
Output: 5
Explanation: After deleting the number in position 4, [0,1,1,1,1,1,0,1] longest subarray with value of 1's is [1,1,1,1,1].

Example 3:

Input: nums = [1,1,1]
Output: 2
Explanation: You must delete one element.

 

Constraints:

    1 <= nums.length <= 105
    nums[i] is either 0 or 1.


*/