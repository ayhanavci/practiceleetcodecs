namespace Top75;

internal class MaxConsecutiveOnesIII1004
{
    public int LongestOnes(int[] nums, int k) 
    {
        if (nums.Length == 1)
        {
            if (k == 1) return 1;
            if (k == 0) return nums[0];
        }

        int ret = 0;
        int left = 0;
        int right = 1;
        int zeros = nums[left] == 0 ? 1 : 0;        
           
        while (right < nums.Length)
        {
            if (nums[right] == 0)
                zeros++;

            while (left <= right && zeros > k)
            {
                if (nums[left++] == 0)
                    zeros--;                           
            }
            ret = Math.Max(ret, right - left + 1);
            right++;
        }
        
        return ret;
    }
    public void Test()
    {
        //var result1 = LongestOnes(new int[] {1,1,1,0,0,0,1,1,1,1,0}, 2);//6
        var result2 = LongestOnes(new int[] {0,0,1,1,0,0,1,1,1,0,1,1,0,0,0,1,1,1,1}, 3);//10
        //var result3 = LongestOnes(new int[] {}, 2);
    }
}
/*
1004. Max Consecutive Ones III
Medium
Topics
Companies
Hint

Given a binary array nums and an integer k, return the maximum number of consecutive 1's in the array if you can flip at most k 0's.

 

Example 1:

Input: nums = [1,1,1,0,0,0,1,1,1,1,0], k = 2
Output: 6
Explanation: [1,1,1,0,0,1,1,1,1,1,1]
Bolded numbers were flipped from 0 to 1. The longest subarray is underlined.

Example 2:

Input: nums = [0,0,1,1,0,0,1,1,1,0,1,1,0,0,0,1,1,1,1], k = 3
Output: 10
Explanation: [0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,1,1,1,1]
Bolded numbers were flipped from 0 to 1. The longest subarray is underlined.

 

Constraints:

    1 <= nums.length <= 105
    nums[i] is either 0 or 1.
    0 <= k <= nums.length


*/