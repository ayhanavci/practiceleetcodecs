namespace Top75;

internal class MinimumNumberofArrowstoBurstBalloons452
{
    public int FindMinArrowShots(int[][] points) 
    {
        List<int[]> pointsList = new List<int[]>(points);

        var sorted = pointsList.OrderBy(x => x[0]).ToArray();

        // if there is at least one balloon 
        // then a minimum of one dart is required

        int darts = 1;
        int currentOverlapR = sorted[0][1]; // right edge of previous overlap
        
        for (int i = 1; i < sorted.Length; ++i)
        {
            // if the current balloon's left edge is greater than the right
            // edge of our current overlap, so we need another dart
            if (sorted[i][0] > currentOverlapR) {
                darts++;
                currentOverlapR = sorted[i][1]; // reset the overlapping edge
                continue;
            }
            if (sorted[i][1] <= currentOverlapR) {
                // don't need to increment darts here due to overlap
                // but we do need to adjust where the right edge ends
                currentOverlapR = Math.Min(currentOverlapR, sorted[i][1]);
            }
        }

        return darts;
    }

    public int FindMinArrowShots_BUG(int[][] points) 
    {
        List<int[]> pointsList = new List<int[]>(points);

        var sortedPoints = pointsList.OrderBy(x => x[1]).ToArray();

        int count = 1;
        
        int end = sortedPoints[0][1];
        
        for (int i = 1; i < sortedPoints.Length; ++i)
        {
            if (sortedPoints[i][0] > end)
            {
                count++;
                end = points[i][1];
            }
        }

        return count;
    }
    public void Test()
    {
        int[][] points1 = new int[4][];
        points1[0] = new int[]{10,16};
        points1[1] = new int[]{2,8};
        points1[2] = new int[]{1,6};
        points1[3] = new int[]{7,12};

        //var result1 = FindMinArrowShots(points1);//2


        int[][] points2 = new int[7][];
        points2[0] = new int[]{9,12};
        points2[1] = new int[]{1,10};
        points2[2] = new int[]{4,11};
        points2[3] = new int[]{8,12};
        points2[4] = new int[]{3,9};
        points2[5] = new int[]{6,9};
        points2[6] = new int[]{6,7};

        //1,10 - 3,9 - 4,11 - 6,7 - 6,9 - 8,12 - 9,12


        var result2 = FindMinArrowShots(points2);//2
    }
}
/*
452. Minimum Number of Arrows to Burst Balloons
Medium
6.6K
193
Companies

There are some spherical balloons taped onto a flat wall that represents the XY-plane. The balloons are represented as a 2D integer array points where points[i] = [xstart, xend] denotes a balloon whose horizontal diameter stretches between xstart and xend. You do not know the exact y-coordinates of the balloons.

Arrows can be shot up directly vertically (in the positive y-direction) from different points along the x-axis. A balloon with xstart and xend is burst by an arrow shot at x if xstart <= x <= xend. There is no limit to the number of arrows that can be shot. A shot arrow keeps traveling up infinitely, bursting any balloons in its path.

Given the array points, return the minimum number of arrows that must be shot to burst all balloons.

 

Example 1:

Input: points = [[10,16],[2,8],[1,6],[7,12]]
Output: 2
Explanation: The balloons can be burst by 2 arrows:
- Shoot an arrow at x = 6, bursting the balloons [2,8] and [1,6].
- Shoot an arrow at x = 11, bursting the balloons [10,16] and [7,12].

Example 2:

Input: points = [[1,2],[3,4],[5,6],[7,8]]
Output: 4
Explanation: One arrow needs to be shot for each balloon for a total of 4 arrows.

Example 3:

Input: points = [[1,2],[2,3],[3,4],[4,5]]
Output: 2
Explanation: The balloons can be burst by 2 arrows:
- Shoot an arrow at x = 2, bursting the balloons [1,2] and [2,3].
- Shoot an arrow at x = 4, bursting the balloons [3,4] and [4,5].

 

Constraints:

    1 <= points.length <= 105
    points[i].length == 2
    -231 <= xstart < xend <= 231 - 1


*/