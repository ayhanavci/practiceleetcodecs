namespace Top75;

internal class NonoverlappingIntervals435
{
    //Fast 78%
    public int EraseOverlapIntervals(int[][] intervals)
    {
        List<(int, int)> sortedIntervals = new();
        foreach (var interval in intervals)
            sortedIntervals.Add((interval[0], interval[1]));
        
        sortedIntervals.Sort((x, y) => 
            {
                if (x.Item1 == y.Item1)
                    return x.Item2.CompareTo(y.Item2);
                return x.Item1.CompareTo(y.Item1);
            }
        );
        int res = 0;
        int prevEnd = sortedIntervals[0].Item2;
        for (int i = 1; i < sortedIntervals.Count; ++i)
        {
            if (sortedIntervals[i].Item1 >= prevEnd)
                prevEnd = sortedIntervals[i].Item2;
            else
            {
                res++;
                prevEnd = Math.Min(sortedIntervals[i].Item2, prevEnd);
            }
        }

        return res;
    }
    //Slow 5%
    public int EraseOverlapIntervals_SLOW(int[][] intervals)
    {
        List<(int, int)> sortedIntervals = new();
        foreach (var interval in intervals)
            sortedIntervals.Add((interval[0], interval[1]));
        
        sortedIntervals.Sort((x, y) => 
            {
                if (x.Item1 == y.Item1)
                    return x.Item2.CompareTo(y.Item2);
                return x.Item1.CompareTo(y.Item1);
            }
        );
        int res = 0;
        //[[1,11],[1,100],[2,12],[11,22]]
        for (int i = 0; i < sortedIntervals.Count - 1; ++i)
        {
            if (sortedIntervals[i].Item1 == sortedIntervals[i+1].Item1)
            {
                sortedIntervals.RemoveAt(i+1);
                i--;
                res++;
            }
            else if (sortedIntervals[i].Item2 > sortedIntervals[i+1].Item2)
            {
                sortedIntervals.RemoveAt(i);
                i--;
                res++;
            }
            else if (sortedIntervals[i].Item2 > sortedIntervals[i+1].Item1)
            {
                sortedIntervals.RemoveAt(i+1);
                i--;
                res++;
            }
        }

        return res;
    }//1,10, 2,8
    public void Test()
    {
        //Test4();
        //Test1();
        Test2();
        //Test3();
    }
    public void Test1()
    {
        int[][] intervals = new int[4][];
        intervals[0] = new int[]{1,2};
        intervals[1] = new int[]{2,3};
        intervals[2] = new int[]{3,4};
        intervals[3] = new int[]{1,3};

        var result = EraseOverlapIntervals(intervals);
    }
    public void Test2()
    {
        int[][] intervals = new int[3][];
        intervals[0] = new int[]{1,2};
        intervals[1] = new int[]{1,2};
        intervals[2] = new int[]{1,2};

        var result = EraseOverlapIntervals(intervals);
    }
    public void Test3()
    {
        int[][] intervals = new int[2][];
        intervals[0] = new int[]{1,2};
        intervals[1] = new int[]{2,3};

        var result = EraseOverlapIntervals(intervals);
    }
    public void Test4()
    {
        //[[1,11],[1,100],[2,12],[11,22]]
        int[][] intervals = new int[4][];
        intervals[0] = new int[]{1,100};
        intervals[1] = new int[]{11,22};
        intervals[2] = new int[]{1,11};
        intervals[3] = new int[]{2,12};

        var result = EraseOverlapIntervals(intervals);//e 2
    }
}
/*
435. Non-overlapping Intervals
Medium
Topics
Companies

Given an array of intervals intervals where intervals[i] = [starti, endi], return the minimum number of intervals you need to remove to make the rest of the intervals non-overlapping.

 

Example 1:

Input: intervals = [[1,2],[2,3],[3,4],[1,3]]
Output: 1
Explanation: [1,3] can be removed and the rest of the intervals are non-overlapping.

Example 2:

Input: intervals = [[1,2],[1,2],[1,2]]
Output: 2
Explanation: You need to remove two [1,2] to make the rest of the intervals non-overlapping.

Example 3:

Input: intervals = [[1,2],[2,3]]
Output: 0
Explanation: You don't need to remove any of the intervals since they're already non-overlapping.

 

Constraints:

    1 <= intervals.length <= 105
    intervals[i].length == 2
    -5 * 104 <= starti < endi <= 5 * 104


*/