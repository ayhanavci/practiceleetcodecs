namespace Top75;

internal class MaximumTwinSumofaLinkedList2130
{
    //Definition for singly-linked list.
    public class ListNode
    {
        public int val;
        public ListNode next;
        public ListNode(int val = 0, ListNode next = null)
        {
            this.val = val;
            this.next = next;
        }
    }
    public int PairSum(ListNode head) 
    {
        ListNode slow = head;
        ListNode fast = head;

        Stack<int> firstHalf = new Stack<int>();
        while (fast != null)
        {
            firstHalf.Push(slow.val);
            slow = slow.next;
            fast = fast.next;
            if (fast != null)
                fast = fast.next;
        }

        Queue<int> secondHalf = new Queue<int>();
        while (slow != null)
        {
            secondHalf.Enqueue(slow.val);
            slow = slow.next;
        }
        int ret = 0;
        while (firstHalf.Count > 0)
        {
            int first = firstHalf.Pop();
            int second = secondHalf.Dequeue();

            ret = Math.Max(first + second, ret);
        }
        return ret;
    }
    public void Test()
    {
        Test1();
        Test2();
        Test3();
    }
    public void Test5()
    {
        ListNode head = new ListNode(1);
        ListNode node1 = new ListNode(2);
        ListNode node2 = new ListNode(3);
        ListNode node3 = new ListNode(4);
        ListNode node4 = new ListNode(5);
        ListNode node5 = new ListNode(6);
        ListNode node6 = new ListNode(7);

        head.next = node1;
        node1.next = node2;
        node2.next = node3;
        node3.next = node4;
        node4.next = node5;
        node5.next = node6;

        var result = PairSum(head);

    }
    public void Test1()
    {
        ListNode head = new ListNode(5);
        ListNode node1 = new ListNode(4);
        ListNode node2 = new ListNode(2);
        ListNode node3 = new ListNode(1);
        
        head.next = node1;
        node1.next = node2;
        node2.next = node3;
        
        var result = PairSum(head);

    }
    public void Test2()
    {
        ListNode head = new ListNode(4);
        ListNode node1 = new ListNode(2);
        ListNode node2 = new ListNode(2);
        ListNode node3 = new ListNode(3);
        
        head.next = node1;
        node1.next = node2;
        node2.next = node3;
        
        var result = PairSum(head);

    }
    public void Test3()
    {
        ListNode head = new ListNode(1);
        ListNode node1 = new ListNode(100000);
      
        
        head.next = node1;
      
        
        var result = PairSum(head);

    }
}

/*
2130. Maximum Twin Sum of a Linked List
Medium
Topics
Companies
Hint

In a linked list of size n, where n is even, the ith node (0-indexed) of the linked list is known as the twin of the (n-1-i)th node, if 0 <= i <= (n / 2) - 1.

    For example, if n = 4, then node 0 is the twin of node 3, and node 1 is the twin of node 2. These are the only nodes with twins for n = 4.

The twin sum is defined as the sum of a node and its twin.

Given the head of a linked list with even length, return the maximum twin sum of the linked list.

 

Example 1:

Input: head = [5,4,2,1]
Output: 6
Explanation:
Nodes 0 and 1 are the twins of nodes 3 and 2, respectively. All have twin sum = 6.
There are no other nodes with twins in the linked list.
Thus, the maximum twin sum of the linked list is 6. 

Example 2:

Input: head = [4,2,2,3]
Output: 7
Explanation:
The nodes with twins present in this linked list are:
- Node 0 is the twin of node 3 having a twin sum of 4 + 3 = 7.
- Node 1 is the twin of node 2 having a twin sum of 2 + 2 = 4.
Thus, the maximum twin sum of the linked list is max(7, 4) = 7. 

Example 3:

Input: head = [1,100000]
Output: 100001
Explanation:
There is only one node with a twin in the linked list having twin sum of 1 + 100000 = 100001.

 

Constraints:

    The number of nodes in the list is an even integer in the range [2, 105].
    1 <= Node.val <= 105


*/