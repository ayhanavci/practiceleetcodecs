namespace Top75;

internal class OddEvenLinkedList328
{
    //Definition for singly-linked list.
    public class ListNode
    {
        public int val;
        public ListNode next;
        public ListNode(int val = 0, ListNode next = null)
        {
            this.val = val;
            this.next = next;
        }
    }
    public ListNode OddEvenList(ListNode head)
    {
        if (head == null) return null;

        ListNode current = head;

        ListNode evenHead = new ListNode();
        ListNode oddsHead = new ListNode();

        ListNode odds = oddsHead;
        ListNode evens = evenHead;

        int count = 0;
        while (current != null)
        {
            if (count % 2 == 0)
            {
                odds.next = current;
                odds = current;                
            }
            else
            {
                evens.next = current;
                evens = current;
            }
            current = current.next;
            count++;
        }
        if (count % 2 == 1)        
            evens.next = null;
        
        odds.next = evenHead.next;
        return oddsHead.next;
    }
    public void Test()
    {
        Test1();
    }
    public void Test1()
    {
        ListNode head = new ListNode(0);
        ListNode node1 = new ListNode(1);
        ListNode node2 = new ListNode(2);
        ListNode node3 = new ListNode(3);
        ListNode node4 = new ListNode(4);
        ListNode node5 = new ListNode(5);
        ListNode node6 = new ListNode(6);

        head.next = node1;
        node1.next = node2;
        node2.next = node3;
        node3.next = node4;
        node4.next = node5;
        node5.next = node6;

        var result = OddEvenList(head);
    }
}
/*
328. Odd Even Linked List
Medium
Topics
Companies

Given the head of a singly linked list, group all the nodes with odd indices together followed by the nodes with even indices, and return the reordered list.

The first node is considered odd, and the second node is even, and so on.

Note that the relative order inside both the even and odd groups should remain as it was in the input.

You must solve the problem in O(1) extra space complexity and O(n) time complexity.

 

Example 1:

Input: head = [1,2,3,4,5]
Output: [1,3,5,2,4]

Example 2:

Input: head = [2,1,3,5,6,4,7]
Output: [2,3,6,7,1,5,4]

 

Constraints:

    The number of nodes in the linked list is in the range [0, 104].
    -106 <= Node.val <= 106


*/