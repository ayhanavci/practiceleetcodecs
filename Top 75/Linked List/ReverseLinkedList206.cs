namespace Top75;

internal class ReverseLinkedList206
{
    //Definition for singly-linked list.
    public class ListNode
    {
        public int val;
        public ListNode next;
        public ListNode(int val = 0, ListNode next = null)
        {
            this.val = val;
            this.next = next;
        }
    }
    public ListNode ReverseList(ListNode head) 
    {
        if (head == null) return null;
        ListNode prev = head;
        ListNode current = prev.next;        
        ListNode next = null;

        prev.next = null;
        while (current != null)
        {
            next = current.next;
            current.next = prev;
            prev = current;
            current = next;
        }

        return prev;
    }
    public void Test()
    {
        Test1();
        Test2();
    }
    public void Test1()
    {
        ListNode head = new ListNode(1);
        ListNode node1 = new ListNode(2);
        ListNode node2 = new ListNode(3);
        ListNode node3 = new ListNode(4);
        ListNode node4 = new ListNode(5);

        head.next = node1;
        node1.next = node2;
        node2.next = node3;
        node3.next = node4;

        var result = ReverseList(head);
    }
     public void Test2()
    {
        ListNode head = new ListNode(1);
        ListNode node1 = new ListNode(2);
      

        head.next = node1;
       

        var result = ReverseList(head);
    }
}
/*
206. Reverse Linked List
Solved
Easy
Topics
Companies

Given the head of a singly linked list, reverse the list, and return the reversed list.

 

Example 1:

Input: head = [1,2,3,4,5]
Output: [5,4,3,2,1]

Example 2:

Input: head = [1,2]
Output: [2,1]

Example 3:

Input: head = []
Output: []

 

Constraints:

    The number of nodes in the list is the range [0, 5000].
    -5000 <= Node.val <= 5000

 

Follow up: A linked list can be reversed either iteratively or recursively. Could you implement both?

*/