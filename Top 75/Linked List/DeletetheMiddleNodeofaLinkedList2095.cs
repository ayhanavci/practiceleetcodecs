namespace Top75;

internal class DeletetheMiddleNodeofaLinkedList2095
{
    //Definition for singly-linked list.
    public class ListNode
    {
        public int val;
        public ListNode next;
        public ListNode(int val = 0, ListNode next = null)
        {
            this.val = val;
            this.next = next;
        }
    }
    public ListNode DeleteMiddle2(ListNode head)
    {
        ListNode slow = head;
        ListNode fast = head;
        ListNode slowPrev = null;

        int count = 0;
        while (fast != null)
        {                   
            fast = fast.next;
            count++;
            if (fast != null)
            {       
                count++;         
                fast = fast.next;
                slowPrev = slow;
                slow = slow.next;     
            }            
        }

        if (slowPrev == null)
            return null;

        slowPrev.next = slow.next;

        return head;
    }
    public ListNode DeleteMiddle(ListNode head)
    {
        Dictionary<int, ListNode> map = new Dictionary<int, ListNode>();

        ListNode current = head;
        int count = 0;
        while (current != null)
        {
            map.Add(count++, current);    
            current = current.next;
        }         
        
        count /= 2;
        ListNode next;
        map.TryGetValue(count + 1, out next);

        ListNode prev;
        if (map.TryGetValue(count - 1, out prev))
            prev.next = next;

        return head;
    }
    public void Test()
    {
        Test6();
        Test5();
        Test4();
        Test1();
        Test2();
        Test3();
    }
    public void Test1()
    {
        ListNode head = new ListNode(1);
        ListNode node1 = new ListNode(2);
        ListNode node2 = new ListNode(3);
        ListNode node3 = new ListNode(4);
        ListNode node4 = new ListNode(5);
        ListNode node5 = new ListNode(6);
        ListNode node6 = new ListNode(7);

        head.next = node1;
        node1.next = node2;
        node2.next = node3;
        node3.next = node4;
        node4.next = node5;
        node5.next = node6;

        var result = DeleteMiddle2(head);

    }
    public void Test2()
    {
        ListNode head = new ListNode(1);
        ListNode node1 = new ListNode(2);
        ListNode node2 = new ListNode(3);
        ListNode node3 = new ListNode(4);
        
        head.next = node1;
        node1.next = node2;
        node2.next = node3;
        
        var result = DeleteMiddle2(head);

    }
    public void Test3()
    {
        ListNode head = new ListNode(1);
        ListNode node1 = new ListNode(2);
       
        head.next = node1;
      

        var result = DeleteMiddle2(head);

    }

    public void Test4()
    {
        ListNode head = new ListNode(1);
        ListNode node1 = new ListNode(2);
        ListNode node2 = new ListNode(3);        
        
        head.next = node1;
        node1.next = node2;        
        
        var result = DeleteMiddle2(head);

    }
    public void Test5()
    {
        ListNode head = new ListNode(1);
        ListNode node1 = new ListNode(2);
        ListNode node2 = new ListNode(3);
        ListNode node3 = new ListNode(4);
        ListNode node4 = new ListNode(5);
        
        head.next = node1;
        node1.next = node2;
        node2.next = node3;
        node3.next = node4;
        
        var result = DeleteMiddle2(head);

    }
    public void Test6()
    {
        ListNode head = new ListNode(1);
        
        var result = DeleteMiddle2(head);

    }
}
/*
Description
Editorial
Editorial
Solutions
Solutions
Submissions
Submissions
Code
Testcase
Test Result
Test Result
2095. Delete the Middle Node of a Linked List
Medium
Topics
Companies
Hint

You are given the head of a linked list. Delete the middle node, and return the head of the modified linked list.

The middle node of a linked list of size n is the ⌊n / 2⌋th node from the start using 0-based indexing, where ⌊x⌋ denotes the largest integer less than or equal to x.

    For n = 1, 2, 3, 4, and 5, the middle nodes are 0, 1, 1, 2, and 2, respectively.

 

Example 1:

Input: head = [1,3,4,7,1,2,6]
Output: [1,3,4,1,2,6]
Explanation:
The above figure represents the given linked list. The indices of the nodes are written below.
Since n = 7, node 3 with value 7 is the middle node, which is marked in red.
We return the new list after removing this node. 

Example 2:

Input: head = [1,2,3,4]
Output: [1,2,4]
Explanation:
The above figure represents the given linked list.
For n = 4, node 2 with value 3 is the middle node, which is marked in red.

Example 3:

Input: head = [2,1]
Output: [2]
Explanation:
The above figure represents the given linked list.
For n = 2, node 1 with value 1 is the middle node, which is marked in red.
Node 0 with value 2 is the only node remaining after removing node 1.

 

Constraints:

    The number of nodes in the list is in the range [1, 105].
    1 <= Node.val <= 105


*/