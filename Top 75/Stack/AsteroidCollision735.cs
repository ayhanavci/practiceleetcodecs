namespace Top75;

internal class AsteroidCollision735
{
    public int[] AsteroidCollision(int[] asteroids) 
    {
        Stack<int> stack = new Stack<int>();
        
        for (int i = 0; i < asteroids.Length; ++i)
        {
            if (stack.Count == 0)
            {
                stack.Push(asteroids[i]);
            }
            else
            {
                int current = asteroids[i];
                
                while (stack.Count > 0)
                {
                    int last = stack.Peek();                                                            
                                        
                    //Clash with the previous
                    if (current < 0 && last > 0)
                    {
                        //They destroy each other
                        if (current + last == 0)
                        {
                            //Don't add current, remove top
                            stack.Pop();
                            break;
                        }
                        else if (Math.Abs(current) > Math.Abs(last)) 
                        {
                            //Top lost. Test with next top.
                            stack.Pop();

                            if (stack.Count == 0) 
                            {
                                stack.Push(current);
                                break;
                            }                                                            
                        }
                        else
                        {
                            //Top won. Don't add new one.
                            break;
                        }
                    }
                    else //No clash
                    {
                        stack.Push(current);
                        break;
                    }                                        
                }                                
            }
            
        }
        List<int> ret = new List<int>();
        while (stack.Count > 0)
        {
            if (ret.Count == 0)
                ret.Add(stack.Pop());
            else
                ret.Insert(0, stack.Pop());
        }
        return ret.ToArray();

    }
    
    public void Test()
    {
        var result6 = AsteroidCollision(new int[]{1,-1,-2,-2});//-2,-2
        var result5 = AsteroidCollision(new int[]{1,-2,-2,-2});//-2,-2,-2
        var result4 = AsteroidCollision(new int[]{-2,-1,1,2});//-2,-1,1,2
        var result1 = AsteroidCollision(new int[]{5,10,-5});//5,10
        var result2 = AsteroidCollision(new int[]{8,-8});
        var result3 = AsteroidCollision(new int[]{10,2,-5});//10
    }
}
/*
735. Asteroid Collision
Medium
Topics
Companies
Hint

We are given an array asteroids of integers representing asteroids in a row.

For each asteroid, the absolute value represents its size, and the sign represents its direction (positive meaning right, negative meaning left). Each asteroid moves at the same speed.

Find out the state of the asteroids after all collisions. If two asteroids meet, the smaller one will explode. If both are the same size, both will explode. Two asteroids moving in the same direction will never meet.

 

Example 1:

Input: asteroids = [5,10,-5]
Output: [5,10]
Explanation: The 10 and -5 collide resulting in 10. The 5 and 10 never collide.

Example 2:

Input: asteroids = [8,-8]
Output: []
Explanation: The 8 and -8 collide exploding each other.

Example 3:

Input: asteroids = [10,2,-5]
Output: [10]
Explanation: The 2 and -5 collide resulting in -5. The 10 and -5 collide resulting in 10.

 

Constraints:

    2 <= asteroids.length <= 104
    -1000 <= asteroids[i] <= 1000
    asteroids[i] != 0


*/