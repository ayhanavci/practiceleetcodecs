using System.Text;

namespace Top75;
internal class RemovingStarsFromaString2390
{
    public string RemoveStars(string s) 
    {
        Stack<char> items = new Stack<char>();
        foreach (char ch in s)
        {
            if (ch == '*')
                items.Pop();
            else
                items.Push(ch);
        }
        StringBuilder res = new StringBuilder();
        if (items.Count == 0)
            return "";
        res.Append(items.Pop());

        while (items.Count > 0)
            res.Insert(0, items.Pop());
        
        return res.ToString();
    }
    public string RemoveStars_Faster(string s)
    {
        StringBuilder res = new StringBuilder();
        for (int i = 0; i < s.Length; ++i)
            if (s[i] == '*')
                res.Remove(res.Length - 1, 1);
            else 
                res.Append(s[i]);
        return res.ToString();
    }
    public void Test()
    {
        var result1 = RemoveStars_Faster("leet**cod*e");//lecoe
        var result2 = RemoveStars("erase*****");//""
    }
   
}

/*
2390. Removing Stars From a String
Medium
Topics
Companies
Hint

You are given a string s, which contains stars *.

In one operation, you can:

    Choose a star in s.
    Remove the closest non-star character to its left, as well as remove the star itself.

Return the string after all stars have been removed.

Note:

    The input will be generated such that the operation is always possible.
    It can be shown that the resulting string will always be unique.

 

Example 1:

Input: s = "leet**cod*e"
Output: "lecoe"
Explanation: Performing the removals from left to right:
- The closest character to the 1st star is 't' in "leet**cod*e". s becomes "lee*cod*e".
- The closest character to the 2nd star is 'e' in "lee*cod*e". s becomes "lecod*e".
- The closest character to the 3rd star is 'd' in "lecod*e". s becomes "lecoe".
There are no more stars, so we return "lecoe".

Example 2:

Input: s = "erase*****"
Output: ""
Explanation: The entire string is removed, so we return an empty string.

 

Constraints:

    1 <= s.length <= 105
    s consists of lowercase English letters and stars *.
    The operation above can be performed on s.


*/