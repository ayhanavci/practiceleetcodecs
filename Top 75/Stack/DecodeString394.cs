using System.Text;

namespace Top75;

internal class DecodeString394
{
    public string DecodeString(string s) 
    {
        StringBuilder countStr = new StringBuilder();
        StringBuilder text = new StringBuilder();
        Stack<int> countStack = new Stack<int>();        
        Stack<string> textStack = new Stack<string>();
            
        foreach (char ch in s)
        {            
            if (ch == '[')
            {    
               countStack.Push(Convert.ToInt32(countStr.ToString()));
               textStack.Push(text.ToString());
               text.Clear();
               countStr.Clear();
            }
            else if (ch == ']')
            {
                StringBuilder temp = new StringBuilder();
                int count = countStack.Pop();                                
                temp.Append(textStack.Pop());                
                while (count-- > 0)
                    temp.Append(text);

                text = temp;
            }
            else if (ch >= '0' && ch <= '9')
            {                
                countStr.Append(ch);
            }
            else
            {
                text.Append(ch);
            }
        }

        return text.ToString();
    }

    public void Test()
    {
        var result5 = DecodeString("100[leetcode]");//
        var result1 = DecodeString("3[a]2[bc]");//aaabcbc
        var result2 = DecodeString("3[a2[c]]");//accaccacc
        var result3 = DecodeString("2[abc]3[cd]ef");//"abcabccdcdcdef"
        var result4 = DecodeString("3[a2[c]de3[gh]kl]");//accdeghghghkl (x3)

    }
}

/*
394. Decode String
Medium
Topics
Companies

Given an encoded string, return its decoded string.

The encoding rule is: k[encoded_string], where the encoded_string inside the square brackets is being repeated exactly k times. Note that k is guaranteed to be a positive integer.

You may assume that the input string is always valid; there are no extra white spaces, square brackets are well-formed, etc. Furthermore, you may assume that the original data does not contain any digits and that digits are only for those repeat numbers, k. For example, there will not be input like 3a or 2[4].

The test cases are generated so that the length of the output will never exceed 105.

 

Example 1:

Input: s = "3[a]2[bc]"
Output: "aaabcbc"

Example 2:

Input: s = "3[a2[c]]"
Output: "accaccacc"

Example 3:

Input: s = "2[abc]3[cd]ef"
Output: "abcabccdcdcdef"

 

Constraints:

    1 <= s.length <= 30
    s consists of lowercase English letters, digits, and square brackets '[]'.
    s is guaranteed to be a valid input.
    All the integers in s are in the range [1, 300].


*/