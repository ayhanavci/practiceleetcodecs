namespace Top75;

internal class MaxNumberofKSumPairs1679
{
    public int MaxOperations(int[] nums, int k) 
    {
        int count = 0;
        Dictionary<int, int> numCount = new Dictionary<int, int>();        

        for (int i = 0; i < nums.Length; ++i)
        {
            if (numCount.ContainsKey(nums[i]))
                numCount[nums[i]]++;
            else
                numCount.Add(nums[i], 1);
        }
        foreach (var num in numCount)
        {
            if (numCount[num.Key] != -1)
            {
                int pairValue = k - num.Key;
                if (pairValue == num.Key)
                {
                    count += num.Value / 2;
                }
                else
                {
                    int pairCount;
                    if (pairValue > 0 && numCount.TryGetValue(pairValue, out pairCount))
                    {
                        count += Math.Min(num.Value, pairCount);
                        numCount[pairValue] = -1;    
                    }                
                }
                
                numCount[num.Key] = -1;                
            }
                        
        }
        return count;
        /*if (!visited.Contains(pair.Key))
            {                
                
                visited.Add(pair.Key);
                visited.Add(k - pair.Key);
            }*/
    }
    public int MaxOperations_TO2(int[] nums, int k) 
    {
        int count = 0;

        List<int> numList = new List<int>(nums);
        for (int i = 0; i < numList.Count - 1; ++i)
        {
            int val = k - numList[i];
            if (val <= 0) continue;
            for (int j = i + 1; j < numList.Count; ++j)
            {
                if (val == numList[j])
                {                    
                    numList.RemoveAt(j);
                    count++;
                    break;
                }
            }   
        }


        return count;
    }
    public int MaxOperations_TO(int[] nums, int k) 
    { 
        int count = 0;
        for (int i = 0; i < nums.Length - 1; ++i)
        {
            if (nums[i] == -1) continue;
            int val = k - nums[i];
            for (int j = i + 1; j < nums.Length; ++j)
            {
                if (nums[j] == -1) continue;
                if (val == nums[j])
                {
                    nums[j] = -1;
                    count++;
                    break;
                }
            }
        }
        return count;
    }
    public void Test()
    {
        var result1 = MaxOperations(new int[] {1,2,3,4}, 5);
        var result2 = MaxOperations(new int[] {3,1,3,4,3}, 6);
        var result3 = MaxOperations(new int[] {4,4,1,3,1,3,2,2,5,5,1,5,2,1,2,3,5,4}, 2);//2
        //var result1 = MaxOperations(new int[] {}, 5);
    }
}
/*
1679. Max Number of K-Sum Pairs
Medium
Topics
Companies
Hint

You are given an integer array nums and an integer k.

In one operation, you can pick two numbers from the array whose sum equals k and remove them from the array.

Return the maximum number of operations you can perform on the array.

 

Example 1:

Input: nums = [1,2,3,4], k = 5
Output: 2
Explanation: Starting with nums = [1,2,3,4]:
- Remove numbers 1 and 4, then nums = [2,3]
- Remove numbers 2 and 3, then nums = []
There are no more pairs that sum up to 5, hence a total of 2 operations.

Example 2:

Input: nums = [3,1,3,4,3], k = 6
Output: 1
Explanation: Starting with nums = [3,1,3,4,3]:
- Remove the first two 3's, then nums = [1,4,3]
There are no more pairs that sum up to 6, hence a total of 1 operation.

 

Constraints:

    1 <= nums.length <= 105
    1 <= nums[i] <= 109
    1 <= k <= 109


*/