namespace Top75;

internal class NearestExitfromEntranceinMaze1926
{        
    //TODO: Slow 5%
    public int NearestExit(char[][] maze, int[] entrance) 
    {
        int rowCount = maze.Length;
        int colCount = maze[0].Length;
        HashSet<Tuple<int, int>> visited = new HashSet<Tuple<int, int>>();
        Queue<Tuple<Tuple<int, int>, int>> queue = new Queue<Tuple<Tuple<int, int>, int>>();

        visited.Add(new Tuple<int, int>(entrance[0], entrance[1]));
        queue.Enqueue(new Tuple<Tuple<int, int>, int>(new Tuple<int, int>(entrance[0] + 1, entrance[1]), 1));
        queue.Enqueue(new Tuple<Tuple<int, int>, int>(new Tuple<int, int>(entrance[0] - 1, entrance[1]), 1));
        queue.Enqueue(new Tuple<Tuple<int, int>, int>(new Tuple<int, int>(entrance[0], entrance[1] + 1), 1));
        queue.Enqueue(new Tuple<Tuple<int, int>, int>(new Tuple<int, int>(entrance[0], entrance[1] - 1), 1));
        
        int minSteps = -1;
        while (queue.Count > 0)
        {
            var next = queue.Dequeue();
            int row = next.Item1.Item1;
            int col = next.Item1.Item2;
            int steps = next.Item2;

            if (row >= rowCount || col >= colCount || row < 0 || col < 0)
                continue;
            if (maze[row][col] == '+')
                continue;
            Tuple<int, int> tuple = new Tuple<int, int>(row, col);
            if (visited.Contains(tuple))
                continue;            
            visited.Add(tuple);
            
            if (row == rowCount - 1 || col == colCount - 1 || row == 0 ||  col == 0)         
            {
                minSteps = minSteps == -1 ? steps : Math.Min(minSteps, steps);                
            }
            //0,1 -> 1,1 -> 2,1 -> 3,1 -> 3,2 -> 3,3 -> 2,3 -> 1,3 -> 1,4 -> 1,5 -> 2,5 -> 3,5 -> 4,5
            else 
            {
                queue.Enqueue(new Tuple<Tuple<int, int>, int>(new Tuple<int, int>(row + 1, col), steps + 1));
                queue.Enqueue(new Tuple<Tuple<int, int>, int>(new Tuple<int, int>(row - 1, col), steps + 1));
                queue.Enqueue(new Tuple<Tuple<int, int>, int>(new Tuple<int, int>(row, col + 1), steps + 1));
                queue.Enqueue(new Tuple<Tuple<int, int>, int>(new Tuple<int, int>(row, col - 1), steps + 1));
            }    
            
            visited.Add(tuple);
        }
        return minSteps;
    }
   
    public void Test()
    {
        Test5();
        /*Test1();
        Test2();
        Test3();
        Test4();*/
    }
    public void Test1()
    {
        char[][] maze = new char[3][];
        maze[0] = new char[]{'+','+','.','+'};
        maze[1] = new char[]{'.','.','.','+'};
        maze[2] = new char[]{'+','+','+','.'};
        
        var result = NearestExit(maze, new int[] {1,2});
    }
    public void Test2()
    {
        char[][] maze = new char[3][];
        maze[0] = new char[]{'+','+','+'};
        maze[1] = new char[]{'.','.','.'};
        maze[2] = new char[]{'+','+','+'};
        
        var result = NearestExit(maze, new int[] {1,0});
    }
    public void Test3()
    {
        char[][] maze = new char[1][];
        maze[0] = new char[]{'.','+'};
        
        var result = NearestExit(maze, new int[] {0,0});
    }
    public void Test4()
    {
        char[][] maze = new char[9][];
        maze[0] = new char[]{'.','+','+','+','.','.','.','+','+'};
        maze[1] = new char[]{'.','.','+','.','+','.','+','+','.'};
        maze[2] = new char[]{'.','.','+','.','.','.','.','.','.'};
        maze[3] = new char[]{'.','+','.','.','+','+','.','+','.'};
        maze[4] = new char[]{'.','.','.','.','.','.','.','+','.'};
        maze[5] = new char[]{'.','.','.','.','.','.','.','.','.'};
        maze[6] = new char[]{'.','.','.','+','.','.','.','.','.'};
        maze[7] = new char[]{'.','.','.','.','.','.','.','.','+'};
        maze[8] = new char[]{'+','.','.','.','+','.','.','.','.'};
        
        var result = NearestExit(maze, new int[] {5,6});//e:2
      
    }//0,1 -> 1,1 -> 2,1 -> 3,1 -> 3,2 -> 3,3 -> 2,3 -> 1,3 -> 1,4 -> 1,5 -> 2,5 -> 3,5 -> 4,5
    public void Test5()
    {
        char[][] maze = new char[5][];
        maze[0] = new char[]{'+','.','+','+','+','+','+'};
        maze[1] = new char[]{'+','.','+','.','.','.','+'};
        maze[2] = new char[]{'+','.','+','.','+','.','+'};
        maze[3] = new char[]{'+','.','.','.','+','.','+'};
        maze[4] = new char[]{'+','+','+','+','+','.','+'};

        
        var result = NearestExit(maze, new int[] {0,1});//e:12
    }
}
/*
1926. Nearest Exit from Entrance in Maze
Medium
Topics
Companies
Hint

You are given an m x n matrix maze (0-indexed) with empty cells (represented as '.') and walls (represented as '+'). You are also given the entrance of the maze, where entrance = [entrancerow, entrancecol] denotes the row and column of the cell you are initially standing at.

In one step, you can move one cell up, down, left, or right. You cannot step into a cell with a wall, and you cannot step outside the maze. Your goal is to find the nearest exit from the entrance. An exit is defined as an empty cell that is at the border of the maze. The entrance does not count as an exit.

Return the number of steps in the shortest path from the entrance to the nearest exit, or -1 if no such path exists.

 

Example 1:

Input: maze = [["+","+",".","+"],[".",".",".","+"],["+","+","+","."]], entrance = [1,2]
Output: 1
Explanation: There are 3 exits in this maze at [1,0], [0,2], and [2,3].
Initially, you are at the entrance cell [1,2].
- You can reach [1,0] by moving 2 steps left.
- You can reach [0,2] by moving 1 step up.
It is impossible to reach [2,3] from the entrance.
Thus, the nearest exit is [0,2], which is 1 step away.

Example 2:

Input: maze = [["+","+","+"],[".",".","."],["+","+","+"]], entrance = [1,0]
Output: 2
Explanation: There is 1 exit in this maze at [1,2].
[1,0] does not count as an exit since it is the entrance cell.
Initially, you are at the entrance cell [1,0].
- You can reach [1,2] by moving 2 steps right.
Thus, the nearest exit is [1,2], which is 2 steps away.

Example 3:

Input: maze = [[".","+"]], entrance = [0,0]
Output: -1
Explanation: There are no exits in this maze.

 

Constraints:

    maze.length == m
    maze[i].length == n
    1 <= m, n <= 100
    maze[i][j] is either '.' or '+'.
    entrance.length == 2
    0 <= entrancerow < m
    0 <= entrancecol < n
    entrance will always be an empty cell.


*/