namespace Top75;

internal class DeleteNodeinaBST450
{
    //Definition for a binary tree node.
    public class TreeNode
    {
        public int val;
        public TreeNode left;
        public TreeNode right;
        public TreeNode(int val = 0, TreeNode left = null, TreeNode right = null)
        {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }
    int key;
    TreeNode newHead;
    public TreeNode DeleteNode(TreeNode root, int key)
    {
        if (root == null) return null;
        newHead = root;
        this.key = key;
        Traverse(null, root);
        return newHead;
    }
    public void SwapLeftNode(TreeNode prev, TreeNode current, TreeNode root)
    {
        while (current != null)
        {
            prev = prev.left;
            current = current.left;
        }
        prev.left = root.left;
    }
    public void Traverse(TreeNode parent, TreeNode root)
    {
        if (root == null) return;

        if (key == root.val)
        {
            if (parent == null)
            {
                if (root.right != null)
                {
                    newHead = root.right;
                    SwapLeftNode(newHead, newHead.left, root);
                }
                else
                {
                    newHead = root.left;
                }
                
            }
            else if (parent.left == root)
            {
                if (root.right != null)
                {
                    parent.left = root.right;
                    SwapLeftNode(root.right, root.right.left, root);
                }
                else                
                    parent.left = root.left;                
            }
            else 
            {
                if (root.right != null)
                {
                    parent.right = root.right;
                    SwapLeftNode(root.right, root.right.left, root);
                }
                else                
                    parent.right = root.left;                
            }

        }
        else if (key > root.val)
            Traverse(root, root.right);
        else
            Traverse(root, root.left);
    }
    public void Test()
    {
        //Test1();
        Test4();
    }
    public void Test1()
    {
        //[3,1,4,3,null,1,5]
        TreeNode root = new TreeNode(5,
            new TreeNode(3,
                new TreeNode(2),
                new TreeNode(4)),
            new TreeNode(6,
                null,
                new TreeNode(8)));
        var result = DeleteNode(root, 3);
    }
    public void Test2()
    {
        //[3,1,4,3,null,1,5]
        TreeNode root = new TreeNode(5,
            new TreeNode(3,
                new TreeNode(2),
                new TreeNode(4)),
            new TreeNode(6,
                null,
                new TreeNode(8)));
        var result = DeleteNode(root, 5);
    }
    public void Test3()
    {
        //[3,1,4,3,null,1,5]
        TreeNode root = new TreeNode(20,
            new TreeNode(10,
                new TreeNode(5, new TreeNode(3), new TreeNode(8)),
                new TreeNode(15, new TreeNode(12), new TreeNode(17))),
            new TreeNode(40,
                new TreeNode(30, new TreeNode(25), new TreeNode(35)),
                new TreeNode(50, new TreeNode(45), new TreeNode(60))));
        var result = DeleteNode(root, 50);
    }
    public void Test4()
    {
        //[3,1,4,3,null,1,5]
        TreeNode root = new TreeNode(0);
        var result = DeleteNode(root, 0);
    }
}

/*
450. Delete Node in a BST
Medium
Topics
Companies

Given a root node reference of a BST and a key, delete the node with the given key in the BST. Return the root node reference (possibly updated) of the BST.

Basically, the deletion can be divided into two stages:

    Search for a node to remove.
    If the node is found, delete the node.

 

Example 1:

Input: root = [5,3,6,2,4,null,7], key = 3
Output: [5,4,6,2,null,null,7]
Explanation: Given key to delete is 3. So we find the node with value 3 and delete it.
One valid answer is [5,4,6,2,null,null,7], shown in the above BST.
Please notice that another valid answer is [5,2,6,null,4,null,7] and it's also accepted.

Example 2:

Input: root = [5,3,6,2,4,null,7], key = 0
Output: [5,3,6,2,4,null,7]
Explanation: The tree does not contain a node with value = 0.

Example 3:

Input: root = [], key = 0
Output: []

 

Constraints:

    The number of nodes in the tree is in the range [0, 104].
    -105 <= Node.val <= 105
    Each node has a unique value.
    root is a valid binary search tree.
    -105 <= key <= 105

 

Follow up: Could you solve it with time complexity O(height of tree)?

*/