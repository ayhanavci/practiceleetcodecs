﻿namespace Top75;

internal class NthTribonacciNumber1137
{
    //Recursion
    Dictionary<int, int> memo;
    public int Tribonacci(int n)
    {
        memo = new Dictionary<int, int>
        {
            { 0, 0 },
            { 1, 1 },
            { 2, 1 }
        };
        return Dp(n);
    }
    private int Dp(int n)
    {
        int value;
        if (memo.TryGetValue(n, out value)) return value;

        value = Dp(n - 3) + Dp(n - 2) + Dp(n - 1);
        memo.Add(n, value);

        return value;
    }

    //Works
    public int Tribonacci_LOOP(int n)
    {
        int[] numbers = new int[n+1];
        numbers[0] = 0;
        if (n >= 1) numbers[1] = 1;
        if (n >= 2) numbers[2] = 1;

        for (int i = 3; i <= n; i++)        
            numbers[i] = numbers[i - 3] + numbers[i - 2] + numbers[i - 1];
        
        return numbers[n];
    }
    public void Test()
    {
        var retVal = Tribonacci(25);

    }
}

/*
 1137. N-th Tribonacci Number
Easy
2.5K
134
Companies

The Tribonacci sequence Tn is defined as follows: 

T0 = 0, T1 = 1, T2 = 1, and Tn+3 = Tn + Tn+1 + Tn+2 for n >= 0.

Given n, return the value of Tn.

 

Example 1:

Input: n = 4
Output: 4
Explanation:
T_3 = 0 + 1 + 1 = 2
T_4 = 1 + 1 + 2 = 4

Example 2:

Input: n = 25
Output: 1389537

 

Constraints:

    0 <= n <= 37
    The answer is guaranteed to fit within a 32-bit integer, ie. answer <= 2^31 - 1.

 */