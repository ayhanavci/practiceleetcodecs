namespace Top75;

//TODO: ?!?!?
internal class DominoandTrominoTiling790
{
   
    public int NumTilings(int n)
    {
        long[] dp = new long[n + 3]; dp[0] = 1; dp[1] = 2; dp[2] = 5;
        for (int i = 3; i < n; i ++) {
            dp[i] = (dp[i - 1] * 2 + dp[i - 3]) % 1000000007;
        }
        return (int)dp[n - 1];

    }
    long[] dp;
    int MOD = (int)Math.Pow(10, 9) + 7;

    public int NumTilings_RECURSE(int n)
    {
        dp = new long[n + 1];
        Array.Fill(dp, -1);

        return (int)solve(n) % MOD;

    }
    private long solve(int n)
    {
        if (n == 0) return 1;
        if (n < 3) return n;

        if (dp[n] != -1) return dp[n];

        return dp[n] = 2 * solve(n - 1) % MOD + solve(n - 3) % MOD;
    }
 

    //https://leetcode.com/problems/domino-and-tromino-tiling/?envType=study-plan-v2&envId=leetcode-75
    public void Test()
    {

    }
}
/*
790. Domino and Tromino Tiling
Medium
Topics
Companies

You have two types of tiles: a 2 x 1 domino shape and a tromino shape. You may rotate these shapes.

Given an integer n, return the number of ways to tile an 2 x n board. Since the answer may be very large, return it modulo 109 + 7.

In a tiling, every square must be covered by a tile. Two tilings are different if and only if there are two 4-directionally adjacent cells on the board such that exactly one of the tilings has both squares occupied by a tile.

 

Example 1:

Input: n = 3
Output: 5
Explanation: The five different ways are show above.

Example 2:

Input: n = 1
Output: 1

 

Constraints:

    1 <= n <= 1000


*/