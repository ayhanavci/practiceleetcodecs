namespace Top75;

internal class DetermineifTwoStringsAreClose1657
{
    //TODO: Slow 14%
    public bool CloseStrings(string word1, string word2) 
    {
        int length = word1.Length;
        //w1 and w2 should be same size
        if (length != word2.Length)
            return false;
        
        Dictionary<char, int> w1Dict = new Dictionary<char, int>();
        Dictionary<char, int> w2Dict = new Dictionary<char, int>();

        for (int i = 0; i < length; ++i)
        {
            if (w1Dict.ContainsKey(word1[i]))
                w1Dict[word1[i]]++;
            else
                w1Dict.Add(word1[i], 1);

            if (w2Dict.ContainsKey(word2[i]))
                w2Dict[word2[i]]++;
            else
                w2Dict.Add(word2[i], 1);            
        }
        if (w1Dict.Count != w2Dict.Count)
            return false;
        
        List<int> w1Sizes = new List<int>();              
        List<int> w2Sizes = new List<int>();       

        //All letters in w1 should exist in w2
        //All letters in w2 should exist in w1
        for (int i = 0; i < w1Dict.Count; ++i)
        {
            var w1Element = w1Dict.ElementAt(i);
            var w2Element = w2Dict.ElementAt(i);

            if (!w1Dict.ContainsKey(w2Element.Key))
                return false;
            
            if (!w2Dict.ContainsKey(w1Element.Key))
                return false;

            w1Sizes.Add(w1Element.Value);
            w2Sizes.Add(w2Element.Value);
        }
        w1Sizes.Sort();
        w2Sizes.Sort();

        //count of letters of each word should be the same
        for (int i = 0; i < w1Sizes.Count; ++i)
            if (w1Sizes[i] != w2Sizes[i])
                return false;

        return true;        
    }
    public void Test()
    {
        var result4 = CloseStrings("uau", "ssx");//f
        var result1 = CloseStrings("abc", "bca");
        var result2 = CloseStrings("a", "aa");
        var result3 = CloseStrings("cabbba", "abbccc");
    }
}

/*
1657. Determine if Two Strings Are Close
Medium
Topics
Companies
Hint

Two strings are considered close if you can attain one from the other using the following operations:

    Operation 1: Swap any two existing characters.
        For example, abcde -> aecdb
    Operation 2: Transform every occurrence of one existing character into another existing character, and do the same with the other character.
        For example, aacabb -> bbcbaa (all a's turn into b's, and all b's turn into a's)

You can use the operations on either string as many times as necessary.

Given two strings, word1 and word2, return true if word1 and word2 are close, and false otherwise.

 

Example 1:

Input: word1 = "abc", word2 = "bca"
Output: true
Explanation: You can attain word2 from word1 in 2 operations.
Apply Operation 1: "abc" -> "acb"
Apply Operation 1: "acb" -> "bca"

Example 2:

Input: word1 = "a", word2 = "aa"
Output: false
Explanation: It is impossible to attain word2 from word1, or vice versa, in any number of operations.

Example 3:

Input: word1 = "cabbba", word2 = "abbccc"
Output: true
Explanation: You can attain word2 from word1 in 3 operations.
Apply Operation 1: "cabbba" -> "caabbb"
Apply Operation 2: "caabbb" -> "baaccc"
Apply Operation 2: "baaccc" -> "abbccc"

 

Constraints:

    1 <= word1.length, word2.length <= 105
    word1 and word2 contain only lowercase English letters.


*/