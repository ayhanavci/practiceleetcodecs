namespace Top75;

internal class EqualRowandColumnPairs2352
{
    public int EqualPairs(int[][] grid) 
    {
        int count = 0;
        for (int i = 0; i < grid.Length; ++i)
        {
            for (int j = 0; j < grid.Length; ++j)
            {
                int x = 0;                

                while (x < grid.Length && grid[i][x] == grid[x][j])
                {
                    if (x++ == grid.Length - 1)
                        count++;
                }
                
            }
        }
        return count;        
    }
    public int EqualPairs_BRUTE(int[][] grid) 
    {
        int count = 0;
        for (int i = 0; i < grid.Length; ++i)
        {
            for (int j = 0; j < grid.Length; ++j)
            {
                int x = 0;                

                while (x < grid.Length && grid[i][x] == grid[x][j])
                {
                    if (x++ == grid.Length - 1)
                        count++;
                }
                
            }
        }
        return count;        
    }
    public void Test()
    {
        Test1();
        Test2();
    }
    public void Test2()
    {
        int[][] grid = new int[4][];
        grid[0] = new int[] {3,1,2,2};
        grid[1] = new int[] {1,4,4,5};
        grid[2] = new int[] {2,4,2,2};
        grid[3] = new int[] {2,4,2,2};
        var result = EqualPairs(grid);
    }
     public void Test1()
    {
        int[][] grid = new int[3][];
        grid[0] = new int[] {3,2,1};
        grid[1] = new int[] {1,7,6};
        grid[2] = new int[] {2,7,7};
        var result = EqualPairs(grid);
    }
}
/*
2352. Equal Row and Column Pairs
Medium
Topics
Companies
Hint

Given a 0-indexed n x n integer matrix grid, return the number of pairs (ri, cj) such that row ri and column cj are equal.

A row and column pair is considered equal if they contain the same elements in the same order (i.e., an equal array).

 

Example 1:

Input: grid = [[3,2,1],[1,7,6],[2,7,7]]
Output: 1
Explanation: There is 1 equal row and column pair:
- (Row 2, Column 1): [2,7,7]

Example 2:

Input: grid = [[3,1,2,2],[1,4,4,5],[2,4,2,2],[2,4,2,2]]
Output: 3
Explanation: There are 3 equal row and column pairs:
- (Row 0, Column 0): [3,1,2,2]
- (Row 2, Column 2): [2,4,2,2]
- (Row 3, Column 2): [2,4,2,2]

 

Constraints:

    n == grid.length == grid[i].length
    1 <= n <= 200
    1 <= grid[i][j] <= 105


*/