namespace Top75;
internal class FindtheDifferenceofTwoArrays2215
{
    public IList<IList<int>> FindDifference(int[] nums1, int[] nums2) 
    {
        HashSet<int> set1 = new();
        HashSet<int> set2 = new();

        foreach (int num in nums1)
            set1.Add(num);
        
        foreach (int num in nums2)
            set2.Add(num);

        List<int> ans0 = new List<int>();
        List<int> ans1 = new List<int>();
        IList<IList<int>> ret = new List<IList<int>>() { ans0, ans1 };
        
        foreach (int num in set1)
            if (!set2.Contains(num))
                ans0.Add(num);

        foreach (int num in set2)
            if (!set1.Contains(num))
                ans1.Add(num);

        return ret;

    }   
    public void Test()
    {
        var result1 = FindDifference(new int[]{1,2,3}, new int[]{2,4,6});
        var result2 = FindDifference(new int[]{1,2,3,3}, new int[]{1,1,2,2});
        //var result3 = FindDifference(new int[]{}, new int[]{});
    }
}
