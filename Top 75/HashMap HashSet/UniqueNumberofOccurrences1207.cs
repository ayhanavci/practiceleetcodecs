namespace Top75;

internal class UniqueNumberofOccurrences1207
{
    public bool UniqueOccurrences(int[] arr) 
    {
        Dictionary<int, int> occurances = new();   
        Dictionary<int, int> reverse = new();

        for (int i = 0; i < arr.Length; ++i)
        {
            if (occurances.ContainsKey(arr[i]))
                occurances[arr[i]]++;
            else
                occurances.Add(arr[i], 1);
        }
        foreach (var pair in occurances)
        {
            int num = pair.Key;
            int count = pair.Value;

            if (reverse.ContainsKey(count))
                return false;
            reverse.Add(count, num);
        }
        return true;
    }
    public void Test()
    {
        var result1 = UniqueOccurrences(new int[]{1,2,2,1,1,3});//t
        var result2 = UniqueOccurrences(new int[]{1,2});//f
        var result3 = UniqueOccurrences(new int[]{-3,0,1,-3,1,1,1,-3,10,0});//t
    }
}

/*
1207. Unique Number of Occurrences
Solved
Easy
Topics
Companies
Hint

Given an array of integers arr, return true if the number of occurrences of each value in the array is unique or false otherwise.

 

Example 1:

Input: arr = [1,2,2,1,1,3]
Output: true
Explanation: The value 1 has 3 occurrences, 2 has 2 and 3 has 1. No two values have the same number of occurrences.

Example 2:

Input: arr = [1,2]
Output: false

Example 3:

Input: arr = [-3,0,1,-3,1,1,1,-3,10,0]
Output: true

 

Constraints:

    1 <= arr.length <= 1000
    -1000 <= arr[i] <= 1000


*/