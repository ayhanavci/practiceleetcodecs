namespace Top75;

internal class LeafSimilarTrees872
{
    public class TreeNode
    {
        public int val;
        public TreeNode left;
        public TreeNode right;
        public TreeNode(int val = 0, TreeNode left = null, TreeNode right = null)
        {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }
    public bool LeafSimilar(TreeNode root1, TreeNode root2) 
    {
        List<int> sequence1 = new List<int>();
        List<int> sequence2 = new List<int>();
        CreateRootLeaves(root1, sequence1);
        CreateRootLeaves(root2, sequence2);

        if (sequence1.Count != sequence2.Count)
            return false;
        
        for (int i = 0; i < sequence1.Count; ++i)
        {
            if (sequence1[i] != sequence2[i])
                return false;
        }
        return true;
    }
    public void CreateRootLeaves(TreeNode root, List<int> sequence)
    {
        if (root == null) return;
        
        if (root.left == null && root.right == null)
            sequence.Add(root.val);
        
        CreateRootLeaves(root.left, sequence);
        CreateRootLeaves(root.right, sequence);        
    }

    public void Test()
    {

    }
}
/*
872. Leaf-Similar Trees
Solved
Easy
Topics
Companies

Consider all the leaves of a binary tree, from left to right order, the values of those leaves form a leaf value sequence.

For example, in the given tree above, the leaf value sequence is (6, 7, 4, 9, 8).

Two binary trees are considered leaf-similar if their leaf value sequence is the same.

Return true if and only if the two given trees with head nodes root1 and root2 are leaf-similar.

 

Example 1:

Input: root1 = [3,5,1,6,2,9,8,null,null,7,4], root2 = [3,5,1,6,7,4,2,null,null,null,null,null,null,9,8]
Output: true

Example 2:

Input: root1 = [1,2,3], root2 = [1,3,2]
Output: false

 

Constraints:

    The number of nodes in each tree will be in the range [1, 200].
    Both of the given trees will have values in the range [0, 200].


*/