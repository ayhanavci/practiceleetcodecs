namespace Top75;

//TODO: REDO
internal class PathSumIII437
{
    //Definition for a binary tree node.
    public class TreeNode
    {
        public int val;
        public TreeNode left;
        public TreeNode right;
        public TreeNode(int val = 0, TreeNode left = null, TreeNode right = null)
        {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }
  
    public int PathSum(TreeNode root, int targetSum) 
    {   
        if(root == null) return 0;
        
        return Dfs(root, targetSum) 
            + PathSum(root.left, targetSum)
            + PathSum(root.right, targetSum);

        int Dfs(TreeNode node, long sum)
        {
            var s = node.val == sum ? 1 : 0;

            if(node.left != null)
                s += Dfs(node.left, sum-node.val);

            if(node.right != null)
                s += Dfs(node.right, sum-node.val);

            return s;
        }
    }
  
    int count = 0;
    
    HashSet<TreeNode> visited;
    public int PathSum_OLD(TreeNode root, int targetSum) 
    {
        count = 0;
        visited = new HashSet<TreeNode>();
        if (root == null) return 0;                
        if (root.val == targetSum) count++;
        this.targetSum = targetSum;
        Dfs_OLD(root, root.val, true);
        Dfs_OLD(root, root.val, false);
        return count;
    }   
    
    public void Dfs_OLD(TreeNode root, int curSum, bool includeParent)
    {
        if (root == null)                     
            return;                
        
        if (root.val == targetSum)
        {
            count++;            
        }
            
        
        if (includeParent)
        {
            int newSum = curSum + root.val;
            if (newSum == targetSum) 
            {
                count++;
            }
            Dfs_OLD(root.left, curSum + root.val, true);
            Dfs_OLD(root.right, curSum + root.val, true);
        }
        else
        {
            Dfs_OLD(root.left, root.val, true);
            Dfs_OLD(root.right, root.val, true);
        }
        
        
    }
  
   public void Test()
    {
        Test5();
        Test1();
        Test2();
        Test3();
        Test4();
        
    }   
    public void Test1()
    {
        //[3,1,4,3,null,1,5]
        TreeNode root = new TreeNode(10,
            new TreeNode(5, 
                new TreeNode(3,
                    new TreeNode(4),
                    new TreeNode(-2)),
                new TreeNode(2,
                    null,
                    new TreeNode(1))),

            new TreeNode(-3,
                null,
                new TreeNode(11)));
        var result = PathSum(root, 8);
    }
    public void Test2()
    {
        //[3,1,4,3,null,1,5]
        TreeNode root = new TreeNode(1, new TreeNode(2));
        var result = PathSum(root, 1);
    }
     public void Test3()
    {
        //[3,1,4,3,null,1,5]
        TreeNode root = new TreeNode(1, new TreeNode(2));
        var result = PathSum(root, 2);
    }
    public void Test4()
    {
        //1,-2,-3,1,3,-2,null,-1
        TreeNode root = new TreeNode(1,
            new TreeNode(-2, new TreeNode(1, new TreeNode(-1), null), new TreeNode(3)),                
            new TreeNode(-3, new TreeNode(-2), null));
        var result = PathSum(root, 3);
    }
    public void Test5()
    {
        //1,-2,-3,1,3,-2,null,-1
        TreeNode root = new TreeNode(1,
            new TreeNode(-2, new TreeNode(1, new TreeNode(-1), null), new TreeNode(3)),                
            new TreeNode(-3, new TreeNode(-2), null));
        var result = PathSum(root, 0);
    }
}
/*
class Solution(object):
    def pathSum(self, root, target):
        """
        :type root: TreeNode
        :type sum: int
        :rtype: int
        """
        # define global return var
        self.numOfPaths = 0
        # 1st layer DFS to go through each node
        self.dfs(root, target)
        # return result
        return self.numOfPaths
    
    # define: traverse through the tree, at each treenode, call another DFS to test if a path sum include the answer
    def dfs(self, node, target):
        # exit condition
        if node is None:
            return 
        # dfs break down 
        self.test(node, target) # you can move the line to any order, here is pre-order
        self.dfs(node.left, target)
        self.dfs(node.right, target)
        
    # define: for a given node, DFS to find any path that sum == target, if find self.numOfPaths += 1
    def test(self, node, target):
        # exit condition
        if node is None:
            return
        if node.val == target:
            self.numOfPaths += 1
            
        # test break down
        self.test(node.left, target-node.val)
        self.test(node.right, target-node.val)
*/
/*
class Solution(object):
    def pathSum(self, root, target):
        # define global result and path
        self.result = 0
        cache = {0:1}
        
        # recursive to get result
        self.dfs(root, target, 0, cache)
        
        # return result
        return self.result
    
    def dfs(self, root, target, currPathSum, cache):
        # exit condition
        if root is None:
            return  
        # calculate currPathSum and required oldPathSum
        currPathSum += root.val
        oldPathSum = currPathSum - target
        # update result and cache
        self.result += cache.get(oldPathSum, 0)
        cache[currPathSum] = cache.get(currPathSum, 0) + 1
        
        # dfs breakdown
        self.dfs(root.left, target, currPathSum, cache)
        self.dfs(root.right, target, currPathSum, cache)
        # when move to a different branch, the currPathSum is no longer available, hence remove one. 
        cache[currPathSum] -= 1
*/
/*
437. Path Sum III
Medium
Topics
Companies

Given the root of a binary tree and an integer targetSum, return the number of paths where the sum of the values along the path equals targetSum.

The path does not need to start or end at the root or a leaf, but it must go downwards (i.e., traveling only from parent nodes to child nodes).

 

Example 1:

Input: root = [10,5,-3,3,2,null,11,3,-2,null,1], targetSum = 8
Output: 3
Explanation: The paths that sum to 8 are shown.

Example 2:

Input: root = [5,4,8,11,null,13,4,7,2,null,null,5,1], targetSum = 22
Output: 3

 

Constraints:

    The number of nodes in the tree is in the range [0, 1000].
    -109 <= Node.val <= 109
    -1000 <= targetSum <= 1000


*/