namespace Top75;

internal class CountGoodNodesinBinaryTree1448
{
    //Definition for a binary tree node.
    public class TreeNode
    {
        public int val;
        public TreeNode left;
        public TreeNode right;
        public TreeNode(int val = 0, TreeNode left = null, TreeNode right = null)
        {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }
    int goodCount = 0;
    public int GoodNodes(TreeNode root) 
    {
        Dfs(root, int.MinValue);
        return goodCount;
    }
    public void Dfs(TreeNode root, int max)
    {
        if (root == null) return;

        if (root.val >= max)
            goodCount++;
        
        int newMax = Math.Max(root.val, max);

        Dfs(root.left, newMax);
        Dfs(root.right, newMax);
    }
    public void Test()
    {
        Test1();
    }   
    public void Test1()
    {
        //[3,1,4,3,null,1,5]
        TreeNode root = new TreeNode(3,
            new TreeNode(1, 
                new TreeNode(3)),
            new TreeNode(4,
                new TreeNode(1),
                new TreeNode(5)));
        var result = GoodNodes(root);
    }
}