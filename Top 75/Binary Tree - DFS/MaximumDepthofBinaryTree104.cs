namespace Top75;

internal class MaximumDepthofBinaryTree104
{
    public class TreeNode
    {
        public int val;
        public TreeNode left;
        public TreeNode right;
        public TreeNode(int val = 0, TreeNode left = null, TreeNode right = null)
        {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }
    int maxDepth = 1;
    public int MaxDepth(TreeNode root) 
    {
        if (root == null) return 0;
        Traverse(root, 1);
        return maxDepth;
    }
    public void Traverse(TreeNode root, int depth)
    {
        if (root == null) return;

        maxDepth = Math.Max(maxDepth, depth);
        Traverse(root.left, depth + 1);
        Traverse(root.right, depth + 1);
    }
    public void Test()
    {
        Test1();
    }
    public void Test1()
    {
        //[3,1,4,3,null,1,5]
        TreeNode root = new TreeNode(3,
            new TreeNode(9),
            new TreeNode(20,
                new TreeNode(15),
                new TreeNode(7)));
        var result = MaxDepth(root);
    }
}
/*
104. Maximum Depth of Binary Tree
Solved
Easy
Topics
Companies

Given the root of a binary tree, return its maximum depth.

A binary tree's maximum depth is the number of nodes along the longest path from the root node down to the farthest leaf node.

 

Example 1:

Input: root = [3,9,20,null,null,15,7]
Output: 3

Example 2:

Input: root = [1,null,2]
Output: 2

 

Constraints:

    The number of nodes in the tree is in the range [0, 104].
    -100 <= Node.val <= 100


*/