namespace Top75;

internal class SingleNumber136
{
    public int SingleNumber(int[] nums) 
    {
        if (nums.Length == 1) return nums[0];
        int res = 0;
        long count = 0;
        int indicator = 1;

        for (int i = 0; i < 32; ++i)
        {
            count = 0;           
            for (int j = 0; j < nums.Length; ++j)            
                count += (nums[j] & indicator) != 0 ? 1 : 0;
            
            if (count % 2 != 0)
                res |= indicator;

            indicator <<= 1;
        }
        return res;
    }
    public void Test()
    {
        //var result = SingleNumber(new int[] {4,1,2,1,2});
        var result = SingleNumber(new int[] {-1, -1, -2});
    }
}

/*
136. Single Number
Solved
Easy
Topics
Companies

Given a non-empty array of integers nums, every element appears twice except for one. Find that single one.

You must implement a solution with a linear runtime complexity and use only constant extra space.

 

Example 1:

Input: nums = [2,2,1]
Output: 1

Example 2:

Input: nums = [4,1,2,1,2]
Output: 4

Example 3:

Input: nums = [1]
Output: 1

 

Constraints:

    1 <= nums.length <= 3 * 104
    -3 * 104 <= nums[i] <= 3 * 104
    Each element in the array appears twice except for one element which appears only once.


*/