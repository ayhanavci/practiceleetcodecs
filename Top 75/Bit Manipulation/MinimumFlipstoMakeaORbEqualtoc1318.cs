namespace Top75;

internal class MinimumFlipstoMakeaORbEqualtoc1318
{
    public int MinFlips(int a, int b, int c) 
    {
        int flips = 0;
        int curIndex = 1;
        for (int i = 0; i < 32; ++i)
        {   
            if ((c & curIndex) > 0)
            {
                //Either a's or b's curIndex bit must be 1 to achieve a | b = 1. If both are 0, we must flip one of them.
                if ((a & curIndex) == 0 && (b & curIndex) == 0)
                    flips++;
            }
            else
            {
                //Both a's and b's curIndex bits must be 0 to achieve a | b = 0
                if ((a & curIndex) > 0) 
                    flips++;
                if ((b & curIndex) > 0) 
                    flips++;
            }

            curIndex <<= 1;
        }
        return flips;
    }
    public void Test()
    {
        var result1 = MinFlips(2, 6, 5);
    }
}
/*
1318. Minimum Flips to Make a OR b Equal to c
Medium
Topics
Companies
Hint

Given 3 positives numbers a, b and c. Return the minimum flips required in some bits of a and b to make ( a OR b == c ). (bitwise OR operation).
Flip operation consists of change any single bit 1 to 0 or change the bit 0 to 1 in their binary representation.

 

Example 1:

Input: a = 2, b = 6, c = 5
Output: 3
Explanation: After flips a = 1 , b = 4 , c = 5 such that (a OR b == c)

Example 2:

Input: a = 4, b = 2, c = 7
Output: 1

Example 3:

Input: a = 1, b = 2, c = 3
Output: 0

 

Constraints:

    1 <= a <= 10^9
    1 <= b <= 10^9
    1 <= c <= 10^9

*/