namespace Top75;

internal class SmallestNumberinInfiniteSet2336
{
    public class SmallestInfiniteSet
    {
        int nextInfinite = -1;
        PriorityQueue<int, int> added;
        HashSet<int> usedNumbers;

        public SmallestInfiniteSet()
        {
            added = new PriorityQueue<int, int>();
            usedNumbers = new HashSet<int>();
        }

        public int PopSmallest()
        {
            if (added.Count > 0)
            {
                int ret = added.Dequeue();
                usedNumbers.Add(ret);
                return ret;
            }

            if (nextInfinite == -1)
            {
                nextInfinite = 1;
                usedNumbers.Add(1);
                return 1;
            }      
            usedNumbers.Add(++nextInfinite);      
            return nextInfinite;
        }

        public void AddBack(int num)
        {
            if (nextInfinite < num || !usedNumbers.Contains(num))
                return;    

            added.Enqueue(num, num);
            usedNumbers.Remove(num);            
        }
    }
    public void Test()
    {
        Test1();
    }
    public void Test1()
    {
        SmallestInfiniteSet smallestInfiniteSet = new SmallestInfiniteSet();
        smallestInfiniteSet.AddBack(2);    // 2 is already in the set, so no change is made.
        int ret = smallestInfiniteSet.PopSmallest(); // return 1, since 1 is the smallest number, and remove it from the set.
        ret = smallestInfiniteSet.PopSmallest(); // return 2, and remove it from the set.
        ret = smallestInfiniteSet.PopSmallest(); // return 3, and remove it from the set.
        smallestInfiniteSet.AddBack(1);    // 1 is added back to the set.
        ret = smallestInfiniteSet.PopSmallest(); // return 1, since 1 was added back to the set and is the smallest number, and remove it from the set.
        ret = smallestInfiniteSet.PopSmallest(); // return 4, and remove it from the set.
        ret = smallestInfiniteSet.PopSmallest(); // return 5, and remove it from the set.
    }
}