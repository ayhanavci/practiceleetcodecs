namespace Top75;

internal class TotalCosttoHireKWorkers2462
{
    //TODO: Çok uğraştırdı ama oldu. tekrar bak belki daha iyi çözüm vardır.
    public long TotalCost(int[] costs, int k, int candidates) 
    {
        int total = 0;
        PriorityQueue<int, (int, int)> pqLeft = new(
            Comparer<(int, int)>.Create((x, y) =>
                {
                    if (x.Item1 == y.Item1)                    
                        return x.Item2 - y.Item2;                    
                    return x.Item1 - y.Item1;
                })
        );
        PriorityQueue<int, (int, int)> pqRight = new(
            Comparer<(int, int)>.Create((x, y) =>
                {
                    if (x.Item1 == y.Item1)                    
                        return x.Item2 - y.Item2;                    
                    return x.Item1 - y.Item1;
                })
        );
                       
        for (int i = 0; i < costs.Length; ++i)
        {                
            if (i == candidates | i > costs.Length - 1 - i)
                break;       
            pqLeft.Enqueue(costs[i], (costs[i], i));                     
            if (i == costs.Length - 1 - i)
                break;
            pqRight.Enqueue(costs[costs.Length - 1 - i], (costs[costs.Length - 1 - i], i)); 
        }
        int leftIndex = pqLeft.Count;
        int rightIndex = costs.Length - pqRight.Count - 1;

        while (k > 0)
        {
            if (pqLeft.Count > 0 && pqRight.Count > 0)
            {              
                if (pqLeft.Peek() <= pqRight.Peek())
                {
                    total += pqLeft.Dequeue();                    
                    if (leftIndex <= rightIndex)
                    {
                        pqLeft.Enqueue(costs[leftIndex], (costs[leftIndex], leftIndex));
                        leftIndex++;
                    }
                }
                else
                {
                    total += pqRight.Dequeue();
                    if (rightIndex >= leftIndex)
                    {
                        pqRight.Enqueue(costs[rightIndex], (costs[rightIndex], rightIndex));
                        rightIndex--;
                    }
                }
            }
            else if (pqLeft.Count > 0)
            {
                total += pqLeft.Dequeue(); 
            }
            else if (pqRight.Count > 0)
            {
                total += pqRight.Dequeue();
            }
            else
            {
                break;
            }   
            k--;         
        }

        return total;        
    }
    public void Test()
    {
        Test1();
    }
    public void Test1()
    {
        int[] costs = new int[] {17,12,10,2,7,2,11,20,8};//2 2 7 

        var result = TotalCost(costs, 3, 4);
    }
    public void Test3()
    {
        int[] costs = new int[] {17,12,10,2,7,4,11,20,8, 40, 35};//2 4 7 8 10 

        var result = TotalCost(costs, 5, 3);
    }
    public void Test2()
    {
        int[] costs = new int[] {1,2,4,1};

        var result = TotalCost(costs, 3, 3);
    }
}
/*
2462. Total Cost to Hire K Workers
Medium
Topics
Companies
Hint

You are given a 0-indexed integer array costs where costs[i] is the cost of hiring the ith worker.

You are also given two integers k and candidates. We want to hire exactly k workers according to the following rules:

    You will run k sessions and hire exactly one worker in each session.
    In each hiring session, choose the worker with the lowest cost from either the first candidates workers or the last candidates workers. Break the tie by the smallest index.
        For example, if costs = [3,2,7,7,1,2] and candidates = 2, then in the first hiring session, we will choose the 4th worker because they have the lowest cost [3,2,7,7,1,2].
        In the second hiring session, we will choose 1st worker because they have the same lowest cost as 4th worker but they have the smallest index [3,2,7,7,2]. Please note that the indexing may be changed in the process.
    If there are fewer than candidates workers remaining, choose the worker with the lowest cost among them. Break the tie by the smallest index.
    A worker can only be chosen once.

Return the total cost to hire exactly k workers.

 

Example 1:

Input: costs = [17,12,10,2,7,2,11,20,8], k = 3, candidates = 4
Output: 11
Explanation: We hire 3 workers in total. The total cost is initially 0.
- In the first hiring round we choose the worker from [17,12,10,2,7,2,11,20,8]. The lowest cost is 2, and we break the tie by the smallest index, which is 3. The total cost = 0 + 2 = 2.
- In the second hiring round we choose the worker from [17,12,10,7,2,11,20,8]. The lowest cost is 2 (index 4). The total cost = 2 + 2 = 4.
- In the third hiring round we choose the worker from [17,12,10,7,11,20,8]. The lowest cost is 7 (index 3). The total cost = 4 + 7 = 11. Notice that the worker with index 3 was common in the first and last four workers.
The total hiring cost is 11.

Example 2:

Input: costs = [1,2,4,1], k = 3, candidates = 3
Output: 4
Explanation: We hire 3 workers in total. The total cost is initially 0.
- In the first hiring round we choose the worker from [1,2,4,1]. The lowest cost is 1, and we break the tie by the smallest index, which is 0. The total cost = 0 + 1 = 1. Notice that workers with index 1 and 2 are common in the first and last 3 workers.
- In the second hiring round we choose the worker from [2,4,1]. The lowest cost is 1 (index 2). The total cost = 1 + 1 = 2.
- In the third hiring round there are less than three candidates. We choose the worker from the remaining workers [2,4]. The lowest cost is 2 (index 0). The total cost = 2 + 2 = 4.
The total hiring cost is 4.

 

Constraints:

    1 <= costs.length <= 105 
    1 <= costs[i] <= 105
    1 <= k, candidates <= costs.length


*/