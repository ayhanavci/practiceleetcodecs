namespace Top75;

internal class BinaryTreeRightSideView199
{
    //Definition for a binary tree node.
    public class TreeNode
    {
        public int val;
        public TreeNode left;
        public TreeNode right;
        public TreeNode(int val = 0, TreeNode left = null, TreeNode right = null)
        {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }
    HashSet<int> depthSet;
    IList<int> ret;
    public IList<int> RightSideView(TreeNode root) 
    {
        depthSet = new();
        ret = new List<int>();
        Traverse(root, 1);        
        
        return ret;
    }
    public void Traverse(TreeNode root, int depth)
    {
        if (root == null) return;

        if (!depthSet.Contains(depth)) 
        {
            depthSet.Add(depth);
            ret.Add(root.val);
        }   
            

        Traverse(root.right, depth + 1);
        Traverse(root.left, depth + 1);
        
    }
    
    public void Test()
    {
        Test1();
        Test2();
        Test3();
    }

    public void Test1()
    {                
        TreeNode l = new TreeNode(2)
        {
            right = new TreeNode(5),            
        };
        
        TreeNode r = new TreeNode(3)
        {
            right = new TreeNode(4)
        };


        TreeNode root = new TreeNode(1)
        {
            right = r,
            left = l
        };

        var result = RightSideView(root);
    }
    public void Test2()
    {        
        

        
        TreeNode r = new TreeNode(3)
        {
           
        };


        TreeNode root = new TreeNode(1)
        {
            right = r,
           
        };

        var result = RightSideView(root);
    }
     public void Test3()
    {        


        var result = RightSideView(null);
    }
}
/*
199. Binary Tree Right Side View
Solved
Medium
Topics
Companies

Given the root of a binary tree, imagine yourself standing on the right side of it, return the values of the nodes you can see ordered from top to bottom.

 

Example 1:

Input: root = [1,2,3,null,5,null,4]
Output: [1,3,4]

Example 2:

Input: root = [1,null,3]
Output: [1,3]

Example 3:

Input: root = []
Output: []

 

Constraints:

    The number of nodes in the tree is in the range [0, 100].
    -100 <= Node.val <= 100


*/