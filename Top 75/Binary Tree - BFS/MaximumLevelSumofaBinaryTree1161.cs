namespace Top75;

internal class MaximumLevelSumofaBinaryTree1161
{

    //Definition for a binary tree node.
    public class TreeNode
    {
        public int val;
        public TreeNode left;
        public TreeNode right;
        public TreeNode(int val = 0, TreeNode left = null, TreeNode right = null)
        {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }
    Dictionary<int, int> depthSums = new Dictionary<int, int>();
    
    public int MaxLevelSum(TreeNode root) 
    {
        Traverse(root, 1);

        int maxSum = int.MinValue;    
        int maxDepth = int.MinValue;

        foreach (var pair in depthSums)
        {
            if (pair.Value > maxSum)
            {
                maxSum = pair.Value;
                maxDepth = pair.Key;
            }
                
        }
        return maxDepth;
    }
    public void Traverse(TreeNode root, int depth)
    {
        if (root == null) return;

        if (depthSums.ContainsKey(depth))
            depthSums[depth] += root.val;
        else
            depthSums.Add(depth, root.val);      
            
        Traverse(root.left, depth + 1);
        Traverse(root.right, depth + 1);
    }
    public void Test()
    {
        Test2();
    }   
    public void Test1()
    {
        //[3,1,4,3,null,1,5]
        TreeNode root = new TreeNode(1,
            new TreeNode(7, 
                new TreeNode(7),
                new TreeNode(-8)),
            new TreeNode(0));
        var result = MaxLevelSum(root);
    }
    public void Test2()
    {
        //[989,null,10250,98693,-89388,null,null,null,-32127]
        TreeNode root = new TreeNode(989,
            null,

            new TreeNode(10250, 
                new TreeNode(98693),
                new TreeNode(-89388)));

        var result = MaxLevelSum(root);
    }
}
/*
1161. Maximum Level Sum of a Binary Tree
Medium
Topics
Companies
Hint

Given the root of a binary tree, the level of its root is 1, the level of its children is 2, and so on.

Return the smallest level x such that the sum of all the values of nodes at level x is maximal.

 

Example 1:

Input: root = [1,7,0,7,-8,null,null]
Output: 2
Explanation: 
Level 1 sum = 1.
Level 2 sum = 7 + 0 = 7.
Level 3 sum = 7 + -8 = -1.
So we return the level with the maximum sum which is level 2.

Example 2:

Input: root = [989,null,10250,98693,-89388,null,null,null,-32127]
Output: 2

 

Constraints:

    The number of nodes in the tree is in the range [1, 104].
    -105 <= Node.val <= 105


*/