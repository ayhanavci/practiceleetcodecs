using System.Text;

namespace Top75;

internal class LetterCombinationsofaPhoneNumber17
{
    public IList<string> LetterCombinations(string digits) 
    {
        IList<string> result = new List<string>();
        if (digits == string.Empty) return result;

        char ch = digits[0];        
        if (digits.Length == 1)
        {
            if (ch == '2')
            {                                
                result.Add("a");
                result.Add("b");                                                
                result.Add("c");
            }
            else if (ch == '3')
            {
                result.Add("d");
                result.Add("e");                                                
                result.Add("f");
            }
            else if (ch == '4')
            {
                result.Add("g");
                result.Add("h");                                                
                result.Add("i");
            }
            else if (ch == '5')
            {
                result.Add("j");
                result.Add("k");                                                
                result.Add("l");
            }
            else if (ch == '6')
            {
                result.Add("m");
                result.Add("n");                                                
                result.Add("o");
            }
            else if (ch == '7')
            {
                result.Add("p");
                result.Add("q");                                                
                result.Add("r");
                result.Add("s");
            }
            else if (ch == '8')
            {
                result.Add("t");
                result.Add("u");                                                
                result.Add("v");
            }
            else if (ch == '9')
            {
                result.Add("w");
                result.Add("x");                                                
                result.Add("y");
                result.Add("z");
            }
            
            return result;
        }
        
        digits = digits.Remove(0, 1);
        IList<string> children = LetterCombinations(digits);
        
        foreach (string child in children)
        {
            StringBuilder builder = new StringBuilder();
            if (ch == '2')
            {                
                builder.Append('a');
                builder.Append(child);
                result.Add(builder.ToString());
                
                builder.Clear();
                builder.Append('b');
                builder.Append(child);
                result.Add(builder.ToString());

                builder.Clear();
                builder.Append('c');
                builder.Append(child);
                result.Add(builder.ToString());
            }
            else if (ch == '3')
            {
                builder.Append('d');
                builder.Append(child);
                result.Add(builder.ToString());
                
                builder.Clear();
                builder.Append('e');
                builder.Append(child);
                result.Add(builder.ToString());

                builder.Clear();
                builder.Append('f');
                builder.Append(child);
                result.Add(builder.ToString());
            }
            else if (ch == '4')
            {
                builder.Append('g');
                builder.Append(child);
                result.Add(builder.ToString());
                
                builder.Clear();
                builder.Append('h');
                builder.Append(child);
                result.Add(builder.ToString());

                builder.Clear();
                builder.Append('i');
                builder.Append(child);
                result.Add(builder.ToString());
            }
            else if (ch == '5')
            {
                builder.Append('j');
                builder.Append(child);
                result.Add(builder.ToString());
                
                builder.Clear();
                builder.Append('k');
                builder.Append(child);
                result.Add(builder.ToString());

                builder.Clear();
                builder.Append('l');
                builder.Append(child);
                result.Add(builder.ToString());
            }
            else if (ch == '6')
            {
                builder.Append('m');
                builder.Append(child);
                result.Add(builder.ToString());
                
                builder.Clear();
                builder.Append('n');
                builder.Append(child);
                result.Add(builder.ToString());

                builder.Clear();
                builder.Append('o');
                builder.Append(child);
                result.Add(builder.ToString());
            }
            else if (ch == '7')
            {
                builder.Append('p');
                builder.Append(child);
                result.Add(builder.ToString());
                
                builder.Clear();
                builder.Append('q');
                builder.Append(child);
                result.Add(builder.ToString());

                builder.Clear();
                builder.Append('r');
                builder.Append(child);
                result.Add(builder.ToString());

                builder.Clear();
                builder.Append('s');
                builder.Append(child);
                result.Add(builder.ToString());
            }
            else if (ch == '8')
            {
                builder.Append('t');
                builder.Append(child);
                result.Add(builder.ToString());
                
                builder.Clear();
                builder.Append('u');
                builder.Append(child);
                result.Add(builder.ToString());

                builder.Clear();
                builder.Append('v');
                builder.Append(child);
                result.Add(builder.ToString());
            }
            else if (ch == '9')
            {
                builder.Append('w');
                builder.Append(child);
                result.Add(builder.ToString());
                
                builder.Clear();
                builder.Append('x');
                builder.Append(child);
                result.Add(builder.ToString());

                builder.Clear();
                builder.Append('y');
                builder.Append(child);
                result.Add(builder.ToString());

                builder.Clear();
                builder.Append('z');
                builder.Append(child);
                result.Add(builder.ToString());
            }
        }
        digits.Insert(0, ch.ToString());
        return result;
    }
    public IList<string> LetterCombinations_SLOWER(string digits) 
    {
        IList<string> combinations = new List<string>();        
        if (digits.Length == 0) return combinations;

        Dictionary<char, string> keys = new Dictionary<char, string>
        {
            { '2', "abc" },
            { '3', "def" },
            { '4', "ghi" },
            { '5', "jkl" },
            { '6', "mno" },
            { '7', "pqrs" },
            { '8', "tuv" },
            { '9', "wxyz" },
        };

        int size = 1;
        for (int i = 0; i < digits.Length; ++i)         
            size *= keys[digits[i]].Length;      
        
        for (int i = 0; i < size; ++i)
            combinations.Add("");        

        if (digits.Length == 1)
        {
            string chars = keys[digits[0]];
            for (int i = 0; i < chars.Length; ++i)            
                combinations[i] += chars[i];            
        }
        else if (digits.Length == 2)
        {
            int currentIndex = 0;
            string firstDigitChars = keys[digits[0]];
            for (int i = 0; i < firstDigitChars.Length; ++i) 
            {
                string secondDigitChars = keys[digits[1]];
                
                for (int j = 0; j < secondDigitChars.Length; ++j)
                {
                    combinations[currentIndex] += firstDigitChars[i]; 
                    combinations[currentIndex++] += secondDigitChars[j];
                }

            }                
        }
        else if (digits.Length == 3)
        {
            string firstDigitChars = keys[digits[0]];
            int currentIndex = 0;
            for (int i = 0; i < firstDigitChars.Length; ++i) 
            {
                string secondDigitChars = keys[digits[1]];                
                for (int j = 0; j < secondDigitChars.Length; ++j)
                {                    
                    string thirdDigitChars = keys[digits[2]];                    
                    for (int k = 0; k < thirdDigitChars.Length; ++k)
                    {
                        combinations[currentIndex] += firstDigitChars[i]; 
                        combinations[currentIndex] += secondDigitChars[j];
                        combinations[currentIndex++] += thirdDigitChars[k];
                    }
                }

            }                
        }
        else if (digits.Length == 4)
        {
            string firstDigitChars = keys[digits[0]];
            int currentIndex = 0;
            for (int i = 0; i < firstDigitChars.Length; ++i) 
            {
                string secondDigitChars = keys[digits[1]];                
                for (int j = 0; j < secondDigitChars.Length; ++j)
                {                    
                    string thirdDigitChars = keys[digits[2]];                    
                    for (int k = 0; k < thirdDigitChars.Length; ++k)
                    {
                        string fourthDigitChars = keys[digits[3]];
                        for (int l = 0; l < fourthDigitChars.Length; ++l)
                        {
                            combinations[currentIndex] += firstDigitChars[i]; 
                            combinations[currentIndex] += secondDigitChars[j];
                            combinations[currentIndex] += thirdDigitChars[k];
                            combinations[currentIndex++] += fourthDigitChars[l];
                        }
                    }
                }

            }                
        }

        return combinations;
    }
    public void Test()
    {
        var result1 = LetterCombinations("23");
        var result2 = LetterCombinations("");
        var result3 = LetterCombinations("7");
        var result4 = LetterCombinations("293");
        var result5 = LetterCombinations("234");
        var result6 = LetterCombinations("2344");
    }
}