namespace Top75;

internal class CombinationSumIII216
{
    IList<IList<int>> result;
    int k;
    int n;
    public IList<IList<int>> CombinationSum3(int k, int n) 
    {     
        this.k = k;   
        this.n = n;
        result = new List<IList<int>>();
        if (n > 45) return result;//Max is 45

        for (int i = 1; i < 10; ++i)
        {            
            List<int> numList = new List<int>() {i};            
            FindSum(i, 1, i, numList);
        }
        return result;
    }
    public void FindSum(int number, int numCount, int curSum, List<int> numList)
    {                
        if (numCount == k && curSum == n)
        {
            List<int> newList = new List<int>(numList);
            result.Add(newList);
            return;
        }
        if (number == 9)
            return;
        
        for (int i = number + 1; i < 10; ++i)
        {
            numList.Add(i);
            FindSum(i, numCount + 1, curSum + i, numList);
            numList.RemoveAt(numList.Count - 1);
        }            
    }
   

    public void Test()
    {
        var result1 = CombinationSum3(3, 7);//[1,2,4]
        var result2 = CombinationSum3(3, 9);//[[1,2,6],[1,3,5],[2,3,4]]
        var result3 = CombinationSum3(4, 1);//[[]]
    }
}

/*
216. Combination Sum III
Medium
Topics
Companies

Find all valid combinations of k numbers that sum up to n such that the following conditions are true:

    Only numbers 1 through 9 are used.
    Each number is used at most once.

Return a list of all possible valid combinations. The list must not contain the same combination twice, and the combinations may be returned in any order.

 

Example 1:

Input: k = 3, n = 7
Output: [[1,2,4]]
Explanation:
1 + 2 + 4 = 7
There are no other valid combinations.

Example 2:

Input: k = 3, n = 9
Output: [[1,2,6],[1,3,5],[2,3,4]]
Explanation:
1 + 2 + 6 = 9
1 + 3 + 5 = 9
2 + 3 + 4 = 9
There are no other valid combinations.

Example 3:

Input: k = 4, n = 1
Output: []
Explanation: There are no valid combinations.
Using 4 different numbers in the range [1,9], the smallest sum we can get is 1+2+3+4 = 10 and since 10 > 1, there are no valid combination.

 

Constraints:

    2 <= k <= 9
    1 <= n <= 60


*/