namespace Top75;

internal class EvaluateDivision399
{
    Dictionary<string, List<(string, double)>> pairs;
    public double[] CalcEquation(IList<IList<string>> equations, double[] values, IList<IList<string>> queries) 
    {
        pairs = new ();

        for (int i = 0; i < equations.Count; ++i)
        {
            var equation = equations[i];
            List<(string, double)> rightSide;
            if (pairs.TryGetValue(equation[0], out rightSide))            
                rightSide.Add((equation[1], values[i]));
            else
                pairs.Add(equation[0], new() { (equation[1], values[i]) });
            
             if (pairs.TryGetValue(equation[1], out rightSide))            
                rightSide.Add((equation[0], 1 / values[i]));
            else
                pairs.Add(equation[1], new() { (equation[0], 1 / values[i]) });
        }
        List<double> ret = new();

        foreach (var query in queries)
            ret.Add(Dfs(query));

        return ret.ToArray();
    }
    private double Dfs(IList<string> query)
    {
        Stack<(string, double)> stack = new();
        stack.Push((query[0], 1));
        HashSet<string> visited = new();

        while (stack.Count > 0)
        {
            var next = stack.Pop();
            if (visited.Contains(next.Item1))
                continue;
            visited.Add(next.Item1);

            foreach (var pair in pairs)
            {
                if (pair.Key.Equals(next.Item1))
                {
                    foreach (var neighbour in pair.Value)
                    {
                        if (neighbour.Item1.Equals(query[1]))                        
                            return neighbour.Item2 * next.Item2;
                        stack.Push((neighbour.Item1, neighbour.Item2 * next.Item2));                        
                    }
                    
                    break;
                }
            }
        }

        return -1.0;
    }
    public void Test()
    {
        Test1();
    }
    public void Test1()
    {
        IList<IList<string>> equations = new List<IList<string>>
        {
            new List<string>() { "a", "b" },
            new List<string>() { "b", "c" },
        };    
        double[] values = new double[] {2.0, 3.0};
        
        IList<IList<string>> queries = new List<IList<string>>
        {
            new List<string>() { "a", "c" },
            new List<string>() { "b", "a" },
            new List<string>() { "a", "e" },
            new List<string>() { "a", "a" },
            new List<string>() { "x", "x" },
        };  
        
        var result = CalcEquation(equations, values, queries); //6.0, 0.5, -1.0, 1.0, -1.0 
    }
}
/*
399. Evaluate Division
Solved
Medium
Topics
Companies
Hint

You are given an array of variable pairs equations and an array of real numbers values, where equations[i] = [Ai, Bi] and values[i] represent the equation Ai / Bi = values[i]. Each Ai or Bi is a string that represents a single variable.

You are also given some queries, where queries[j] = [Cj, Dj] represents the jth query where you must find the answer for Cj / Dj = ?.

Return the answers to all queries. If a single answer cannot be determined, return -1.0.

Note: The input is always valid. You may assume that evaluating the queries will not result in division by zero and that there is no contradiction.

Note: The variables that do not occur in the list of equations are undefined, so the answer cannot be determined for them.

 

Example 1:

Input: equations = [["a","b"],["b","c"]], values = [2.0,3.0], queries = [["a","c"],["b","a"],["a","e"],["a","a"],["x","x"]]
Output: [6.00000,0.50000,-1.00000,1.00000,-1.00000]
Explanation: 
Given: a / b = 2.0, b / c = 3.0
queries are: a / c = ?, b / a = ?, a / e = ?, a / a = ?, x / x = ? 
return: [6.0, 0.5, -1.0, 1.0, -1.0 ]
note: x is undefined => -1.0

Example 2:

Input: equations = [["a","b"],["b","c"],["bc","cd"]], values = [1.5,2.5,5.0], queries = [["a","c"],["c","b"],["bc","cd"],["cd","bc"]]
Output: [3.75000,0.40000,5.00000,0.20000]

Example 3:

Input: equations = [["a","b"]], values = [0.5], queries = [["a","b"],["b","a"],["a","c"],["x","y"]]
Output: [0.50000,2.00000,-1.00000,-1.00000]

 

Constraints:

    1 <= equations.length <= 20
    equations[i].length == 2
    1 <= Ai.length, Bi.length <= 5
    values.length == equations.length
    0.0 < values[i] <= 20.0
    1 <= queries.length <= 20
    queries[i].length == 2
    1 <= Cj.length, Dj.length <= 5
    Ai, Bi, Cj, Dj consist of lower case English letters and digits.


*/