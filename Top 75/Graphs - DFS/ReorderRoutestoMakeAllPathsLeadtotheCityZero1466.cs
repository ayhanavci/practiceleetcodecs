namespace Top75;

internal class ReorderRoutestoMakeAllPathsLeadtotheCityZero1466
{
    HashSet<int> visited;
    int reorders;
    Dictionary<int, Dictionary<int, int>> neighbours;
    public int MinReorder(int n, int[][] connections) 
    {
        reorders = 0;
        visited = new HashSet<int>();
        neighbours = new Dictionary<int, Dictionary<int, int>>();
        
        //Add all nodes to graph without direction. The direction is represented as either 1 or 0
        //1 means we will have to reverse it if we go that way. 0 means no change
        foreach (var connection in connections)
        {
            if (neighbours.ContainsKey(connection[0]))            
                neighbours[connection[0]].Add(connection[1], 1);
            else 
                neighbours.Add(connection[0], new Dictionary<int, int>{{ connection[1], 1 } });

            if (neighbours.ContainsKey(connection[1]))            
                neighbours[connection[1]].Add(connection[0], 0);
            else            
                neighbours.Add(connection[1], new Dictionary<int, int>{{ connection[0], 0 } });
            
        }
        Dfs(0);
        return reorders;
    }
    public bool Dfs(int n)
    {
        if (visited.Contains(n)) return false;

        visited.Add(n);
        var nodeNeighbours = neighbours[n];

        foreach (var neighbour in nodeNeighbours)
        {
            if (Dfs(neighbour.Key))
                reorders += neighbour.Value;                        
        }
        return true;         
    }
    public void Test()
    {
        Test1();
        Test2();
        Test3();
    }
    public void Test1()
    {
        int[][] connections = new int[5][];
        connections[0] = new int[]{0,1};
        connections[1] = new int[]{1,3};
        connections[2] = new int[]{2,3};
        connections[3] = new int[]{4,0};
        connections[4] = new int[]{4,5};

        var result = MinReorder(6, connections);
    }
    public void Test2()
    {
        int[][] connections = new int[4][];
        connections[0] = new int[]{1,0};
        connections[1] = new int[]{1,2};
        connections[2] = new int[]{3,2};
        connections[3] = new int[]{3,4};

        var result = MinReorder(5, connections);
    }
    public void Test3()
    {
        int[][] connections = new int[2][];
        connections[0] = new int[]{1,0};
        connections[1] = new int[]{2,0};

        var result = MinReorder(3, connections);
    }
}
/*
1466. Reorder Routes to Make All Paths Lead to the City Zero
Medium
Topics
Companies
Hint

There are n cities numbered from 0 to n - 1 and n - 1 roads such that there is only one way to travel between two different cities (this network form a tree). Last year, The ministry of transport decided to orient the roads in one direction because they are too narrow.

Roads are represented by connections where connections[i] = [ai, bi] represents a road from city ai to city bi.

This year, there will be a big event in the capital (city 0), and many people want to travel to this city.

Your task consists of reorienting some roads such that each city can visit the city 0. Return the minimum number of edges changed.

It's guaranteed that each city can reach city 0 after reorder.

 

Example 1:

Input: n = 6, connections = [[0,1],[1,3],[2,3],[4,0],[4,5]]
Output: 3
Explanation: Change the direction of edges show in red such that each node can reach the node 0 (capital).

Example 2:

Input: n = 5, connections = [[1,0],[1,2],[3,2],[3,4]]
Output: 2
Explanation: Change the direction of edges show in red such that each node can reach the node 0 (capital).

Example 3:

Input: n = 3, connections = [[1,0],[2,0]]
Output: 0

 

Constraints:

    2 <= n <= 5 * 104
    connections.length == n - 1
    connections[i].length == 2
    0 <= ai, bi <= n - 1
    ai != bi


*/