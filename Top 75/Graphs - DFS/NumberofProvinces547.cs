namespace Top75;

internal class NumberofProvinces547
{
    
    HashSet<int> visitedCities;
    int[][] isConnected;
    public int FindCircleNum(int[][] isConnected) 
    {
        int count = 0;
        this.isConnected = isConnected;
        visitedCities = new HashSet<int>();

        for (int i = 0; i < isConnected.Length; ++i)  
        {
            if (!visitedCities.Contains(i))
            {
                VisitLinks(i);              
                count++;
            }
        }     
            

        return count;
    }
    public void VisitLinks(int i)
    {
        if (visitedCities.Contains(i)) return;
        visitedCities.Add(i);        

        for (int province = 0; province < isConnected[i].Length; ++province)
        {
            if (isConnected[i][province] == 1)            
                VisitLinks(province);            
        }                    
    }
    public void Test()
    {
        Test1();
        Test2();
    }
    public void Test1()
    {
        int[][] isConnected = new int[3][];
        isConnected[0] = new int[] {1,1,0};
        isConnected[1] = new int[] {1,1,0};
        isConnected[2] = new int[] {0,0,1};

        var result = FindCircleNum(isConnected);
    }
    public void Test2()
    {
        int[][] isConnected = new int[3][];
        isConnected[0] = new int[] {1,0,0};
        isConnected[1] = new int[] {0,1,0};
        isConnected[2] = new int[] {0,0,1};

        var result = FindCircleNum(isConnected);
    }
}

/*
547. Number of Provinces
Medium
Topics
Companies

There are n cities. Some of them are connected, while some are not. If city a is connected directly with city b, and city b is connected directly with city c, then city a is connected indirectly with city c.

A province is a group of directly or indirectly connected cities and no other cities outside of the group.

You are given an n x n matrix isConnected where isConnected[i][j] = 1 if the ith city and the jth city are directly connected, and isConnected[i][j] = 0 otherwise.

Return the total number of provinces.

 

Example 1:

Input: isConnected = [[1,1,0],[1,1,0],[0,0,1]]
Output: 2

Example 2:

Input: isConnected = [[1,0,0],[0,1,0],[0,0,1]]
Output: 3

 

Constraints:

    1 <= n <= 200
    n == isConnected.length
    n == isConnected[i].length
    isConnected[i][j] is 1 or 0.
    isConnected[i][i] == 1
    isConnected[i][j] == isConnected[j][i]


*/