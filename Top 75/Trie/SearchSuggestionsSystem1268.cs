namespace Top75;

internal class SearchSuggestionsSystem1268
 {
    public class TrieNode
    {
        public char ch;
        public Dictionary<char, TrieNode> children;
        public bool endOfWord;        
        public TrieNode(char ch)
        {
            children = new Dictionary<char, TrieNode>();
            this.ch = ch;
            endOfWord = false;
        }
    }
    TrieNode root;
    List<string> curItems;
    
    public IList<IList<string>> SuggestedProducts(string[] products, string searchWord) 
    {
        IList<IList<string>> result = new List<IList<string>>();
        for (int i = 0; i < searchWord.Length; ++i)
            result.Add(new List<string>());

        root = new TrieNode('.');

        foreach (string product in products)        
            AddTrie(product);
        
        TrieNode current = root;
        string searchWordPart = "";
        for (int i = 0; i < searchWord.Length; ++i)
        {            
            TrieNode child;
            if (!current.children.TryGetValue(searchWord[i], out child))   
                break;                  

            current = child;
            curItems = (List<string>)result[i];
            GetAllChildren(current, searchWordPart);
            curItems.Sort();
            if (curItems.Count > 3)             
                curItems.RemoveRange(3, curItems.Count - 3);            
            searchWordPart += searchWord[i];           
            
        }

        return result;
    }
    public void AddTrie(string product)
    {
        TrieNode current = root;
        for (int i = 0; i < product.Length; ++i)
        {
            TrieNode child;
            if (!current.children.TryGetValue(product[i], out child))            
            {
                child = new TrieNode(product[i]); 
                current.children.Add(product[i], child);               
            }                
            
            if (i == product.Length - 1)
                child.endOfWord = true;
                        
            current = child;
        }
    }   
    public void GetAllChildren(TrieNode cur, string searchWordPart)
    {
        string nextPart = searchWordPart + cur.ch;
        if (cur.endOfWord)        
            curItems.Add(nextPart);            
        
        foreach (var child in cur.children)
            GetAllChildren(child.Value, nextPart);
        
    }
   
    public void Test()
    {
        var result1 = SuggestedProducts(new string[]{"mobile","mouse","moneypot","monitor","mousepad"}, "mouse");
        var result2 = SuggestedProducts(new string[]{"havana"}, "havana");
        var result3 = SuggestedProducts(new string[]{"bags","baggage","banner","box","cloths"}, "bags");
        //o:[["baggage","bags","banner"],["bags","baggage","banner"],["bags","baggage"],["bags"]]
        //e:[["baggage","bags","banner"],["baggage","bags","banner"],["baggage","bags"],["bags"]]
        var result4 = SuggestedProducts(new string[]{"havana"}, "tatiana");
    }
 }

 /*
 1268. Search Suggestions System
Medium
Topics
Companies
Hint

You are given an array of strings products and a string searchWord.

Design a system that suggests at most three product names from products after each character of searchWord is typed. Suggested products should have common prefix with searchWord. If there are more than three products with a common prefix return the three lexicographically minimums products.

Return a list of lists of the suggested products after each character of searchWord is typed.

 

Example 1:

Input: products = ["mobile","mouse","moneypot","monitor","mousepad"], searchWord = "mouse"
Output: [["mobile","moneypot","monitor"],["mobile","moneypot","monitor"],["mouse","mousepad"],["mouse","mousepad"],["mouse","mousepad"]]
Explanation: products sorted lexicographically = ["mobile","moneypot","monitor","mouse","mousepad"].
After typing m and mo all products match and we show user ["mobile","moneypot","monitor"].
After typing mou, mous and mouse the system suggests ["mouse","mousepad"].

Example 2:

Input: products = ["havana"], searchWord = "havana"
Output: [["havana"],["havana"],["havana"],["havana"],["havana"],["havana"]]
Explanation: The only word "havana" will be always suggested while typing the search word.

 

Constraints:

    1 <= products.length <= 1000
    1 <= products[i].length <= 3000
    1 <= sum(products[i].length) <= 2 * 104
    All the strings of products are unique.
    products[i] consists of lowercase English letters.
    1 <= searchWord.length <= 1000
    searchWord consists of lowercase English letters.


 */